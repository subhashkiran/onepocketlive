package com.subhashe.onepocket.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Window;

import com.subhashe.onepocket.R;

/**
 * Created by SubhashE on 1/6/2016.
 */
public class SplashActivity extends Activity{

    private final int welcomeScreenDisplay = 1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        startThread();

    }

    private void startThread(){
        Thread welcomeThread = new Thread(){
            int wait = 0;

            @Override
            public void run() {
                try{
                    super.run();
                    /**
                     * use while to get the splash time. Use sleep() to increase
                     * the wait variable for every 100L.
                     */
                    while(wait < welcomeScreenDisplay){
                        sleep(100);
                        wait += 50;
                    }
                }catch (Exception e){
                    System.out.println("EXc=" + e);
                }finally {
                    setIntent();
                }
            }
        };
        welcomeThread.start();
    }

    private void setIntent(){
        Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
        startActivity(intent);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)''
        overridePendingTransition(R.anim.fedin, R.anim.fedout);
        finish();
    }
}
