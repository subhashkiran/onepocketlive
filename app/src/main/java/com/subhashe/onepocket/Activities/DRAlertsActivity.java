package com.subhashe.onepocket.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.subhashe.onepocket.Core.Constants;
import com.subhashe.onepocket.Core.MapModelAll;
import com.subhashe.onepocket.DataBase.DbHelper;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.NetworkChecker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SubhashE on 1/6/2016.
 */
public class DRAlertsActivity extends AppCompatActivity{

    android.support.v7.app.ActionBar mActionBar;
    private LinearLayout ll_DrAlertsActive,ll_DrAlertsSchedule,ll_DrAlertsSettings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dralertsactivity);
        initToolbar();

        ll_DrAlertsActive = (LinearLayout)findViewById(R.id.ll_DrAlertsActive);
        ll_DrAlertsSchedule = (LinearLayout)findViewById(R.id.ll_DrAlertsSchedule);
        ll_DrAlertsSettings = (LinearLayout)findViewById(R.id.ll_DrAlertsSettings);

        ll_DrAlertsActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DRAlertsActivity.this, DRAlertsActiveActivity.class);
                startActivity(intent);
            }
        });

        ll_DrAlertsSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DRAlertsActivity.this, DRAlertsScheduleActivity.class);
                startActivity(intent);
            }
        });

        ll_DrAlertsSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent(DRAlertsActivity.this, DRAlertsSettingsActivity.class);
                startActivity(intent);*/
            }
        });

    }
    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Edit Profile");
//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

//        String title = mActionBar.getTitle().toString();
        String title = "DR Events ";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();

                new KeyBoardHide().hideKeyBoard(DRAlertsActivity.this);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
