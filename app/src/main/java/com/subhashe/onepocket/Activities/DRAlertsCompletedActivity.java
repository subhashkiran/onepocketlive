package com.subhashe.onepocket.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.subhashe.onepocket.Adapters.DrAlertActiveAdapter;
import com.subhashe.onepocket.Adapters.DrAlertCompletedAdapter;
import com.subhashe.onepocket.Adapters.DrAlertScheduleAdapter;
import com.subhashe.onepocket.Core.DrActiveModel;
import com.subhashe.onepocket.Core.DrCompletedModel;
import com.subhashe.onepocket.Core.DrScheduleModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by axxera on 07-07-2017.
 */

public class DRAlertsCompletedActivity extends AppCompatActivity {

    ActionBar mActionBar;
    private RecyclerView mRecyclerViewCompleted;
    private ProgressDialog progress;
    private DrAlertCompletedAdapter mMyAdapter;
    private JsonUtil mJsonUtil;
    private String response;
    private LinearLayout drAlertCompletedLayoutEmpty;
    private List<DrCompletedModel> mDrAlertCompletedList = new ArrayList<>();
    LinearLayout ll_scheduled, ll_active, ll_completed;
    TextView tv_scheduled, tv_active, tv_completed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dralertcompleted);

        drAlertCompletedLayoutEmpty = (LinearLayout) findViewById(R.id.drCompletedEmptyLayout);
        initToolbar();

        tv_scheduled = (TextView) findViewById(R.id.tv_drscheduled_completed);

        tv_active = (TextView) findViewById(R.id.tv_drscheduled_active);

        tv_completed = (TextView) findViewById(R.id.tv_drscheduled_completed);

        ll_scheduled = (LinearLayout) findViewById(R.id.ll_drcompleted_scheduled);

        ll_active = (LinearLayout) findViewById(R.id.ll_drcompleted_active);

        ll_completed = (LinearLayout) findViewById(R.id.ll_drcompleted_completed);


        ll_scheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(DRAlertsCompletedActivity.this);

                ll_scheduled.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_scheduled.setTextColor(getResources().getColor(R.color.white));
                ll_active.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_active.setTextColor(getResources().getColor(R.color.appColor));
                ll_completed.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_completed.setTextColor(getResources().getColor(R.color.appColor));

                Intent intent = new Intent(DRAlertsCompletedActivity.this, DRAlertsScheduleActivity.class);
                startActivity(intent);
            }
        });

        ll_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(DRAlertsCompletedActivity.this);

                ll_scheduled.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_scheduled.setTextColor(getResources().getColor(R.color.appColor));
                ll_active.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_active.setTextColor(getResources().getColor(R.color.white));
                ll_completed.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_completed.setTextColor(getResources().getColor(R.color.appColor));

                Intent intent = new Intent(DRAlertsCompletedActivity.this, DRAlertsActiveActivity.class);
                startActivity(intent);

            }
        });

        ll_completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(DRAlertsCompletedActivity.this);

                ll_scheduled.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_scheduled.setTextColor(getResources().getColor(R.color.appColor));
                ll_active.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_active.setTextColor(getResources().getColor(R.color.appColor));
                ll_completed.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_completed.setTextColor(getResources().getColor(R.color.white));

                Intent intent = new Intent(DRAlertsCompletedActivity.this, DRAlertsCompletedActivity.class);
                startActivity(intent);

            }
        });

        mRecyclerViewCompleted = (RecyclerView) findViewById(R.id.recyclerViewCompleted);
        mRecyclerViewCompleted.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewCompleted.setHasFixedSize(true);
        new drAlertCompletedListGet().execute();
    }

    public class drAlertCompletedListGet extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.GETALLDRALERTCOMPLETED() ;
                response = mJsonUtil.getMethod(stringurl,null,null);
                OnePocketLog.d("dralertComplete resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("dralertComplete  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            try {
                if(response != null || !response.contains("[]") || response != "") {

                    JSONArray temp = new JSONArray(response);

                    if (temp.length() == 0) {

                        drAlertCompletedLayoutEmpty.setVisibility(View.VISIBLE);

                    } else if (temp != null) {

                        for (int i = 0; i < temp.length(); i++) {

                            drAlertCompletedLayoutEmpty.setVisibility(View.INVISIBLE);

                            JSONObject e = temp.getJSONObject(i);
                            DrCompletedModel mDrCompletedModel = new DrCompletedModel();

//                            OnePocketLog.d("mDrActiveModel" , String.valueOf(mDrActiveModel.getIdFav()));

                            mDrCompletedModel.setStartDate(e.getString("StartDate"));
                            mDrCompletedModel.setStartTime(e.getString("StartTime"));
                            mDrCompletedModel.setEndDate(e.getString("EndDate"));
                            mDrCompletedModel.setEndTime(e.getString("EndTime"));
                            mDrCompletedModel.setSignalName(e.getString("SignalName"));
                            mDrCompletedModel.setSignalType(e.getString("SignalType"));

                            mDrAlertCompletedList.add(mDrCompletedModel);

                        }

                    }
                    mMyAdapter = new DrAlertCompletedAdapter(DRAlertsCompletedActivity.this, mDrAlertCompletedList);
                    mRecyclerViewCompleted.setAdapter(mMyAdapter);
                }
            }catch (Exception e){

            }
        }
    }

    public void showAlertBox(final String msg) {

    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Edit Profile");
//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

//        String title = mActionBar.getTitle().toString();
        String title = "Notifications";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                //onBackPressed();

                Intent intent = new Intent(DRAlertsCompletedActivity.this , NavigationDrawerActivity.class );
                startActivity(intent);

                new KeyBoardHide().hideKeyBoard(DRAlertsCompletedActivity.this);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
