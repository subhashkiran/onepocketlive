package com.subhashe.onepocket.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.subhashe.onepocket.Core.EditProfileModel;
import com.subhashe.onepocket.Core.EditProfileModelUpdate;
import com.subhashe.onepocket.Core.ProfileInfoModel;
import com.subhashe.onepocket.Core.UserSecurity;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by axxera on 30-12-2016.
 */

public class EditProfileActivity extends AppCompatActivity {

    Point p;
    Button btnSubmit, btnCancel;
    CheckBox passwordCheck, securityQuestionsCheck;
    LinearLayout passwordCheckLayout, securityQuestionsCheckLayout;
    TextView firstName, lastName, email, phone, gender, addressLine1, city, state, country, zipcode, tveditButton;
    EditText editFirstName, editLastName, editPhone, editAddress1, editCity, editState, editCountry, editZipcode, editfirstQAns,
            editsecondQAns, confirmPwd, newpwd, oldPwd;
    ImageView img_back;

    private String emailString;
    private android.support.v4.app.Fragment fragment = null;

    char[] acceptedChars = new char[]{',', ' ', '.'};
    char[] acceptedCharsNoSpace = new char[]{'-', '_', '.'};
    char[] blockedCharSet = new char[]{'π'};

    private boolean passwordFlag, secQuesFlag, securityFlag;
    JSONObject jsonObject, addressObject;
    JSONArray jsonArray;
    private View rootView;
    private ProgressDialog progress;
    private ConnectionDetector mConnectionDetector;
    private JsonUtil mJsonUtil;
    private String userName, item1, item2, mQid1, mQid2, pwd1, pwd2;
    private int mSQid1, mSQid2;
    String temp, temp1, secItem1, secItem2;
    private int /*mQid1,mQid2,*/
            address_id;
    android.support.v7.app.ActionBar mActionBar;
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USERNAME_ID = "USERNAME_ID_PREFS_String";
    public static final String ADDRESS_USER_ID = "ADDRESS_USER_ID";

    private EditProfileModelUpdate profileEditModel;
    private ProfileInfoModel profileEModel;
    private ArrayList<String> questions;
    ArrayList<UserSecurity> arraylistEdit = new ArrayList<UserSecurity>();

    // String[] questions = new String[] {"Select your option","Credit Cart","PayPal"};
    private Spinner securityQOne, securityQTwo;
    ArrayAdapter<String> adapter;

    PopupWindow popup;

    private TextView mAccBalance;
    private TextView addMoney;
    private String response, token, getAccBal, mAmount;
    private int user_id, accBalence;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor1;
    private boolean paymentPopup = false;

    public static final String ACC_BALENCE = "ACC_BALENCE";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";
    public static final String USER_REG_ID = "USERID_REG_String";

    //    fixed the bug in sprint-7 Ticket_Id-158 starts
    InputFilter filtertxtCharSpace, filtertxtChar, filtertxtCharDig;
    EditProfileModel finalResult, finalEditResult;

    private boolean successFlag, successAlertFlag, passwordCheckedFlag = false, securityCheckedFlag = false;
    boolean editProfileFlag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.editclicklayout);
        mConnectionDetector = new ConnectionDetector(this);
        profileEditModel = new EditProfileModelUpdate();
        profileEModel = new ProfileInfoModel();


        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext().getApplicationContext());
        editor1 = prefs.edit();
        paymentPopup = prefs.getBoolean(PAYMENT_FLAG_ALERT, false);

        initToolbar();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");
        userName = prefs.getString(USERNAME_ID, "NOT FOUND");
        address_id = prefs.getInt(ADDRESS_USER_ID, 0);

        if (mConnectionDetector.isOnline()) {
            new SecurityQuestionsTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            new EditProfileInfo().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }

//        fixed the bug in sprint-7 Ticket_Id-158 starts
        filtertxtCharDig = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetterOrDigit(source.charAt(i)) &&
                            !new String(acceptedCharsNoSpace).contains(String.valueOf(source.charAt(i))) /*&&
                        new String(blockedCharSet).contains(String.valueOf(source.charAt(i)))*/) {
                        return "";
                    }
                }
                return null;
            }
        };

        filtertxtChar = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetter(source.charAt(i)) && !(Character.isSpace(source.charAt(i))) &&
                            !new String(acceptedChars).contains(String.valueOf(source.charAt(i))) /*&&
                        new String(blockedCharSet).contains(String.valueOf(source.charAt(i)))*/) {
                        return "";
                    }
                }
                return null;
            }
        };

        filtertxtCharSpace = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetter(source.charAt(i)) && !(Character.isSpace(source.charAt(i)))) {
                        return "";
                    }
                }
                return null;
            }
        };
//        fixed the bug in sprint-7 Ticket_Id-158 end
        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editFirstName.setFilters(new InputFilter[]{filtertxtCharSpace});
        editLastName = (EditText) findViewById(R.id.editLastName);
        editLastName.setFilters(new InputFilter[]{filtertxtCharSpace});
        editPhone = (EditText) findViewById(R.id.editPhone);
        editAddress1 = (EditText) findViewById(R.id.editAddress1);
//            editAddress2 = (EditText) layout.findViewById(R.id.editAddress2);
        editCity = (EditText) findViewById(R.id.editCity);
        editCity.setFilters(new InputFilter[]{filtertxtChar});
        editState = (EditText) findViewById(R.id.editState);
        editState.setFilters(new InputFilter[]{filtertxtChar});
//            editCountry = (EditText) layout.findViewById(R.id.editCountry);
//            editCountry.setFilters(new InputFilter[]{filtertxtChar});
        editZipcode = (EditText) findViewById(R.id.editZipcode);
        editfirstQAns = (EditText) findViewById(R.id.firstQAns);
        editsecondQAns = (EditText) findViewById(R.id.secondQAns);
        confirmPwd = (EditText) findViewById(R.id.editConfirmPassword);
        newpwd = (EditText) findViewById(R.id.editNewPassword);
        oldPwd = (EditText) findViewById(R.id.editOldPassword);

        editFirstName.setText(finalResult.getFirstName());
        editLastName.setText(finalResult.getLastName());
        editPhone.setText(finalResult.getPhone());
        editAddress1.setText(finalResult.getAddressLine1());
//            editAddress2.setText(finalResult.getAddressLine2());
        editCity.setText(finalResult.getCity());
        editState.setText(finalResult.getState());
//            editCountry.setText(finalResult.getCountry());
        editZipcode.setText(finalResult.getZipCode());


        passwordCheck = (CheckBox) findViewById(R.id.passwordCheck);
        passwordCheckLayout = (LinearLayout) findViewById(R.id.passwordCheckLayout);
        passwordCheckLayout.setVisibility(View.GONE);

        securityQuestionsCheck = (CheckBox) findViewById(R.id.securityQuestionsCheck);
        securityQuestionsCheckLayout = (LinearLayout) findViewById(R.id.securityQuestionsCheckLayout);
        securityQuestionsCheckLayout.setVisibility(View.GONE);

        securityQOne = (Spinner) findViewById(R.id.securityQOne);
        securityQTwo = (Spinner) findViewById(R.id.securityQTwo);

            /*adapter = new ArrayAdapter(EditProfile.this,android.R.layout.simple_spinner_item, questions);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/

        ArrayAdapter<UserSecurity> adapter1 =
                new ArrayAdapter<UserSecurity>(EditProfileActivity.this, android.R.layout.simple_spinner_item, arraylistEdit);
//            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter1.setDropDownViewResource(R.layout.custom_spinner_item);

        securityQOne.setAdapter(adapter1);
        securityQTwo.setAdapter(adapter1);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        // btnCancel = (Button) layout.findViewById(R.id.btnCancel);

        mQid1 = mQid2 = null;
        passwordCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passwordFlag = true;
                    passwordCheckLayout.setVisibility(View.VISIBLE);
                } else {
                    passwordFlag = false;
                    passwordCheckLayout.setVisibility(View.GONE);
                }
            }
        });


        securityQuestionsCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    securityQuestionsCheckLayout.setVisibility(View.VISIBLE);
                    secQuesFlag = true;
                    new getSecQuestionIDs().execute();

                    securityQOne.setSelection(mSQid1 - 1);
                    securityQTwo.setSelection(mSQid2 - 1);

                    securityQOne.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            item1 = parent.getItemAtPosition(position).toString();
                            mQid1 = String.valueOf(getQuestionID(item1));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    securityQTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            item2 = parent.getItemAtPosition(position).toString();
                            mQid2 = String.valueOf(getQuestionID(item2));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                } else {
                    securityQuestionsCheckLayout.setVisibility(View.GONE);
                    secQuesFlag = false;
                    mQid1 = null;
                    mQid2 = null;
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
//                fixed the bug in sprint-7 Ticket_Id-158 starts
            public void onClick(View v) {
                if (mConnectionDetector.isOnline()) {

                    passwordCheckedFlag = false;
                    securityCheckedFlag = false;
                    editProfileFlag = false;

                    if (editFirstName.getText().toString().length() == 0) {
                        editFirstName.requestFocus();
                        showAlertBox(getString(R.string.enterFirstName));
                    } else if (editLastName.getText().toString().length() == 0) {
                        editLastName.requestFocus();
                        showAlertBox(getString(R.string.enterLastName));
                    } else if (editPhone.getText().toString().length() == 0) {
                        editPhone.requestFocus();
                        showAlertBox(getString(R.string.entermobilenum));
                    } /*else if (editPhone.getText().toString().length() < 10) {
                            editPhone.requestFocus();
                            showAlertBox(getString(R.string.mobileNumberCount));
//                    DialogUtil("Password is too short");
                        } */ else if ((editPhone.getText().toString().contains("0000000000")) ||
                            (editPhone.getText().toString().length() > 10)) {
                        showAlertBox(getString(R.string.invalidmobile));
                    } else if (editAddress1.getText().toString().length() == 0) {
                        editAddress1.requestFocus();
                        showAlertBox(getString(R.string.enterAdd1));
                    } else if (editCity.getText().toString().length() == 0) {
                        editCity.requestFocus();
                        showAlertBox(getString(R.string.entercity));
                    } else if (editState.getText().toString().length() == 0) {
                        editState.requestFocus();
                        showAlertBox(getString(R.string.enterstate));
                    } /*else if (editCountry.getText().toString().length() == 0) {
                            editCountry.requestFocus();
                            showAlertBox(getString(R.string.entercountry));
                        } */ else if (editZipcode.getText().toString().length() == 0) {
                        editZipcode.requestFocus();
                        showAlertBox(getString(R.string.enterzip));
                    } else if ((editZipcode.getText().toString().length() < 5) || (editZipcode.getText().toString().length() > 7)) {
                        editZipcode.requestFocus();
                        showAlertBox(getString(R.string.enterzipAlert));
                    } /*else if (passwordCheck.isChecked() && securityQuestionsCheck.isChecked()) {
                            editProfileFlag = true;
                            pwdcheck();
                            secu_check();
                        }*/

                    if (passwordCheck.isChecked() && securityQuestionsCheck.isChecked()) {
                        editProfileFlag = true;
                        pwdcheck();
                    } else if (passwordCheck.isChecked()) {
                        editProfileFlag = true;
                        pwdcheck();

                    } else if (securityQuestionsCheck.isChecked()) {
                        editProfileFlag = true;
                        secu_check();
                    }

                    if (editProfileFlag) {

                            /*if(securityQuestionsCheck.isChecked()) {
                                secu_check();
                            }*/

                        Log.d("password checked flag 1", " : " + passwordCheckedFlag);
                        Log.d("security checked flag 1", " : " + securityCheckedFlag);
                        Log.d("mQid1  1", " : " + mQid1);
                        Log.d("mQid2  1", " : " + mQid2);

                        if (passwordCheckedFlag && securityCheckedFlag) {

                            Log.d("password checked flag ", "true : " + passwordCheckedFlag);
                            Log.d("security checked flag ", "true : " + securityCheckedFlag);
                            Log.d("mQid  1", " : " + mQid1);
                            Log.d("mQid  1", " : " + mQid2);

                            passwordCheckedFlag = true;
                            securityCheckedFlag = true;
                            editProfileUpdate();
                            new EditProfileUpdate().execute();
                            editProfileFlag = false;
                        } else if (passwordCheckedFlag) {
                            passwordCheckedFlag = true;
                            securityCheckedFlag = false;

                            Log.d("password checked flag ", "true : " + passwordCheckedFlag);
                            Log.d("security checked flag ", "false : " + securityCheckedFlag);
                            Log.d("mQid  1", " : " + mQid1);
                            Log.d("mQid  1", " : " + mQid2);

                            editProfileUpdate();
                            new EditProfileUpdate().execute();
                            editProfileFlag = false;
                        } else if (securityCheckedFlag) {
                            passwordCheckedFlag = false;
                            securityCheckedFlag = true;

                            Log.d("password checked flag ", "false : " + passwordCheckedFlag);
                            Log.d("security checked flag ", "true : " + securityCheckedFlag);
                            Log.d("mQid  1", " : " + mQid1);
                            Log.d("mQid  1", " : " + mQid2);

                            editProfileUpdate();
                            new EditProfileUpdate().execute();
                            editProfileFlag = false;
                        }

                    } else {
                        passwordCheckedFlag = false;
                        securityCheckedFlag = false;

                        Log.d("password checked flag ", "false : " + passwordCheckedFlag);
                        Log.d("security checked flag ", "false : " + securityCheckedFlag);
                        Log.d("mQid  1", " : " + mQid1);
                        Log.d("mQid  1", " : " + mQid2);

                        editProfileUpdate();
                        new EditProfileUpdate().execute();
                        editProfileFlag = false;
                    }

                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }

            }
        });
    }


    public class SecurityQuestionsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL(), null, null);
                OnePocketLog.d("singup resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("singup error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideProgress();
            if (response == null || response == "" || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response.contains("Error Code")) {
                showAlertBox(getString(R.string.incorrectusername));
            } else {
                try {
                  /*  JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray  = jsonObject.getJSONArray();*/
                    JSONArray temp = new JSONArray(response);
                    if (arraylistEdit != null) {
                        arraylistEdit.clear();
                    }
                    for (int i = 0; i < temp.length(); i++) {
                        UserSecurity mUser = new UserSecurity("", -1);
                        JSONObject e = temp.getJSONObject(i);
                        mUser.setQuestion(e.getString("questions"));
                        mUser.setId(e.getInt("id"));
                        arraylistEdit.add(i, mUser);
                    }
                    securityFlag = true;
                } catch (Exception ep) {
                    OnePocketLog.d("posts resp----------", "exception");
                }

            }
        }
    }

    public class EditProfileInfo extends AsyncTask<String, String, EditProfileModel> {
        //        EditProfileModel finalResult;
        EditProfileModel editProfileModel = new EditProfileModel();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.pleaseWait));
        }


        @Override
        protected EditProfileModel doInBackground(String... params) {
            try {
                String mUrl = OnePocketUrls.SETTINGS_EDITPROFILE() + user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            try {
                jsonObject = new JSONObject(response);

                jsonArray = jsonObject.getJSONArray("address");

                emailString = jsonObject.getString("email");

                for (int i = 0; i < jsonArray.length(); i++) {
                    // for(int i=0;i<5;i++){
                    addressObject = jsonArray.getJSONObject(i);
                    editProfileModel.setState(addressObject.getString("state"));
//                    editProfileModel.setCountry(addressObject.getString("country"));
                    editProfileModel.setPhone(addressObject.getString("phone"));
                    editProfileModel.setCity(addressObject.getString("city"));
                    editProfileModel.setZipCode(addressObject.getString("zipCode"));
                    editProfileModel.setAddressLine1(addressObject.getString("addressLine1"));
                    editProfileModel.setAddressLine2(addressObject.getString("addressLine2"));
                }

                editProfileModel.setFirstName(jsonObject.getString("firstName"));
                editProfileModel.setLastName(jsonObject.getString("lastName"));
                editProfileModel.setEmail(jsonObject.getString("email"));
                editProfileModel.setGender(jsonObject.getString("gender"));

                Log.d("EditProfileModel", "Json Object Value " + jsonObject);
                return editProfileModel;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(EditProfileModel result) {
            super.onPostExecute(editProfileModel);

//            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {

                if (result != null) {
                    finalResult = result;
                }

                if (firstName != null) {
                    firstName.setText(result.getFirstName());
                }

                if (lastName != null) {
                    lastName.setText(result.getLastName());
                }

                if (email != null) {
                    email.setText(result.getEmail());
                }

                if (phone != null) {
                    phone.setText(result.getPhone());
                }

//                gender.setText(result.getGender());
                if (addressLine1 != null) {
                    addressLine1.setText(result.getAddressLine1());
                }

//                addressLine2.setText(result.getAddressLine2());
                if (city != null) {
                    city.setText(result.getCity());
                }

                if (state != null) {
                    state.setText(result.getState());
                }
//                country.setText(result.getCountry());
                if (zipcode != null) {
                    zipcode.setText(result.getZipCode());
                }


            }
        }
    }
    private void secu_check() {
        if (editfirstQAns.getText().toString().length() == 0) {
            editfirstQAns.requestFocus();
            showAlertBox(getString(R.string.answer1required));
        } else if (editsecondQAns.getText().toString().length() == 0) {
            editsecondQAns.requestFocus();
            showAlertBox(getString(R.string.answer2required));
        } else if (item2.equals(item1)) {
            showAlertBox(getString(R.string.questionreselected));
        } else {
            securityCheckedFlag = true;
        }
    }

    private void pwdcheck() {

        if (oldPwd.getText().toString().length() == 0) {
            oldPwd.requestFocus();
            showAlertBox(getString(R.string.enteroldpwd));
        } else if (newpwd.getText().toString().length() == 0) {
            newpwd.requestFocus();
            showAlertBox(getString(R.string.enternewpwd));
        } else if (newpwd.getText().toString().length() < 6) {
            newpwd.requestFocus();
            showAlertBox(getString(R.string.pwdshort));
        } else if (confirmPwd.getText().toString().length() == 0) {
            confirmPwd.requestFocus();
            showAlertBox(getString(R.string.enterconfirmpwd));
        } else if (!passwordTest()) {
//            showAlertBox(getString(R.string.pwdmismatch));
        } else {
            passwordCheckedFlag = true;
        }
    }

    private void pwdSecuCheck(){
        if (oldPwd.getText().toString().length() == 0) {
            oldPwd.requestFocus();
            showAlertBox(getString(R.string.enteroldpwd));
        } else if (newpwd.getText().toString().length() == 0) {
            newpwd.requestFocus();
            showAlertBox(getString(R.string.enternewpwd));
        } else if (newpwd.getText().toString().length() < 6) {
            newpwd.requestFocus();
            showAlertBox(getString(R.string.pwdshort));
        } else if (confirmPwd.getText().toString().length() == 0) {
            confirmPwd.requestFocus();
            showAlertBox(getString(R.string.enterconfirmpwd));
        } else if (!passwordTest()) {
//            showAlertBox(getString(R.string.pwdmismatch));
        } else if (editfirstQAns.getText().toString().length() == 0) {
            editfirstQAns.requestFocus();
            showAlertBox(getString(R.string.answer1required));
        } else if (editsecondQAns.getText().toString().length() == 0) {
            editsecondQAns.requestFocus();
            showAlertBox(getString(R.string.answer2required));
        } else if (item2.equals(item1)) {
            showAlertBox(getString(R.string.questionreselected));
        } else {
            passwordCheckedFlag = true;
            securityCheckedFlag = true;
        }
    }


    private boolean passwordTest() {
        pwd1 = newpwd.getText().toString();
        pwd2 = confirmPwd.getText().toString();
        if (!(pwd1.equals(pwd2))) {
            showAlertBox(getString(R.string.pwdmismatch));
            return false;
        } else {
            return true;
        }
    }

    private int getQuestionID(String item) {
        int id;
        for (int i = 0; i < arraylistEdit.size(); i++) {
            if (arraylistEdit.get(i).getQuestion().equals(item)) {
                return arraylistEdit.get(i).getId();
            }
        }
        return 0;
    }


    private void editProfileUpdate() {

        profileEditModel.setId(user_id);
        profileEditModel.setUsername(userName);
        profileEditModel.setFirstName(editFirstName.getText().toString());
        profileEditModel.setid(user_id);
        profileEditModel.setLastName(editLastName.getText().toString());

        profileEditModel.setConfirmPassword(confirmPwd.getText().toString());
        profileEditModel.setNewPassword(newpwd.getText().toString());
        profileEditModel.setOldPassword(oldPwd.getText().toString());
        profileEditModel.setShowPassword(passwordCheckedFlag);
        profileEditModel.setShowSecQuestions(securityCheckedFlag);

        temp = temp1 = null;
        editfirstQAns.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                temp = editfirstQAns.getText().toString();
            }
        });

        if (editfirstQAns.getText().toString().isEmpty()) {
            profileEditModel.setAnswer1(null);
        } else {
            profileEditModel.setAnswer1(editfirstQAns.getText().toString());
        }

        editsecondQAns.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                temp1 = editsecondQAns.getText().toString();
            }
        });

        if (editsecondQAns.getText().toString().isEmpty()) {
            profileEditModel.setAnswer2(null);
        } else {
            profileEditModel.setAnswer2(editsecondQAns.getText().toString());
        }

        profileEditModel.setSecurityQuesId1(mQid1);
        profileEditModel.setSecurityQuesId2(mQid2);

        profileEModel.setId(address_id);
        profileEModel.setAddressLine1(editAddress1.getText().toString());
        profileEModel.setAddressLine2("");
        profileEModel.setCity(editCity.getText().toString());
        profileEModel.setCountry("UK");
        profileEModel.setPhone(editPhone.getText().toString());
//        profileEModel.setPhone(mlong);
        profileEModel.setState(editState.getText().toString());
        profileEModel.setZipCode(editZipcode.getText().toString());

        profileEditModel.setAddress(profileEModel);

    }

    public class EditProfileUpdate extends AsyncTask<String, String, EditProfileModel> {

        //        EditProfileModel finalEditResult;
        EditProfileModel editProfileSubModel = new EditProfileModel();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected EditProfileModel doInBackground(String... params) {
            try {
                String mUrl = OnePocketUrls.USER_BALENCE_CHECK() + user_id;
                response = mJsonUtil.postMethod(mUrl, null, null, null, null, null, profileEditModel, null, null,
                        token,null,null,null);//profileEditModel
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            try {
                jsonObject = new JSONObject(response);

                jsonArray = jsonObject.getJSONArray("address");

                for (int i = 0; i < jsonArray.length(); i++) {
                    // for(int i=0;i<5;i++){
                    addressObject = jsonArray.getJSONObject(i);
                    editProfileSubModel.setState(addressObject.getString("state"));
//                    editProfileSubModel.setCountry(addressObject.getString("country"));
                    editProfileSubModel.setPhone(addressObject.getString("phone"));
                    editProfileSubModel.setCity(addressObject.getString("city"));
                    editProfileSubModel.setZipCode(addressObject.getString("zipCode"));
                    editProfileSubModel.setAddressLine1(addressObject.getString("addressLine1"));
                    editProfileSubModel.setAddressLine2(addressObject.getString("addressLine2"));
                }

                editProfileSubModel.setFirstName(jsonObject.getString("firstName"));
                editProfileSubModel.setLastName(jsonObject.getString("lastName"));
                editProfileSubModel.setEmail(jsonObject.getString("email"));
                editProfileSubModel.setGender(jsonObject.getString("gender"));

                Log.d("EditProfileModel", "Json Object Value " + jsonObject);
                return editProfileSubModel;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(EditProfileModel result) {
            super.onPostExecute(editProfileSubModel);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response.contains("Please select Security Question2")) {
                showAlertBox(getString(R.string.invalidans2));
            } else if (response.contains("Invalid Old Password") || response.contains("Old Password is Invalid")) {
                showAlertBox(getString(R.string.invalioldans));
            } else if (response.contains("Error Code-209") || response.contains("Old Password New Password should not be same")) {
                showAlertBox(getString(R.string.oldandnewpasswordsame));
            } else if (response.contains("Error Code-188")) {
                showAlertBox(getString(R.string.invalidSecurityAnswer1));
            } else if (response.contains("Error Code-190")) {
                showAlertBox(getString(R.string.invalidSecurityAnswer2));
            } else if (response.contains("Error Code-174")) {
                showAlertBox(getString(R.string.invalidMobileNumber));
            }else if (response.contains("Invalid Security Answer 1")) {
                showAlertBox(getString(R.string.invalidans1));
            } else if (response.contains("Please enter Security Answer1")) {
                showAlertBox(getString(R.string.invalidans1));
            } else if (response.contains("Invalid Security Answer 2")) {
                showAlertBox(getString(R.string.invalidans2));
            } else if (response.contains("Error Code-284")) {
                showAlertBox(getString(R.string.accountprofileedit));
            }  else if (response.contains("Unauthorized") || response.contains("invalid credentials")) {
                showAlertBox(getString(R.string.invalidCredintials));
            } else {
                try {

//                    fixed the bug in sprint-7 Ticket_Id-158 starts
                    if (result != null) {
                        finalEditResult = result;
                        finalResult = result;
                        successFlag = true;

//                    fixed the bug in sprint-7 Ticket_Id-158 end
                   /* if(finalEditResult!=null){
                        finalResult = finalEditResult;
                    }*/

                        firstName.setText(result.getFirstName());
                        lastName.setText(result.getLastName());
                        email.setText(result.getEmail());
                        phone.setText(result.getPhone());
//                        gender.setText(result.getGender());
                        addressLine1.setText(result.getAddressLine1());
//                      addressLine2.setText(result.getAddressLine2());
                        city.setText(result.getCity());
                        state.setText(result.getState());
//                        country.setText(result.getCountry());
                        zipcode.setText(result.getZipCode());
                    }


                    if (passwordCheck.isChecked() && securityQuestionsCheck.isChecked()) {
                        if (passwordCheckedFlag ) {
                            popup.dismiss();
                            successAlertFlag = true;
                            showAlertBox("Profile updated successfully");
                        }
                    } else if (passwordCheck.isChecked()) {
                        if (passwordCheckedFlag) {
                            popup.dismiss();
                            successAlertFlag = true;
                            showAlertBox("Profile updated successfully");
                        }
                    } else if (securityQuestionsCheck.isChecked()) {
                        if (securityCheckedFlag) {
                            popup.dismiss();
                            successAlertFlag = true;
                            showAlertBox(getString(R.string.editProfileUpdateSuccess));
                        }

                    } else if (successFlag) {
                        successAlertFlag = true;
                        showAlertBox(getString(R.string.editProfileUpdateSuccess));
                        popup.dismiss();
                    }

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    public class getSecQuestionIDs extends AsyncTask<Void, Void, Void> {
        /* @Override
         protected void onPreExecute() {
             super.onPreExecute();
             showProgress(getString(R.string.pleaseWait));
         }
 */
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String stringurl = OnePocketUrls.GET_SECURITY_QUE_IDS() + "username="+emailString;
                response = mJsonUtil.getMethod(stringurl,null,null);
//                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL(),null,null);
                OnePocketLog.d("getSecQuestionIDs resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("getSecQuestionIDs error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
//            hideProgress();
            if (response.contains("SecurityQuestionId")) {
                try{
                    JSONArray temp = new JSONArray(response);
                    for (int i = 0; i < temp.length(); i++) {
                        JSONObject e = temp.getJSONObject(i);
                        JSONObject ee = temp.getJSONObject(i+1);
                        mSQid1 = e.getInt("SecurityQuestionId1");
                        mSQid2 = ee.getInt("SecurityQuestionId2");
                        i++;
                    }
                    securityQOne.setSelection(mSQid1-1);
                    securityQTwo.setSelection(mSQid2-1);

                }catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }else if(response.contains("")) {
                showAlertBox(getString(R.string.entervalidmailid));
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                if (mConnectionDetector.isOnline()) {
                    onBackPressed();
                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    //    fixed the bug in sprint-10 Ticket_Id-206 starts
    public void showAlertBox(final String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if (!successAlertFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successAlertFlag= false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                        if (msg.equals("Profile updated successfully")) {
                            finish();
                            Intent intent1 = new Intent(EditProfileActivity.this, LoginActivity.class);
                            startActivity(intent1);
                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

    }

//    fixed the bug in sprint-10 Ticket_Id-206 end


    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Edit Profile");
//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

//        String title = mActionBar.getTitle().toString();
        String title = "Edit Profile";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);
    }


}
