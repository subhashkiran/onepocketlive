package com.subhashe.onepocket.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.subhashe.onepocket.Core.UserSecurity;
import com.subhashe.onepocket.Core.UserSecurityPayment;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by SubhashE on 1/27/2016.
 */
@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class MoreInfoActivity extends AppCompatActivity {

    ActionBar mActionBar;
    HashMap<String, String > moreInfoList;
    TextView mStationNamemore,mStationAddressMore,mPortMore,mStartTimeMore,mStartDateMore;
    TextView mEndTimeMore,mEndDateMore,mSessionTimeMore,mUsedKwhMore,mRateMore,mFinalCostMore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moreinfo);
        Intent intent = getIntent();
        moreInfoList = ( HashMap<String, String >) intent.getSerializableExtra("moremap");
        initToolbar();
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Edit Profile");
//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

//        String title = mActionBar.getTitle().toString();
        String title = "Activity Info";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

        mStationNamemore = (TextView)findViewById(R.id.StationNameMore);
        mStationAddressMore = (TextView)findViewById(R.id.StationAddressMore);
        mPortMore = (TextView)findViewById(R.id.PortMore);
        mStartTimeMore = (TextView)findViewById(R.id.StartTimeMore);
        mStartDateMore = (TextView)findViewById(R.id.StartDateMore);
        mEndTimeMore = (TextView)findViewById(R.id.EndTimeMore);
        mEndDateMore = (TextView)findViewById(R.id.EndDateMore);
        mSessionTimeMore = (TextView)findViewById(R.id.SessionTimeMore);
        mUsedKwhMore = (TextView)findViewById(R.id.UsedKwhMore);
        mRateMore = (TextView)findViewById(R.id.RateMore);
        mFinalCostMore = (TextView)findViewById(R.id.FinalCostMore);

        setMoredata(moreInfoList);
    }

    public void setMoredata(HashMap<String, String > moreInfoList){
        mStationNamemore.setText(moreInfoList.get("STANAME"));
        mStationAddressMore.setText(moreInfoList.get("STAADDRESS"));
        mPortMore.setText(moreInfoList.get("LEVEL"));
        mStartTimeMore.setText(moreInfoList.get("STARTTIME"));
        mStartDateMore.setText(moreInfoList.get("STARTDATE"));
        mEndTimeMore.setText(moreInfoList.get("ENDTIME"));
        mEndDateMore.setText(moreInfoList.get("ENDDATE"));
        mSessionTimeMore.setText(moreInfoList.get("SESSION TIME"));
        mUsedKwhMore.setText(moreInfoList.get("USED KWH"));
        mRateMore.setText(moreInfoList.get("RATE/KWH"));
        mFinalCostMore.setText(moreInfoList.get("FINALCOST"));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
