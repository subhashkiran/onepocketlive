package com.subhashe.onepocket.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.paypal.android.sdk.payments.ShippingAddress;
import com.subhashe.onepocket.Core.PayPalHeader;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by SubhashE on 3/9/2016.
 */
public class PayPalActivity extends AppCompatActivity {
    private static final String TAG = "PayPalActivity";
//    public static final String PUBLISHABLE_KEY = "pk_test_k3uATw8TceLRfJOQbVRHrAnP";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
//    private static final String CONFIG_CLIENT_ID = "credential from developer.paypal.com";
    private static final String CONFIG_CLIENT_ID = "AYL9hFa1n1kHNOVwlmgtGHbv5kiK8_Ht4-8RksbW1JBgLxVyKYA-u--7v91hKmz16AxUNH9Uw3DO86vL";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USERNAME_ID = "USERNAME_ID_PREFS_String";

    private Button paypalButton;
    private EditText paypalEditAmount;
    private String response,accessToken,state,total,currency,payId,token,xToken,user_name;
    private ProgressDialog progress;
    private JsonUtil mJsonUtil;
    private ConnectionDetector mConnectionDetector;
    private PayPalHeader payPalHeader;
    private boolean successFlag;

    JSONArray jObject1;
    JSONObject mTemp;

    LinearLayout ll_card,ll_paypal;
    TextView tv_card,tv_paypal;

    android.support.v7.app.ActionBar mActionBar;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
                    // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paypal_activity);
        initToolbar();
        paypalEditAmount = (EditText)findViewById(R.id.paypalEnterAmout);
        paypalButton = (Button)findViewById(R.id.payPalButton);

        payPalHeader = new PayPalHeader();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        xToken = prefs.getString(TOKEN_ID, "NOT FOUND");
        user_name = prefs.getString(USERNAME_ID, "NOT FOUND");

        paypalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PayPalActivity.this, PayPalService.class);
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                startService(intent);
                int i = 0;
                if(paypalEditAmount.getText().toString().length()!=0) {
                    i = Integer.parseInt(paypalEditAmount.getText().toString());
                }
                if(paypalEditAmount.getText().toString().length()==0) {
                    showAlertBox(getString(R.string.currencyEdit));
                }else if(i< 25) {
                    showAlertBox(getString(R.string.invalidLessAmount));
                }else {
                    onBuyPressed();
                }
            }
        });

        tv_card = (TextView) findViewById(R.id.tv_payment_card);
        tv_paypal=(TextView)findViewById(R.id.tv_payment_paypal);

        ll_card = (LinearLayout)findViewById(R.id.ll_payment_card);
        ll_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_card.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_card.setTextColor(getResources().getColor(R.color.white));
                ll_paypal.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_paypal.setTextColor(getResources().getColor(R.color.appColor));

                Intent intent = new Intent(PayPalActivity.this,CreditCardPayActivity.class);
                startActivity(intent);

            }
        });

        ll_paypal = (LinearLayout) findViewById(R.id.ll_payment_paypal);
        ll_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_card.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_card.setTextColor(getResources().getColor(R.color.appColor));
                ll_paypal.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_paypal.setTextColor(getResources().getColor(R.color.white));

                Intent intent = new Intent(PayPalActivity.this, PayPalActivity.class);
                startActivity(intent);
            }
        });


    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("PayPal");

        String title = "Add Money";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

//        mActionBar.setHomeButtonEnabled(true);
        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);

        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));
    }

    public void onBuyPressed() {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        Intent intent = new Intent(PayPalActivity.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        String temp = paypalEditAmount.getText().toString();
        return new PayPalPayment(new BigDecimal(temp), "USD", "PayPal",
                paymentIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
//                onBackPressed();
                //finish();
                Intent intent = new Intent(PayPalActivity.this , EditProfile.class);
                startActivity(intent);
                return true;
            default:return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        System.out.println("Responseeee" + confirm);
                        Log.i("paymentExample", confirm.toJSONObject().toString());


                        JSONObject jsonObj=new JSONObject(confirm.toJSONObject().toString());

                        String paymentId=jsonObj.getJSONObject("response").getString("id");
                        System.out.println("payment id:-==" + paymentId);
                        payId =paymentId ;
                        payPalHeader.setId(payId);
                        payPalHeader.setUsername(user_name);
//                        Toast.makeText(getApplicationContext(), paymentId, Toast.LENGTH_LONG).show();

                        new getAccessToken().execute();
                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment was submitted. Please see the docs.");
            }
        }
    }

    public class getAccessToken extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.postMethod(OnePocketUrls.CARD_PAYPAL_GETTOKEN(), null, null,null,null,null,null,null,
                        null, null,null,null,null);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            hideProgress();
            if (response.contains("access_token")){
                try {
                    JSONObject jObject = new JSONObject(response);
                    accessToken = jObject.getString("access_token");
                    token = "Bearer "+accessToken;
                    new setAccessToken().execute();
                }catch (Exception e){

                }
            }
        }
    }


    public class setAccessToken extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(OnePocketUrls.CARD_PAYPAL_SENDTOKEN() + payId, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            hideProgress();
            if (response.contains(payId)){
                try {
                    JSONObject jObject = new JSONObject(response);
                    state = jObject.getString("state");
                    jObject1 = jObject.getJSONArray("transactions");

                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp1 = jObject1.getJSONObject(i);
                        JSONArray mTemp2 = mTemp1.getJSONArray("related_resources");
                        JSONObject mTemp3 = mTemp2.getJSONObject(i);
                        JSONObject mTemp4 = mTemp3.getJSONObject("sale");
                        JSONObject mTemp5= mTemp4.getJSONObject("amount");
                        total = mTemp5.getString("total");
                        currency = mTemp5.getString("currency");
                        payPalHeader.setTotal(total);
                        payPalHeader.setCurrency(currency);
                        new sendPaypalToServer().execute();
                    }

                }catch (Exception e){

                }
            }
        }
    }

    public class sendPaypalToServer extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressCredit(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.postMethod(OnePocketUrls.CARD_PAYPAL_SERVER(),null,null,null,null,null,null,payPalHeader,
                        null, xToken,null,null,null);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgressCredit();
            if (response.contains("Success")) {
                successFlag = true;
                showAlertBox(getString(R.string.paymentSuccessful));
            }else if(response.contains("Error Code-118")){
                showAlertBox("Got error while sending Acknowledgment mail.");
            }
        }
    }


    private void showProgressCredit(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgressCredit() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if(!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        if(successFlag) {
                            successFlag = false;
                            finish();
                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                PayPalActivity.this).create();
        if(!successFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialog.setMessage(msg);
//        alertDialog.setCancelable(false);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(successFlag) {
                    successFlag = false;
                    finish();
                }
            }
        });
        alertDialog.show();*/
    }

}
