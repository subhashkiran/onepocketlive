package com.subhashe.onepocket.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.subhashe.onepocket.Activities.EVEditActivity;
import com.subhashe.onepocket.Adapters.EVRecyclerViewAdapter;
import com.subhashe.onepocket.Adapters.RecyclerViewAdapter;
import com.subhashe.onepocket.Core.EvModel;
import com.subhashe.onepocket.Core.EvModelAcc;
import com.subhashe.onepocket.Fragments.AccountFragment;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EVListActivity extends AppCompatActivity {


    Point p1;
    private Button ButAddNewEv, btnSubmit, close;
    LinearLayout ll_fast_charge;
    private ProgressDialog progress;
    private int user_id;
    private long vehicleId;
    private boolean firstFlag,firsttime,evAddError;
    private JsonUtil mJsonUtil;
    private String response1,response2,token,sItem;
    String[] SelectYears = new String[] {"Select Year","2017","2016","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005"};
    private Spinner spinner;
    private ConnectionDetector mConnectionDetector;
    private EditText editEnterMake,editEnterModel/*,editEnterVin,editEnterDiscription*/;
    ArrayAdapter<String> adapter;
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String FAST_SWITCH = "FAST_SWITCH";
    public static final String VEHICEID = "VEHICEID";

    private Switch dcFastSwitchEV;
    ImageView img_back;
    LinearLayout ll_actionbar;
    private FragmentManager fragmentManager;

    private boolean successFlag;
    TableLayout tableLayout;
    TableRow tableRow;
    TextView year,make,model/*,vin,description*/;
    //    int id=60;

    private EvModelAcc evListEdit;
    private PopupWindow popup;
    FloatingActionButton butAddNewEv;
    private RecyclerView myEVRecyclerView;
    private EVRecyclerViewAdapter mEVAdapter;
    private LinearLayout eVLayoutEmpty;
    ActionBar mActionBar;
    private Fragment fragment = null;
    ArrayList<EvModel> evList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evlist);

        initToolbar();

        mConnectionDetector = new ConnectionDetector(getApplicationContext());

        new KeyBoardHide().hideKeyBoard(EVListActivity.this);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(ACC_USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

        evList = new ArrayList<EvModel>();

        if (mConnectionDetector.isOnline()) {
            new ElectrivVehicleLIST().execute();
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }

        eVLayoutEmpty = (LinearLayout)findViewById(R.id.evEmptyLayout);

        myEVRecyclerView = (RecyclerView) findViewById(R.id.ev_recyclerView);
        myEVRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        myEVRecyclerView.setHasFixedSize(true);

        butAddNewEv = (FloatingActionButton) findViewById(R.id.buttonAddNewEV);
        butAddNewEv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                int[] location = new int[2];
                butAddNewEv.getLocationOnScreen(location);

                //Initialize the Point with x, and y positions
                p1 = new Point();
                p1.x = location[0];
                p1.y = location[1];
                //Open popup window
                if (p1 != null)
                    showPopup(EVListActivity.this, p1);

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        new ElectrivVehicleLIST().execute();

    }

    /*public void calllayout(){}*/

    private void showPopup(final Activity context, Point p1) {
        int popupWidth = 700;
        int popupHeight = 800;
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.AddNewEVLayout);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.ev_addnew_ev_view, viewGroup);

        evListEdit = new EvModelAcc();

        final PopupWindow popup = new PopupWindow(layout, WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT,true);

        ll_actionbar = (LinearLayout)layout.findViewById(R.id.ll_add_ev);
        ll_actionbar.setVisibility(View.VISIBLE);

        spinner =(Spinner)layout.findViewById(R.id.SelectYearSpinner);
        adapter = new ArrayAdapter<String>(this ,android.R.layout.simple_spinner_item, SelectYears);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sItem = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        img_back = (ImageView) layout.findViewById(R.id.addev_back_arrow);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        editEnterMake =  (EditText) layout.findViewById(R.id.editTextenterMake);
        editEnterModel =  (EditText) layout.findViewById(R.id.editTextEnterModel);
        dcFastSwitchEV = (Switch)layout.findViewById(R.id.evDCFastSwitch);
        /*editEnterVin=  (EditText) layout.findViewById(R.id.editTextEnterVIN);
        editEnterDiscription=  (EditText) layout.findViewById(R.id.editTextenterDescription);*/
       /* editEnterVin.setText("");
        editEnterDiscription.setText("");*/
        // Clear the default translucent background
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

//        popup.setOutsideTouchable(false);
//        popup.setFocusable(false);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.update();

        // Displaying the popup at the specified location, + offsets.
        /*popup.showAtLocation(layout, Gravity.NO_GRAVITY, p1.x + OFFSET_X, p1.y + OFFSET_Y);*/


        btnSubmit = (Button)layout.findViewById(R.id.btnAddNewEvSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinner.getSelectedItem().toString().contains("Select Year")) {
                    spinner.requestFocus();
                    showAlertBox(getString(R.string.AddEv_SelectYear));
                }else if (editEnterMake.getText().toString().length() == 0) {
                    editEnterMake.requestFocus();
                    showAlertBox(getString(R.string.enterMake));
                }else if(editEnterModel.getText().toString().length() == 0){
                    editEnterModel.requestFocus();
                    showAlertBox(getString(R.string.enterModel));
                }else if(editEnterMake.getText().toString().length() <= 2 || editEnterMake.getText().toString().length() > 20 ){
                    editEnterMake.requestFocus();
                    showAlertBox(getString(R.string.makeError));
                } else if(editEnterModel.getText().toString().length() <= 2 || editEnterModel.getText().toString().length() > 20){
                    editEnterModel.requestFocus();
                    showAlertBox(getString(R.string.modelError));
                } else{
                    evListEdit.setYear(spinner.getSelectedItem().toString());
                    evListEdit.setMake(editEnterMake.getText().toString());
                    evListEdit.setModel(editEnterModel.getText().toString());
                    evListEdit.setVehicleId(user_id);
                   /* if(editEnterVin.getText().length()>1) {
                        evListEdit.setVin(editEnterVin.getText().toString());
                    }*/
                    evListEdit.setVehicleId(user_id);
                   /* if(editEnterDiscription.getText().length()>1) {
                        evListEdit.setDescription(editEnterDiscription.getText().toString());
                    }*/

//                    fixed the bug in sprint-7 Ticket_Id-114 starts
                    if (mConnectionDetector.isOnline()) {
                        if(validateEvList()) {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            if (dcFastSwitchEV.isChecked()) {
                                dcFastSwitchEV.setChecked(true);
                                editor1.putBoolean(FAST_SWITCH, true);
                            } else {
                                dcFastSwitchEV.setChecked(false);
                                editor1.putBoolean(FAST_SWITCH, false);
                            }
                            editor1.commit();

                            new ElectrivVehicleADD().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            if(!evAddError) {
                                new ElectrivVehicleLIST().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            }
                            popup.dismiss();
                        }
                    } else {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
//                    fixed the bug in sprint-7 Ticket_Id-114 end
                }
            }
        });

        close = (Button) layout.findViewById(R.id.btnAddNewEvCancel);
        close.setVisibility(View.GONE);

        ll_fast_charge = (LinearLayout) layout.findViewById(R.id.ll_add_ev_fastcharge);
        ll_fast_charge.setVisibility(View.GONE);
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);

        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

        String title = "My EV";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                //fragment = new AccountFragment();
               /* fragment = new AccountFragment();

                if (mConnectionDetector.isOnline()) {
                    initFragment();
                } else {
                    Toast.makeText(EVListActivity.this, getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }*/

                finish();

                //onBackPressed();

               /* Intent intent = new Intent(EVListActivity.this , AccountsListActivity.class);
                startActivity(intent);*/

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initFragment() {
        if (fragment != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.main_layout, fragment).addToBackStack(null).commit();

            new KeyBoardHide().hideKeyBoard(EVListActivity.this);
            // update selected item and title, then close the drawer
//                mDrawerList.setItemChecked(position, true);
//                mDrawerList.setSelection(position);
//                mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.d("NavigationDrActivity", "Error  creating fragment");
        }
    }

    private boolean validateEvList(){
        if((editEnterMake.getText().toString().length() > 0) &&
                (editEnterModel.getText().toString().length() > 0)){
            return true;
        }
        else{
            return false;
        }
    }

    public class ElectrivVehicleADD extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                String mUrl = OnePocketUrls.ACCOUNT_EV();
                response1 = mJsonUtil.postMethod(OnePocketUrls.EV_ADD(),null,null,null, null,evListEdit,null,null,
                        null,null,null,null,null);
                OnePocketLog.d("EV Add resp----------", response1);


            } catch (Exception e) {
                OnePocketLog.d("EV Add error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //hideProgress();
            if (response1 == null || response1 == "error") {
                showAlertBoxMakeModel(getString(R.string.servernotresponce));
            } else if(response1.contains("Error Code-114") || response1.contains("Vehicle already exists.")){
                showAlertBoxMakeModel(getString(R.string.vinExists));
            }else if(response1.contains("Error Code-195") || response1.contains("Make is Invalid.")){
                //evAddError = true;
                showAlertBoxMakeModel(getString(R.string.makeError));
            }else if(response1.contains("Error Code-197") || response1.contains("Model is Invalid.")){
                //evAddError = true;
                showAlertBoxMakeModel(getString(R.string.modelError));
            }else {
                try {
                    // showPopup(getActivity() , p1);
                    popup.dismiss();

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    public class ElectrivVehicleLIST extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.EV_LIST()+user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response2 = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("EVADDNEW resp----------", response2);


            } catch (Exception e) {
                OnePocketLog.d("EVADDNEW error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ArrayList<EvModel> evList = new ArrayList<EvModel>();

            hideProgress();
            if (response2 == null || response2 == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    JSONArray temp = new JSONArray(response2);

                    if (temp.length() == 0) {

                        eVLayoutEmpty.setVisibility(View.VISIBLE);

                    } else {

                        if (!evAddError) {

                            eVLayoutEmpty.setVisibility(View.INVISIBLE);

                            Log.d("MainActivity.java", "temp : " + temp);

                            for (int i = 0; i < temp.length(); i++) {
                                // for(int i=0;i<5;i++){
                                JSONObject e = temp.getJSONObject(i);

                                EvModel evModel = new EvModel();

                                Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                                evModel.setYear(e.getString("year"));
                                evModel.setDescription(e.getString("description"));
                                evModel.setModel(e.getString("model"));
                                evModel.setVehicleId((int) e.getLong("vehicleId"));
                                evModel.setMake(e.getString("make"));
                                evModel.setVin(e.getString("vin"));

                                evList.add(evModel);

                            }
                        } else if (!firstFlag) {
                            Log.d("MainActivity.java", "temp : " + temp);

                            for (int i = 0; i < evList.size(); i++) {
                                // for(int i=0;i<5;i++){
                                JSONObject e1 = temp.getJSONObject(i);

                                EvModel evModel = new EvModel();
                                evModel = evList.get(i);
                                Log.d("Main Activity", "Json Object Value " + i + " : " + e1);

                                evModel.setYear(e1.getString("year"));
                                evModel.setDescription(e1.getString("description"));
                                evModel.setModel(e1.getString("model"));
                                //evModel.setVehicleId(e.getLong("vehicleId"));
                                evModel.setMake(e1.getString("make"));
                                evModel.setVin(e1.getString("vin"));

                                evList.add(evModel);
                            }
                            if (firsttime) {
                                successFlag = true;
                                showAlertBox(getString(R.string.saveInformaiton));
                            }
                            firstFlag = true;
                        }

                        mEVAdapter = new EVRecyclerViewAdapter(EVListActivity.this, evList);
                        myEVRecyclerView.setAdapter(mEVAdapter);

                    }
                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(EVListActivity.this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if(!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                        evAddError = false;
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClicsk(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();
    }

    public void showAlertBoxMakeModel(final String msg1) {

        AlertDialog.Builder alertDialogBuilder1 = new AlertDialog.Builder(this);
        if(!successFlag) {
            alertDialogBuilder1.setTitle(R.string.Alert);
            alertDialogBuilder1.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialogBuilder1
                .setMessage(msg1)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                        //evAddError = false;
                       /* finish();
                        Intent intent1 = new Intent(EVListActivity.this,EVListActivity.class);
                        startActivity(intent1);*/

                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClicsk(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog1= alertDialogBuilder1.create();
        alertDialog1.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog1.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog1.show();
    }

}


