package com.subhashe.onepocket.Activities;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.subhashe.onepocket.Utils.OnePocketUrls;
import com.subhashe.onepocket.Utils.SharedPreferencesStore;
import io.fabric.sdk.android.Fabric;

/**
 * Created by SubhashE on 2/4/2016.
 */
public class OnePocketApp extends Application {
    private static final String LOG_TAG = "OnePocketApp";

    private static Context context;
    private SharedPreferencesStore mStore;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        // Set up & Initialize Fabric with the debug-disabled crashlytics.
        /*Fabric.with(this, new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build(), new Crashlytics());*/

        mStore = new SharedPreferencesStore();
        if (OnePocketUrls.getVersionType(this).equalsIgnoreCase("PROD")) {
            OnePocketUrls.CURRENT_BUILD_CONFIG = OnePocketUrls.ServerConfig.ServerConfigProd;
        } else if (OnePocketUrls.getVersionType(this).equalsIgnoreCase("QA")) {
            OnePocketUrls.CURRENT_BUILD_CONFIG = OnePocketUrls.ServerConfig.ServerConfigQA;
        } else if (OnePocketUrls.getVersionType(this).equalsIgnoreCase("INT")) {
            OnePocketUrls.CURRENT_BUILD_CONFIG = OnePocketUrls.ServerConfig.ServerConfigQA;
        }

        context = this;
    }

    public static boolean isLoggedIn() {
        return !SharedPreferencesStore.getEncryptedSharedPref(SharedPreferencesStore.ONEPOCKET_USER_NAME).
                equals(SharedPreferencesStore.KEY_UNASSIGNED);
    }
}
