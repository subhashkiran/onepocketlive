package com.subhashe.onepocket.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.Marker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.subhashe.onepocket.Adapters.ClearableAutoCompleteTextView;
import com.subhashe.onepocket.Core.FavModel;
import com.subhashe.onepocket.Core.MapListModel;
import com.subhashe.onepocket.Core.MapModel;
import com.subhashe.onepocket.Core.MapModelFilters;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;
import com.subhashe.onepocket.Utils.Util;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

/**
 * Created by axxera on 07-03-2017.
 */

public class FavoritesMoreInfoActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener  {

    //    fixed the bug in sprint-7 Ticket_Id-147 end
    public static final String CHARGER_ID = "CHARGER_ID";
    public static final String FAVORITEDATA = "FAVORITEDATA";
    public static final String FAVJSONSTRING = "FAVJSONSTRING";
    public static final String USER_ID = "USERID_PREFS_String";
    private static final int ZBAR_CAMERA_PERMISSION = 1;
    private static final String LOG_TAG = "Charger Info";
    private static final LatLngBounds AXXERA_HYD = new LatLngBounds(new LatLng(17.2168886, 78.1599217),
            new LatLng(17.6078088, 78.6561694));
    protected LatLng start;
    protected LatLng end;
    protected GoogleApiClient mGoogleApiClient;
    android.support.v7.app.ActionBar mActionBar;
    HashMap<String, String> hashMap;
    HashMap<String, List<String>> favData;
    List<MapListModel> mapListModels;
    List<MapModel> mapListTellus;
    //    fixed the bug in sprint-7 Ticket_Id-147 starts
    List<MapModelFilters> mapListModelsFilter;
    LinearLayout tripPlannerLayout, chargerDetailsLayout;
    LinearLayout.LayoutParams tripPlannerLayoutParams;
    ImageButton backNavigation;
    LinearLayout showStartTextView, showDestinationTextView, showStartAutoCompleteView, showDestinationAutoCompleteView;
    TextView yourLocation, destinationLocation;
    String destinationName;
    ImageView searchIcon;
    AutoCompleteTextView starting;
    AutoCompleteTextView destination;
    LinearLayout layoutStatus;
    Spinner placeTypeSpinner;
    ArrayAdapter<String> adapter;
    String[] mPlaceType = null;
    String[] mPlaceTypeName = null;
    GPSTracker gpsTracker;
    double yourLocationLatitude = 0;
    double yourLocationLongitude = 0;
    LatLng yourLocationLatLng, destinationLocationLatLng;
    boolean currentLocationFlag = true, destinationLocationFlag = true;
    boolean favFlag;
    InputMethodManager inputMethodManager;
    boolean singleMarker = false;

    //Button goButton;
    Marker marker1, marker2;
    View v;
    Intent intent1 = new Intent();
    Double destinationLattitude, destinationLongitude;
    ProgressDialog progressDialog;
    FavModel mFavModel;
    int selectedPosition;
    String spinnerValueType;
    ImageView goButton;
    FloatingActionButton mFab;
    ImageView img_nav;
    private Toolbar toolbar;
    private String chargeId, markerCheckT;
    private boolean tellus, allStaion;
    //    private TextView mstationNameAct, mstationStatus, mchargersAct, mlevelType1Act, mlevelType2Act;
    /*private TextView accessDaysTime, fuelTypeCode, groupsWithAccessCode, stationName, stationPhone, statusCode, city, state, streetAddress, zip, bdBlends, e85ConnectoPump, evConnectorTypes, evDcFastNum, evLevel1EvseNum, evLevel2EvseNum, evNetwork, evNetworkWeb,
            evOtherEvse, hyStatusLink, lpgPrimary, ngFillTypeCode, ngPsi, ngVehicleClass;*/
    private TextView stationName, stationAdress,/*chargerType, */
            network, fee, connectorType, liveStatus, vendingprice1, vendingprice2, city, state, zipCode, notes, port1, port2, parking, adaParking;
    private ProgressDialog progress;
    private ConnectionDetector mConnectionDetector;
    private Class<?> mClss;
    private String strSavFav = "";
    private String stationId = "";
    private int user_id;
    private JsonUtil mJsonUtil;
    private DrawerLayout drawerLayout;
    private LatLng locationInfo;
    private MenuItem menuTripPlanner;
    private PlaceAutoCompleteAdapter mAdapter;
    private ClearableAutoCompleteTextView searchBox;
    private String response, favoriteHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_info_layout);
        initToolbar();
        initTripPlanner();
        mConnectionDetector = new ConnectionDetector(this);

        setResult(5, intent1);

        mFavModel = new FavModel();

        favData = new HashMap<String, List<String>>();
        Intent intent = getIntent();
        try {
            tellus = intent.getExtras().getBoolean("tellusmarker");
            if (tellus) {
//                hashMap = (HashMap<String, String>) intent.getSerializableExtra("sendListTellus");
                mapListTellus = (List<MapModel>) intent.getSerializableExtra("sendListTellus");

            }
        } catch (Exception e) {
            Log.d("MapMarkerInfoActivity", "catch");
            e.printStackTrace();
        }

       /* try {
            mapListModels = (List<MapListModel>) intent.getSerializableExtra("sendListData");
//            chargeId = intent.getExtras().getString("chargerMkey");
//            markerCheckT = intent.getExtras().getString("markerCheck");
        } catch (Exception e) {
            Log.d("MapMarkerInfoActivity", "catch");
            e.printStackTrace();
        }

        try {
            //            fixed the bug in sprint-7 Ticket_Id-147 starts
            mapListModelsFilter = (List<MapModelFilters>) intent.getSerializableExtra("sendListDataFilter");
//            fixed the bug in sprint-7 Ticket_Id-147 end

//            chargeId = intent.getExtras().getString("chargerMkey");
//            markerCheckT = intent.getExtras().getString("markerCheck");
        } catch (Exception e) {
            Log.d("MapMarkerInfoActivity", "catch");
            e.printStackTrace();
        }*/

        chargeId = intent.getExtras().getString("chargerMkey");
        markerCheckT = intent.getExtras().getString("markerCheck");

       /* mstationNameAct = (TextView) findViewById(R.id.stationNameAct);
        mstationStatus = (TextView) findViewById(R.id.stationStatus);
        mchargersAct = (TextView) findViewById(R.id.chargersAct);
        mlevelType1Act = (TextView) findViewById(R.id.levelType1Act);
        mlevelType2Act = (TextView) findViewById(R.id.levelType2Act);*/


        stationName = (TextView) findViewById(R.id.stationNameAct);
        stationAdress = (TextView) findViewById(R.id.stationAddres);
//        chargerType = (TextView) findViewById(R.id.fuelTypeCode);
       /* network = (TextView) findViewById(R.id.evNetwork);
        fee = (TextView) findViewById(R.id.cardsAccepted);*/
        //connectorType = (TextView) findViewById(R.id.evConnectorTypes);
        port1 = (TextView) findViewById(R.id.station_port1);
        port2 = (TextView) findViewById(R.id.station_port2);
        liveStatus = (TextView) findViewById(R.id.stationLiveStatus);
        vendingprice1 = (TextView) findViewById(R.id.vprice1);
        vendingprice2 = (TextView) findViewById(R.id.vprice2);

        parking = (TextView) findViewById(R.id.stationparking);
        adaParking = (TextView) findViewById(R.id.stationada);

        layoutStatus = (LinearLayout) findViewById(R.id.ll_station_status);


        initFab();

      /*  accessDaysTime = (TextView) findViewById(R.id.accessDaysTime);
        fuelTypeCode = (TextView) findViewById(R.id.fuelTypeCode);
        groupsWithAccessCode = (TextView) findViewById(R.id.groupsWithAccessCode);
        stationName = (TextView) findViewById(R.id.stationName);
        stationPhone = (TextView) findViewById(R.id.stationPhone);
        statusCode = (TextView) findViewById(R.id.statusCode);
        city = (TextView) findViewById(R.id.city);
        state = (TextView) findViewById(R.id.state);
        streetAddress = (TextView) findViewById(R.id.streetAddress);
        zip = (TextView) findViewById(R.id.zip);
        bdBlends = (TextView) findViewById(R.id.bdBlends);
        e85ConnectoPump = (TextView) findViewById(R.id.e85ConnectoPump);
        evConnectorTypes = (TextView) findViewById(R.id.evConnectorTypes);
        evDcFastNum = (TextView) findViewById(R.id.evDcFastNum);
        evLevel1EvseNum = (TextView) findViewById(R.id.evLevel1EvseNum);
        evLevel2EvseNum = (TextView) findViewById(R.id.evLevel2EvseNum);
        evNetwork = (TextView) findViewById(R.id.evNetwork);
        evNetworkWeb = (TextView) findViewById(R.id.evNetworkWeb);
        evOtherEvse = (TextView) findViewById(R.id.evOtherEvse);
        hyStatusLink = (TextView) findViewById(R.id.hyStatusLink);
        lpgPrimary = (TextView) findViewById(R.id.lpgPrimary);
        ngFillTypeCode = (TextView) findViewById(R.id.ngFillTypeCode);
        ngPsi = (TextView) findViewById(R.id.ngPsi);
        ngVehicleClass = (TextView) findViewById(R.id.ngVehicleClass);
*/

        if (mapListTellus != null/* && (allStaion != true)*/) {
            if (markerCheckT.equals("Type1")) {
//                displayMarkerData(hashMap, chargeId);
                displayTellusData(mapListTellus);
                tellus = true;
            }
        }

        if (mapListModels != null /*&& (tellus != true)*/) {
            if (markerCheckT.equals("Type2")) {
                displayMarkerListData(mapListModels);
                allStaion = true;
            }
        }
        if (mapListModelsFilter != null /*&& (tellus != true)*/) {
            if (markerCheckT.equals("Type2")) {
//                fixed the bug in sprint-7 Ticket_Id-147 starts
                displayMarkerListDataFilters(mapListModelsFilter);
//                fixed the bug in sprint-7 Ticket_Id-147 end
                allStaion = true;
            }
        }
    }

    private void initFab() {
        img_nav = (ImageView) findViewById(R.id.img_navigation);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        img_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // toggleSearch(true);

                double lat = 0;
                double lang = 0;
                lat = mapListTellus.get(0).getLattitude();
                lang = mapListTellus.get(0).getLongitude();
                String parseString = "google.navigation:q=" + lat + "," + lang;
                Uri gmmIntentUri = Uri.parse(parseString);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");

                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(FavoritesMoreInfoActivity.this, "Google maps is disabled,Please enable it.", Toast.LENGTH_SHORT).show();
                }
//                startActivity(mapIntent);
            }
        });
    }

  /*  protected void toggleSearch(boolean reset) {

        searchBox = (ClearableAutoCompleteTextView) findViewById(R.id.search_box);
        searchIcon = (ImageView) findViewById(R.id.search_icon);

        if (mConnectionDetector.isOnline()) {
            if (reset) {
                // hide search box and show search icon
               *//* myMenu.findItem(R.id.action_sortFilter).setVisible(true);
                myMenu.findItem(R.id.action_barcodeScannerH).setVisible(true);*//*
                searchBox.setText("");
                searchBox.setVisibility(View.GONE);
                searchIcon.setVisibility(View.VISIBLE);
//                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
                mFab.setVisibility(View.GONE);
                // hide the keyboard
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
            } else {
                // hide search icon and show search box
               *//* myMenu.findItem(R.id.action_sortFilter).setVisible(false);
                myMenu.findItem(R.id.action_barcodeScannerH).setVisible(false);*//*

                if (singleMarker) {
                    if (marker1 != null) {
                        marker1.remove();
//                marker1 = null;
                    }
                }
                searchIcon.setVisibility(View.GONE);
                searchBox.setVisibility(View.VISIBLE);
                searchBox.requestFocus();
                // show the keyboard
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchBox, InputMethodManager.SHOW_FORCED);
            }
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }
    }*/

    private void displayTellusData(List<MapModel> result) {
        for (int i = 0; i < result.size(); i++) {
            MapModel mapModelListTellus = new MapModel();
            mapModelListTellus = result.get(i);

            stationId = mapModelListTellus.getStationID();
            stationName.setText(mapModelListTellus.getStationName());

            String tempAdd = "";
            if (mapModelListTellus.getStationAddress().contains("null")) {
                tempAdd = "Not Available";
            } else {
                tempAdd = mapModelListTellus.getStationAddress();
            }
            stationAdress.setText(tempAdd);
//            chargerType.setText("NA");
            //connectorType.setText(mapModelListTellus.getConn1portType()+","+mapModelListTellus.getConn2PortType());
            port1.setText(mapModelListTellus.getConn1portType());
            port2.setText(mapModelListTellus.getConn2PortType());
            liveStatus.setText(mapModelListTellus.getLiveStatus());
            String statusMsg = "";
            if (mapModelListTellus.getLiveStatus().contains("Available")) {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
            } else if (mapModelListTellus.getLiveStatus().contains("In-Use")) {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
            }
            vendingprice1.setText(mapModelListTellus.getVendingPrice1());
            vendingprice2.setText(mapModelListTellus.getVendingPrice2());

            //parking.setText(mapModelListTellus.getParkingPricePH());
            //adaParking.setText(mapModelListTellus.getAdaStatus());

            /*stationName.setText(mapListModel.getStation_name());
            chargerType.setText(mapListModel.getFuel_type_code());
            network.setText(mapListModel.getEv_network());
            fee.setText(mapListModel.getCards_accepted());
            connectorType.setText(mapListModel.getEv_connector_types());
            liveStatus.setText(mapListModel.getAccess_days_time());
            streetAddress.setText(mapListModel.getStreet_address());
            city.setText(mapListModel.getCity());
            state.setText(mapListModel.getState());
            zipCode.setText(mapListModel.getZip());
            notes.setText(mapListModel.getIntersection_directions());*/


//            destinationName = streetAddress.getText().toString();
            destinationLattitude = mapModelListTellus.getLattitude();
            destinationLongitude = mapModelListTellus.getLongitude();
            Log.d("mapListModel", "" + mapModelListTellus);

            LatLng temp = new LatLng(destinationLattitude, destinationLongitude);

        }
    }


    //    fixed the bug in sprint-7 Ticket_Id-147 starts
    private void displayMarkerListDataFilters(List<MapModelFilters> result) {
        for (int i = 0; i < result.size(); i++) {
            MapModelFilters mapListModel = new MapModelFilters();
            mapListModel = result.get(i);

           /* Log.d("mapListModel", ": " + mapListModel);
            Log.d("connector list", ": " + mapListModel.getConnectorTypesList());*/

           /* for(int count=0;count<mapListModel.getConnectorTypesList().size();count++){
                Log.d("data list",": "+mapListModel.getConnectorTypesList().get(count));
            }*/

           /* mstationNameAct.setText("");
            mstationStatus.setText("");
            mchargersAct.setText("");
            mlevelType1Act.setText("");
            mlevelType2Act.setText("");*/


            stationName.setText(mapListModel.getStation_name());
//            chargerType.setText(mapListModel.getFuel_type_code());
            network.setText(mapListModel.getEv_network());
            fee.setText(mapListModel.getCards_accepted());
            //connectorType.setText(mapListModel.getEv_connector_types());
            port1.setText(mapListModel.getConn1_porttype());
            port2.setText(mapListModel.getConn2_porttype());
            liveStatus.setText(mapListModel.getStation_live_status());
            String statusMsg = "";
            if (mapListModel.getStation_live_status().contains("Available")) {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
            } else if (mapListModel.getStation_live_status().contains("In-Use")) {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
            }
         /*   streetAddress.setText(mapListModel.getStreet_address());
            city.setText(mapListModel.getCity());
            state.setText(mapListModel.getState());
            zipCode.setText(mapListModel.getZip());
            notes.setText(mapListModel.getIntersection_directions());*/

           /* accessDaysTime.setText(mapListModel.getAccess_days_time());
            fuelTypeCode.setText(mapListModel.getFuel_type_code());
            groupsWithAccessCode.setText(mapListModel.getGroups_with_access_code());
            stationName.setText(mapListModel.getStation_name());
            stationPhone.setText(mapListModel.getStation_phone());
            statusCode.setText(mapListModel.getStatus_code());
            city.setText(mapListModel.getCity());
            state.setText(mapListModel.getState());
            streetAddress.setText(mapListModel.getStreet_address());
            zip.setText(mapListModel.getZip());
            bdBlends.setText(mapListModel.getBd_blends());
            e85ConnectoPump.setText(mapListModel.getE85_blender_pump());

            //evConnectorTypes.setText(mapListModel.getConnectorTypesList().size());
            evConnectorTypes.setText(mapListModel.getEv_connector_types());
            evDcFastNum.setText(Integer.toString(mapListModel.getEv_dc_fast_num()));
            evLevel1EvseNum.setText(Integer.toString(mapListModel.getEv_level1_evse_num()));
            evLevel2EvseNum.setText(Integer.toString(mapListModel.getEv_level2_evse_num()));
            evNetwork.setText(mapListModel.getEv_network());
            evNetworkWeb.setText(mapListModel.getEv_network_web());
            evOtherEvse.setText(mapListModel.getEv_other_evse());
            hyStatusLink.setText(mapListModel.getHy_status_link());
            lpgPrimary.setText(mapListModel.getLpg_primary());
            ngFillTypeCode.setText(mapListModel.getNg_fill_type_code());
            ngPsi.setText(mapListModel.getNg_psi());
            ngVehicleClass.setText(mapListModel.getNg_vehicle_class());*/

            /*destinationName = streetAddress.getText().toString();
            destinationLattitude = mapListModel.getLattitude();
            destinationLongitude = mapListModel.getLongitude();*/
            Log.d("mapListModel", "" + mapListModel);

        }
    }
//    fixed the bug in sprint-7 Ticket_Id-147 end

    private void displayMarkerListData(List<MapListModel> result) {
        for (int i = 0; i < result.size(); i++) {
            MapListModel mapListModel = new MapListModel();
            mapListModel = result.get(i);

        /*    Log.d("mapListModel", ": " + mapListModel);
            Log.d("connector list", ": " + mapListModel.getConnectorTypesList());*/

           /* for(int count=0;count<mapListModel.getConnectorTypesList().size();count++){
                Log.d("data list",": "+mapListModel.getConnectorTypesList().get(count));
            }*/

           /* mstationNameAct.setText("");
            mstationStatus.setText("");
            mchargersAct.setText("");
            mlevelType1Act.setText("");
            mlevelType2Act.setText("");*/


            stationName.setText(mapListModel.getStation_name());
//            chargerType.setText(mapListModel.getFuel_type_code());
            network.setText(mapListModel.getEv_network());
            fee.setText(mapListModel.getCards_accepted());
            //connectorType.setText(mapListModel.getEv_connector_types());
            port1.setText(mapListModel.getConn1_porttype());
            port2.setText(mapListModel.getConn2_porttype());
            liveStatus.setText(mapListModel.getStation_live_status());
            String statusMsg = "";
            if (mapListModel.getStation_live_status().contains("Available")) {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
            } else if (mapListModel.getStation_live_status().contains("In-Use")) {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
            }
           /* streetAddress.setText(mapListModel.getStreet_address());
            city.setText(mapListModel.getCity());
            state.setText(mapListModel.getState());
            zipCode.setText(mapListModel.getZip());
            notes.setText(mapListModel.getIntersection_directions());*/

            /*accessDaysTime.setText(mapListModel.getAccess_days_time());
            fuelTypeCode.setText(mapListModel.getFuel_type_code());
            groupsWithAccessCode.setText(mapListModel.getGroups_with_access_code());
            stationName.setText(mapListModel.getStation_name());
            stationPhone.setText(mapListModel.getStation_phone());
            stationPhone.setText(mapListModel.getStation_phone());
            statusCode.setText(mapListModel.getStatus_code());
            city.setText(mapListModel.getCity());
            state.setText(mapListModel.getState());
            streetAddress.setText(mapListModel.getStreet_address());
            zip.setText(mapListModel.getZip());
            bdBlends.setText(mapListModel.getBd_blends());
            e85ConnectoPump.setText(mapListModel.getE85_blender_pump());

            //evConnectorTypes.setText(mapListModel.getConnectorTypesList().size());
            evConnectorTypes.setText(mapListModel.getEv_connector_types());
            evDcFastNum.setText(Integer.toString(mapListModel.getEv_dc_fast_num()));
            evLevel1EvseNum.setText(Integer.toString(mapListModel.getEv_level1_evse_num()));
            evLevel2EvseNum.setText(Integer.toString(mapListModel.getEv_level2_evse_num()));
            evNetwork.setText(mapListModel.getEv_network());
            evNetworkWeb.setText(mapListModel.getEv_network_web());
            evOtherEvse.setText(mapListModel.getEv_other_evse());
            hyStatusLink.setText(mapListModel.getHy_status_link());
            lpgPrimary.setText(mapListModel.getLpg_primary());
            ngFillTypeCode.setText(mapListModel.getNg_fill_type_code());
            ngPsi.setText(mapListModel.getNg_psi());
            ngVehicleClass.setText(mapListModel.getNg_vehicle_class());*/


           /* destinationName = streetAddress.getText().toString();
            destinationLattitude = mapListModel.getLattitude();
            destinationLongitude = mapListModel.getLongitude();*/
            Log.d("mapListModel", "" + mapListModel);

        }
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }


        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Marker info");

        String title = "Station Info";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

//        mActionBar.setHomeButtonEnabled(true);
        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

    }


    private void initTripPlanner() {

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        chargerDetailsLayout = (LinearLayout) findViewById(R.id.chargerDetailsLayout);
        tripPlannerLayout = (LinearLayout) findViewById(R.id.tripPlannerLayout);

        tripPlannerLayoutParams = (LinearLayout.LayoutParams) tripPlannerLayout.getLayoutParams();
        tripPlannerLayout.setLayoutParams(new LinearLayout.LayoutParams(tripPlannerLayoutParams.width, tripPlannerLayoutParams.height, tripPlannerLayoutParams.weight));
        tripPlannerLayout.setVisibility(View.GONE);
        chargerDetailsLayout.setVisibility(View.VISIBLE);

        placeTypeSpinner = (Spinner) findViewById(R.id.placeTypeSpinner1);

        mPlaceType = getResources().getStringArray(R.array.place_type);
        mPlaceTypeName = getResources().getStringArray(R.array.place_type_name);

        adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, mPlaceTypeName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        placeTypeSpinner.setAdapter(adapter);

        backNavigation = (ImageButton) findViewById(R.id.backNavigation1);

        showStartAutoCompleteView = (LinearLayout) findViewById(R.id.showStartAutoCompleteView1);
        showStartAutoCompleteView.setVisibility(View.GONE);

        showStartTextView = (LinearLayout) findViewById(R.id.showStartTextView1);
        showStartTextView.setVisibility(View.VISIBLE);

        showDestinationAutoCompleteView = (LinearLayout) findViewById(R.id.showDestinationAutoCompleteView1);
        showDestinationAutoCompleteView.setVisibility(View.GONE);

        showDestinationTextView = (LinearLayout) findViewById(R.id.showDestinationTextView1);
        showDestinationTextView.setVisibility(View.VISIBLE);

        yourLocation = (TextView) findViewById(R.id.yourLocation1);
        destinationLocation = (TextView) findViewById(R.id.destinationLocation1);

        starting = (AutoCompleteTextView) findViewById(R.id.start1);
        destination = (AutoCompleteTextView) findViewById(R.id.destination1);
        goButton = (ImageView) findViewById(R.id.send1);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        user_id = prefs.getInt(USER_ID, 0);
    }

    public void addFavorite() {

        if (!favFlag) {

            LatLng temp = new LatLng(destinationLattitude, destinationLongitude);
        /*    HashMap<String, List<String>> favDataOutput = new HashMap<String, List<String>>();
            List<String> valStationName = new ArrayList<String>();
            List<String> valStationAddress = new ArrayList<String>();
            List<String> valLatLng = new ArrayList<String>();

            valStationName.add(stationName.getText().toString());
            valStationAddress.add(stationAdress.getText().toString());
            valLatLng.add(temp.toString());

            favData.put("StationName",valStationName);
            favData.put("LatLng",valLatLng);
            favData.put("StationAdress",valStationAddress);
//            saveFavdate(favData);
            favDataOutput = loadFavorite();

            if(favDataOutput != null) {
                for (HashMap.Entry<String, List<String>> entry : favDataOutput.entrySet()) {
                    String key = entry.getKey();
                    List<String> values = entry.getValue();

                   *//* HashSet<String> hashSet = new HashSet<String>();
                    hashSet.addAll(values);
                    values.clear();
                    values.addAll(hashSet);*//*

                    if (key.equals("LatLng")) {
                        values.add(temp.toString());
                    }

                    if (key.equals("StationName")) {
                        values.add(stationName.getText().toString());
                    }

                    if (key.equals("StationAdress")) {
                        values.add(stationAdress.getText().toString());
                    }
                    System.out.println("Key = " + key);
                    System.out.println("Values = " + values + "n");

                    saveFavdate(favDataOutput);
                }
            }else{
                saveFavdate(favData);
            }
            favFlag = true;*/


            int random = (int) (Math.random() * 50 + 1);

            mFavModel.setIdFav(user_id + random);
            mFavModel.setUserId(user_id);
            mFavModel.setStationAddModel(temp.toString());
            mFavModel.setStationNameModel(stationName.getText().toString());
            mFavModel.setStationAddressModel(stationAdress.getText().toString());

            mFavModel.setLiveStatus(liveStatus.getText().toString());
            mFavModel.setConn1portType(port1.getText().toString());
            mFavModel.setConn2PortType(port2.getText().toString());
            mFavModel.setVendingPrice1(vendingprice1.getText().toString());
            mFavModel.setVendingPrice2(vendingprice2.getText().toString());
            mFavModel.setParkingPricePH(parking.getText().toString());
            mFavModel.setAdaStatus(adaParking.getText().toString());


            mFavModel.setStationid(stationId);

            OnePocketLog.d("StationName", stationName.getText().toString());
            OnePocketLog.d("Port1", port1.getText().toString());
            OnePocketLog.d("Price1", vendingprice1.getText().toString());


            /*favoriteHeader = "LiveStatus=" + mFavModel.getLiveStatus() + "&" + "conn1portType=" + mFavModel.getConn1portType() + "&" +
                    "conn2PortType=" + mFavModel.getConn2PortType() + "&" + "vendingPrice1=" + mFavModel.getVendingPrice1() + "&" +
                    "vendingPrice2=" + mFavModel.getVendingPrice2() + "&" + "parkingPricePH=" + mFavModel.getParkingPricePH() + "&" +
                    "adaStatus=" + mFavModel.getAdaStatus() ;

            OnePocketLog.d("FavoriteHeader" ,favoriteHeader);*/

            new FavListSetSingle().execute();


        }
    }

    public void showAlertAlreadyFavBox() {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.Alert);
        //alertDialogBuilder.setTitle(Html.fromHtml("<font color='#e54636'>Alert</font>"));
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder.setMessage(Html.fromHtml("<font color='#e54636'>Station is already added to your Favorites</font>"));
        alertDialogBuilder
                //  .setMessage(msg1)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.stationBusyColor));
            }
        });
        alertDialog.show();
    }

    public void showAlertBox() {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        //alertDialogBuilder.setTitle(R.string.Alert);
        //alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder.setTitle(Html.fromHtml("<font color='#088A08'>Station added to your Favorites.</font>"));
        alertDialogBuilder
                //.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.green));
            }
        });
        alertDialog.show();


    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    private void saveFavdate(HashMap<String, List<String>> inputFavData) {
       /* SharedPreferences pSharedPref = getApplicationContext().getSharedPreferences(FAVORITEDATA, Context.MODE_PRIVATE);
        if (pSharedPref != null){
            JSONObject jsonObject = new JSONObject(inputFavData);
            String jsonString = jsonObject.toString();
            SharedPreferences.Editor editor = pSharedPref.edit();
//            editor.remove("My_map").commit();
//            editor.remove("Fav_data").commit();

            for (String s : inputFavData.keySet()) {
                editor.putString("Fav_data", jsonString);
            }

            editor.commit();
        }*/

        Gson gson = new Gson();
        strSavFav = gson.toJson(inputFavData);
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (pSharedPref != null) {
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.putString(FAVJSONSTRING, strSavFav);
            editor.commit();
        }
    }

    private HashMap<String, List<String>> loadFavorite() {
        HashMap<String, List<String>> outputFavData = new HashMap<String, List<String>>();
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = pSharedPref.edit();
       /* try{
            if (pSharedPref != null){
                String jsonString = pSharedPref.getString("Fav_data", (new JSONObject()).toString());
                JSONObject jsonObject = new JSONObject(jsonString);
                Iterator<String> keysItr = jsonObject.keys();
                while(keysItr.hasNext()) {
                    String key = keysItr.next();
                    String value = (String) jsonObject.get(key);
                    outputFavData.put(key, value);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }*/
        String jsonString = pSharedPref.getString(FAVJSONSTRING, "");
        editor.commit();
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, List<String>>>() {
        }.getType();
        outputFavData = gson.fromJson(jsonString, type);

        return outputFavData;
    }

    public void showTripPlanner() {

        tripPlannerLayout.setVisibility(View.VISIBLE);
//        mActionBar.hide();
        menuTripPlanner.setVisible(false);

        currentLocationFlag = true;
        destinationLocationFlag = true;

        currentLocationLatLng();
        destinationLocationLatLng();

        showStartTextView.setVisibility(View.VISIBLE);
        showStartAutoCompleteView.setVisibility(View.GONE);
        showDestinationTextView.setVisibility(View.VISIBLE);
        showDestinationAutoCompleteView.setVisibility(View.GONE);


        destinationLocation.setText(stationName.getText().toString());
        yourLocation.setText(getResources().getText(R.string.your_location));
//        destination.setText("");
//        starting.setText("");

        routeMapDisplay();

        yourLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLocationFlag = false;
                start = null;
                showStartTextView.setVisibility(View.GONE);
//                starting.setText("");
                showStartAutoCompleteView.setVisibility(View.VISIBLE);
//                showStartAutoCompleteView.requestFocus();
                routeMapDisplay();

            }
        });

        destinationLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destinationLocationFlag = false;
                showDestinationTextView.setVisibility(View.GONE);
                destination.setText("");
                end = null;
                showDestinationAutoCompleteView.setVisibility(View.VISIBLE);
//                showDestinationAutoCompleteView.requestFocus();

                routeMapDisplay();
            }
        });

        backNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tripPlannerLayout.setVisibility(View.GONE);
                mActionBar.show();
            }
        });

        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.Operations.isOnline(FavoritesMoreInfoActivity.this)) {
//                    Toast.makeText(getApplicationContext(), "GO Clicked", Toast.LENGTH_SHORT).show();
                    tripPlannerLayout.setVisibility(View.GONE);
                    mActionBar.show();
                    route();
//                onBackPressed();
                } else {
                    Toast.makeText(FavoritesMoreInfoActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void route() {
        if (start == null || end == null) {
            if (!currentLocationFlag) {
                if (start == null) {
                    mActionBar.hide();
                    tripPlannerLayout.setVisibility(View.VISIBLE);
                    if (starting.getText().length() > 0) {
//                        viewC.setVisibility(View.VISIBLE);
                        starting.setError("Choose location from dropdown.");
                    } else {
//                        starting.setError("Please choose a starting");
                        Toast.makeText(this, "Please choose a starting point", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            if (!destinationLocationFlag) {
                if (end == null) {
                    tripPlannerLayout.setVisibility(View.VISIBLE);
                    mActionBar.hide();
                    if (destination.getText().length() > 0) {
//                    viewC.setVisibility(View.VISIBLE);
                        destination.setError("Choose location from dropdown.");
                    } else {
//                    destination.setError("Please choose a destination");
                        Toast.makeText(this, "Please choose a destination point", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        } else {
//            progressDialog = ProgressDialog.show(this, "Please wait.","Fetching route information.", true);

//            Toast.makeText(this, "Success Data", Toast.LENGTH_SHORT).show();
//            routeLatLngList.clear();

            selectedPosition = placeTypeSpinner.getSelectedItemPosition();
            spinnerValueType = mPlaceType[selectedPosition];

            Log.d("spiner Type vallue", " : " + spinnerValueType);
            Log.d("start vallue", " : " + start);
            Log.d("destination vallue", " : " + end);

//            Intent intent1=new Intent();
            intent1.putExtra("spinnerTypeValueFromChargerInfo", spinnerValueType);

            Bundle args = new Bundle();
            args.putParcelable("startLatLangValueFromChargerInfo", start);
            args.putParcelable("endLatLangValueFromChargerInfo", end);

            intent1.putExtra("bundle", args);

            /*intent.putExtra("startLatLangValueFromChargerInfo",start);
            intent.putExtra("endLatLangValueFromChargerInfo",end);*/

            setResult(7, intent1);
            onBackPressed();
//            finish();


/*
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(start, end)
                    .build();
            routing.execute();*/
        }
    }

    public void routeMapDisplay() {

//        polylines = new ArrayList<>();
        mGoogleApiClient = new GoogleApiClient.Builder(FavoritesMoreInfoActivity.this)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        MapsInitializer.initialize(this);
        mGoogleApiClient.connect();

        mAdapter = new PlaceAutoCompleteAdapter(this, android.R.layout.simple_list_item_1, mGoogleApiClient, AXXERA_HYD, null);

//        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
//        BoundsÂ value:  : LatLngBounds{southwest=lat/lng: (17.39814050498623,78.41548729687929), northeast=lat/lng: (17.430022350489985,78.43902736902237)}

        LatLngBounds bounds = null;
        mAdapter.setBounds(bounds);

        startAndDestinationPoints();
    }

    public void startAndDestinationPoints() {

//        fixed the bug in sprint-7 Ticket_Id-166 starts
        /*if (Util.Operations.isOnline(getContext())) {
            destination.setAdapter(mAdapter);
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }*/

        if (!currentLocationFlag) {

            if (Util.Operations.isOnline(this)) {
                starting.setAdapter(mAdapter);
                starting.requestFocus();
            } else {
                Toast.makeText(this, getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }

//            fixed the bug in sprint-7 Ticket_Id-166 end

            starting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    //keyboard closing window code starts


                    inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                    //keyboard closing window code ends

                    final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                    final String placeId = String.valueOf(item.placeId);
                    Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);


                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                            .getPlaceById(mGoogleApiClient, placeId);
                    placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (!places.getStatus().isSuccess()) {
                                // Request did not complete successfully
                                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                                places.release();
                                return;
                            }
                            // Get the Place object from the buffer.
                            final Place place = places.get(0);

                            start = place.getLatLng();
                        }
                    });

                }
            });

            starting.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int startNum, int before, int count) {
                    if (start != null) {
                        start = null;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        if (!destinationLocationFlag) {

            if (Util.Operations.isOnline(this)) {
                destination.setAdapter(mAdapter);
            } else {
                Toast.makeText(this, getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }

            destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    //keyboard closing window code starts
                    inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                    //keyboard closing window code ends

                    final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                    final String placeId = String.valueOf(item.placeId);
                    Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);


                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                            .getPlaceById(mGoogleApiClient, placeId);
                    placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (!places.getStatus().isSuccess()) {
                                // Request did not complete successfully
                                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                                places.release();
                                return;
                            }
                            // Get the Place object from the buffer.
                            final Place place = places.get(0);

                            end = place.getLatLng();
                        }
                    });

                }
            });


            destination.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    if (end != null) {
                        end = null;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    public void currentLocationLatLng() {
        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation) {
            yourLocationLatitude = gpsTracker.getLatitude();
            yourLocationLongitude = gpsTracker.getLongitude();
            yourLocationLatLng = new LatLng(yourLocationLatitude, yourLocationLongitude);

            if (yourLocationLatLng != null) {
                start = yourLocationLatLng;
            }
        } else {
            gpsTracker.showSettingsAlert();
        }

    }

    public void destinationLocationLatLng() {

        destinationLocationLatLng = new LatLng(destinationLattitude, destinationLongitude);
        if (destinationLocationLatLng != null) {
            end = destinationLocationLatLng;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.barcode, menu);
        menuTripPlanner = menu.findItem(R.id.action_maps_navigation);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.action_barcodeScanner:
                if (mConnectionDetector.isOnline()) {
                    launchActivity(FullScannerActivity.class);
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(this, getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;*/
            case R.id.action_favaorite:

                if (!favFlag) {
                    new AlertDialog.Builder(FavoritesMoreInfoActivity.this)
                            //.setTitle(R.string.Alert)
                            .setMessage(R.string.favoriteAlert)
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    addFavorite();
                                }
                            }).create().show();
                } else {
                    new AlertDialog.Builder(FavoritesMoreInfoActivity.this)
                            .setTitle(R.string.Alert)
                            .setMessage(R.string.favoriteUpdateAlert)
                            .setPositiveButton(android.R.string.ok, null)
                            .create().show();
                }

                return true;

            case R.id.action_maps_navigation:
                if (mConnectionDetector.isOnline()) {
                    showTripPlanner();

//                    Toast.makeText(this,"Navigation Clicked",Toast.LENGTH_SHORT).show();
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(this, getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;

            case android.R.id.home:
                setResult(5, intent1);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void launchActivity(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClss = clss;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZBAR_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(this, clss);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZBAR_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mClss != null) {
                        Intent intent = new Intent(this, mClss);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public class FavListSetSingle extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.SETFAVORITELIST();
                response = mJsonUtil.postMethod(stringurl, null, null, null,null, null, null, null, null, null, null, mFavModel,null);
                OnePocketLog.d("favorites add resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("favorites add  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();

            if (response.contains("already added to your Favourites")) {
                showAlertAlreadyFavBox();
            } else {
                showAlertBox();
            }
        }
    }

}
