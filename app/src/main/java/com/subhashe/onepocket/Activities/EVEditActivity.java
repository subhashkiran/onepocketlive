package com.subhashe.onepocket.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.subhashe.onepocket.Adapters.EVRecyclerViewAdapter;
import com.subhashe.onepocket.Core.EvModel;
import com.subhashe.onepocket.Core.EvModelAcc;
import com.subhashe.onepocket.Fragments.AccountFragment;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class EVEditActivity extends AppCompatActivity {

    HashMap<String, String> moreEVInfoList;
    HashMap<String, String> moreEVInfoYear;
    private EvModel evModel;
    private EvModelAcc evModelAcc, finalResult, finalEditResult;
    private Fragment fragment = null;

    String[] SelectYears = new String[]{ "Select Year", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005"};
    private Spinner spinner;
    ArrayAdapter<String> adapter;
    private String response1, response2, token, sItem, item1;
    private EditText editEnterMake, editEnterModel;
    private Switch dcFastSwitchEV;
    ActionBar mActionBar;
    ImageView imgBack;
    private Button btnSubmit, btnEvYear;
    private boolean successFlag, successAlertFlag, evAddError, firstFlag, firsttime, evFlag, mFastChargeTemp;
    private long userId;
    private JsonUtil mJsonUtil;
    private ConnectionDetector mConnectionDetector;
    private ProgressDialog progress;
    private RecyclerView myEVRecyclerView;
    private EVRecyclerViewAdapter mEVAdapter;
    private String mYearTemp, mMakeTemp, mModelTemp, year, year1;
    int spinnerPosition = 0;
    private Switch dcFastSwitch;
    private String evUpdateHeader;
    JSONObject jsonObject;
    private int user_id, mQid1;
    boolean editEVFlag = false;
    JSONArray jsonArray;
    Boolean temp = false;
    Boolean check = false;
    String selectionPosition;
    int position;
    LinearLayout ll_fast_charge;

    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String FAST_SWITCH = "FAST_SWITCH";
    public static final String USER_ID = "USER_ID";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ev_edit);

        new KeyBoardHide().hideKeyBoard(EVEditActivity.this);

        evModel = new EvModel();
        evModelAcc = new EvModelAcc();

        Intent intent = getIntent();
        moreEVInfoList = (HashMap<String, String>) intent.getSerializableExtra("moreevdata");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(ACC_USER_ID, 0);
        userId = prefs.getLong(USER_ID, 0);

        initToolbar();

        btnEvYear = (Button) findViewById(R.id.btnSpinnerEditEV);
        btnEvYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnEvYear.setVisibility(View.GONE);
                spinner.setVisibility(View.VISIBLE);

            }
        });
        editEnterMake = (EditText) findViewById(R.id.editEVTextenterMake);
        editEnterModel = (EditText) findViewById(R.id.editEVTextEnterModel);
        dcFastSwitchEV = (Switch) findViewById(R.id.evEditDCFastSwitch);

        ll_fast_charge = (LinearLayout) findViewById(R.id.ll_edit_ev_fastcharge);
        ll_fast_charge.setVisibility(View.GONE);

        setMoreEVdata(moreEVInfoList);

        spinner = (Spinner) findViewById(R.id.editSelectYearSpinner);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SelectYears);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        position = adapter.getPosition(moreEVInfoList.get("YEAR"));
        selectionPosition = adapter.getItem(position);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sItem = parent.getItemAtPosition(position).toString();


                temp = true;
                if (temp) {
                    check = true;
                }

              /* // OnePocketLog.d("TempValue" , String.valueOf(position));

                OnePocketLog.d("Spinnervalue" , sItem );

                if (sItem == "Select Year")
                {
                    spinner.requestFocus();
                    showAlertBox(getString(R.string.AddEv_SelectYear));
                }*/

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                check = false;
            }
        });

        imgBack = (ImageView) findViewById(R.id.edit_ev_back_arrow);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new AccountFragment();
            }
        });

        btnSubmit = (Button) findViewById(R.id.btnEditNewEvSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (check) {
                    mYearTemp = spinner.getSelectedItem().toString();
                }else
                {
                    mYearTemp = String.valueOf(selectionPosition);
                }
                OnePocketLog.d("TempValue" , mYearTemp);

                if (mYearTemp == "Select Year") {
                    spinner.requestFocus();
                    showAlertBox(getString(R.string.AddEv_SelectYear));
                } else
                if (editEnterMake.getText().toString().length() == 0) {
                    editEnterMake.requestFocus();
                    showAlertBox(getString(R.string.enterMake));
                } else if (editEnterModel.getText().toString().length() == 0) {
                    editEnterModel.requestFocus();
                    showAlertBox(getString(R.string.enterModel));
                } else if(editEnterMake.getText().toString().length() <= 2 || editEnterMake.getText().toString().length() > 20 ){
                    editEnterMake.requestFocus();
                    showAlertBox(getString(R.string.makeError));
                } else if(editEnterModel.getText().toString().length() <= 2 || editEnterModel.getText().toString().length() > 20){
                    editEnterModel.requestFocus();
                    showAlertBox(getString(R.string.modelError));
                } else {

                    if (check) {
                        mYearTemp = spinner.getSelectedItem().toString();
                        spinnerPosition = Integer.parseInt(mYearTemp);

                    } else {
                        mYearTemp = String.valueOf(selectionPosition);
                        spinnerPosition = Integer.parseInt(mYearTemp);
                    }

                    mMakeTemp = editEnterMake.getText().toString();
                    mModelTemp = editEnterModel.getText().toString();

                    OnePocketLog.d("myear", mYearTemp);
                    OnePocketLog.d("mmake", mMakeTemp);
                    OnePocketLog.d("mmodel", mModelTemp);

                    OnePocketLog.d("spinnerposition", String.valueOf(spinnerPosition));

                    evUpdateHeader = "{" + "\"year\":" + "\"" + mYearTemp + "\"" + "," + "\"model\":" + "\"" + mModelTemp
                            + "\"" + "," + "\"make\":" + "\"" + mMakeTemp + "\"" + "," + "\"vehicleId\":" + "\"" +
                            moreEVInfoList.get("VEHICLEID") + "\"" + "}";

                    OnePocketLog.d("VehicleIdEditUpdate----------", moreEVInfoList.get("VEHICLEID"));

                    OnePocketLog.d("UpdateEditHeader----------", evUpdateHeader);

/*
                    if (dcFastSwitch.isChecked()) {
                        mFastChargeTemp = true;
                    } else {
                        mFastChargeTemp = false;
                    }
*/
                    editEVFlag = false;

                    if (validateEvList_Reg()) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor1 = prefs.edit();
                        evFlag = true;
                       /* if (dcFastSwitch.isChecked()) {
                            dcFastSwitch.setChecked(true);
                            mFastChargeTemp = true;
                            editor1.putBoolean(FAST_SWITCH, true);
                        } else {
                            dcFastSwitch.setChecked(false);
                            mFastChargeTemp = false;
                            editor1.putBoolean(FAST_SWITCH, false);
                        }*/
                        editor1.commit();

                        if (editEVFlag) {
                            eVUpdate();
                            new ElectrivVehicleUpdate().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            editEVFlag = false;
                        } else {
                            eVUpdate();
                            new ElectrivVehicleUpdate().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            editEVFlag = false;
                        }
                    }
                }

            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void setMoreEVdata(HashMap<String, String> moreEVInfoList) {

        editEnterMake.setText(moreEVInfoList.get("MAKE"));
        editEnterModel.setText(moreEVInfoList.get("MODEL"));
        btnEvYear.setText(moreEVInfoList.get("YEAR"));
    }

    private boolean validateEvList_Reg() {
        if ((editEnterMake.getText().toString().length() > 0) &&
                (editEnterModel.getText().toString().length() > 0)) {
            return true;
        } else {
            return false;
        }
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

        String title = "Edit EV";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if (!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        evAddError = false;
                        if (msg.contains("EV updated successfully!")) {

                            new KeyBoardHide().hideKeyBoard(EVEditActivity.this);

                            /*Intent intent1 = new Intent(EVEditActivity.this, EVListActivity.class);
                            startActivity(intent1);
                            finish();*/

                            onBackPressed();
                        }
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("EVEdit Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    public class ElectrivVehicleUpdate extends AsyncTask<String, String, EvModelAcc> {

        EvModelAcc evModelAccUpdate = new EvModelAcc();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgress(mActivity.getString(R.string.pleaseWait));
        }

        @Override
        protected EvModelAcc doInBackground(String... params) {
            try {
                String mUrl = OnePocketUrls.EV_UPDATE();
                response2 = mJsonUtil.postMethod(mUrl, null, null, null, null, null, null, null, evUpdateHeader, null, null, null, null);
                OnePocketLog.d("EVUPDATERESPONSE", response2);
            } catch (Exception e) {
                OnePocketLog.d("EVUPDATERESPONSEERROR----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(EvModelAcc result) {
            super.onPostExecute(evModelAccUpdate);

            hideProgress();
            if (response2 == null || response2 == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response2.contains("Error Code-114") || response2.contains("Vehicle already exists.")) {
                showAlertBox(getString(R.string.vinExists));
            } else if (response2.contains("Error Code-188") || response2.contains("Make is Invalid.")) {
                evAddError = true;
                showAlertBox(getString(R.string.makeError));
            } else if (response2.contains("Error Code-190") || response2.contains("Model is Invalid.")) {
                evAddError = true;
                showAlertBox(getString(R.string.modelError));
            } else {
                try {

                    showAlertBox(getString(R.string.eVUpdateSuccess));

                    evModelAcc.setYear(moreEVInfoList.get("YEAR"));
                    evModelAcc.setMake(moreEVInfoList.get("MAKE"));
                    evModelAcc.setModel(moreEVInfoList.get("MODEL"));

                } catch (Exception ep) {
                }
            }
        }
    }

    private void eVUpdate() {

        EvModelAcc evModelAcc = new EvModelAcc();

        evModelAcc.setYear(mYearTemp);
        evModelAcc.setMake(mMakeTemp);
        evModelAcc.setModel(mModelTemp);

        OnePocketLog.d("maccear", mYearTemp);
        OnePocketLog.d("maccmake", mMakeTemp);
        OnePocketLog.d("maccmodel", mModelTemp);
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

}

