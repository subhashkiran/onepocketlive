package com.subhashe.onepocket.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.subhashe.onepocket.Fragments.Graphs_statsFragment;
import com.subhashe.onepocket.Fragments.TableStatsFragment;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;

/**
 * Created by subhash on 10-04-2016.
 */
/*

public class StatsActivity extends AppCompatActivity {
    android.support.v7.app.ActionBar mActionBar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private StatsPageAdapter adapter;
    private int pageIndex = 0;
    private ConnectionDetector mConnectionDetector;
    private final String[] TITLES = { "Graphs", "Table Stats"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mConnectionDetector = new ConnectionDetector(this);
        viewPager = (ViewPager)findViewById(R.id.viewpager);
//        setupViewPager(viewPager);
        adapter = new StatsPageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                try {
                    viewPager.setCurrentItem(tab.getPosition());


//                    adapter.setPrimaryItem(null, pageIndex, fragment);
//                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.i("Error ", e + "");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void initToolbar() {
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        mActionBar.setTitle("Stats");
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00BCD4")));
    }

    public class StatsPageAdapter extends FragmentPagerAdapter {
        private Fragment fragment;
        FragmentManager fm;

        public StatsPageAdapter(FragmentManager fm) {

            super(fm);
            this.fm = fm;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            if (mConnectionDetector.isOnline()) {

                if (position == 0) {
                    fragment = new Graphs_statsFragment();
                } else if (position == 1) {
                    fragment = new TableStatsFragment();
                }
            } else {
                showAlertBox(getResources().getString(R.string.netNotAvl));
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO Auto-generated method stub
            if (fm.getFragments().contains(object))
                return POSITION_NONE;
            else
                return POSITION_UNCHANGED;
        }
    }

    public void showAlertBox(final String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                StatsActivity.this).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:return super.onOptionsItemSelected(item);
        }
    }
}
*/



import android.content.pm.ActivityInfo;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class StatsActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Graphs_statsFragment(), "Graphs");
        adapter.addFragment(new TableStatsFragment(), "Table Stats");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}



