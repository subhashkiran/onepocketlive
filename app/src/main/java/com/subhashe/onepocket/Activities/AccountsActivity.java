package com.subhashe.onepocket.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import com.subhashe.onepocket.R;

/**
 * Created by SubhashE on 2/5/2016.
 */
public class AccountsActivity extends ActionBarActivity {
    android.support.v7.app.ActionBar mActionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        mActionBar.setTitle(R.string.app_name);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.mipmap.app_icon));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#48100f")));
    }
}
