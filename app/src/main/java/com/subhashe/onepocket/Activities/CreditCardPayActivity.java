package com.subhashe.onepocket.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.*;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.subhashe.onepocket.Core.PayPalHeader;
import com.subhashe.onepocket.Core.PaymentForm;
import com.subhashe.onepocket.Core.TokenList;
import com.subhashe.onepocket.Dialog.ErrorDialogFragment;
import com.subhashe.onepocket.Dialog.ProgressDialogFragment;
import com.subhashe.onepocket.Fragments.PaymentsFragment;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by SubhashE on 3/9/2016.
 */
public class CreditCardPayActivity extends AppCompatActivity {

     //public static final String PUBLISHABLE_KEY = "pk_test_rxcWb0VOFdq4b20BfcRJxg1t";
    public static final String PUBLISHABLE_KEY = "pk_live_VbodzlwxXw7bvpemvk74c6kh";
    private String mToken = null;
    private String mAmount = null;
    private String mCvc = null;
    private String mCardNumber = null;
    private Integer mMonth, mYear;
    private int myNum = 0;
    private String response, cardPayHeader, token , tokenPayment;
    private ConnectionDetector mConnectionDetector;
    android.support.v7.app.ActionBar mActionBar;
    private ProgressDialogFragment progressFragment;
    private PaymentForm form;
    private boolean singleClick, successFlag;
    private Fragment fragment = null;
    private FragmentManager fragmentManager;
    public static final String TOKEN_ID_CREDITCARD = "TOKEN_ID_PREFS_String";

    LinearLayout ll_card, ll_paypal;
    TextView tv_card, tv_paypal;

    private int user_id;

    private static final String TAG = "PayPalActivity";
//    public static final String PUBLISHABLE_KEY = "pk_test_k3uATw8TceLRfJOQbVRHrAnP";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
//    private static final String CONFIG_CLIENT_ID = "credential from developer.paypal.com";
    private static final String CONFIG_CLIENT_ID = "AYL9hFa1n1kHNOVwlmgtGHbv5kiK8_Ht4-8RksbW1JBgLxVyKYA-u--7v91hKmz16AxUNH9Uw3DO86vL";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USERNAME_ID = "USERNAME_ID_PREFS_String";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";
    public static final String USER_REG_ID = "USERID_REG_String";
    public static final String USER_ID = "USERID_PREFS_String";
    private boolean paymentPopup = false;

    private Button paypalButton;
    private EditText paypalEditAmount;
    private String  accessToken, state, total, currency, payId, xToken, user_name;
    private ProgressDialog progress;
    private JsonUtil mJsonUtil;
    private PayPalHeader payPalHeader;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor1;

    JSONArray jObject1;
    JSONObject mTemp;
    LinearLayout ll_paypalpayment, ll_creditcard;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);

        initToolbar();

        new KeyBoardHide().hideKeyBoard(CreditCardPayActivity.this);

//        fragment = new PaymentsFragment();
        progressFragment = ProgressDialogFragment.newInstance(R.string.pleaseWait);
        mConnectionDetector = new ConnectionDetector(this);
        SharedPreferences prefsCreditCard = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefsCreditCard.edit();
        token = prefsCreditCard.getString(TOKEN_ID_CREDITCARD, "NOT FOUND");

        OnePocketLog.d("CREDITCARDTOKEN" , token);

        //OnePocketLog.d("TOKEN" , tokenPayment);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor1 = prefs.edit();
        paymentPopup = prefs.getBoolean(PAYMENT_FLAG_ALERT, false);
        Log.d("PAYMENT_FLAG_VALUE"," value : "+PAYMENT_FLAG_ALERT);

        user_id = prefs.getInt(USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

        /*if(paymentPopup){
            showPaymentAlertBox(getString(R.string.paymentPop1));
        }*/

        paypalEditAmount = (EditText) findViewById(R.id.paypalEnterAmout);
        paypalButton = (Button) findViewById(R.id.payPalButton);

        /*Paypal*/
        payPalHeader = new PayPalHeader();
        SharedPreferences prefsPayPal = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefsPayPal.edit();
        xToken = prefsPayPal.getString(TOKEN_ID, "NOT FOUND");
        user_name = prefsPayPal.getString(USERNAME_ID, "NOT FOUND");

        OnePocketLog.d("PAYPALTOKEN" , xToken);

        paypalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CreditCardPayActivity.this, PayPalService.class);
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                startService(intent);
                int i = 0;
                if (paypalEditAmount.getText().toString().length() != 0) {
                    i = Integer.parseInt(paypalEditAmount.getText().toString());
                }
                if (paypalEditAmount.getText().toString().length() == 0) {
                    showAlertBox(getString(R.string.currencyEdit));
                } else if (i < 20) {
                    showAlertBox(getString(R.string.invalidLessAmount));
                } else {
                    onBuyPressed();
                }
            }
        });


        tv_card = (TextView) findViewById(R.id.tv_payment_card);
        tv_paypal = (TextView) findViewById(R.id.tv_payment_paypal);

        ll_creditcard = (LinearLayout)findViewById(R.id.ll_payment_creditcard);
        ll_paypalpayment = (LinearLayout)findViewById(R.id.paymeny_paypal);
        ll_paypalpayment.setVisibility(View.GONE);

        ll_card = (LinearLayout) findViewById(R.id.ll_payment_card);
        ll_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_card.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_card.setTextColor(getResources().getColor(R.color.white));
                ll_paypal.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_paypal.setTextColor(getResources().getColor(R.color.appColor));

                ll_creditcard.setVisibility(View.VISIBLE);
                ll_paypalpayment.setVisibility(View.GONE);
                /*Intent intent = new Intent(CreditCardPayActivity.this, CreditCardPayActivity.class);
                startActivity(intent);*/

            }
        });

        ll_paypal = (LinearLayout) findViewById(R.id.ll_payment_paypal);
        ll_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_card.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_card.setTextColor(getResources().getColor(R.color.appColor));
                ll_paypal.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_paypal.setTextColor(getResources().getColor(R.color.white));

                ll_creditcard.setVisibility(View.GONE);
                ll_paypalpayment.setVisibility(View.VISIBLE);

                /*Intent intent = new Intent(CreditCardPayActivity.this, PayPalActivity.class);
                startActivity(intent);*/
            }
        });
    }


    public void onBuyPressed() {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        Intent intent = new Intent(CreditCardPayActivity.this, com.paypal.android.sdk.payments.PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        String temp = paypalEditAmount.getText().toString();
        return new PayPalPayment(new BigDecimal(temp), "USD", "PayPal",
                paymentIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        System.out.println("Responseeee" + confirm);
                        Log.i("paymentExample", confirm.toJSONObject().toString());


                        JSONObject jsonObj = new JSONObject(confirm.toJSONObject().toString());

                        String paymentId = jsonObj.getJSONObject("response").getString("id");
                        System.out.println("payment id:-==" + paymentId);
                        payId = paymentId;
                        payPalHeader.setId(payId);
                        payPalHeader.setUsername(user_name);
//                        Toast.makeText(getApplicationContext(), paymentId, Toast.LENGTH_LONG).show();

                        new getAccessToken().execute();
                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment was submitted. Please see the docs.");
            }
        }
    }

    public class getAccessToken extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.postMethod(OnePocketUrls.CARD_PAYPAL_GETTOKEN(), null, null,null, null, null, null, null,
                        null, null, null, null,null);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            hideProgress();
            if (response.contains("access_token")) {
                try {
                    JSONObject jObject = new JSONObject(response);
                    accessToken = jObject.getString("access_token");
                    token = "Bearer " + accessToken;
                    new setAccessToken().execute();
                } catch (Exception e) {

                }
            }
        }
    }


    public class setAccessToken extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(OnePocketUrls.CARD_PAYPAL_SENDTOKEN() + payId, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            hideProgress();
            if (response.contains(payId)) {
                try {
                    JSONObject jObject = new JSONObject(response);
                    state = jObject.getString("state");
                    jObject1 = jObject.getJSONArray("transactions");

                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp1 = jObject1.getJSONObject(i);
                        JSONArray mTemp2 = mTemp1.getJSONArray("related_resources");
                        JSONObject mTemp3 = mTemp2.getJSONObject(i);
                        JSONObject mTemp4 = mTemp3.getJSONObject("sale");
                        JSONObject mTemp5 = mTemp4.getJSONObject("amount");
                        total = mTemp5.getString("total");
                        currency = mTemp5.getString("currency");
                        payPalHeader.setTotal(total);
                        payPalHeader.setCurrency(currency);
                        new sendPaypalToServer().execute();
                    }

                } catch (Exception e) {

                }
            }
        }
    }

    public class sendPaypalToServer extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.postMethod(OnePocketUrls.CARD_PAYPAL_SERVER(), null, null, null,null, null, null, payPalHeader,
                        null, xToken, null, null,null);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response.contains("Success")) {
                successFlag = true;
                showAlertBox(getString(R.string.paymentSuccessful));
            } else if (response.contains("Error Code-118")) {
                showAlertBox("Got error while sending Acknowledgment mail.");
            }
        }
    }


    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showPaymentAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilderPayment = new AlertDialog.Builder(getApplicationContext());
        alertDialogBuilderPayment.setTitle(R.string.Alert);
        alertDialogBuilderPayment.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilderPayment
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                        if(paymentPopup){
                            showAlertBox(getString(R.string.newUserPaymentPopUp));
                            paymentPopup = false;
                            editor1.putBoolean(PAYMENT_FLAG_ALERT, false);

                            editor1.putInt(USER_REG_ID, user_id);

                            editor1.commit();
                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialogPayment = alertDialogBuilderPayment.create();
        alertDialogPayment.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialogPayment.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialogPayment.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
//        alertDialog.setCancelable(false);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                hideProgress();
                if(paymentPopup){
                    showAlertBox(getString(R.string.paymentPop2));
                    paymentPopup = false;
                    editor1.putBoolean(PAYMENT_FLAG, false);

                    editor1.putInt(USER_REG_ID, user_id);

                    editor1.commit();
                }
            }
        });
        alertDialog.show();*/
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if (!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (successFlag) {
                            successFlag = false;
                            finish();
                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                PayPalActivity.this).create();
        if(!successFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialog.setMessage(msg);
//        alertDialog.setCancelable(false);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(successFlag) {
                    successFlag = false;
                    finish();
                }
            }
        });
        alertDialog.show();*/
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Card Payment");

        String title = "Add Money";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);

        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));
    }


    public void saveCreditCard(PaymentForm form) {

        final Card card = new Card(
                form.getCardNumber(),
                form.getExpMonth(),
                form.getExpYear(),
                form.getCvc());
        mAmount = form.getCurrency();
        mCardNumber = form.getCardNumber();
        mCvc = form.getCvc();
        mMonth = form.getExpMonth();
        mYear = form.getExpYear();
//        centsToDollor(mAmount);
//        card.setCurrency(form.getCurrency());

        /*if(!mCardNumber.isEmpty()){
            mCardNumber.
        }*/

        boolean validation = card.validateCard();
        boolean validCVC = mCvc.isEmpty();
        boolean validCardNumber = mCardNumber.isEmpty();
        try {
            myNum = Integer.parseInt(mAmount);
            myNum *= 100;
        } catch (NumberFormatException nfe) {
//                                    System.out.println("Could not parse " + nfe);
        }

        if (validation && !validCVC && !mAmount.isEmpty() && myNum >= 2000 && !validCardNumber && mMonth != 0 && mYear != 0) {
//            startProgress();
            new Stripe().createToken(
                    card,
                    PUBLISHABLE_KEY,
                    new TokenCallback() {
                        public void onSuccess(Token token) {
//                            getTokenList().addToList(token);
//                            finishProgress();
                            mToken = token.getId();
                            if (mConnectionDetector.isOnline()) {
                                cardPayHeader = "token=" + mToken + "&" + "amt=" + myNum;
                                if (mCvc.isEmpty()) {
                                    Toast.makeText(CreditCardPayActivity.this, "Enter CVC", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (!singleClick) {
                                        singleClick = true;
                                        new CarPayment().execute();
                                    }
                                }
                            } else {
                                showAlertBoxCreditCard(getResources().getString(R.string.netNotAvl));
                            }
                        }

                        public void onError(Exception error) {
                            handleError(error.getLocalizedMessage());
//                            handleError(getResources().getString(R.string.cardIncorrect));
//                            finishProgress();
                        }
                    });
        } else if (mAmount.isEmpty()) {
            handleError("Enter Amount");
        } else if (myNum < 2000) {
            handleError("Invalid amount! The amount should be at least $20");
        }else if (validCardNumber) {
            handleError("PLease enter Card Number");
        } else if ((mCardNumber.length() < 17) || (mCardNumber.length() > 19)) {
            handleError("The card number is invalid");
        } else if (mMonth == 0) {
            handleError("Please enter Month");
        } else if (mYear == 0) {
            handleError("Please enter Year");
        } else if (!card.validateExpiryDate()) {
            handleError("The expiration date is invalid");
        } else if (validCVC) {
            handleError("Please enter CVV");
        } else if (!card.validateCVC()) {
            handleError("The CVV code is invalid");
        } else if (!card.validateNumber()) {
            handleError("The card number is invalid");
        } else {
            handleError("The card details are invalid");
        }
    }

    public void centsToDollor(String cents) {
      /*  Long value = Long.valueOf(cents).longValue();
        NumberFormat num = NumberFormat.getCurrencyInstance(Locale.US);
        mAmount = num.format((value / 100.0)*100);*/

    }

    public void showAlertBoxCreditCard(final String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if (!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                        /*fragment = new PaymentsFragment();
                        if(fragment != null) {
                            fragmentManager = getSupportFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.frameLayout, fragment).addToBackStack(null).commit();
                        }*/
                        finish();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                CreditCardPayActivity.this).create();
        if (!successFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialog.setMessage(msg);
        singleClick = false;
        successFlag = false;
//        alertDialog.setCancelable(false);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                hideProgress();
                *//*fragment = new PaymentsFragment();
                if(fragment != null) {
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frameLayout, fragment).addToBackStack(null).commit();
                }*//*
                finish();
            }
        });
        alertDialog.show();*/
    }

    public class CarPayment extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.postMethod(OnePocketUrls.CARD_PAYPAL_PAYMENT() + "?" + cardPayHeader, null, null,null,
                        null, null, null, null, null, token, null, null,null);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
//            fixed the bug in sprint-10 Ticket_Id-107 starts
            if (response.contains("Success")) {
                successFlag = true;
                showAlertBox(getString(R.string.paymentSuccessful));
            } else if (response.contains("Your card was declined.")) {
                showAlertBox(getResources().getString(R.string.cardDeclined));
            } else if (response.contains("IOException during") ||
                    response.contains("API request to Stripe")) {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response.contains("Error Code-118")) {
                showAlertBox(getString(R.string.paymentError));
            } else if (response.contains("java.net.ConnectException: failed to connect to") || response.contains("connect failed") ||
                    response.contains("ENETUNREACH") || response.contains("Network is unreachable")) {
                showAlertBox(getResources().getString(R.string.netNotAvl));
            } else if (response.contains("ETIMEDOUT")) {
                showAlertBox(getResources().getString(R.string.paymentETimedOut));
            }
//            fixed the bug in sprint-10 Ticket_Id-107 end
        }
    }

  /*  private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }
*/
    private void startProgress() {
        progressFragment.show(getSupportFragmentManager(), "Progress");
        progressFragment.setCancelable(false);
    }

    private void finishProgress() {
        progressFragment.dismiss();
    }

    private void handleError(String error) {
        DialogFragment fragment = ErrorDialogFragment.newInstance(R.string.Alert, error);
        fragment.show(getSupportFragmentManager(), "error");
    }

    private TokenList getTokenList() {
//        return (TokenList)(getSupportFragmentManager().findFragmentById(R.id.token_list));
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                //finish();
                /*Intent intent = new Intent(CreditCardPayActivity.this , EditProfile.class);
                startActivity(intent);*/

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
