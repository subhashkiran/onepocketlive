package com.subhashe.onepocket.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.subhashe.onepocket.R;

import java.util.HashMap;

/**
 * Created by axxera on 08-07-2017.
 */

public class DrAlertEventsMoreInfo extends AppCompatActivity {

    ActionBar mActionBar;
    HashMap<String, String > moreInfoList;
    TextView mStartDate,mEndDate,mStartTime,mEndTime,mSignalName , mSignalType , mStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dralerts_moreinfo);
        Intent intent = getIntent();
        moreInfoList = ( HashMap<String, String >) intent.getSerializableExtra("moretotaldata");
        initToolbar();
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Edit Profile");
//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

//        String title = mActionBar.getTitle().toString();
        String title = "DrAlerts Info";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

        mStartDate = (TextView)findViewById(R.id.drAlertMoreInfoStartDate);
        mEndDate = (TextView)findViewById(R.id.drAlertMoreInfoEndDate);
        mStartTime = (TextView)findViewById(R.id.drAlertMoreInfoStartTime);
        mEndTime = (TextView)findViewById(R.id.drAlertMoreInfoEndTime);
        mSignalName = (TextView)findViewById(R.id.drAlertMoreInfoSignalName);
        mSignalType = (TextView)findViewById(R.id.drAlertMoreInfoSignalType);
        mStatus = (TextView)findViewById(R.id.drAlertMoreInfoStatus);

        setMoredata(moreInfoList);
    }

    public void setMoredata(HashMap<String, String > moreInfoList){
        mStartDate.setText(moreInfoList.get("STARTDATE"));
        mEndDate.setText(moreInfoList.get("ENDDATE"));
        mStartTime.setText(moreInfoList.get("STARTTIME"));
        mEndTime.setText(moreInfoList.get("ENDTIME"));
        mSignalName.setText(moreInfoList.get("SIGNALNAME"));
        mSignalType.setText(moreInfoList.get("SIGNALTYPE"));
        mStatus.setText(moreInfoList.get("STATUS"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
