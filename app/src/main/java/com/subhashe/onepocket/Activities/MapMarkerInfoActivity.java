package com.subhashe.onepocket.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.Marker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.subhashe.onepocket.Adapters.ClearableAutoCompleteTextView;
import com.subhashe.onepocket.Adapters.RecyclerViewAdapter;
import com.subhashe.onepocket.Core.FavModel;
import com.subhashe.onepocket.Core.MapListModel;
import com.subhashe.onepocket.Core.MapModel;
import com.subhashe.onepocket.Core.MapModelFilters;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;
import com.subhashe.onepocket.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by SubhashE on 3/4/2016.
 */
public class MapMarkerInfoActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Toolbar toolbar;
    private String chargeId, markerCheckT;
    private boolean tellus, allStaion;
    //    private TextView mstationNameAct, mstationStatus, mchargersAct, mlevelType1Act, mlevelType2Act;
    /*private TextView accessDaysTime, fuelTypeCode, groupsWithAccessCode, stationName, stationPhone, statusCode, city, state, streetAddress, zip, bdBlends, e85ConnectoPump, evConnectorTypes, evDcFastNum, evLevel1EvseNum, evLevel2EvseNum, evNetwork, evNetworkWeb,
            evOtherEvse, hyStatusLink, lpgPrimary, ngFillTypeCode, ngPsi, ngVehicleClass;*/
    private TextView stationName, stationAdress,/*chargerType, */network, fee, connectorType, liveStatus1,liveStatus2, vendingprice1,vendingprice2, city, state, zipCode ,
            port1 , port2 , parking , adaParking , notes , openTime , closeTime;
    LinearLayout ll_port2;
    android.support.v7.app.ActionBar mActionBar;
    HashMap<String, String> hashMap;
    HashMap<String,  List<String>> favData;
    private ProgressDialog progress;
    List<MapListModel> mapListModels;
    List<MapModel> mapListTellus;
    private ConnectionDetector mConnectionDetector;
    private static final int ZBAR_CAMERA_PERMISSION = 1;
    private Class<?> mClss;
    //    fixed the bug in sprint-7 Ticket_Id-147 starts
    List<MapModelFilters> mapListModelsFilter;
    //    fixed the bug in sprint-7 Ticket_Id-147 end
    public static final String CHARGER_ID = "CHARGER_ID";
    private static final String LOG_TAG = "Charger Info";
    public static final String FAVORITEDATA = "FAVORITEDATA";
    public static final String FAVJSONSTRING = "FAVJSONSTRING";
    public static final String USER_ID = "USERID_PREFS_String";
    private String strSavFav = "";
    private String stationId;
    private int user_id,newAddedFavid;
    private JsonUtil mJsonUtil;
    LinearLayout tripPlannerLayout, chargerDetailsLayout;
    LinearLayout.LayoutParams tripPlannerLayoutParams;
    ImageButton backNavigation;
    LinearLayout showStartTextView, showDestinationTextView, showStartAutoCompleteView, showDestinationAutoCompleteView;
    TextView yourLocation, destinationLocation , dollar;
    String destinationName;
    ImageView searchIcon;
    private DrawerLayout drawerLayout;

    AutoCompleteTextView starting;
    AutoCompleteTextView destination;

    LinearLayout layoutStatus;

    Spinner placeTypeSpinner;
    ArrayAdapter<String> adapter;
    String[] mPlaceType = null;
    String[] mPlaceTypeName = null;

    //Button goButton;

    GPSTracker gpsTracker;
    double yourLocationLatitude = 0;
    double yourLocationLongitude = 0;
    LatLng yourLocationLatLng,destinationLocationLatLng;
    protected LatLng start;
    protected LatLng end;
    private LatLng locationInfo;
    private MenuItem menuTripPlanner , favorite , addedFavourite;

    boolean currentLocationFlag = true, destinationLocationFlag = true;
    boolean favFlag;

    protected GoogleApiClient mGoogleApiClient;
    private PlaceAutoCompleteAdapter mAdapter;

    private static final LatLngBounds AXXERA_HYD = new LatLngBounds(new LatLng(17.2168886, 78.1599217),
            new LatLng(17.6078088, 78.6561694));
    InputMethodManager inputMethodManager;

    boolean singleMarker = false;
    Marker marker1, marker2;
    View v;
    private ClearableAutoCompleteTextView searchBox;

    Intent intent1=new Intent();
    Double destinationLattitude,destinationLongitude;
    ProgressDialog progressDialog;
    FavModel mFavModel;
    int selectedPosition;
    String spinnerValueType;
    ImageView goButton;
    FloatingActionButton mFab;
    ImageView img_nav;
    private String response,favHeader,favoriteHeader;
    private String stationidFav,favId;
    ImageView back_img,fav_img,filled_fav_img;
    private List<FavModel> mFavList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_info_layout);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        user_id = prefs.getInt(USER_ID, 0);

        //initToolbar();

        mConnectionDetector = new ConnectionDetector(this);

        setResult(5,intent1);

        mFavModel = new FavModel();

        favData =  new HashMap<String, List<String>>();
        Intent intent = getIntent();
        try {
            tellus = intent.getExtras().getBoolean("tellusmarker");
            if (tellus) {
//                hashMap = (HashMap<String, String>) intent.getSerializableExtra("sendListTellus");
                mapListTellus = (List<MapModel>) intent.getSerializableExtra("sendListTellus");

            }
        } catch (Exception e) {
            Log.d("MapMarkerInfoActivity", "catch");
            e.printStackTrace();
        }

        chargeId = intent.getExtras().getString("chargerMkey");
        markerCheckT = intent.getExtras().getString("markerCheck");

        stationName = (TextView) findViewById(R.id.stationNameAct);
        stationAdress = (TextView) findViewById(R.id.stationAddres);

        port1 = (TextView) findViewById(R.id.station_port1);
        port2 = (TextView)findViewById(R.id.station_port2);

        liveStatus1 = (TextView) findViewById(R.id.stationLiveStatus);
        //liveStatus2 = (TextView) findViewById(R.id.stationLiveStatus2);

        vendingprice1 = (TextView) findViewById(R.id.vprice1);
        vendingprice2 = (TextView) findViewById(R.id.vprice2);

        parking = (TextView) findViewById(R.id.stationparking);
        adaParking = (TextView) findViewById(R.id.stationada);

        dollar = (TextView) findViewById(R.id.dollar);

        layoutStatus  = (LinearLayout)findViewById(R.id.ll_station_status);

        ll_port2 = (LinearLayout) findViewById(R.id.ll_port2);
        notes = (TextView) findViewById(R.id.stationNotes);

        openTime = (TextView) findViewById(R.id.opentime);
        closeTime = (TextView) findViewById(R.id.closetime);

        back_img = (ImageView) findViewById(R.id.map_info_back_arrow);
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        fav_img = (ImageView) findViewById(R.id.map_info_img);

        filled_fav_img = (ImageView) findViewById(R.id.map_info_fav_img);
        initFab();

        if (mapListTellus != null/* && (allStaion != true)*/) {
            if (markerCheckT.equals("Type1")) {
//                displayMarkerData(hashMap, chargeId);
                displayTellusData(mapListTellus);
                tellus = true;
            }
        }

        if (mapListModels != null /*&& (tellus != true)*/) {
            if (markerCheckT.equals("Type2")) {
                displayMarkerListData(mapListModels);
                allStaion = true;
            }
        }
        if (mapListModelsFilter != null /*&& (tellus != true)*/) {
            if (markerCheckT.equals("Type2")) {
//                fixed the bug in sprint-7 Ticket_Id-147 starts
                displayMarkerListDataFilters(mapListModelsFilter);
//                fixed the bug in sprint-7 Ticket_Id-147 end
                allStaion = true;
            }
        }

       /* if (mFavList.size() == 0)
        {
            fav_img.setVisibility(View.VISIBLE);
            fav_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!favFlag) {
                        new AlertDialog.Builder(MapMarkerInfoActivity.this)
                                .setMessage(R.string.favoriteAlert)
                                .setNegativeButton(android.R.string.no, null)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface arg0, int arg1) {
                                        addFavorite();

                                    }
                                }).create().show();

                    }
                }
            });
        }

        new FavListGet().execute();*/
    }

    @Override
    protected void onResume() {

        if (mFavList.size() == 0)
        {
            if(filled_fav_img.getVisibility() != View.VISIBLE){
                fav_img.setVisibility(View.VISIBLE);
                filled_fav_img.setVisibility(View.INVISIBLE);
            }else{
                fav_img.setVisibility(View.INVISIBLE);
                filled_fav_img.setVisibility(View.VISIBLE);
            }
//            fav_img.setVisibility(View.VISIBLE);

            /*filled_fav_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(MapMarkerInfoActivity.this)
                            .setMessage(R.string.favoriteAlertDelete)
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    addFavorite();

                                }
                            }).create().show();

                }
            });*/


            fav_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!favFlag) {
                        new AlertDialog.Builder(MapMarkerInfoActivity.this)
                                .setMessage(R.string.favoriteAlert)
                                .setNegativeButton(android.R.string.no, null)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface arg0, int arg1) {
                                        addFavorite();

                                    }
                                }).create().show();

                    }
                }
            });
        }

        new FavListGet().execute();

        super.onResume();
    }

    private void initFab() {
        img_nav = (ImageView) findViewById(R.id.img_navigation);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        img_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // toggleSearch(true);

                double lat = 0;
                double lang = 0;
                lat = mapListTellus.get(0).getLattitude();
                lang = mapListTellus.get(0).getLongitude();
                String parseString = "google.navigation:q=" + lat + "," + lang;
                Uri gmmIntentUri = Uri.parse(parseString);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");

                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }else{
                    Toast.makeText(MapMarkerInfoActivity.this, "Google maps is disabled,Please enable it.", Toast.LENGTH_SHORT).show();
                }
//                startActivity(mapIntent);
            }
        });
    }

    private void displayTellusData(List<MapModel> result){
        for (int i = 0; i < result.size(); i++) {
            MapModel mapModelListTellus = new MapModel();
            mapModelListTellus = result.get(i);

            stationId = mapModelListTellus.getStationID();

            OnePocketLog.d("StationID" , mapModelListTellus.getStationID());

            stationName.setText(mapModelListTellus.getStationName());
            String tempAdd = "";
            if(mapModelListTellus.getStationAddress().contains("null")){
                tempAdd = "Not Available";
            }else{
                tempAdd = mapModelListTellus.getStationAddress();
            }
            stationAdress.setText(tempAdd);
//            chargerType.setText("NA");
            //connectorType.setText(mapModelListTellus.getConn1portType()+","+mapModelListTellus.getConn2PortType());
            port1.setText(mapModelListTellus.getConn1portLevel());
            port2.setText(mapModelListTellus.getConn2portLevel());
            liveStatus1.setText(mapModelListTellus.getLiveStatus());
            String statusMsg = "";
            if (mapModelListTellus.getLiveStatus().contains("Available"))
            {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
            }
            else if (mapModelListTellus.getLiveStatus().contains("In-Use"))
            {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
            }

            vendingprice1.setText(mapModelListTellus.getVendingPrice1());
            vendingprice2.setText(mapModelListTellus.getVendingPrice2());
            notes.setText(mapModelListTellus.getNotes());

            OnePocketLog.d("NOTES" , mapModelListTellus.getNotes());

            openTime.setText(mapModelListTellus.getOpenTime());

            OnePocketLog.d("OPENTIME" , mapModelListTellus.getOpenTime());

            closeTime.setText(mapModelListTellus.getCloseTime());

            OnePocketLog.d("CLOSETIME" , mapModelListTellus.getCloseTime());

            String tempParking = "";
            if(mapModelListTellus.getParkingPricePH().contains("null"))
            {
                tempParking = "Not Available";
            }else if (mapModelListTellus.getParkingPricePH().contains("0"))
            {
                dollar.setVisibility(View.GONE);
                tempParking = "free";
            }else {
                tempParking = mapModelListTellus.getParkingPricePH();
            }
            parking.setText(tempParking);

            String temAdaParking ="";
            if(mapModelListTellus.getStationADAStatus().contains("null"))
            {
                temAdaParking = "Not Available";

            }else {
                temAdaParking = mapModelListTellus.getStationADAStatus();
            }
            adaParking.setText(temAdaParking);

            String noOfPorts = mapModelListTellus.getPortQuantity();
            int noOfPortsIntegerValue=Integer.parseInt(noOfPorts);

            if(noOfPortsIntegerValue == 1)
            {
                ll_port2.setVisibility(View.GONE);
            }
            else if(noOfPortsIntegerValue == 2)
            {
                ll_port2.setVisibility(View.VISIBLE);
            }


            destinationLattitude = mapModelListTellus.getLattitude();
            destinationLongitude = mapModelListTellus.getLongitude();
            Log.d("mapListModel", "" + mapModelListTellus);

            LatLng temp = new LatLng(destinationLattitude,destinationLongitude);

        }
    }


    //    fixed the bug in sprint-7 Ticket_Id-147 starts
    private void displayMarkerListDataFilters(List<MapModelFilters> result) {
        for (int i = 0; i < result.size(); i++) {
            MapModelFilters mapListModel = new MapModelFilters();
            mapListModel = result.get(i);

            stationName.setText(mapListModel.getStation_name());
//            chargerType.setText(mapListModel.getFuel_type_code());
            network.setText(mapListModel.getEv_network());
            fee.setText(mapListModel.getCards_accepted());
            //connectorType.setText(mapListModel.getEv_connector_types());
            port1.setText(mapListModel.getConn1_porttype());
            port2.setText(mapListModel.getConn2_porttype());
            liveStatus1.setText(mapListModel.getStation_live_status());
            notes.setText(mapListModel.getNotes());
            openTime.setText(mapListModel.getOpenTime());
            closeTime.setText(mapListModel.getCloseTime());
            String statusMsg = "";
            if (mapListModel.getStation_live_status().contains("Available"))
            {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
            }
            else if (mapListModel.getStation_live_status().contains("In-Use"))
            {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
            }

        }
    }
//    fixed the bug in sprint-7 Ticket_Id-147 end

    private void displayMarkerListData(List<MapListModel> result) {

        for (int i = 0; i < result.size(); i++) {
            MapListModel mapListModel = new MapListModel();
            mapListModel = result.get(i);

            stationName.setText(mapListModel.getStation_name());
//            chargerType.setText(mapListModel.getFuel_type_code());
            network.setText(mapListModel.getEv_network());
            fee.setText(mapListModel.getCards_accepted());
            //connectorType.setText(mapListModel.getEv_connector_types());
            port1.setText(mapListModel.getConn1_porttype());
            port2.setText(mapListModel.getConn2_porttype());
            liveStatus1.setText(mapListModel.getStation_live_status());
            notes.setText(mapListModel.getNotes());
            openTime.setText(mapListModel.getOpenTime());
            closeTime.setText(mapListModel.getCloseTime());
            String statusMsg = "";
            if (mapListModel.getStation_live_status().contains("Available"))
            {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
            }
            else if (mapListModel.getStation_live_status().contains("In-Use"))
            {
                layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
            }
        }
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);

        String title = "Station Info";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

    }

    public void addFavorite(){
        if(!favFlag) {

            LatLng temp = new LatLng(destinationLattitude,destinationLongitude);

            int random = (int )(Math.random() * 50 + 1);

            newAddedFavid = user_id + random;
            mFavModel.setIdFav(user_id + random);
            mFavModel.setUserId(user_id);
            mFavModel.setStationAddModel(temp.toString());
            mFavModel.setStationNameModel(stationName.getText().toString());
            mFavModel.setStationAddressModel(stationAdress.getText().toString());

           /* mFavModel.setLiveStatus(liveStatus1.getText().toString());
            mFavModel.setConn1portType(port1.getText().toString());
            mFavModel.setConn2PortType(port2.getText().toString());
            mFavModel.setVendingPrice1(vendingprice1.getText().toString());
            mFavModel.setVendingPrice2(vendingprice2.getText().toString());
            mFavModel.setParkingPricePH(parking.getText().toString());
            mFavModel.setAdaStatus(adaParking.getText().toString());*/


            mFavModel.setStationid(stationId);

            String favID ;
            favID = mFavModel.getStationid();

            OnePocketLog.d("Favorite Id" , favID);

            OnePocketLog.d("StationName" ,stationName.getText().toString());
            OnePocketLog.d("Port1" ,port1.getText().toString());
            OnePocketLog.d("Price1" ,vendingprice1.getText().toString());


            /*favoriteHeader = "LiveStatus1=" + mFavModel.getLiveStatus() + "&" + "conn1portType=" + mFavModel.getConn1portType() + "&" +
                    "conn2PortType=" + mFavModel.getConn2PortType() + "&" + "vendingPrice1=" + mFavModel.getVendingPrice1() + "&" +
                    "vendingPrice2=" + mFavModel.getVendingPrice2() + "&" + "parkingPricePH=" + mFavModel.getParkingPricePH() + "&" +
                    "adaStatus=" + mFavModel.getAdaStatus() ;

            OnePocketLog.d("FavoriteHeader" ,favoriteHeader);*/

            new FavListSetSingle().execute();

            fav_img.setVisibility(View.GONE);
            filled_fav_img.setVisibility(View.VISIBLE);


        }
    }

    public class FavListSetSingle extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.SETFAVORITELIST();
                response = mJsonUtil.postMethod(stringurl,null,null,null,null,null,null,null,null,null,null, mFavModel ,null);
                OnePocketLog.d("favorites add resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("favorites add  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();

            if (response.contains("already added to your Favourites")) {
                showAlertAlreadyFavBox(getString(R.string.favstationExists));
            } else {
                showAlertBox();
            }

        }
    }

    public class FavListGet extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.GETALLFAVORITELIST() + user_id;
                response = mJsonUtil.getMethod(stringurl,null,null);
                OnePocketLog.d("favoritelsitmore fragment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("favoritelsitmore fragment  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            try {
                if(response != null || !response.contains("[]") || response != "") {

                    JSONArray temp = new JSONArray(response);


                        for (int i = 0; i < temp.length(); i++) {

                            JSONObject e = temp.getJSONObject(i);
                            FavModel mFavModelGet = new FavModel();

                           // String stationid = e.getString("StationId");

                            try {
                                stationidFav  = e.getString("StationId");
                            } catch (JSONException ie) {
                                // TODO Auto-generated catch block
                                ie.printStackTrace();
                            }

                            OnePocketLog.d("FavIDStationMore" , stationidFav);

                            if(stationidFav.equals(stationId))
                            {
                                favId = e.getString("FavId");
                                filled_fav_img.setVisibility(View.VISIBLE);
                                //filled_fav_img.setOnClickListener(null);
                                filled_fav_img.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        new AlertDialog.Builder(MapMarkerInfoActivity.this)
                                                .setTitle(R.string.Alert)
                                                .setMessage(R.string.favoriteAlertRemove)
                                                .setNegativeButton(android.R.string.cancel, null)
                                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                                    public void onClick(DialogInterface arg0, int arg1) {
                                                        favHeader = "idFav=" + favId + "&" + "userId=" + user_id;

                                                        OnePocketLog.d("favheader", favHeader);
                                                        new deleteNFavList().execute();
                                                    }
                                                }).create().show();

//                                        showAlertAlreadyFavBox(getString(R.string.favstationExists));

                                    }
                                });

                            }
                            else
                            {
                                fav_img.setVisibility(View.VISIBLE);
                                fav_img.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(!favFlag) {
                                            new AlertDialog.Builder(MapMarkerInfoActivity.this)
                                                    .setMessage(R.string.favoriteAlert)
                                                    .setNegativeButton(android.R.string.no, null)
                                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                                        public void onClick(DialogInterface arg0, int arg1) {
                                                            addFavorite();

                                                        }
                                                    }).create().show();
                                        }
                                    }
                                });

                                filled_fav_img.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        new AlertDialog.Builder(MapMarkerInfoActivity.this)
                                                .setTitle(R.string.Alert)
                                                .setMessage(R.string.favoriteAlertRemove)
                                                .setNegativeButton(android.R.string.cancel, null)
                                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                                    public void onClick(DialogInterface arg0, int arg1) {
                                                        favHeader = "idFav=" + newAddedFavid + "&" + "userId=" + user_id;

                                                        OnePocketLog.d("favheader", favHeader);
                                                        new deleteNFavList().execute();
                                                    }
                                                }).create().show();

//                                        showAlertAlreadyFavBox(getString(R.string.favstationExists));

                                    }
                                });
                            }



                    }
                }
            }catch (Exception e){

            }
        }
    }

    public class deleteNFavList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.DELETEFAVORITELIST();
                response = mJsonUtil.postMethod(stringurl,null,null,null,null,null,null,null,favHeader,null,null,null,null);
                OnePocketLog.d("favoritelsit delete resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("favoritelsit delete  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            fav_img.setVisibility(View.VISIBLE);
            filled_fav_img.setVisibility(View.INVISIBLE);
            hideProgress();
            showAlertBoxFav();
        }
    }



    public void showAlertAlreadyFavBox(final String msg1) {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.Alert);
        //alertDialogBuilder.setTitle(Html.fromHtml("<font color='#e54636'>Alert</font>"));
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        //alertDialogBuilder.setMessage(Html.fromHtml("<font color='#e54636'>Station is already added to your Favorites</font>"));
        alertDialogBuilder
                .setMessage(msg1)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                //alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.stationBusyColor));
                alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));

            }
        });
        alertDialog.show();
    }

    public void showAlertBox() {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        //alertDialogBuilder.setTitle(R.string.Alert);
        //alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder.setTitle(Html.fromHtml("<font color='#088A08'>Station added to your Favorites.</font>"));
        alertDialogBuilder
                //.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.green));
            }
        });
        alertDialog.show();


    }

    public void showAlertBoxFav() {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        //alertDialogBuilder.setTitle(R.string.Alert);
        //alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder.setTitle(Html.fromHtml("<font color='#088A08'>Station removed from your Favorites.</font>"));
        alertDialogBuilder
                //.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.green));
            }
        });
        alertDialog.show();


    }


    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    private void saveFavdate(HashMap<String, List<String>> inputFavData){
       /* SharedPreferences pSharedPref = getApplicationContext().getSharedPreferences(FAVORITEDATA, Context.MODE_PRIVATE);
        if (pSharedPref != null){
            JSONObject jsonObject = new JSONObject(inputFavData);
            String jsonString = jsonObject.toString();
            SharedPreferences.Editor editor = pSharedPref.edit();
//            editor.remove("My_map").commit();
//            editor.remove("Fav_data").commit();

            for (String s : inputFavData.keySet()) {
                editor.putString("Fav_data", jsonString);
            }

            editor.commit();
        }*/

        Gson gson = new Gson();
        strSavFav = gson.toJson(inputFavData);
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (pSharedPref != null){
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.putString(FAVJSONSTRING, strSavFav);
            editor.commit();
        }
    }


    private HashMap<String, List<String>> loadFavorite(){
        HashMap<String, List<String>> outputFavData = new HashMap<String, List<String>>();
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = pSharedPref.edit();
       /* try{
            if (pSharedPref != null){
                String jsonString = pSharedPref.getString("Fav_data", (new JSONObject()).toString());
                JSONObject jsonObject = new JSONObject(jsonString);
                Iterator<String> keysItr = jsonObject.keys();
                while(keysItr.hasNext()) {
                    String key = keysItr.next();
                    String value = (String) jsonObject.get(key);
                    outputFavData.put(key, value);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }*/
        String jsonString = pSharedPref.getString(FAVJSONSTRING,"");
        editor.commit();
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, List<String>>>(){}.getType();
        outputFavData = gson.fromJson(jsonString, type);

        return outputFavData;
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.barcode, menu);
        menuTripPlanner = menu.findItem(R.id.action_maps_navigation);

        favorite = menu.findItem(R.id.action_favaorite);

        addedFavourite = menu.findItem(R.id.action_addedfavaorite);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            *//*case R.id.action_barcodeScanner:
                if (mConnectionDetector.isOnline()) {
                    launchActivity(FullScannerActivity.class);
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(this, getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;*//*

            case R.id.action_favaorite:

                if(!favFlag) {
                    new AlertDialog.Builder(MapMarkerInfoActivity.this)
                            //.setTitle(R.string.Alert)
                            .setMessage(R.string.favoriteAlert)
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    addFavorite();
                                    *//*favorite.setVisible(false);
                                    addedFavourite.setVisible(true);*//*

                                }
                            }).create().show();
                }else{
                    new AlertDialog.Builder(MapMarkerInfoActivity.this)
                            .setTitle(R.string.Alert)
                            .setMessage(R.string.favoriteUpdateAlert)
                            .setPositiveButton(android.R.string.ok, null)
                            .create().show();
                }

                return true;

            case android.R.id.home:
                setResult(5,intent1);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
*/
    public void launchActivity(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClss = clss;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZBAR_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(this, clss);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZBAR_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mClss != null) {
                        Intent intent = new Intent(this, mClss);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
