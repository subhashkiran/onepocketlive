package com.subhashe.onepocket.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.subhashe.onepocket.Core.EvModel;
import com.subhashe.onepocket.Core.UserRegisterInfo;
import com.subhashe.onepocket.Core.UserAddres;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.AsteriskPasswordTransformationMethod;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by SubhashE on 1/21/2016.
 */
public class RegisterNow extends ActionBarActivity {

    private ConnectionDetector mConnectionDetector;
    private UserRegisterInfo mUserRegInfo;
    private JsonUtil mJsonUtil;
    private String response;
    private UserAddres mUseradd;
    EditText firstName, lastName, userName, /*gender,*/ confirmPassword, email, phone, password, address1, address2, city, zipcode, country, state;
    private EditText editEnterMake,editEnterModel;
    String pwd1,pwd2,sItem,token;
    private Button evButton,btnSubmit,close;
    PopupWindow popup;
    Point p1;
    Spinner gender;
    private Spinner spinner;
    private int user_id_reg;
    ArrayAdapter<String> adapter;
    private boolean successFlag,evFlag;
    String[] SelectYears = new String[] {"Select Year","2010","2011","2012","2013","2014","2015","2016"};
    String remberme;
    String googleemailid;
    String googlepersonName;
    String fbname;
    String fbemail;
    String emailid;
    String personName, lastActivity;
    private ProgressDialog progress;
    android.support.v7.app.ActionBar mActionBar;
    public static final String PREFS_NAME = "USERID_PREFS";
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String USER_REG_ID = "USERID_REG_String";
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String EMAIL_ID = "EMAIL_ID_PREFS_String";
    public static final String FAST_SWITCH = "FAST_SWITCH";
    SharedPreferences settings;
    char[] acceptedChars = new char[]{',',' ','.'};
    char[] acceptedCharsNoSpace = new char[]{'-','_','.'};
    char[] blockedCharSet = new char[]{'π'};
    //    ArrayList<PaymentTypes> paymentTypes=new ArrayList<PaymentTypes>();
//    ArrayList<String> paymentTypesText=new ArrayList<String>();
    int keyDel = 0;
    int onCreate = 0;
    private EvModel evListEdit;
    private Switch dcFastSwitch;
    private String mYearTemp,mMakeTemp,mModelTemp , tempEmail;
    int spinnerPosition = 0;
    private boolean mFastChargeTemp;
    TextView tvName , tvEmail , tvPassword , tvCPassword , tvMobile , tvAddress , tvCity , tvState , tvZipCode ;
    private int registerUser;
    public static final String REGISTER_USER = "REGISTER_USER_String";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
//        String[] items = new String[] {"Select Gender","Male","Female"};
        mConnectionDetector = new ConnectionDetector(this);
        mUserRegInfo = new UserRegisterInfo();
        mUseradd = new UserAddres();
        evListEdit = new EvModel();
        mJsonUtil = new JsonUtil();
        Intent imt = getIntent();
        lastActivity = imt.getStringExtra("lastActivity");
        onCreate = 1;

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);

        String title = "Sign Up";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);

        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

        int titleId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        } else {
            titleId = R.id.action_bar_title;
        }

        TextView abTitle = (TextView) findViewById(titleId);
        if (abTitle != null) {
            abTitle.setTextColor(getResources().getColor(R.color.white));
        }


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //SharedPreferences.Editor editor1 = prefs.edit();


        user_id_reg = prefs.getInt(ACC_USER_ID, 0);
        //token = prefs.getString(TOKEN_ID, "NOT FOUND");

//        new BackGroundTask_Types().execute();

        firstName = (EditText) findViewById(R.id.firstName);
        firstName.setFilters(new InputFilter[]{filtertxtCharSpace});

        lastName = (EditText) findViewById(R.id.lastName);
        lastName.setFilters(new InputFilter[]{filtertxtCharSpace});
        //evButton = (Button)findViewById(R.id.addEvReg);
      /*  userName = (EditText) findViewById(R.id.userName);
        userName.setFilters(new InputFilter[]{filtertxtCharDig});*/
//        gender = (Spinner) findViewById(R.id.genderspiner);
        password = (EditText) findViewById(R.id.regispassword);

        confirmPassword = (EditText) findViewById(R.id.confirmPassword);

        email = (EditText) findViewById(R.id.enterMailid);

        phone = (EditText) findViewById(R.id.enterPhoneNum);

        address1 = (EditText) findViewById(R.id.addressLine1);
        //address1.setFilters(new InputFilter[]{filtertxtCharSpace});

//        address2 = (EditText) findViewById(R.id.addressLine2);

        city = (EditText) findViewById(R.id.enterCity);
        city.setFilters(new InputFilter[]{filtertxtCharSpace});

        state = (EditText) findViewById(R.id.enterState);
        state.setFilters(new InputFilter[]{filtertxtCharSpace});

      /*  country = (EditText) findViewById(R.id.country);
        country.setFilters(new InputFilter[]{filtertxtChar});*/

        zipcode = (EditText) findViewById(R.id.enterZipcode);
        //zipcode.setFilters(new InputFilter[]{filter});
        //filter the data

        password.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        confirmPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                pwd1 = password.getText().toString();
                pwd2 = confirmPassword.getText().toString();
                if (!(pwd1.equals(pwd2))) {
                    confirmPassword.setError(getString(R.string.pwdmismatch));
                }
            }
        });

        Button submitbtn = (Button) findViewById(R.id.btnSubmit);
        LinearLayout cancel = (LinearLayout) findViewById(R.id.ll_register_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent agn = new Intent(RegisterNow.this, LoginActivity.class);
                startActivity(agn);
                finish();
            }
        });
        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (firstName.getText().toString().length() == 0) {
                    firstName.requestFocus();
                    showAlertBox(getString(R.string.enterFirstName));
                } /*else if (lastName.getText().toString().length() == 0) {
                    lastName.requestFocus();
                    showAlertBox(getString(R.string.enterLastName));
                }*/ /* else if (userName.getText().toString().length() == 0) {
                    userName.requestFocus();
                    showAlertBox(getString(R.string.enterusername));
                } */else if (email.getText().toString().length() == 0) {
                    email.requestFocus();
                    showAlertBox(getString(R.string.entermailidconfirm));
                }else if (password.getText().toString().length() == 0) {
                    password.requestFocus();
                    showAlertBox(getString(R.string.enterPassword));
                } else if (password.getText().toString().length() < 6) {
                    password.requestFocus();
                    password.setError(getString(R.string.pwdshort));
//                    DialogUtil("Password is too short");
                } else if (confirmPassword.getText().toString().length() == 0) {
                    confirmPassword.requestFocus();
                    showAlertBox(getString(R.string.enterConfirmPassword));
                } /*else if (gender.getSelectedItem().toString().contains("Select Gender")) {
                    gender.requestFocus();
                    showAlertBox(getString(R.string.entergender));
                }*/ /*else if (email.getText().toString().length() == 0) {
                    email.requestFocus();
                    showAlertBox(getString(R.string.entermailidconfirm));
                }*//*else if (!evFlag) {
                    showAlertBox(getString(R.string.addNewEvBtn));
                } */else if (!isValidEmail(email.getText().toString())) {
                    email.requestFocus();
                    showAlertBox(getString(R.string.entervalidmailid));
                } else if (phone.getText().toString().length() == 0) {
                    phone.requestFocus();
                    showAlertBox(getString(R.string.entermobilenum));
                } else if ((phone.getText().toString().length() < 10) ||
                        (phone.getText().toString().length() > 10)) {
                    phone.requestFocus();
                    phone.setError(getString(R.string.mobileNumberCount));
//                    DialogUtil("Password is too short");
                } else if (phone.getText().toString().contains("0000000000")){
                    phone.setError(getString(R.string.invalidmobile));
                } else if (address1.getText().toString().length() == 0) {
                    address1.requestFocus();
                    showAlertBox(getString(R.string.enterAdd1));

                }/* else if (address2.getText().toString().length() == 0) {
                    address2.requestFocus();
                    showAlertBox(getString(R.string.enterAdd2));

                }*/ else if (city.getText().toString().length() == 0) {
                    city.requestFocus();
                    showAlertBox(getString(R.string.entercity));

                } else if (state.getText().toString().length() == 0) {
                    state.requestFocus();
                    showAlertBox(getString(R.string.enterstate));

                } /*else if (country.getText().toString().length() == 0) {
                    country.requestFocus();
                    showAlertBox(getString(R.string.entercountry));

                } */else if (zipcode.getText().toString().length() == 0) {
                    zipcode.requestFocus();
                    showAlertBox(getString(R.string.enterzip));

                }else if((zipcode.getText().toString().length() < 5)||(zipcode.getText().toString().length() > 7)) {
                    zipcode.requestFocus();
                    zipcode.setError(getString(R.string.enterzipAlert));
                }
               /* else if(paymentSpinner.getSelectedItem().toString().equalsIgnoreCase("Choose Payment")){
                    paymentSpinner.requestFocus();
                    DialogUtil("Select Payment Type");
                }*/
              //  else
                {
                    if (mConnectionDetector.isOnline()) {
                        if(validateUser() && passwordMatch()) {
                          /*  if(validateEvList_Reg()) {
                                *//*new ElectrivVehicleUpdate().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);*//*
//                                popup.dismiss();
                            }*/

                            OnePocketLog.d("HI" , "HHI");

                            new BackGroundTask1().execute();

                            OnePocketLog.d("HIIII" , "HHIII");

                           /* Intent values = new Intent ( RegisterNow.this, LoginActivity.class );
                            values.putExtra("EMAIL_ID",email.getText().toString());
                            startActivity(values);

                            OnePocketLog.d("emailid" , email.getText().toString());*/

                        }
                    } else {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
//                    evFlag = false;
                }
            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            private boolean mFormatting; // this is a flag which prevents the  stack overflow.
            private int mAfter;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
                mAfter = after; // flag to detect backspace..
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (!mFormatting) {
                    mFormatting = true;
                    // using US formatting...
                    if (mAfter != 0) // in case back space ain't clicked...
                        PhoneNumberUtils.formatNumber(s, PhoneNumberUtils.getFormatTypeForLocale(Locale.US));
                    mFormatting = false;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                new KeyBoardHide().hideKeyBoard(RegisterNow.this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /* private boolean validateEvList_Reg(){
         if((editEnterMake.getText().toString().length() > 0) &&
                 (editEnterModel.getText().toString().length() > 0)){
             return true;
         }
         else{
             return false;
         }
     }
 */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if((event.getAction()==KeyEvent.ACTION_DOWN)&&(keyCode == KeyEvent.KEYCODE_SPACE)){
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean passwordMatch(){
        pwd1 = password.getText().toString();
        pwd2 = confirmPassword.getText().toString();
        if(!(pwd1.equals(pwd2)))
        {
            showAlertBox(getString(R.string.pwdmismatch));
            return false;
        }else{
            return true;
        }
    }

    private boolean validateUser(){
        if((firstName.getText().toString().length() > 0)&&
                /*(userName.getText().toString().length() > 0) && */(password.getText().toString().length() > 0 )&&
                (confirmPassword.getText().toString().length() > 0) && (email.getText().toString().length() > 0 )&&
                (phone.getText().toString().length() > 0) && (email.getText().toString().length() > 0 )&&
                (address1.getText().toString().length() > 0) && /*(address2.getText().toString().length() > 0 )&&*/
                (city.getText().toString().length() > 0) && (state.getText().toString().length() > 0 )&&
                /*(country.getText().toString().length() > 0) &&*/ (zipcode.getText().toString().length() > 0 )){
            return true;
        }else{
            return false;
        }
    }

    @Override
    protected void onPause() {
        hideProgress();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        hideProgress();
        evFlag = false;
        super.onDestroy();
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    private void initUserRegisterInfo() {

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

        mUserRegInfo.setFirstName(firstName.getText().toString());
        mUserRegInfo.setLastName(lastName.getText().toString());
        mUserRegInfo.setUsername(email.getText().toString());
        mUserRegInfo.setPassword(password.getText().toString());
        mUserRegInfo.setRolename("Driver");
        mUserRegInfo.setConfirmPassword(confirmPassword.getText().toString());
        mUserRegInfo.setGender("");
        //mUserRegInfo.setEmail(email.getText().toString());

        tempEmail = email.getText().toString().trim();
        mUserRegInfo.setEmail(tempEmail);

        String temp = phone.getText().toString().trim();
        long mlong =0;
        if (!temp.equals("")) {
            mlong = Long.parseLong(temp);
        }
        mUseradd.setPhone(mlong);
        mUseradd.setAddressLine1(address1.getText().toString());
        mUseradd.setAddressLine2("");
        mUseradd.setCity(city.getText().toString());
        mUseradd.setState(state.getText().toString());
        mUseradd.setCountry("UK");
        String temp1 = zipcode.getText().toString();
        int mlong1 = Integer.parseInt(temp1);
        mUseradd.setZipCode(mlong1);

//        mUserRegInfo.setVehicle(evListEdit);
        mUserRegInfo.setMuserAddres(mUseradd);

        mUserRegInfo.setDeviceName(Build.MANUFACTURER);
        mUserRegInfo.setDeviceType("Android");
        mUserRegInfo.setAppVersion(version);
        mUserRegInfo.setDeviceVersion(Build.VERSION.RELEASE);

       /* String deviceID = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);*/
       /* try {
            InstanceID instanceID = InstanceID.getInstance(this);

           token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            OnePocketLog.d("DEVICETOKEN123" , token);

        }catch (Exception e) {

        }

        mUserRegInfo.setDeviceToken(token);

        OnePocketLog.d("DEVICETOKEN" , token);
*/
    }

    public class BackGroundTask1 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.registerProgress));
        }

        @Override
        protected Void doInBackground(Void... arg0) {
//            UserFunctions uf=new UserFunctions();

            initUserRegisterInfo();
            try {
                String stringurl = OnePocketUrls.REGISTER_URL();
                response = mJsonUtil.postMethod(stringurl,mUserRegInfo,null,null,null,null,null,null,null,null,null,null,null);
                OnePocketLog.d("Register resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("Register error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if(response.contains("Error Code-101")){
                showAlertBox(getString(R.string.userAlreadyExists));
            } else if(response.contains("Error Code-113")){
                showAlertBox(getString(R.string.emailAlreadyRegis));
            }else if(response.contains("Error Code-120")){
                showAlertBox(getString(R.string.zipInvalid));
            }else if(response.contains("Error Code-179")){
                showAlertBox(getString(R.string.cityInvalid));
            }else if(response.contains("Error Code-181")){
                showAlertBox(getString(R.string.stateInvalid));
            }else if(response.contains("Error Code-162")){
                showAlertBox(getString(R.string.firstNameInvalid));
            } else if(response.contains("Error Code-284")){
                showAlertBox(getString(R.string.accountprofileedit));
            }/*if(response.contains("Error Code-188") || response.contains("Make is Invalid.")){
                evFlag = false;
                showAlertBox(getString(R.string.makeError));
            }else if(response.contains("Error Code-190") || response.contains("Model is Invalid.")){
                evFlag = false;
                showAlertBox(getString(R.string.modelError));
            } */else {
                try {
                    JSONObject jObject = new JSONObject(response);
                    int id;

                    id = jObject.getInt("id");

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putInt(USER_ID, id);
                    editor1.putInt(USER_REG_ID, id);
                    editor1.putString(EMAIL_ID , tempEmail);

                    OnePocketLog.d("EMAILID" , tempEmail);
//                    editor1.putString("UserName", resonce);
//                    editor1.putString("RememberMe", "false");
                    editor1.commit();

//                            Intent agn = new Intent(RegisterNow.this,MapsFragment.class);
//                            agn.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(agn);
                    successFlag = true;
                    new KeyBoardHide().hideKeyBoard(RegisterNow.this);
                    showAlertBoxEmail(getString(R.string.emailVerficationLink));

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }
    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if(!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        if (msg.contains("We have sent the verification link to your email, Please use the link to get verified.")) {
                            finish();
                            new KeyBoardHide().hideKeyBoard(RegisterNow.this);
                            Intent intent1 = new Intent(RegisterNow.this,LoginActivity.class);
                            startActivity(intent1);

                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                RegisterNow.this).create();
        if(!successFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(msg.contains("Registration Successful.")){
                    finish();
                    Intent intent1 = new Intent(RegisterNow.this,LoginActivity.class);
                    startActivity(intent1);
                }
            }
        });
        alertDialog.show();*/
    }

    public void showAlertBoxEmail(final String msg1) {

        AlertDialog.Builder alertDialogBuilderEmail = new AlertDialog.Builder(this);
        if(!successFlag) {
            alertDialogBuilderEmail.setTitle(R.string.registersucess);
            alertDialogBuilderEmail.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialogBuilderEmail
                .setTitle("Registration Successful")
                .setMessage(msg1)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        if (msg1.contains(getString(R.string.emailVerficationLink))) {
                            finish();
                            new KeyBoardHide().hideKeyBoard(RegisterNow.this);
                            Intent intent2 = new Intent(RegisterNow.this,LoginActivity.class);
                            startActivity(intent2);

                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog1 = alertDialogBuilderEmail.create();
        alertDialog1.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog1.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog1.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                RegisterNow.this).create();
        if(!successFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(msg.contains("Registration Successful.")){
                    finish();
                    Intent intent1 = new Intent(RegisterNow.this,LoginActivity.class);
                    startActivity(intent1);
                }
            }
        });
        alertDialog.show();*/
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        int userId= Integer.parseInt(prefs.getString("UserId", "0"));
//        if(lastActivity!=null && onCreate==0 && userId!=0){
//            hideProgress();
//            finish();
//        }
    }

    InputFilter filtertxtCharSpace = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i)) && !(Character.isSpace(source.charAt(i))) &&
                        !new String(acceptedChars).contains(String.valueOf(source.charAt(i)))) {
                    return "";
                }
            }
            return null;
        }
    };

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}

