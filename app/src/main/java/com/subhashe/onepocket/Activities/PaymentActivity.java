package com.subhashe.onepocket.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.subhashe.onepocket.R;

/**
 * Created by axxera on 28-12-2016.
 */

public class PaymentActivity extends ActionBarActivity {

    android.support.v7.app.ActionBar mActionBar;
    LinearLayout ll_card,ll_paypal;
    TextView tv_card,tv_paypal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        //Action bar
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);

        String title = "Forgot Password";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);

        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#48100f")));

        int titleId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        } else {
            titleId = R.id.action_bar_title;
        }

        TextView abTitle = (TextView) findViewById(titleId);
        if (abTitle != null) {
            abTitle.setTextColor(getResources().getColor(R.color.white));
        }

        Intent intent = new Intent(PaymentActivity.this,CreditCardPayActivity.class);
        startActivity(intent);

        tv_card = (TextView) findViewById(R.id.tv_payment_card);
        tv_paypal=(TextView)findViewById(R.id.tv_payment_paypal);

        ll_card = (LinearLayout)findViewById(R.id.ll_payment_card);
        ll_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_card.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_card.setTextColor(getResources().getColor(R.color.white));
                ll_paypal.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_paypal.setTextColor(getResources().getColor(R.color.appColor));

                Intent intent = new Intent(PaymentActivity.this,CreditCardPayActivity.class);
                startActivity(intent);

            }
        });

        ll_paypal = (LinearLayout) findViewById(R.id.ll_payment_paypal);
        ll_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_card.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                tv_card.setTextColor(getResources().getColor(R.color.appColor));
                ll_paypal.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                tv_paypal.setTextColor(getResources().getColor(R.color.white));

                Intent intent = new Intent(PaymentActivity.this, PayPalActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
