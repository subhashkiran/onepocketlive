package com.subhashe.onepocket.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.subhashe.onepocket.Core.Constants;
import com.subhashe.onepocket.Core.LatLangAll;
import com.subhashe.onepocket.Core.MapModelAll;
import com.subhashe.onepocket.DataBase.DbHelper;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.NetworkChecker;
import com.subhashe.onepocket.Utils.OnePocketLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SubhashE on 1/6/2016.
 */
public class SplashActivityTestAsnc extends Activity{
    private final int welcomeScreenDisplay = 2000;
    private ProgressDialog progDailog;
    private URL url;
    private InputStream stream;
    StringBuffer buffer;
    String line = "";
    JsonFactory factory;
    JsonToken token;
    JsonParser jsonParser;
    String fieldName;
    JSONArray jsonArray;
    int jsonArrayLength;
    int jsonLength;
    JSONObject jsonObject;
    String finalJson="";
    private ArrayList<MapModelAll> mapArrayList;
    private DbHelper dbHelper;
    private ArrayList dbList;
   // private ConnectionDetector mConnectionDetector;
    private BroadcastReceiver ReceivefrmSERVICE;
    private boolean networkCheckerFlag,firstConnect,intentFlag;
    private AlertDialog alertDialog;
    public static final String JSON_LENGTH = "JSON_LENGTH";
    public static final String LOGIN_USER = "LOGIN_USER_String";
    private IntentFilter filter;
    private SharedPreferences.Editor editor1;

    ImageView spalshScreenImage;
    String loginUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        spalshScreenImage= (ImageView)findViewById(R.id.bar);


        int width = this.getResources().getDisplayMetrics().widthPixels;
        int height = this.getResources().getDisplayMetrics().heightPixels;
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
        spalshScreenImage.setLayoutParams(parms);

//        WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT


        //mConnectionDetector = new ConnectionDetector(this);
        mapArrayList = new ArrayList<MapModelAll>();
        dbHelper  = new DbHelper(this);
        firstConnect = true;

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        startThread();
        filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");

        loginUser = "1";

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor1 = prefs.edit();
        editor1.putString(LOGIN_USER, loginUser);
        editor1.commit();

        OnePocketLog.d("LOGINUSER" , loginUser);
    }

//    fixed the bug in sprint-7 Ticket_Id-125 starts
    @Override
    protected void onStop() {
        super.onStop();
        if(progDailog != null && progDailog.isShowing()){
            progDailog.dismiss();
            progDailog = null;
        }
    }
//    fixed the bug in sprint-7 Ticket_Id-125 end

    private class BackgroundSplashTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         /*   progDailog = new ProgressDialog(SplashActivityTestAsnc.this);
            progDailog.setMessage("Loading... Please Wait");
//            progDailog.getWindow().setGravity(Gravity.BOTTOM);
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(welcomeScreenDisplay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            progDailog.dismiss();
//            if(intentFlag) {
            if(loginUser.equals("1")) {
                Intent intent1 = new Intent(SplashActivityTestAsnc.this, LoginActivity.class);
                startActivity(intent1);
//                intentFlag = false;
                overridePendingTransition(R.anim.fedin, R.anim.fedout);
                finish();
//            }
            }
        }
    }

    private void startThread(){
        Thread welcomeThread = new Thread(){
            int wait = 0;

            @Override
            public void run() {
                try{
                    super.run();
                    /**
                     * use while to get the splash time. Use sleep() to increase
                     * the wait variable for every 100L.
                     */
                    while(wait < welcomeScreenDisplay){
                        sleep(100);
                        wait += 50;
                    }
                }catch (Exception e){
                    System.out.println("EXc=" + e);
                }finally {
                    setIntent();
                }
            }
        };
        welcomeThread.start();
    }

    private void setIntent(){
        ReceivefrmSERVICE = new BroadcastReceiver(){
            @Override
            public void onReceive(final Context context, final Intent intent) {

                    if (firstConnect) {
                        networkCheckerFlag = NetworkChecker.getConnectivityStatusCheck(context);
                        dbList = dbHelper.getAllStationRecords();
                        firstConnect = false;


                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor1 = prefs.edit();
                        jsonLength = prefs.getInt(JSON_LENGTH, 0);

                        new BackgroundSplashTask().execute();
                        /*if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                           *//* if (alertDialog != null && alertDialog.isShowing()) {
                                alertDialog.cancel();
                            }
                            if (dbList.size() == 0 && mapArrayList.size() == 0) {
                                editor1.putBoolean(PAYMENT_FLAG,true);
                                editor1.commit();
                                mapArrayList.clear();
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    new GetJsonString().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                                    new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                                   *//**//* taskCanceler = new TaskCanceler (new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR));
                                    handler.postDelayed(taskCanceler, 240*1000);*//**//*
                                } else {
                                    new GetJsonString().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                                }
                            } else if (dbList.size() != 0 && (dbList.size() != jsonLength)) {
                                dbHelper.deleteTableData();
                                if (mapArrayList.size() == 0) {
                                    // jsonString(Constants.JSON_URL);
                                    new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                                } else {
                                    mapArrayList.clear();
                                    new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL;
                                }
                            }  else if (dbList.size() == 0 && (mapArrayList.size()!= jsonLength)) {
                                dbHelper.deleteTableData();
                                if (mapArrayList.size() == 0) {
                                    // jsonString(Constants.JSON_URL);
                                    new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                                } else {
                                    mapArrayList.clear();
                                    new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL;
                                }
                            } else {*//*
                                new BackgroundSplashTask().execute();
                        *//*Intent intent1 = new Intent(SplashActivityTestAsnc.this,LoginActivity.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.fedin, R.anim.fedout);
                        finish();*//*
//                            }
                        } else {
                            showAlertBox(getResources().getString(R.string.netNotAvl));
//                    Toast.makeText(getApplicationContext(), R.string.netNotAvl, Toast.LENGTH_LONG).show();
//                    finish();
                        }*/
                    }
               /* else {
                    firstConnect = true;
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }*/
            } //onReceive
        };
        registerReceiver(ReceivefrmSERVICE, filter);
    }

    public class GetJsonString extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... url1) {

            //public String jsonString(String url1){

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                    url = new URL(Constants.JSON_URL);
                    connection = (HttpURLConnection) url.openConnection();
                    //connection.connect();
                    connection.connect();
                    stream = connection.getInputStream();
                    reader = new BufferedReader(new InputStreamReader(stream));
                    buffer = new StringBuffer();

                while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                        finalJson = buffer.toString();
                }

                try {
                        jsonObject = new JSONObject(finalJson);
                        jsonArray = jsonObject.getJSONArray(Constants.JSON_ARRAY_NAME);
                        jsonArrayLength = jsonArray.length();
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor1 = prefs.edit();
                        editor1.putInt(JSON_LENGTH, jsonArrayLength);
                        editor1.commit();
                        Log.d("Json Array Length", " : " + jsonArrayLength);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return finalJson;

        }
    }

/*    public class TaskCanceler implements Runnable{
        private AsyncTask task;

        public TaskCanceler(AsyncTask task) {
            this.task = task;
        }

        @Override
        public void run() {
            if (task.getStatus() == AsyncTask.Status.RUNNING )
                task.cancel(true);

            if(progDailog.isShowing()){
                progDailog.dismiss();
                showAlertBox(getResources().getString(R.string.netNotAvlGovApi));
            }
        }
    }*/

    public class GetJsonTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(SplashActivityTestAsnc.this);
            progDailog.setMessage("Loading... Please Wait");
//            progDailog.getWindow().setGravity(Gravity.BOTTOM);
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected String doInBackground(String... params) {


            try {

                    factory = new MappingJsonFactory();

                Log.d("Json factory: ", "" + factory);
                    if(factory!=null) {
                        jsonParser = factory.createParser(finalJson);
                    Log.d("Json Parser: ", "" + jsonParser);
                }else{
                    return "network";
                }



                    if(jsonParser!=null) {
                        token = jsonParser.nextToken();
                    }else{
                        return "network";

                }

                Log.d("Json token: ", "" + token);
                //* ArrayList objectArray = new ArrayList();*//*

                if (token != JsonToken.START_OBJECT) {
                        System.out.println("Error: root should be object: quiting.");
                }

                while (jsonParser.nextToken() != JsonToken.END_OBJECT) {

                        // move from field name to field value
                        if(jsonParser!=null) {
                            fieldName = jsonParser.getCurrentName();
                            Log.d("fieldName: ", "" + fieldName);
                            token = jsonParser.nextToken();
                        }else{
                            return "network";
                        }
                        Log.d("token: ", "" + token);

                        if(fieldName!=null) {
                            if (fieldName.equals(Constants.JSON_ARRAY_NAME)) {
                                if(token!=null) {
                                    if (token == JsonToken.START_ARRAY) {

                                            // For each of the records in the array
                                            Long startTime = System.currentTimeMillis();
                                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {

                                                    // read the record into a tree model,
                                                    // this moves the parsing position to the end of it
                                                    JsonNode node = jsonParser.readValueAsTree();
                                                    Log.d("node: ", "" + node);

                                                    MapModelAll mapModel = new MapModelAll();

                                                    mapModel.setAccess_days_time(node.get(Constants.ACCESS_DAYS_TIME).textValue());
                                                    mapModel.setCards_accepted(node.get(Constants.CARDS_ACCEPTED).textValue());
                                                    mapModel.setDate_last_confirmed(node.get(Constants.DATE_LAST_CONFIRMED).textValue());
                                                    mapModel.setExpected_date(node.get(Constants.EXPECTED_DATE).textValue());
                                                    mapModel.setFuel_type_code(node.get(Constants.FUEL_TYPE_CODE).textValue());
                                                    mapModel.setId(node.get(Constants.ID).intValue());
                                                    mapModel.setGroups_with_access_code(node.get(Constants.GROUPS_WITH_ACCESS_CODE).textValue());

                                                    mapModel.setOpen_date(node.get(Constants.OPEN_DATE).textValue());
                                                    mapModel.setOwner_type_code(node.get(Constants.OWNER_TYPE_CODE).textValue());
                                                    mapModel.setStatus_code(node.get(Constants.STATUS_CODE).textValue());
                                                    mapModel.setStation_name(node.get(Constants.STATION_NAME).textValue());
                                                    mapModel.setStation_phone(node.get(Constants.STATION_PHONE).textValue());
                                                    mapModel.setUpdated_at(node.get(Constants.UPDATED_AT).textValue());
                                                    mapModel.setGeocode_status(node.get(Constants.GEOCODE_STATUS).textValue());

                                                    mapModel.setLattitude(node.get(Constants.LATITUDE).doubleValue());
                                                    mapModel.setLongitude(node.get(Constants.LONGITUDE).doubleValue());

                                                    /*LatLangAll latLang = new LatLangAll();
                                                    latLang.setLatitude(node.get(Constants.LATITUDE).doubleValue());
                                                    latLang.setLongitude(node.get(Constants.LONGITUDE).doubleValue());
                                                    mapModel.setLatLang(latLang);*/

                                                    mapModel.setCity(node.get(Constants.CITY).textValue());
                                                    mapModel.setIntersection_directions(node.get(Constants.INTERSECTION_DIRECTION).textValue());
                                                    mapModel.setPlus4(node.get(Constants.PLUS4).textValue());
                                                    mapModel.setState(node.get(Constants.STATE).textValue());
                                                    mapModel.setStreet_address(node.get(Constants.STREET_ADDRESS).textValue());
                                                    mapModel.setZip(node.get(Constants.ZIP).textValue());
                                                    mapModel.setBd_blends(node.get(Constants.BD_BLENDS).textValue());
                                                    mapModel.setE85_blender_pump(node.get(Constants.E85_BLENDER_PUMP).textValue());


                                                    List connectorList = new ArrayList();

                                                    for (int count = 0; count < node.get(Constants.EV_CONNECTOR_TYPES).size(); count++) {
                                                        connectorList.add(node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                                    }

                                                    mapModel.setConnectorTypesList(connectorList);

                                                    StringBuffer sb = new StringBuffer();
                                                    for (int count = 0; count < mapModel.getConnectorTypesList().size(); count++) {
                                                        sb.append(mapModel.getConnectorTypesList().get(count) + " \t");
                                                    }


                                                    mapModel.setEv_connector_types(sb.toString());
                                                    mapModel.setEv_dc_fast_num(node.get(Constants.EV_DC_FAST_NUM).intValue());
                                                    mapModel.setEv_level1_evse_num(node.get(Constants.EV_LEVEL1_EVSE_NUM).intValue());
                                                    mapModel.setEv_level2_evse_num(node.get(Constants.EV_LEVEL2_EVSE_NUM).intValue());
                                                    mapModel.setEv_network(node.get(Constants.EV_NETWORK).textValue());
                                                    mapModel.setEv_network_web(node.get(Constants.EV_NETWORK_WEB).textValue());
                                                    mapModel.setEv_other_evse(node.get(Constants.EV_OTHER_EVSE).textValue());
                                                    mapModel.setHy_status_link(node.get(Constants.HY_STATUS_LINK).textValue());
                                                    mapModel.setLpg_primary(node.get(Constants.LPG_PRIMARY).textValue());
                                                    mapModel.setNg_fill_type_code(node.get(Constants.NG_FILL_TYPE_CODE).textValue());
                                                    mapModel.setNg_psi(node.get(Constants.NG_PSI).textValue());
                                                    mapModel.setNg_vehicle_class(node.get(Constants.NG_VEHICLE_CLASS).textValue());


                                                    mapArrayList.add(mapModel);

                                            }

                                            dbHelper.bulkInsert(mapArrayList);

                                            Long endTime = System.currentTimeMillis();
                                            Log.d("Total Time", ":" + (endTime - startTime));
                                    } else {
                                        System.out.println("Error: records should be an array: skipping.");
                                        jsonParser.skipChildren();
                                    }
                                }else{
                                    return "network";
                                }
                            } else {
                                System.out.println("Unprocessed property: " + fieldName);
                                jsonParser.skipChildren();
                            }
                        }else{
                            return "network";
                        }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            intentFlag = true;
            return finalJson;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            fixed the bug in sprint-7 Ticket_Id-125 starts
            if(progDailog != null && progDailog.isShowing()){
                progDailog.dismiss();
                progDailog = null;
            }
//            fixed the bug in sprint-7 Ticket_Id-125 end

          /*  if(result.equals("network")||result.equalsIgnoreCase("null")||result.equals("")){*/
            if(result.equalsIgnoreCase("null")||result.equals("")){
                Toast.makeText(getApplicationContext(), R.string.netNotAvl, Toast.LENGTH_SHORT).show();
//                showAlertBox(getResources().getString(R.string.netNotAvl));
            }
            if(intentFlag) {
                Intent intent1 = new Intent(SplashActivityTestAsnc.this, LoginActivity.class);
                startActivity(intent1);
                intentFlag = false;
                overridePendingTransition(R.anim.fedin, R.anim.fedout);
                finish();
            }
        }
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent intent2 = new Intent(SplashActivityTestAsnc.this, LoginActivity.class);
                        startActivity(intent2);
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

        /*alertDialog = new AlertDialog.Builder(
                SplashActivityTestAsnc.this).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                *//*if(msg.contains(getString(R.string.netNotAvlGovApi))){
                    Intent intent1 = new Intent(SplashActivityTestAsnc.this, LoginActivity.class);
                    startActivity(intent1);
                    intentFlag = false;
                    overridePendingTransition(R.anim.fedin, R.anim.fedout);
                }*//*
                finish();
            }
        });
        alertDialog.show();*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(ReceivefrmSERVICE != null) {
            unregisterReceiver(ReceivefrmSERVICE);
        }
    }
}
