package com.subhashe.onepocket.Activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.KeyBoardHide;

/**
 * Created by SubhashE on 1/6/2016.
 */
public class DRAlertsSettingsActivity extends AppCompatActivity{

    ActionBar mActionBar;
    private LinearLayout ll_DrAlertsActive,ll_DrAlertsSchedule,ll_DrAlertsSettings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dralertsactivity);
        initToolbar();



    }
    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Edit Profile");
//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
//        mActionBar.setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

//        String title = mActionBar.getTitle().toString();
        String title = "Settings ";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();

                new KeyBoardHide().hideKeyBoard(DRAlertsSettingsActivity.this);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
