package com.subhashe.onepocket.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.subhashe.onepocket.Core.EvModel;
import com.subhashe.onepocket.Core.UserAddres;
import com.subhashe.onepocket.Core.UserRegisterInfo;
import com.subhashe.onepocket.Core.UserSecurity;
import com.subhashe.onepocket.Core.UserSecurityPayment;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import java.util.ArrayList;

/**
 * Created by SubhashE on 1/27/2016.
 */
@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class login_payment_security extends ActionBarActivity {
    String response,secHeader,item1,item2,item3,token;
    int mQid1,mQid2,user_id;
    Spinner mPaySpinner;
    Spinner mSecSpinner1;
    Spinner mSecSpinner2;
    EditText mAnswer1;
    EditText mAnswer2;
    Button msecurityButton;
    private JsonUtil mJsonUtil;
    android.support.v7.app.ActionBar mActionBar;
    ArrayList<UserSecurity> arraylist = new ArrayList<UserSecurity>();
    ArrayList<String> mQuestions = new ArrayList<String>();
    private ConnectionDetector mConnectionDetector;
    UserSecurityPayment mUserSecurityPayment;
    private LinearLayout paymentLayout;
    private boolean secondAnswer;
    private UserRegisterInfo mUserRegInfo;
    private UserAddres mUseradd;
    private EvModel evListEdit;
    String personName, lastActivity;
    int onCreate = 0;

    //    public static final String PREFS_NAME = "USERID_PREFS";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String SECQUESTION1_ID = "SECQUESTION1_ID";
    public static final String SECQUESTION2_ID = "SECQUESTION2_ID";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginsecuritypayment);
        mConnectionDetector = new ConnectionDetector(this);
        mUserSecurityPayment = new UserSecurityPayment();
        mUserRegInfo = new UserRegisterInfo();
        mUseradd = new UserAddres();
        evListEdit = new EvModel();
        mJsonUtil = new JsonUtil();
        Intent imt = getIntent();
        lastActivity = imt.getStringExtra("lastActivity");
        onCreate = 1;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            arraylist = bundle.getParcelableArrayList("data");
//            mConnectionDetector = new ConnectionDetector(this);
        }

//        final String[] items = new String[] {"Select your option","Credit Card","PayPal"};


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }


        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);

        String title = "Security Questions";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);

        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#48100f")));
        int titleId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        } else {
            titleId = R.id.action_bar_title;
        }
        TextView abTitle = (TextView) findViewById(titleId);
        if (abTitle != null) {
            abTitle.setTextColor(getResources().getColor(R.color.white));
        }

       /* paymentLayout = (LinearLayout)findViewById(R.id.paymentLayout);
        paymentLayout.setVisibility(LinearLayout.GONE);
        mPaySpinner =(Spinner)findViewById(R.id.paymentspiner);*/
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.credit_spinner_items));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       /* mPaySpinner.setAdapter(adapter);
        mPaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item1 = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        mSecSpinner1 = (Spinner)findViewById(R.id.questionspiner1);
        ArrayAdapter<UserSecurity> adapter1 = new ArrayAdapter<UserSecurity>(this,android.R.layout.simple_spinner_item, arraylist);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSecSpinner1.setAdapter(adapter1);
        mSecSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item2 = parent.getItemAtPosition(position).toString();
                mQid1 = getQuestionID(item2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSecSpinner2 = (Spinner)findViewById(R.id.questionspiner2);
        ArrayAdapter<UserSecurity> adapter2 = new ArrayAdapter<UserSecurity>(this,android.R.layout.simple_spinner_item, arraylist);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSecSpinner2.setAdapter(adapter2);
        mSecSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item3 = parent.getItemAtPosition(position).toString();
                mQid2 = getQuestionID(item3);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor1 = prefs.edit();
        editor1.putInt(SECQUESTION1_ID, mQid1);
        editor1.putInt(SECQUESTION2_ID, mQid2);
        editor1.commit();

        mAnswer1 = (EditText)findViewById(R.id.answer1);
        mAnswer2 = (EditText)findViewById(R.id.answer2);
        msecurityButton = (Button)findViewById(R.id.securityButton);
        msecurityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(login_payment_security.this);

               /* if(mPaySpinner.getSelectedItem().toString().contains("Select your option")){
                    showAlertBox(getString(R.string.selectpaymenttype));
                }else*/ if (mAnswer1.getText().toString().length() == 0) {
//                    flag = 0;
                    mAnswer1.requestFocus();
                    showAlertBox(getString(R.string.answer1required));
                } else if (mAnswer2.getText().toString().length() == 0) {
//                    flag = 0;
                    mAnswer2.requestFocus();
                    showAlertBox(getString(R.string.answer2required));
                } else if (item2.equals(item3)){
                    showAlertBox(getString(R.string.questionreselected));
                }else{
                    getUserSecPayInfo();
                    if( mConnectionDetector.isOnline()){

                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        {
                            new LoginDriverAddInfo().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            new LoginServicesUserID().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                        }else {
                            new LoginDriverAddInfo().execute();
                        }
                    }else{
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public class LoginServicesUserID extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            try{
                String stringurl = OnePocketUrls.SERVICES_USER_ID_URL()+user_id;
                if(stringurl.contains("User id") && stringurl.contains("not found")){
                    showAlertBox(getString(R.string.UserIDNotFound));
                }else{
                    if(!secondAnswer) {
                        response = mJsonUtil.getMethod(stringurl, null, token);
                    }
                    secondAnswer = false;
                }

                OnePocketLog.d("singup services user id resp----------", response);
            }catch (Exception e) {
                OnePocketLog.d("singup services user id error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (response == null || response == "error" || response == "") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if(response.contains("Error Code-103")){
                showAlertBox(getString(R.string.incorrectusername));
            }else if(response.contains("Bad credentials")) {
                showAlertBox(getString(R.string.incorrectusername));
            }else if(response.contains("\"status\":\"ACTIVE\"")) {

                try{
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putBoolean(PAYMENT_FLAG_ALERT,true);
                    editor1.commit();
                    login_payment_security.this.finish();
                    new KeyBoardHide().hideKeyBoard(login_payment_security.this);
                    Intent intent = new Intent(login_payment_security.this, AddNewEVActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
//                    Intent mIntent = new Intent(login_payment_security.this, login_payment_security.class);
//                    Toast.makeText(login_payment_security.this, "Get Profile User", Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    OnePocketLog.d("posts resp----------","exception");
                }
            }
        }
    }

    public class LoginDriverAddInfo extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try{
                String stringurl = OnePocketUrls.DRIVERINFO_URL();
                response = mJsonUtil.postMethod(stringurl,null,mUserSecurityPayment,null,null,null,null,null,null,token,null,null,null);
                OnePocketLog.d("singup security resp----------", response);
            }catch (Exception e) {
                OnePocketLog.d("singup security error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(response.contains("Error Code-191")){
                secondAnswer = true;
                showAlertBox(getString(R.string.secanswer1invalid));
            }else if(response.contains("Error Code-192")){
                secondAnswer = true;
                showAlertBox(getString(R.string.secanswer2invalid));
            }/*else if (*//*response == null ||*//* response == "error") {
                secondAnswer = true;
                showAlertBox(getString(R.string.servernotresponce));
            }*/ else if(response.contains("Error Code-103")){
                secondAnswer = true;
                showAlertBox(getString(R.string.incorrectusername));
            }else if(response.contains("Bad credentials")) {
                secondAnswer = true;
                showAlertBox(getString(R.string.incorrectusername));
            }else if(response.contains("User id") && response.contains("not found")) {
                secondAnswer = true;
                showAlertBox(getString(R.string.UserIDNotFound));
            } else{
                try{
                    Intent intent = new Intent(login_payment_security.this, AddNewEVActivity.class);
                    startActivity(intent);
                }catch (Exception e){
                    OnePocketLog.d("posts resp----------","exception");
                }
            }
        }
    }

    private int getQuestionID(String item){
        int id;
        for(int i =0 ;i < arraylist.size(); i++){
            if(arraylist.get(i).getQuestion().equals(item)){
                return arraylist.get(i).getId();
            }
        }
        return 0;
    }

    private void getUserSecPayInfo(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(USER_ID, 0);
        token = prefs.getString(TOKEN_ID,"NOT FOUND");

        mUserSecurityPayment.setAnswer1(mAnswer1.getText().toString());
        mUserSecurityPayment.setAnswer2(mAnswer2.getText().toString());
        mUserSecurityPayment.setPaymentType("PayPal");
        mUserSecurityPayment.setUserId(user_id);
        mUserSecurityPayment.setSecurityQuesId1(mQid1);
        mUserSecurityPayment.setSecurityQuesId2(mQid2);
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

        /*AlertDialog alertDialog = new AlertDialog.Builder(
                login_payment_security.this).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();*/
    }
}
