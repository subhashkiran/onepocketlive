package com.subhashe.onepocket.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.subhashe.onepocket.Core.UserForgotPassword;
import com.subhashe.onepocket.Core.UserSecurity;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.AsteriskPasswordTransformationMethod;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SubhashE on 2/4/2016.
 */
public class ForgotPassword extends ActionBarActivity {
    android.support.v7.app.ActionBar mActionBar;
    private Button mResetButton, mSubmit, mCancel;
    String response, emailStr, emailStr1, item2, item3, pwd1, pwd2, stringHeader, sectemp;
    private boolean msecQues, msecQuesRstPwd, mPwdEmailSuccess, mSingleClick, successFlag, emailIdFlag;
    private JsonUtil mJsonUtil;
    private ConnectionDetector mConnectionDetector;
    UserForgotPassword mUserForgotPassword;
    private LinearLayout securityQuestionLayout, emailLayout, resetPwdLayout, submitCancelLayout , securityQuestions;
    private EditText mEmailText;
    private EditText mAnswer1;
    private EditText mAnswer2;
    //    private EditText mUsername;
    private EditText mNewPwd, mConNewPwd;
    private TextView mSecQuestion, mEmail;
    private Spinner mSecSpinner1, mSecSpinner2;

    private Button clickEnterEmailButton;
    Point p1;
    PopupWindow popup;
    EditText editTextEnterEmail;
    Button btnEnterEmailSubmit, btnEnterEmailCancel;
    String userEmailId;
    private boolean emailCheckFlag = false;

    LinearLayout ll_security_question , ll_email;

    private ProgressDialog progress;
    int mSQid1, mSQid2, mQid1, mQid2, user_id;
    ArrayList<UserSecurity> arraylist = new ArrayList<UserSecurity>();
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String SECQUESTION1_ID = "SECQUESTION1_ID";
    public static final String SECQUESTION2_ID = "SECQUESTION2_ID";
    public static final String EMAIL_ID_APPEND = "EMAIL_ID_APPEND_PREFS_String";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgetpassword);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        //Action bar
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);

        String title = "Forgot Password";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);

        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#48100f")));

        int titleId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        } else {
            titleId = R.id.action_bar_title;
        }

        TextView abTitle = (TextView) findViewById(titleId);
        if (abTitle != null) {
            abTitle.setTextColor(getResources().getColor(R.color.white));
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            arraylist = bundle.getParcelableArrayList("data");
//            mConnectionDetector = new ConnectionDetector(this);
        }
        mUserForgotPassword = new UserForgotPassword();
        mConnectionDetector = new ConnectionDetector(this);
        securityQuestionLayout = (LinearLayout) findViewById(R.id.securityQuestionLayout);
        emailLayout = (LinearLayout) findViewById(R.id.emailSecurityLayout);
        resetPwdLayout = (LinearLayout) findViewById(R.id.resetPasswordLayout);
        submitCancelLayout = (LinearLayout) findViewById(R.id.submitCancelLayout);

        securityQuestions = (LinearLayout) findViewById(R.id.ll_security_questions);
        securityQuestions.setVisibility(View.GONE);

        mEmailText = (EditText) findViewById(R.id.enterRegisterEmailEditText);
        securityQuestionLayout.setVisibility(LinearLayout.GONE);
        emailLayout.setVisibility(LinearLayout.GONE);
        resetPwdLayout.setVisibility(LinearLayout.GONE);
        submitCancelLayout.setVisibility(LinearLayout.GONE);

        ll_security_question = (LinearLayout)findViewById(R.id.ll_frgtpwd_sandq);
        ll_email = (LinearLayout)findViewById(R.id.ll_frgtpwd_email);

        // clickEnterEmailButton = (Button) findViewById(R.id.clickEnterEmailButton);

        mSecQuestion = (TextView) findViewById(R.id.secutityQuestionRadio);
        securityQuestionLayout.setVisibility(LinearLayout.VISIBLE);

        mEmail = (TextView) findViewById(R.id.tv_frgt_email);

        mAnswer1 = (EditText) findViewById(R.id.EditText1);
        mAnswer2 = (EditText) findViewById(R.id.EditText2);
//        mUsername = (EditText)findViewById(R.id.EnterUserNameEditTextView);
        mNewPwd = (EditText) findViewById(R.id.newPwdEditText);
        mConNewPwd = (EditText) findViewById(R.id.confirtmPwdEditText);
        mNewPwd.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        mConNewPwd.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        mConNewPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                pwd1 = mNewPwd.getText().toString();
                pwd2 = mConNewPwd.getText().toString();
                if (!(pwd1.equals(pwd2))) {
                    mConNewPwd.setError(getString(R.string.pwdmismatch));
                }
            }
        });

        mSecQuestion.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(ForgotPassword.this);

                ll_security_question.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                mSecQuestion.setTextColor(getResources().getColor(R.color.white));
                ll_email.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                mEmail.setTextColor(getResources().getColor(R.color.appColor));

                editTextEnterEmail.setText("");


                securityQuestions.setVisibility(View.GONE);

                emailLayout.setVisibility(LinearLayout.GONE);
                if (msecQuesRstPwd) {
                    resetPwdLayout.setVisibility(LinearLayout.VISIBLE);
                    submitCancelLayout.setVisibility(LinearLayout.VISIBLE);

                } else {

                    securityQuestionLayout.setVisibility(LinearLayout.VISIBLE);
                    submitCancelLayout.setVisibility(LinearLayout.GONE);
                    if (emailIdFlag) {
                        if (mConnectionDetector.isOnline()) {
                            new getSecQuestionIDs().execute();
                        } else {
                            showAlertBox(getResources().getString(R.string.netNotAvl));
                        }
                    }
                }

            }
        });

        mEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(ForgotPassword.this);

                ll_security_question.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                mSecQuestion.setTextColor(getResources().getColor(R.color.appColor));
                ll_email.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                mEmail.setTextColor(getResources().getColor(R.color.white));


                securityQuestionLayout.setVisibility(LinearLayout.GONE);
                resetPwdLayout.setVisibility(LinearLayout.GONE);
                emailLayout.setVisibility(LinearLayout.VISIBLE);
                submitCancelLayout.setVisibility(LinearLayout.VISIBLE);
            }
        });

        editTextEnterEmail = (EditText) findViewById(R.id.editTextEnterEmail);

        btnEnterEmailSubmit = (Button) findViewById(R.id.btnEnterEmailSubmit);

        btnEnterEmailSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(ForgotPassword.this);

                if (editTextEnterEmail.getText().toString().length() == 0) {
                    editTextEnterEmail.requestFocus();
                    showAlertBox("Please enter email");
                } else if (!isValidEmail(editTextEnterEmail.getText().toString())) {
                    editTextEnterEmail.requestFocus();
                    showAlertBox(getString(R.string.entervalidmailid));

                    securityQuestions.setVisibility(View.GONE);

                } else {
                    userEmailId = editTextEnterEmail.getText().toString();
                    emailCheckFlag = true;

                    sectemp = editTextEnterEmail.getText().toString();
                    new getSecQuestionIDs().execute();
//                    Toast.makeText(getApplicationContext(),"User Email : "+userEmailId,Toast.LENGTH_SHORT).show();
                 //   popup.dismiss();
                    securityQuestions.setVisibility(View.VISIBLE);
                    //btnEnterEmailSubmit.setVisibility(View.GONE);

                }
            }
        });

        mResetButton = (Button) findViewById(R.id.pwdResetButton);
        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (mUsername.getText().toString().length() == 0) {
//                    flag = 0;
                    mUsername.requestFocus();
//                    fixed the bug in sprint-10 Ticket_Id-220 starts
                    showAlertBox(getString(R.string.emailRequired));
//                    fixed the bug in sprint-10 Ticket_Id-220 end
//                    showAlertBox(getString(R.string.usernamerequired));
                } else */
                new KeyBoardHide().hideKeyBoard(ForgotPassword.this);

                if (editTextEnterEmail.getText().toString().length() == 0) {
                    showAlertBox(getString(R.string.pleaseEnterEmail));
                } else if (mAnswer1.getText().toString().length() == 0) {
//                    flag = 0;
                    mAnswer1.requestFocus();
                    showAlertBox(getString(R.string.answer1required));
                } else if (mAnswer2.getText().toString().length() == 0) {
//                    flag = 0;
                    mAnswer2.requestFocus();
                    showAlertBox(getString(R.string.answer2required));
                } else if (item2.equals(item3)) {
                    showAlertBox(getString(R.string.questionreselected));
                } else {
                    getForgotPwdInfo();
                    if (mConnectionDetector.isOnline()) {
                        new PwdResetInfo().execute();
                    } else {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
                }
            }
        });

        mSubmit = (Button) findViewById(R.id.btnSubmitEmailSecurity);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(ForgotPassword.this);

                if (mSingleClick) {
                    return;
                } else {
                    mSingleClick = true;
                    if (msecQues) {
                        if (mNewPwd.getText().toString().length() == 0) {
                            mNewPwd.requestFocus();
                            mSingleClick = false;
                            showAlertBox(getString(R.string.enternewpwd));
                        }/*else if (mNewPwd.getText().toString().length() < 6) {
                            mNewPwd.requestFocus();
                            showAlertBox(getString(R.string.pwdshort));
                        }*/ else if (mConNewPwd.getText().toString().length() == 0) {
                            mConNewPwd.requestFocus();
                            mSingleClick = false;
                            showAlertBox(getString(R.string.enterconfirmpwd));
                        }

                        String temp, temp1, temp2;
                        temp = sectemp;
//                        temp = sectemp=  mUsername.getText().toString();
                        temp1 = mNewPwd.getText().toString();
                        temp2 = mConNewPwd.getText().toString();
                        stringHeader = "{" + "\"username\":" + "\"" + temp + "\"" + "," + "\"newPassword\":" + "\"" + temp1 + "\"" +
                                "," + "\"confirmPassword\":" + "\"" + temp2 + "\"" + "}";
                        if (mConnectionDetector.isOnline()) {
                            if (validatePwd() && passwordTest()) {
                                msecQues = false;
                                msecQuesRstPwd = false;
                                new PwdResetSecQuesInfo().execute();
                            }
                        } else {
                            mSingleClick = false;
                            showAlertBox(getResources().getString(R.string.netNotAvl));
                        }

                    } else {
                        if (mEmailText.getText().toString().length() == 0) {
                            mEmailText.requestFocus();
                            mSingleClick = false;
                            showAlertBox(getString(R.string.entermailidconfirm));
                        }
                        emailStr = emailStr1 = mEmailText.getText().toString();
                        SpaceToPlus(emailStr);
                        if (mConnectionDetector.isOnline()) {
                            if (mEmailText.getText().toString().length() > 0) {
                                mSingleClick = false;
                                new ForgetPasswordTask().execute();
                            }
                        } else {
                            mSingleClick = false;
                            showAlertBox(getResources().getString(R.string.netNotAvl));
                        }
                    }
                }
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        user_id = prefs.getInt(USER_ID, 0);
        /*mSQid1 = prefs.getInt(SECQUESTION1_ID, 0);
        mSQid2 = prefs.getInt(SECQUESTION2_ID, 0);*/

        mSecSpinner1 = (Spinner) findViewById(R.id.questionspiner1);
        ArrayAdapter<UserSecurity> adapter1 = new ArrayAdapter<UserSecurity>(this, android.R.layout.simple_spinner_item, arraylist);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSecSpinner1.setAdapter(adapter1);
        mSecSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item2 = parent.getItemAtPosition(position).toString();
                mQid1 = getQuestionID(item2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        mSecSpinner1.setSelection(mSQid1);

        mSecSpinner2 = (Spinner) findViewById(R.id.questionspiner2);
        ArrayAdapter<UserSecurity> adapter2 = new ArrayAdapter<UserSecurity>(this, android.R.layout.simple_spinner_item, arraylist);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSecSpinner2.setAdapter(adapter2);
        mSecSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item3 = parent.getItemAtPosition(position).toString();
                mQid2 = getQuestionID(item3);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        mSecSpinner2.setSelection(mSQid2);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                new KeyBoardHide().hideKeyBoard(ForgotPassword.this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




  /*  private void showPopup(final Activity context, Point p1) {

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.clickEnterEmailLayout);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.forgotpassword_enter_email_layout, viewGroup);

        popup = new PopupWindow(layout, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT, true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        editTextEnterEmail = (EditText) layout.findViewById(R.id.editTextEnterEmail);

        btnEnterEmailSubmit = (Button) layout.findViewById(R.id.btnEnterEmailSubmit);
       // btnEnterEmailCancel = (Button) layout.findViewById(R.id.btnEnterEmailCancel);

        btnEnterEmailSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextEnterEmail.getText().toString().length() == 0) {
                    editTextEnterEmail.requestFocus();
                    showAlertBox("Please enter email");
                } else if (!isValidEmail(editTextEnterEmail.getText().toString())) {
                    editTextEnterEmail.requestFocus();
                    showAlertBox(getString(R.string.entervalidmailid));
                } else {
                    userEmailId = editTextEnterEmail.getText().toString();
                    emailCheckFlag = true;

                    sectemp = editTextEnterEmail.getText().toString();
                    new getSecQuestionIDs().execute();
//                    Toast.makeText(getApplicationContext(),"User Email : "+userEmailId,Toast.LENGTH_SHORT).show();
                    popup.dismiss();
                }
            }
        });
    }*/

    private boolean validatePwd() {
        if (/*(mUsername.getText().toString().length() > 0) && */(mNewPwd.getText().toString().length() > 0) &&
                (mConNewPwd.getText().toString().length() > 0)) {
            return true;
        } else {
            return false;
        }
    }

    private void SpaceToPlus(String test) {
        emailStr = test.replaceAll(" ", "+");
    }

    private boolean passwordTest() {
        pwd1 = mNewPwd.getText().toString();
        pwd2 = mConNewPwd.getText().toString();
        if (!(pwd1.equals(pwd2))) {
            showAlertBox(getString(R.string.pwdmismatch));
            return false;
        } else {
            return true;
        }
    }

    public class PwdResetInfo extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.registerProgress));
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.FORGET_LOGIN_VALIDATE_URL();
                response = mJsonUtil.postMethod(stringurl, null, null, mUserForgotPassword, null, null,null, null, null,
                        null,null,null,null);
                OnePocketLog.d("pwdeset security resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("pwdeset security error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();

            if (response == null || response == "" || response == "error") {
                showAlertBox(getString(R.string.servernotresponce) + " Please try again.");
            } else if (response.contains("questions and answers is invalid")) {
                showAlertBox(getString(R.string.secQuesAnswInvalid) + " Please try again.");
            } else if (response.contains("Answers has not set yet")) {
                showAlertBox(getString(R.string.answersHasNotYet));
            } else if (response.contains("User is invalid")) {
                showAlertBox(getString(R.string.userInvalid) + " Please try again.");
            }else if(response.contains("Error Code-254")){
                showAlertBox(getString(R.string.forgotwrongsecquestions));
            } else /*if(response.equals(String.valueOf(user_id)))*/ {
                try {
                    securityQuestionLayout.setVisibility(LinearLayout.GONE);
                    emailLayout.setVisibility(LinearLayout.GONE);
                    resetPwdLayout.setVisibility(LinearLayout.VISIBLE);
                    submitCancelLayout.setVisibility(LinearLayout.VISIBLE);
                    msecQues = true;
                    msecQuesRstPwd = true;
                } catch (Exception e) {
                    OnePocketLog.d("posts resp----------", "exception");
                }
            }
        }
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public class PwdResetSecQuesInfo extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.RESET_PASSWORD_WITH_SEC_QUESTIONS();
                response = mJsonUtil.postMethod(stringurl, null, null, null, null, null,null, null, stringHeader, null,null,null,null);
                OnePocketLog.d("singup security resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("singup security error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
            user_id = prefs.getInt(USER_ID, 0);

            if (response == null || response == "" || response == "error" || response.contains("Could not read JSON")) {
                showAlertBox(getString(R.string.servernotresponce) + " Please try again.");
            } else if (response.contains("password not same")) {
                showAlertBox(getString(R.string.newConfirmPwdNotSame));
            } else if (response.contains("New password field required")) {
                showAlertBox(getString(R.string.newPwdFieldReq));
            } else if (response.contains("Error Code-199")) {
                showAlertBox(getString(R.string.newPwdInvalid));
            } else {
//                else if(response.equals(String.valueOf(user_id))){
                successFlag = true;
                showAlertBox(getString(R.string.pwdResetSuccess));
                /*try{
                    Intent intent = new Intent(ForgotPassword.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }catch (Exception e){
                    OnePocketLog.d("posts resp----------","exception");
                }*/
            }
        }
    }


    private void getForgotPwdInfo() {
        mUserForgotPassword.setAnswer1(mAnswer1.getText().toString());
        mUserForgotPassword.setAnswer2(mAnswer2.getText().toString());
        mUserForgotPassword.setSecurityQuesId1(mQid1);
        mUserForgotPassword.setSecurityQuesId2(mQid2);
        mUserForgotPassword.setUsername(userEmailId);
//        mUserForgotPassword.setUsername("testcloud1");
//        mUserForgotPassword.setUsername(mUsername.getText().toString());
        mUserForgotPassword.setOptions("questions");
    }

    public class getSecQuestionIDs extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String stringurl = OnePocketUrls.GET_SECURITY_QUE_IDS() + "username=" + sectemp;
                response = mJsonUtil.getMethod(stringurl, null, null);
//                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL(),null,null);
                OnePocketLog.d("getSecQuestionIDs resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("getSecQuestionIDs error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideProgress();
            if (response.contains("SecurityQuestionId")) {
                try {
                    JSONArray temp = new JSONArray(response);
                    for (int i = 0; i < temp.length(); i++) {
                        JSONObject e = temp.getJSONObject(i);
                        JSONObject ee = temp.getJSONObject(i + 1);
                        mSQid1 = e.getInt("SecurityQuestionId1");
                        mSQid2 = ee.getInt("SecurityQuestionId2");
                        i++;
                    }
                    mSecSpinner1.setSelection(mSQid1 - 1);
                    mSecSpinner2.setSelection(mSQid2 - 1);

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            } else if (response.contains("")) {
                showAlertBox(getString(R.string.entervalidmailid));
            }

        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public class ForgetPasswordTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String stringurl = OnePocketUrls.FORGET_PASSWORD_URL() + "email=" + emailStr;
                response = mJsonUtil.postMethod(stringurl, null, null, null, null, null, null, null, null, null,null,null,null);
//                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL(),null,null);
                OnePocketLog.d("forgot pwd resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("forgot error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideProgress();
            if (response.contains("Password Reset Email shortly")) {
                mPwdEmailSuccess = true;
                successFlag = true;
                showAlertBox(getString(R.string.pwdresetshortly));
            } else if (response.contains("User email") && response.contains("not found")) {
                showAlertBox(getString(R.string.userEmail) + " " + emailStr1 + " " + getString(R.string.notFound) + " Please try again.");
            } else {
                showAlertBox(getString(R.string.servernotresponce) + " Please try again.");
            }

        }
    }

    private int getQuestionID(String item) {
        int id;
        for (int i = 0; i < arraylist.size(); i++) {
            if (arraylist.get(i).getQuestion().equals(item)) {
                return arraylist.get(i).getId();
            }
        }
        return 0;
    }

    public void showAlertBox(final String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if(!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                        if (mPwdEmailSuccess) {
                            mPwdEmailSuccess = false;
                            finish();
                        }
                        if (msg.contains("Password reset successfully")) {
                            finish();
                            Intent intent = new Intent(ForgotPassword.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                ForgotPassword.this).create();
        if(!successFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (mPwdEmailSuccess) {
                    mPwdEmailSuccess = false;
                    finish();
                }
                if (msg.contains("Password reset successfully")) {
                    finish();
                    Intent intent = new Intent(ForgotPassword.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });
        alertDialog.show();*/
    }
}

