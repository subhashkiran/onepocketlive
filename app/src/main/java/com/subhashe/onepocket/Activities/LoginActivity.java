package com.subhashe.onepocket.Activities;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.subhashe.onepocket.Core.DeviceDetailsInfo;
import com.subhashe.onepocket.Core.UserRegisterInfo;
import com.subhashe.onepocket.Core.UserSecurity;
import com.subhashe.onepocket.Core.UserSecurityAnswers;
import com.subhashe.onepocket.Fragments.MapsFragmentComplete;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;
import com.subhashe.onepocket.Utils.SharedPreferencesStore;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import static android.view.Gravity.*;

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class LoginActivity extends ActionBarActivity implements OnClickListener {
    public String remberstatus , loginUser , registerUser;
    private static Context context;
    int id1, addressId,accBalence;
    String first_name , last_name;
    ProgressDialog progress;
    public EditText emailid, password;
    private Button mForgetPwdButton;
    public CheckBox chkrember;
    private JsonUtil mJsonUtil;
    private boolean mForgotPwd;
    ArrayList<UserSecurity> mUserarray = new ArrayList<UserSecurity>();
    //    ArrayList<UserSecurityAnswers> mUserSecurityAnswersarray = new ArrayList<UserSecurityAnswers>();
    private AsyncTask userQuestionAsync, loginAuthAsync;
    String remberme;
    String googleemailid;
    String googlepersonName;
    String fbname;
    String fbemail;
    String name, email, phone, livepassword, address, paymentinfo, cartype, lastActivity;
    // Google client to interact with Google API
    private ConnectionDetector mConnectionDetector;
    private UserSecurityAnswers mUserSecurityAnswers;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    // Your Face book APP ID
    private static String APP_ID = "700674833354709"; // Replace with your App ID
    // Instance of Face book Class
    String FILENAME = "AndroidSSO_data";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USERNAME_ID = "USERNAME_ID_PREFS_String";
    public static final String EMAIL_ID = "EMAIL_ID_PREFS_String";
    public static final String FIRSTNAME_ID = "FRISTNAME_PREFS_String";
    public static final String LASTNAME_ID = "LASTNAME_PREFS_String";
    public static final String EMAIL_ID_APPEND = "EMAIL_ID_APPEND_PREFS_String";
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String LOGIN_USER = "LOGIN_USER_String";
    public static final String REGISTER_USER = "REGISTER_USER_String";
    public static final String USER_REG_ID = "USERID_REG_String";
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String ACC_BALENCE = "ACC_BALENCE";
    public static final String ADDRESS_USER_ID = "ADDRESS_USER_ID";
    public static final String PAYMENT_FLAG = "PAYMENT_FLAG";
    public static final String FAVLATRETURN = "FAVLATRETURN";
    public static final String FAVLATRETURNFLAG = "FAVLATRETURNFLAG";
    public static final String STATUSFLAG = "STATUSFLAG";

    private static final int RC_SIGN_IN = 0;
    String personName;
    String response, loginHeader ,emailVerification, deviceDetailsHeader;
    String gcmRegistrationId = null, device_id;
    android.support.v7.app.ActionBar mActionBar;
    int onCreate = 0;
    EditText edttxtforgotemailid;
    private UserRegisterInfo mUserRegInfo;
    private DeviceDetailsInfo mdeviceDetailsInfo;
    private int userId;
    String emailId;
    SharedPreferences.Editor editor1;
    boolean isChecked = false;

    private int existingCheck;
    //private boolean paymentPopup = false;
    public static final String REGISTRATION_LOGIN_ALERT = "PAYMENT_FLAG_ALERT";

    /*private static final String TAG = LoginActivity.class.getName();
    private static final Uri BASE_APP_URI = Uri.parse("android-app://com.subhashe.onepocket/http/subhashe.onepocket.com/subhashe/");

    private GoogleApiClient mClient;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Intent intent = getIntent();
        mUserRegInfo = new UserRegisterInfo();
        mdeviceDetailsInfo = new DeviceDetailsInfo();
        lastActivity = intent.getStringExtra("lastActivity");
        onCreate = 1;

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mConnectionDetector = new ConnectionDetector(this);
//        mUserSecurityAnswers = new UserSecurityAnswers();

        emailid = (EditText) findViewById(R.id.edttxtemailid);

        password = (EditText) findViewById(R.id.edttxtpassword);

        final Button loginbtn = (Button) findViewById(R.id.btnLogin);
        chkrember = (CheckBox) findViewById(R.id.chkremember);

        //keepMeLoggedIn = (CheckBox) findViewById(R.id.keepmeloggedin);


        /*if (OnePocketUrls.getVersionType(this).equalsIgnoreCase("PROD")) {
            OnePocketUrls.CURRENT_BUILD_CONFIG = OnePocketUrls.ServerConfig.ServerConfigProd;
        } else if (OnePocketUrls.getVersionType(this).equalsIgnoreCase("QA")) {
            OnePocketUrls.CURRENT_BUILD_CONFIG = OnePocketUrls.ServerConfig.ServerConfigQA;
        } else if (OnePocketUrls.getVersionType(this).equalsIgnoreCase("INT")) {
            OnePocketUrls.CURRENT_BUILD_CONFIG = OnePocketUrls.ServerConfig.ServerConfigQA;
        }*/

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor1 = prefs.edit();

        userId = prefs.getInt(USER_ID, 0);
        registerUser = prefs.getString(EMAIL_ID , " ");

        OnePocketLog.d("RegisterEmailId" , registerUser);

        emailid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (SharedPreferencesStore.getRememberMe()) {
                    SharedPreferencesStore.setEncryptedSharedPref(
                            SharedPreferencesStore.ONEPOCKET_USER_NAME,
                            emailid.getText().toString());
                    SharedPreferencesStore.setEncryptedSharedPref(
                            SharedPreferencesStore.ONEPOCKET_PASSWORD,
                            password.getText().toString());
                    SharedPreferencesStore.setRememberMe(true);
                }
            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (SharedPreferencesStore.getRememberMe()) {
                    SharedPreferencesStore.setEncryptedSharedPref(
                            SharedPreferencesStore.ONEPOCKET_USER_NAME,
                            emailid.getText().toString());
                    SharedPreferencesStore.setEncryptedSharedPref(
                            SharedPreferencesStore.ONEPOCKET_PASSWORD,
                            password.getText().toString());
                    SharedPreferencesStore.setRememberMe(true);
                }
            }
        });
        if (SharedPreferencesStore.getRememberMe()) {
            String temp, temp1;
            temp = SharedPreferencesStore.getEncryptedSharedPref(
                    SharedPreferencesStore.ONEPOCKET_USER_NAME);
            temp1 = SharedPreferencesStore.getEncryptedSharedPref(
                    SharedPreferencesStore.ONEPOCKET_PASSWORD);
            emailid.setText(temp);
            password.setText(temp1);
            if ((emailid.getText().toString().length() == 0) &&
                    (password.getText().toString().length() == 0)) {
                chkrember.setChecked(false);
            } else {
                chkrember.setChecked(true);
            }
        }

        chkrember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferencesStore.setEncryptedSharedPref(
                            SharedPreferencesStore.ONEPOCKET_USER_NAME,
                            emailid.getText().toString());
                    SharedPreferencesStore.setEncryptedSharedPref(
                            SharedPreferencesStore.ONEPOCKET_PASSWORD,
                            password.getText().toString());
                    SharedPreferencesStore.setRememberMe(true);
                } else {
                    SharedPreferencesStore.setRememberMe(false);
                    SharedPreferencesStore.setEncryptedSharedPref(
                            SharedPreferencesStore.ONEPOCKET_USER_NAME,
                            "");
                    SharedPreferencesStore.setEncryptedSharedPref(
                            SharedPreferencesStore.ONEPOCKET_PASSWORD,
                            "");
                }
            }
        });

       /* keepMeLoggedIn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences settings = getSharedPreferences("PREFS_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("isChecked", isChecked);
                editor.commit();
            }
        });*/

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setCustomView(R.layout.action_bar_home);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#48100f")));

        int titleId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        } else {
            titleId = R.id.action_bar_title;
        }
        TextView abTitle = (TextView) findViewById(titleId);
        if (abTitle != null) {
            abTitle.setTextColor(getResources().getColor(R.color.white));

        }

        loginbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int flag = 1;
                if (emailid.getText().toString().length() == 0) {
                    flag = 0;
                    emailid.requestFocus();
                    showAlertBox(getString(R.string.entermailidconfirm));
                } /*else if (!isValidEmail(emailid.getText().toString())) {
                    flag = 0;
                    emailid.requestFocus();
                    DialogUtil("Enter Vaild Mail Id");
                }*/ else if (password.getText().toString().length() == 0) {
                    flag = 0;
                    password.requestFocus();
                    showAlertBox(getString(R.string.enterPassword));
                }

                if (flag == 1) {

                    if (mConnectionDetector.isOnline()) {

                        emailVerification = "userName=" + emailid.getText().toString();

                        loginHeader = "username=" + emailid.getText().toString() + "&" + "password=" + password.getText().toString() + "&" + "mailid=" + emailid.getText().toString();

                        OnePocketLog.d("emailloginheader" , emailVerification);

                        OnePocketLog.d("loginheader" , loginHeader);

                        new EmailVerificationTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);

                       // if(flagValue  == 1) {
                          /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                                new ForgetPasswordTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                                new BackGroundTask1().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);

                            } else {
                                new BackGroundTask1().execute();

                            }*/
                       // }


                    } else {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
                }
            }
        });

        LinearLayout register = (LinearLayout) findViewById(R.id.ll_login_signup);

        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (mConnectionDetector.isOnline()) {
                    Intent imt = new Intent(LoginActivity.this, RegisterNow.class);
                    imt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    imt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (lastActivity != null) {
                        imt.putExtra("lastActivity", "login");
                    }
//                    finish();
                    startActivity(imt);
                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
              /*  CheckNetworkConnection obj = new CheckNetworkConnection();
                final boolean net = obj.isConnectionAvailable(getApplicationContext());
                if (net) {
                    Intent imt = new Intent(LoginActivity.this, Signup.class);
                    imt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    imt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (lastActivity != null) {
                        imt.putExtra("lastActivity", "login");
                    }
                    finish();
                    startActivity(imt);
                } else {
                    DialogUtil(getResources().getString(R.string.netNotAvl));
                }*/
        });

        mForgetPwdButton = (Button) findViewById(R.id.btnForgot);
        mForgetPwdButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mConnectionDetector.isOnline()) {
                    mForgotPwd = true;
                    new ForgetPasswordTask().execute();
                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });
    }


    @Override
    protected void onPause() {
        hideProgress();
        onCreate = 0;
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        hideProgress();
        super.onDestroy();
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public class BackGroundTask1 extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.));
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            try {
                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(), null,null, null, null, null, null, null,
                        loginHeader, null,null,null, null);
//                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL());
                OnePocketLog.d("login response----------", response);
            } catch (Exception e) {
                OnePocketLog.d("login error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
//            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response.contains("Error Code-113")) {
                showAlertBox(getString(R.string.incorrectusername));
            } else if (response.contains("Error Code-103")) {
                showAlertBox(getString(R.string.incorrectusername));
            } else if (response.contains("Bad credentials")) {
                showAlertBox(getString(R.string.incorrectusername));
            } else if (response.contains("Access is denied due to invalid credentials")) {
                showAlertBox(getString(R.string.incorrectusername));
            } else {
                try {
//                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                    SharedPreferences.Editor editor1 = prefs.edit();
//                    editor1.putString("UserId", response);
//                    editor1.putString("RememberMe", "false");
//                    editor1.commit();

//                    DialogUtil("Registration Succesfull.");
                    // validate with security questions : if answers are present redirect to navigation home page else
                    // redirect to security questions page
                    JSONObject jObject = new JSONObject(response);
                    JSONArray jObject1 = jObject.getJSONArray("profiles");
                    JSONArray jObject2 = jObject.getJSONArray("accountses");
                    JSONArray jObject3 = jObject.getJSONArray("address");

                    String val = "INACTIVE";
                    String idToken;
                    String emailRes, userRes ;
                    String  firstName , lastName;
                    int id;
                    id = jObject.getInt("id");
                    idToken = jObject.getString("token");
                    emailRes = jObject.getString("email");
                    userRes = jObject.getString("username");
                    firstName = jObject.getString("firstName");
                    lastName = jObject.getString("lastName");

                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp = jObject1.getJSONObject(i);
                        val = mTemp.getString("status");
                    }

                    for (int i = 0; i < jObject2.length(); i++) {
                        JSONObject mTemp = jObject2.getJSONObject(i);
                        id1 = mTemp.getInt("id");
                        accBalence = mTemp.getInt("accountBalance");

                    }

                    for (int i = 0; i < jObject3.length(); i++) {
                        JSONObject mTemp = jObject3.getJSONObject(i);
                        addressId = mTemp.getInt("id");
                    }

                    /*JSONArray temp = new JSONArray(mUserSecurityAnswers);

                    if(mUserSecurityAnswersarray != null){
                        mUserSecurityAnswersarray.clear();
                    }

                    for(int i = 0; i< temp.length();i++){
                        JSONObject e = temp.getJSONObject(i);
                        mUserSecurityAnswers.setAnswer(e.getString("answer"));
                        mUserSecurityAnswers.setUpdatedOn(e.getString("updatedOn"));
                        mUserSecurityAnswers.setId(e.getInt("id"));
                        mUserSecurityAnswersarray.add(i,mUserSecurityAnswers);
                    }*/


                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor1 = prefs.edit();
                    int regid;
                    String temp;

                    regid = prefs.getInt(USER_REG_ID,0);
                    temp = prefs.getString(EMAIL_ID,"NOT FOUND");

                    temp += "," + emailRes;

                    if(id != regid){
                        editor1.putBoolean(PAYMENT_FLAG,true);
                    }

                    editor1.putString(TOKEN_ID, idToken);
                    editor1.putString(FIRSTNAME_ID, firstName);
                    editor1.putString(LASTNAME_ID , lastName);
                    editor1.putString(USERNAME_ID, userRes);
                    editor1.putString(EMAIL_ID, emailRes);
                    editor1.putString(EMAIL_ID_APPEND, temp);
                    editor1.putInt(USER_ID, id);
                    editor1.putInt(ACC_USER_ID, id1);
                    editor1.putInt(ACC_BALENCE, accBalence);
                    editor1.putInt(ADDRESS_USER_ID, addressId);


                    OnePocketLog.d("EMailIDDDDDDDD", emailRes);
//                    editor1.putString("UserName", resonce);
//                    editor1.putString("RememberMe", "false");
                    editor1.commit();


                    if (val.contentEquals("ACTIVE")) {

                            SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = pSharedPref.edit();
                            editor.putString(FAVLATRETURN, "");
                            editor.putBoolean(FAVLATRETURNFLAG, false);
                            editor.commit();

                            LoginActivity.this.finish();
                            new KeyBoardHide().hideKeyBoard(LoginActivity.this);
                            Intent intent = new Intent(LoginActivity.this, NavigationDrawerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                    } else {
                        LoginActivity.this.finish();
                        Intent mIntent = new Intent(LoginActivity.this, login_payment_security.class);
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("data", mUserarray);
                        mIntent.putExtras(bundle);
                        startActivity(mIntent);
                    }
                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    public class EmailVerificationTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                String stringurl = OnePocketUrls.EMAIL_VERIFICATION_URL();
                response = mJsonUtil.postMethod(stringurl,null,null,null,null,null,null,null,emailVerification,null,null,null,null);

//                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL());
                OnePocketLog.d("email response----------", response);
            } catch (Exception e) {
                OnePocketLog.d("email error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
           // hideProgress();

            if(response.contains("1"))
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    new ForgetPasswordTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                    new BackGroundTask1().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);

                } else {
                    new BackGroundTask1().execute();

                }
            }
            else if(response.contains("0"))
            {
                showAlertBox(getString(R.string.userEmailverfication));
            }


        }
    }

    public class ForgetPasswordTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL(), null, null);
                OnePocketLog.d("forgotpassword resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("forgotpassword error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideProgress();

            if (response == null || response == "" || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response.contains("Error Code")) {
                showAlertBox(getString(R.string.incorrectusername));
            } else {
                try {
                  /*  JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray  = jsonObject.getJSONArray();*/
                    JSONArray temp = new JSONArray(response);
                    if (mUserarray != null) {
                        mUserarray.clear();
                    }
                    for (int i = 0; i < temp.length(); i++) {
                        UserSecurity mUser = new UserSecurity("", -1);
                        JSONObject e = temp.getJSONObject(i);
                        mUser.setQuestion(e.getString("questions"));
                        mUser.setId(e.getInt("id"));
                        mUserarray.add(i, mUser);
                    }

                    if (mForgotPwd) {
                        mForgotPwd = false;
                        Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("data", mUserarray);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else {

//                        Intent intent = new Intent(LoginActivity.this, NavigationDrawerActivity.class);
//                        startActivity(intent);
                    }
                   /* JSONArray temp = new JSONArray();
                    temp =  new JSONArray(response);
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    List<UserSecurity> posts = new ArrayList<UserSecurity>();
                    posts = Arrays.asList(gson.fromJson(temp.toString(), UserSecurity[].class));
                    showAlertBox("Incorrect Username or Password");*/
                } catch (Exception ep) {
                    OnePocketLog.d("posts resp----------", "exception");
                }
                /*try {
                    if (lastActivity == null) {
                        Intent mIntent = new Intent(LoginActivity.this, login_payment_security.class);
                        if (lastActivity != null) {
                            mIntent.putExtra("lastActivity", "login");
                        }
                        startActivity(mIntent);
                    }
                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }*/
            }
        }
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);

        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

        /*AlertDialog alertDialog = new AlertDialog.Builder(
                LoginActivity.this).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);

        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                hideProgress();
            }
        });
        alertDialog.show();*/
    }

    @SuppressLint("InflateParams")
    private void showAlert() {
      /*  AlertDialog.Builder alert = new AlertDialog.Builder(this);

        final LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.forgot, null);
        textEntryView.setBackgroundResource(R.color.light);

        alert.setView(textEntryView);
        final AlertDialog a = alert.create();
        a.setView(textEntryView, 0, 0, 0, 0);
        a.show();
        edttxtforgotemailid = (EditText) a.findViewById(R.id.edttxtforgotemailid);
        Button btncancel = (Button) a.findViewById(R.id.btnForgotCancel);
        btncancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                a.dismiss();
            }
        });

        Button btnsubmit = (Button) a.findViewById(R.id.btnForgotsumit);
        btnsubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edttxtforgotemailid.getText().toString().equalsIgnoreCase("")) {
                    DialogUtil("Enter registered email id");

                } else {
                    new BackGroundTask4().execute();
                    a.dismiss();
                }

            }
        });*/
    }

    @SuppressLint("InflateParams")
    public void passwordsent() {
       /* AlertDialog.Builder alert = new AlertDialog.Builder(this);
        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.passwodsenttetx, null);
        alert.setView(textEntryView);
        final AlertDialog a = alert.create();
        a.setView(textEntryView, 0, 0, 0, 0);
        a.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                 *//* Create an Intent that will start the Menu-Activity. *//*
                Intent mainIntent = new Intent(LoginActivity.this, LoginActivity.class);
                finish();
                startActivity(mainIntent);
                finish();
            }
        }, 2000);*/
    }


    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;
        }
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {

//        signInWithGplus();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!SharedPreferencesStore.getRememberMe()) {
            emailid.setText("");
            password.setText("");
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        //finish();

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            new AlertDialog.Builder(this)
                    //.setIcon(android.R.drawable.ic_dialog_alert)
                    //.setTitle("Exit?")
                    .setMessage("Do you want to close the App?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            //onBackPressed();
                            finish();
                        }
                    })
                    .show();

        } else {
            super.onBackPressed();
        }
    }
}
