package com.subhashe.onepocket.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.subhashe.onepocket.Core.EvModel;
import com.subhashe.onepocket.Core.EvModelAcc;
import com.subhashe.onepocket.Core.UserAddres;
import com.subhashe.onepocket.Core.UserRegisterInfo;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

/**
 * Created by axxera on 22-12-2016.
 */

public class AddNewEVActivity extends AppCompatActivity {

    private ConnectionDetector mConnectionDetector;
    private UserRegisterInfo mUserRegInfo;
    private JsonUtil mJsonUtil;
    private String response;
    private UserAddres mUseradd;
    private EditText editEnterMake,editEnterModel;
    String pwd1,pwd2,sItem,token;
    private Button evButton,btnSubmit,close;
    Point p1;
    Spinner gender;
    private Spinner spinner;
    private int user_id_reg;
    ArrayAdapter<String> adapter;
    private boolean successFlag,evFlag , submitFlag;
    String[] SelectYears = new String[] {"Select year","2017","2016","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005"};
    String  lastActivity;
    private ProgressDialog progress;
    android.support.v7.app.ActionBar mActionBar;
    public static final String PREFS_NAME = "USERID_PREFS";
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String USER_REG_ID = "USERID_REG_String";
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String FAST_SWITCH = "FAST_SWITCH";
    SharedPreferences settings;
    int onCreate = 0;
    private EvModelAcc evListEdit;
    private String mYearTemp,mMakeTemp,mModelTemp;
    int spinnerPosition = 0;
    private boolean mFastChargeTemp;
    private Switch dcFastSwitch;
    private boolean firstFlag,firsttime,evAddError;
    LinearLayout ll_actionbar;

    @Override

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.ev_addnew_ev_view);

        mConnectionDetector = new ConnectionDetector(this);
        mUserRegInfo = new UserRegisterInfo();
        mUseradd = new UserAddres();
        evListEdit = new EvModelAcc();
        mJsonUtil = new JsonUtil();
        Intent imt = getIntent();
        lastActivity = imt.getStringExtra("lastActivity");
        onCreate = 1;

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Edit Profile");
//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);


        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

        String title = "Add EV";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id_reg = prefs.getInt(ACC_USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

        ll_actionbar = (LinearLayout)findViewById(R.id.ll_add_ev);
        ll_actionbar.setVisibility(View.GONE);

        editEnterMake =  (EditText) findViewById(R.id.editTextenterMake);
        editEnterModel =  (EditText) findViewById(R.id.editTextEnterModel);
        dcFastSwitch = (Switch)findViewById(R.id.evDCFastSwitch);

        spinner =(Spinner)findViewById(R.id.SelectYearSpinner);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, SelectYears);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sItem = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(mYearTemp != null && mYearTemp.length()!= 0){
            evListEdit.setYear(mYearTemp);
            spinner.setSelection(spinnerPosition);
        }
        if(mMakeTemp != null && mMakeTemp.length() != 0){
            editEnterMake.setText(mMakeTemp);
        }
        if(mModelTemp != null && mModelTemp.length() != 0){
            editEnterModel.setText(mModelTemp);
        }
        if(mFastChargeTemp){
            dcFastSwitch.setChecked(true);
        }else{
            dcFastSwitch.setChecked(false);
        }

        btnSubmit = (Button) findViewById(R.id.btnAddNewEvSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(AddNewEVActivity.this);

                if (spinner.getSelectedItem().toString().contains("Select Year")) {
                    spinner.requestFocus();
                    showAlertBox(getString(R.string.AddEv_SelectYear));
                }else if (editEnterMake.getText().toString().length() == 0) {
                    editEnterMake.requestFocus();
                    showAlertBox(getString(R.string.enterMake));
                }else if(editEnterModel.getText().toString().length() == 0){
                    editEnterModel.requestFocus();
                    showAlertBox(getString(R.string.enterModel));
                }else if(editEnterMake.getText().toString().length() <=2 || editEnterMake.getText().toString().length() > 20){
                    editEnterMake.requestFocus();
                    showAlertBox(getString(R.string.makeError));
                } else if(editEnterModel.getText().toString().length() <=2 || editEnterModel.getText().toString().length() > 20) {
                    editEnterModel.requestFocus();
                    showAlertBox(getString(R.string.modelError));
                }else{
                    evListEdit.setYear(spinner.getSelectedItem().toString());
                    evListEdit.setMake(editEnterMake.getText().toString());
                    evListEdit.setModel(editEnterModel.getText().toString());
                   /* if(editEnterVin.getText().length()>1) {
                        evListEdit.setVin(editEnterVin.getText().toString());
                    }*/
                    evListEdit.setVehicleId(user_id_reg);

                    if (mConnectionDetector.isOnline()) {
                        if(validateEvList_Reg()) {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            evFlag = true;
                            if (dcFastSwitch.isChecked()) {
                                dcFastSwitch.setChecked(true);
                                mFastChargeTemp = true;
                                editor1.putBoolean(FAST_SWITCH, true);
                            } else {
                                dcFastSwitch.setChecked(false);
                                mFastChargeTemp = false;
                                editor1.putBoolean(FAST_SWITCH, false);
                            }
                            editor1.commit();
                            new ElectrivVehicleADD().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                        }
                    } else {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
                }
            }
        });

        close = (Button) findViewById(R.id.btnAddNewEvCancel);
        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddNewEVActivity.this , NavigationDrawerActivity.class);
                startActivity(intent);
            }
        });
    }

    public class ElectrivVehicleADD extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
//                String mUrl = OnePocketUrls.ACCOUNT_EV();
                response = mJsonUtil.postMethod(OnePocketUrls.EV_ADD(),null,null,null,null,evListEdit,null,null,
                        null,null,null,null,null);
                OnePocketLog.d("EV Add resp----------", response);


            } catch (Exception e) {
                OnePocketLog.d("EV Add error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if(response.contains("Error Code-114") || response.contains("Vehicle already exists.")){
                showAlertBox(getString(R.string.vinExists));
            }else if(response.contains("Error Code-195") || response.contains("Make is Invalid.")){
                evAddError = true;
                showAlertBox(getString(R.string.makeError));
            }else if(response.contains("Error Code-197") || response.contains("Model is Invalid.")){
                evAddError = true;
                showAlertBox(getString(R.string.modelError));
            }else {
                try {
                    showAlertBox(getString(R.string.saveInformaiton));

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    private boolean validateEvList_Reg(){
        if((editEnterMake.getText().toString().length() > 0) &&
                (editEnterModel.getText().toString().length() > 0)){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    protected void onPause() {
        hideProgress();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        hideProgress();
        evFlag = false;
        super.onDestroy();
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if(!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        if(msg.contains("Added EV Successfully")){
                            finish();
                            new KeyBoardHide().hideKeyBoard(AddNewEVActivity.this);
                            Intent intent1 = new Intent(AddNewEVActivity.this,NavigationDrawerActivity.class);
                            startActivity(intent1);
                        }
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

