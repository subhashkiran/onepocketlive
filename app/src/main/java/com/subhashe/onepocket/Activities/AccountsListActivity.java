package com.subhashe.onepocket.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.subhashe.onepocket.Fragments.AccountFragment;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by axxera on 30-12-2016.
 */

public class AccountsListActivity extends AppCompatActivity {

    private static final String TAG = "ACCOUNT_FRAGMENT";
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View rootView;
    private JsonUtil mJsonUtil;
    private ConnectionDetector mConnectionDetector;
    private Fragment fragment;
    ListView listView;
    Intent intent = null;
    private LinearLayout my_profile, grid_cards, my_ev, my_fav, notifications;
    TextView tv_myprofile, tv_myev, tv_mymoney;
    private boolean successFlag;

    private TextView mAccBalance;
    private String response, token, getAccBal;
    private int user_id, accBalence;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor1;
    private ProgressDialog progress;
    LinearLayout ll_bal;
    ActionBar mActionBar;

    public static final String ACC_BALENCE = "ACC_BALENCE";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";
    private boolean paymentPopup = false;
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USER_REG_ID = "USERID_REG_String";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_accounts);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor1 = prefs.edit();
        paymentPopup = prefs.getBoolean(PAYMENT_FLAG_ALERT, false);
        user_id = prefs.getInt(USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

        initToolbar();

        mConnectionDetector = new ConnectionDetector(getApplicationContext());

        ll_bal = (LinearLayout)findViewById(R.id.ll_activity_acc1_balance);

        my_profile = (LinearLayout) findViewById(R.id.ll_accounts_myProfile);
        my_ev = (LinearLayout) findViewById(R.id.ll_accounts_myev);

        tv_myprofile = (TextView) findViewById(R.id.accounts_myProfile);
        tv_myev = (TextView) findViewById(R.id.accounts_myev);
        tv_mymoney = (TextView)findViewById(R.id.accounts_mymoney);

        mAccBalance = (TextView) findViewById(R.id.accBalance);

        my_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mConnectionDetector.isOnline()) {
                    Intent intent = new Intent(AccountsListActivity.this, EditProfile.class);
                    startActivity(intent);

                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });

        my_ev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                fragment = new ElectricVehicleFragment();

                Intent intent = new Intent(AccountsListActivity.this , EVListActivity.class);
                startActivity(intent);

                if (mConnectionDetector.isOnline()) {
                    initFragment();

                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });
        tv_mymoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(AccountsListActivity.this);

                if (mConnectionDetector.isOnline()) {
                    Intent intent = new Intent(AccountsListActivity.this, CreditCardPayActivity.class);
                    startActivity(intent);

                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        new BalenceCheck().execute();
    }

    public class BalenceCheck extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.USER_BALENCE_CHECK() + user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    JSONObject jObject = new JSONObject(response);
                    JSONArray jObject1 = jObject.getJSONArray("accountses");
                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp = jObject1.getJSONObject(i);
                        getAccBal = mTemp.getString("accountBalance");
                        accBalence = mTemp.getInt("accountBalance");


                        NumberFormat num = NumberFormat.getCurrencyInstance(Locale.US);
//                        BigDecimal bd = new BigDecimal(getAccBal.toString());
//                        mAmount = num.format(getAccBal);
                        mAccBalance.setText("");
                        mAccBalance.setText(num.format(new BigDecimal(getAccBal)));

                        try {
//                            accBalence = Integer.parseInt(getAccBal);
                            editor1.putInt(ACC_BALENCE, accBalence);
                        } catch (NumberFormatException e) {
                            System.out.println("parse value is not valid : " + e);
                        }
                    }

                } catch (Exception ep) {
                }
            }
        }
}
    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        if (!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();
    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();

        mActionBar.setDisplayOptions( ActionBar.DISPLAY_SHOW_TITLE);
//        mActionBar.setTitle("Edit Profile");
//        mActionBar.setHomeButtonEnabled(true);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        //getSupportActionBar().setHomeAsUpIndicator(backArrow);
        mActionBar.setHomeAsUpIndicator(backArrow);

        // mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

        String title = "Account";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //getActivity().finish();
                getFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initFragment() {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frameLayout, fragment).addToBackStack(null).commit();

        } else {
            Log.d("NavigationDrActivity", "Error  creating fragment");
        }
    }

}
