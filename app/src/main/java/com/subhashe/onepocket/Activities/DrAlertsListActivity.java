package com.subhashe.onepocket.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;


import com.subhashe.onepocket.Adapters.DrAlertTotalAdapter;
import com.subhashe.onepocket.Adapters.EVRecyclerViewAdapter;
import com.subhashe.onepocket.Core.DrTotalEventsModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by axxera on 07-07-2017.
 */

public class DrAlertsListActivity extends AppCompatActivity {

    private ConnectionDetector mConnectionDetector;
    ActionBar mActionBar;
    private RecyclerView myDRAlertListRecyclerView;
    private EVRecyclerViewAdapter mDRAlertsListAdapter;
    private String response;
    private JsonUtil mJsonUtil;
    private List<DrTotalEventsModel> mDrAlertList = new ArrayList<>();
    private DrAlertTotalAdapter mMyAdapter;
    private ProgressDialog progress;
    private LinearLayout drAlertLayoutEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dralert);

        initToolbar();

        mConnectionDetector = new ConnectionDetector(getApplicationContext());

        new KeyBoardHide().hideKeyBoard(DrAlertsListActivity.this);

        drAlertLayoutEmpty = (LinearLayout) findViewById(R.id.drAlertEmptyLayout);

        myDRAlertListRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewDrAlert);
        myDRAlertListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myDRAlertListRecyclerView.setHasFixedSize(true);

        new drAlertListGet().execute();

    }
    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        mActionBar = getSupportActionBar();

        mActionBar.setDisplayOptions( ActionBar.DISPLAY_SHOW_TITLE);

        /*final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);*/

        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

        String title = "DR Events";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

    }

    public class drAlertListGet extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.GETALLDRALERTTOTALEVENTS();
                response = mJsonUtil.getMethod(stringurl, null, null);
                OnePocketLog.d("dralerttotal resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("dralerttotal  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            try {
                if (response != null || !response.contains("[]") || response != "") {

                    JSONArray temp = new JSONArray(response);

                    if (temp.length() == 0) {

                        drAlertLayoutEmpty.setVisibility(View.VISIBLE);

                    } else if (temp != null) {

                        for (int i = 0; i < temp.length(); i++) {

                            drAlertLayoutEmpty.setVisibility(View.INVISIBLE);

                            JSONObject e = temp.getJSONObject(i);
                            DrTotalEventsModel mDralertModel = new DrTotalEventsModel();

//                            OnePocketLog.d("mDrActiveModel" , String.valueOf(mDrActiveModel.getIdFav()));

                            mDralertModel.setStartDate(e.getString("StartDate"));
                            mDralertModel.setStartTime(e.getString("StartTime"));
                            mDralertModel.setEndDate(e.getString("EndDate"));
                            mDralertModel.setEndTime(e.getString("EndTime"));
                            mDralertModel.setSignalName(e.getString("SignalName"));
                            mDralertModel.setSignalType(e.getString("SignalType"));
                            mDralertModel.setStatus(e.getString("Status"));

                            mDrAlertList.add(mDralertModel);

                            OnePocketLog.d("startdateList", mDralertModel.getStartDate());
                            OnePocketLog.d("startTimeList", mDralertModel.getStartDate());
                            OnePocketLog.d("enddateList", mDralertModel.getStartDate());
                            OnePocketLog.d("endtimeList", mDralertModel.getStartDate());
                            OnePocketLog.d("signatimeList", mDralertModel.getStartDate());
                            OnePocketLog.d("signaltypeList", mDralertModel.getStartDate());
                            OnePocketLog.d("statusList", mDralertModel.getStatus());
                        }

                    }
                    mMyAdapter = new DrAlertTotalAdapter(DrAlertsListActivity.this, mDrAlertList);
                    myDRAlertListRecyclerView.setAdapter(mMyAdapter);
                }
            } catch (Exception e) {

            }
        }
    }

    public void showAlertBox(final String msg) {

    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(DrAlertsListActivity.this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Intent intent = new Intent(DrAlertsListActivity.this , NavigationDrawerActivity.class );
                startActivity(intent);

                new KeyBoardHide().hideKeyBoard(DrAlertsListActivity.this);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
