package com.subhashe.onepocket.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.subhashe.onepocket.Adapters.GooglePlacesAutocompleteAdapter;
import com.subhashe.onepocket.Core.DeviceDetailsInfo;
import com.subhashe.onepocket.Core.EditProfileModelUpdate;
import com.subhashe.onepocket.Core.ProfileInfoModel;
import com.subhashe.onepocket.Fragments.AccountFragment;
import com.subhashe.onepocket.Fragments.ChargeActivityFragment;
import com.subhashe.onepocket.Fragments.ContactUsFragment;
import com.subhashe.onepocket.Fragments.DrAlertsFragment;
import com.subhashe.onepocket.Fragments.FavoriteListFragment;
//import com.subhashe.onepocket.Fragments.MapsFragment;
//import com.subhashe.onepocket.Fragments.MapsFragmentAllStationsRoute;
import com.subhashe.onepocket.Fragments.MapsFragmentComplete;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import static com.subhashe.onepocket.Utils.OnePocketUrls.CONTACTUS_FLAG;

/**
 * Created by SubhashE on 2/5/2016.
 */
public class NavigationDrawerActivity extends AppCompatActivity /*implements RecyclerViewAdapter.OnItemClickListener*/ {
    //    public static final String AVATAR_URL = "http://lorempixel.com/200/200/people/1/";
   /* private static List<ViewModel> items = new ArrayList<>();

    static {
        for (int i = 1; i <= 10; i++) {
            items.add(new ViewModel("Item " + i, "http://lorempixel.com/500/500/animals/" + i));
        }
    }*/

    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private View content, header , footer;
    private NavigationView headerNavigation , footerNavigation;
    private RecyclerView recyclerView;
    private Fragment fragment = null;
    private NavigationView navigationView;
    private FragmentManager fragmentManager;
    private TextView userHeader;
    private TextView emailHeader;
    private TextView firstName;
    private TextView lastName;
    private TextView tv_appVersion;
    private TextView paymentTrue;
    private String first_name , last_name , user_id, email_id;
    private GooglePlacesAutocompleteAdapter mGooglePlaceAdapter;
    private ConnectionDetector mConnectionDetector;
    private ImageView searchIcon;

    private TextView mAccBalance;
    private String response,token,getAccBal,mAmount , userName ;

    private ProfileInfoModel profileEModel;
    private EditProfileModelUpdate profileEditModel;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor1;
    private JsonUtil mJsonUtil;
    private ProgressDialog progress;
    private boolean successFlag, successAlertFlag;
    LinearLayout ll_drawerheader;
    private int user_id_int, accBalence ;
    private DeviceDetailsInfo mdeviceDetailsInfo;
    String appVersion;

    public static final String ACC_BALENCE = "ACC_BALENCE";
    private boolean homeFlag;
    public static final String FAVLATRETURN = "FAVLATRETURN";
    public static final String FAVLATRETURNFLAG = "FAVLATRETURNFLAG";
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String FIRSTNAME_ID = "FRISTNAME_PREFS_String";
    public static final String LASTNAME_ID = "LASTNAME_PREFS_String";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";
    public static final String EMAIL_ID = "EMAIL_ID_PREFS_String";
    public static final String USERNAME_ID = "USERNAME_ID_PREFS_String";
    public static final String USER_REG_ID = "USERID_REG_String";

    int i =0;
    private int paymentCheck;
    private boolean paymentPopup = false;

   /* @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
//        searchIcon = (ImageView) findViewById(R.id.search_icon);

        mGooglePlaceAdapter = new GooglePlacesAutocompleteAdapter(NavigationDrawerActivity.this, R.layout.search_list);
        mConnectionDetector = new ConnectionDetector(this);
        mdeviceDetailsInfo = new DeviceDetailsInfo();
//        initRecyclerView();
//        initFab();
        initToolbar();
        setupDrawerLayout();

        content = findViewById(R.id.content);
        headerNavigation = (NavigationView) findViewById(R.id.navigation_view);
        header = headerNavigation.inflateHeaderView(R.layout.drawer_header);

        /*footerNavigation = (NavigationView) findViewById(R.id.navigation_view) ;
        footer = footerNavigation.inflateHeaderView(R.layout.drawer_footer);*/

        firstName = (TextView)header.findViewById(R.id.tv_drawer_head_fname);
        lastName = (TextView)header.findViewById(R.id.tv_drawer_head_lname);
        userHeader = (TextView) header.findViewById(R.id.usernameHeader);
        emailHeader = (TextView) header.findViewById(R.id.emailHeader);
        emailHeader.setVisibility(View.GONE);

        ll_drawerheader =(LinearLayout)header.findViewById(R.id.ll_drawer_header);
        ll_drawerheader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavigationDrawerActivity.this , CreditCardPayActivity.class);
                startActivity(intent);
            }
        });
        mAccBalance = (TextView)header. findViewById(R.id.accBalance);

        tv_appVersion = (TextView) findViewById(R.id.navigation_appversion);

//        fragment = new MapsFragment();
//          fragment = new MapsFragmentTest();
//          fragment = new MapsFragmentAllStations();
//          fragment = new MapsFragmentAllStationsRoute();

        fragment = new MapsFragmentComplete();

        navigationView.setCheckedItem(R.id.drawer_home);
        initFragment();

        prefs = PreferenceManager.getDefaultSharedPreferences(NavigationDrawerActivity.this.getApplicationContext());
        editor1 = prefs.edit();


        user_id_int = prefs.getInt(USER_ID, 0);
        token = prefs.getString(TOKEN_ID, " ");

        OnePocketLog.d("userIDNavigation" , String.valueOf(user_id_int));

        paymentCheck = prefs.getInt(ACC_BALENCE , 0 );
        paymentPopup = prefs.getBoolean(PAYMENT_FLAG_ALERT, false);


        first_name = prefs.getString(FIRSTNAME_ID ," ");
        //last_name = prefs.getString(LASTNAME_ID, " ");
        user_id = prefs.getString(USERNAME_ID, "NOT FOUND");
        email_id = prefs.getString(EMAIL_ID, "NOT FOUND");

        firstName.setText(first_name);

        OnePocketLog.d("Accountpayment", String.valueOf(paymentCheck));

        //OnePocketLog.d("AccountBalance1234455", String.valueOf(paymentCheck));

       // OnePocketLog.d("FIRSTNAME-----", String.valueOf(firstName));

        //String lastName = "";
        if (prefs.getString(LASTNAME_ID , "").contains("null"))
        {
            last_name = " ";
        }
        else {
            last_name = prefs.getString(LASTNAME_ID, " ");
        }

        lastName.setText(last_name);

       // OnePocketLog.d("LASTNAME-----", String.valueOf(lastName));

        userHeader.setText(user_id);
        //OnePocketLog.d("USERNAME-----", String.valueOf(userHeader));

        emailHeader.setText(email_id);
        //OnePocketLog.d("EMAILHEADER-----", String.valueOf(emailHeader));


        initDeviceDetailsInfo();

        appVersion = mdeviceDetailsInfo.getAppVersion();

        tv_appVersion.setText(appVersion);

        OnePocketLog.d("APP" , appVersion);

        new deviceDetailsTask().execute();

        if ( paymentCheck < 20) {

            OnePocketLog.d("NavigationExistingPaymentBalance" , String.valueOf(paymentCheck));

            showExistingPaymentAlertBox(getString(R.string.existingUserPaymentPopUp));

        }

        // Initializing Drawer Layout and ActionBarToggle
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                if(CONTACTUS_FLAG){
                    navigationView.setCheckedItem(R.id.drawer_home);
                    CONTACTUS_FLAG = false;
                }
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

//        final ImageView avatar = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.avatar);
//        Picasso.with(this).load(AVATAR_URL).transform(new CircleTransform()).into(avatar);

       /* if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            setRecyclerAdapter(recyclerView);
        }*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        new BalenceCheck().execute();

    }

    public void showExistingPaymentAlertBox(final String msg) {

        android.app.AlertDialog.Builder alertDialogBuilderPayment = new android.app.AlertDialog.Builder(NavigationDrawerActivity.this);
        //alertDialogBuilderPayment.setTitle(R.string.Alert);
        //alertDialogBuilderPayment.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilderPayment
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton( "Go to My Wallet",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        Intent intent = new Intent(NavigationDrawerActivity.this , CreditCardPayActivity.class);
                        startActivity(intent);

                    }
                });
        alertDialogBuilderPayment
                .setNegativeButton("No,Thanks",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        dialog.cancel();

                    }
                });

        final android.app.AlertDialog alertDialogPayment = alertDialogBuilderPayment.create();
        alertDialogPayment.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialogPayment.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialogPayment.show();

    }

    public class BalenceCheck extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.USER_BALENCE_CHECK()+user_id_int;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    JSONObject jObject = new JSONObject(response);
                    JSONArray jObject1 = jObject.getJSONArray("accountses");
                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp = jObject1.getJSONObject(i);
                        getAccBal = mTemp.getString("accountBalance");
                        accBalence = mTemp.getInt("accountBalance");


                        NumberFormat num = NumberFormat.getCurrencyInstance(Locale.US);
//                        BigDecimal bd = new BigDecimal(getAccBal.toString());
//                        mAmount = num.format(getAccBal);
                        mAccBalance.setText("");
                        mAccBalance.setText(num.format(new BigDecimal(getAccBal)));

                        try {
//                            accBalence = Integer.parseInt(getAccBal);
                            editor1.putInt(ACC_BALENCE, accBalence);
                        } catch (NumberFormatException e) {
                            System.out.println("parse value is not valid : " + e);
                        }
                    }

                        /*Fragment currentFragment = new PaymentsFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.detach(currentFragment);
                        fragmentTransaction.attach(currentFragment);
                        fragmentTransaction.commit();*/
                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    private void initDeviceDetailsInfo() {

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

        mdeviceDetailsInfo.setAppVersion(version);
        mdeviceDetailsInfo.setDeviceName(Build.MANUFACTURER);
        mdeviceDetailsInfo.setDeviceType("Android");
        mdeviceDetailsInfo.setDeviceVersion(Build.VERSION.RELEASE);
        mdeviceDetailsInfo.setUserId(user_id_int);

        OnePocketLog.d("APPINFO" , mdeviceDetailsInfo.getAppVersion());
        OnePocketLog.d("DEVICENAME" , mdeviceDetailsInfo.getDeviceName());
        OnePocketLog.d("DEVICETYPE" , mdeviceDetailsInfo.getDeviceType());
        OnePocketLog.d("DEVICEVERSION" , mdeviceDetailsInfo.getDeviceVersion());
        OnePocketLog.d("USERID" , String.valueOf(mdeviceDetailsInfo.getUserId()));

    }

    public class deviceDetailsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.DEVICE_DETAILS_URL();
                response = mJsonUtil.postMethod(stringurl,null,null,null,null,null,null,null,null,null,null,null,mdeviceDetailsInfo);
                OnePocketLog.d("deviceDetails resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("deviceDetails error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
        }
    }


    private void showProgress(String msg) {
        progress = ProgressDialog.show(this, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        if (!successAlertFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successAlertFlag= false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                        if (msg.equals("Profile updated successfully")) {
                            finish();
                            new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                            Intent intent1 = new Intent(NavigationDrawerActivity.this, NavigationDrawerActivity.class);
                            startActivity(intent1);
                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

        /*AlertDialog alertDialog = new AlertDialog.Builder(
                this).create();
        if (!successAlertFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successAlertFlag = false;
        alertDialog.setMessage(msg);

        String ok = getString(R.string.ok);
        SpannableString okString = new SpannableString(ok);
        okString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primary)), 0, okString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        alertDialog.setButton(okString, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (msg.equals("Profile updated successfully")) {
                    finish();
                    Intent intent1 = new Intent(EditProfile.this, LoginActivity.class);
                    startActivity(intent1);
                }

            }
        });
        alertDialog.show();*/
    }

   /* @Override public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        setRecyclerAdapter(recyclerView);
        recyclerView.scheduleLayoutAnimation();
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

    }

    private void setRecyclerAdapter(RecyclerView recyclerView) {
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(items);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }*/

   /* private void initFab() {
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Snackbar.make(content, "FAB Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
    }*/

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

     /*   if (actionBar != null) {
            actionBar.setTitle("1Pocket");
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
//            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
        }*/
      /*  LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.search_bar, null);*/


      /*  final ImageView searchIcon = (ImageView) v.findViewById(R.id.searchIcon);
        // the view that contains the new clearable autocomplete text view
        final ClearableAutoCompleteTextView searchBox =  (ClearableAutoCompleteTextView) v.findViewById(R.id.autoCompleteTextView);

        // start with the text view hidden in the action bar
        searchBox.setVisibility(View.INVISIBLE);
        searchIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleSearch(false);
            }
        });

        searchBox.setOnClearListener(new ClearableAutoCompleteTextView.OnClearListener() {

            @Override
            public void onClear() {
                toggleSearch(true);
            }
        });

        searchBox.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // handle clicks on search resaults here
            }

        });*/

       /* AutoCompleteTextView textView = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteTextView);
        textView.setAdapter(mGooglePlaceAdapter);
        textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

            }
        });
        actionBar.setCustomView(v);*/
    }

   /* protected void toggleSearch(boolean reset) {
        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        ImageView searchIcon = (ImageView) findViewById(R.id.searchIcon);
        if (reset) {
            // hide search box and show search icon
            searchBox.setText("");
            searchBox.setVisibility(View.GONE);
            searchIcon.setVisibility(View.VISIBLE);
            // hide the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
        } else {
            // hide search icon and show search box
            searchIcon.setVisibility(View.GONE);
            searchBox.setVisibility(View.VISIBLE);
            searchBox.requestFocus();
            // show the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(searchBox, InputMethodManager.SHOW_IMPLICIT);
        }

    }*/

    private void setupDrawerLayout() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
//        navigationView.getMenu().getItem(0).setChecked(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
               /* Snackbar.make(content, menuItem.getTitle() + " pressed", Snackbar.LENGTH_LONG).show();
                menuItem.setChecked(true);
                drawerLayout.closeDrawers();*/
                int id;

               /* if(fragmentManager.findFragmentById(R.id.frameLayout) == null){
                    fragment = new MapsFragment();
                    initFragment();
                }*/
                new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                drawerLayout.closeDrawers();

                id = menuItem.getItemId();
                Intent intent = null;
              /*  if(searchIcon.isActivated()) {
                    searchIcon.setVisibility(View.INVISIBLE);
                }*/
                switch (id) {

                    case R.id.drawer_home:
//                        fragment = new MapsFragment();
//                        fragment = new MapsFragmentTest();
//                        fragment = new MapsFragmentAllStations();
//                        fragment = new MapsFragmentAllStationsRoute();
                        new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                        fragment = new MapsFragmentComplete();
                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    case R.id.drawer_accounts:

                        new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);

                        fragment = new AccountFragment();

                        //Intent intent1 = new Intent(NavigationDrawerActivity.this , AccountsListActivity.class);
                        //startActivity(intent1);

                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                        Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    case R.id.drawer_charging_activity:
//                        fragment = new MapsFragment();
//                        fragment = new MapsFragmentTest();
//                        fragment = new MapsFragmentAllStations();
//                        fragment = new MapsFragmentAllStationsRoute();

                        new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                        fragment = new ChargeActivityFragment();
                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();
                        }
                        return true;
                   /* case R.id.drawer_stats:
                        fragment = new StatsFragment();
                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();

                        }
                       *//* intent = new Intent(NavigationDrawerActivity.this,StatsActivity.class);
                        startActivity(intent);*//*
                        return true;*/

                    case R.id.drawer_favourites:
                        new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                        fragment = new FavoriteListFragment();
                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();

                        }
                        return true;

                    case R.id.drawer_dralerts:
                        new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                        //fragment = new DrAlertsFragment();

                        Intent intent2 = new Intent(NavigationDrawerActivity.this , DrAlertsListActivity.class );
                        startActivity(intent2);

                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();

                        }
                        return true;

                    case R.id.drawer_contactus:
                        new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                        fragment = new ContactUsFragment();
                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    case R.id.drawer_aboutus:
                        new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                        //fragment = new MapsFragmentComplete();
                        if (mConnectionDetector.isOnline()) {
                            //initFragment();
                            Intent intentabout = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://telluspowertech.com/about-us/"));
                            startActivity(intentabout);
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();

                        }
                        return true;
                    case R.id.drawer_Logout:
                        new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
                        initLogoutAlert();
                       /* fragment = new LogoutFragment();
//                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        *//*}else{
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();

                        }*/
                        return true;

                    default:
//                        intent = new Intent(NavigationDrawerActivity.this, MapsFragment.class);
//                        startActivity(intent);
                        return true;
                  /*  case R.id.drawer_stats:
                        fragment = new StatsFragment();
                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();

                        }
                       *//* intent = new Intent(NavigationDrawerActivity.this,StatsActivity.class);
                        startActivity(intent);*//*
                        return true;*/
                  /*  case R.id.drawer_Payments:
                        fragment = new PaymentsFragment();
                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();

                        }
                        return true;*/
                  /*  case R.id.drawer_settings:
                        fragment = new SettingsFragment();
                        if (mConnectionDetector.isOnline()) {
                            initFragment();
                        } else {
                            Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();

                        }
                        return true;*/


                }
            }
        });
    }

    public void initLogoutAlert() {
        new AlertDialog.Builder(NavigationDrawerActivity.this)
                .setTitle(R.string.Alert)
                .setMessage(R.string.exitWarning)
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
//                        NavigationDrawerActivity.super.onBackPressed();
                        if (getFragmentManager().getBackStackEntryCount() == 0) {

                        } else {
                            getFragmentManager().popBackStack();
                        }
                        finish();
                        Intent intent = new Intent(NavigationDrawerActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }).create().show();
    }

    public void initFragment() {
        if (fragment != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frameLayout, fragment).addToBackStack(null).commit();

            new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
            // update selected item and title, then close the drawer
//                mDrawerList.setItemChecked(position, true);
//                mDrawerList.setSelection(position);
//                mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.d("NavigationDrActivity", "Error  creating fragment");
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbaritems, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_search:
                // search action
                return true;
            case R.id.action_gpslocater:
                // location found
                MapsFragment mMapsFragment = (MapsFragment)getSupportFragmentManager().findFragmentById(R.id.map);
                mMapsFragment.LocationFound();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.closeDrawer(GravityCompat.START);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
       /* super.onBackPressed();
        AlertDialog.Builder builder = new AlertDialog.Builder(NavigationDrawerActivity.this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();*/
        /*new AlertDialog.Builder(this)
                .setTitle(R.string.Alert)
                .setMessage(R.string.exitWarning)
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
//                        NavigationDrawerActivity.super.onBackPressed();
                       *//* if (getFragmentManager().getBackStackEntryCount() == 0) {

                        } else {
                            getFragmentManager().popBackStack();
                        }*//*
                        finish();
                        Intent intent = new Intent(NavigationDrawerActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }).create().show();*/

        Fragment fName = getSupportFragmentManager().findFragmentById(R.id.drawer_home);

       if(fragment.getClass().getName().equals(MapsFragmentComplete.class.getName())){
           setFavFalg();
           //finish();
           //System.exit(0);

       }else{
           new KeyBoardHide().hideKeyBoard(NavigationDrawerActivity.this);
           fragment = new MapsFragmentComplete();
           navigationView.setCheckedItem(R.id.drawer_home);
           if (mConnectionDetector.isOnline()) {
               initFragment();
           } else {
               Toast.makeText(NavigationDrawerActivity.this, getResources().getString(R.string.netNotAvl),
                       Toast.LENGTH_SHORT).show();

           }
       }
    }

    @Override
    protected void onDestroy() {
//        android.os.Process.killProcess(android.os.Process.myPid());
        super.onDestroy();
        setFavFalg();
    }

    private void setFavFalg(){
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = pSharedPref.edit();

        editor.putString(FAVLATRETURN, "");
        editor.putBoolean(FAVLATRETURNFLAG, false);
        editor.commit();
    }
/* @Override
    public void onItemClick(View view, ViewModel viewModel) {
        DetailActivity.navigate(this, view.findViewById(R.id.image), viewModel);
    }*/
}
