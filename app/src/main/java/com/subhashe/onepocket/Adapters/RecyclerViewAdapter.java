/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subhashe.onepocket.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.subhashe.onepocket.Core.FavModel;
import com.subhashe.onepocket.Core.ViewModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import java.util.HashMap;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<FavModel> favListItems;
    private Activity mActivity;
    private int posDelete;
    private ProgressDialog progress;
    private String response,favHeader;
    private JsonUtil mJsonUtil;
    private OnItemClickListener onItemClickListener;
    Intent intent1=new Intent();
    ViewHolder mView ;
    public static final String FAVLATRETURN = "FAVLATRETURN";
    public static final String FAVLATRETURNFLAG = "FAVLATRETURNFLAG";

    public RecyclerViewAdapter(Activity mActivity,List<FavModel> mfavListItems) {
        this.mActivity = mActivity;
        this.favListItems = mfavListItems;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_favlistitems, parent, false);
//        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    public void remove(int position ) {
//        int position = favListItems.indexOf(item);
        favListItems.remove(position);
        new deleteFavList().execute();
        notifyItemRemoved(position);
        if(favListItems.size() == 0) {
//            mView.mlistCount.setVisibility(View.GONE);
            mView.mfavEmptyLayoutList.setVisibility(View.VISIBLE);
        }
    }

    @Override public void onBindViewHolder(final ViewHolder holder, int position) {
        final FavModel favModel = favListItems.get(position);
        final int pos = position;
        mView = holder;
        mView.mlistCount.setVisibility(View.VISIBLE);
        final String temp = favModel.getStationAddModel().toString();
        holder.mStationName.setText(favModel.getStationNameModel());

        String addAvailable = "";
        if(favModel.getStationAddressModel().contains("null")){
            addAvailable = "Not Available";
        }else{
            addAvailable = favModel.getStationAddressModel();
        }
        holder.mStationAddress.setText(addAvailable);

        //holder.mStationAddress.setText(favModel.getStationAddressModel());

        holder.mFavNavigationImge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity.getApplicationContext());
                SharedPreferences.Editor editor = pSharedPref.edit();


                editor.putString(FAVLATRETURN, temp);
                editor.putBoolean(FAVLATRETURNFLAG,true);
                editor.commit();

                /*Bundle args = new Bundle();
                args.putString("favLatLng", temp);ssssss
                args.putBoolean("FAVFLAG",true);

                intent1.putExtra("bundle",args);

            *//*intent.putExtra("startLatLangValueFromChargerInfo",start);
            intent.putExtra("endLatLangValueFromChargerInfo",end);*//*

                mActivity.setResult(8,intent1);*/
                mActivity.onBackPressed();
            }
        });

        holder.mfavDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                posDelete =holder.getLayoutPosition();
                new AlertDialog.Builder(mActivity)
                        .setTitle(R.string.Alert)
                        .setMessage(R.string.favoriteAlertDelete)
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {
                                favHeader = "idFav=" + favModel.getIdFav() + "&" + "userId=" + favModel.getUserId();

                                OnePocketLog.d("favheader", favHeader);

                                if(favListItems.size() > 0) {
                                    holder.mfavEmptyLayoutList.setVisibility(View.INVISIBLE);
                                    if(favListItems.size() == 1){
                                        remove(0);
                                        holder.mfavEmptyLayoutList.setVisibility(View.VISIBLE);
                                    }else {
                                        remove(posDelete);
                                    }
                                }else if (favListItems.size() == 0){
                                    holder.mfavEmptyLayoutList.setVisibility(View.VISIBLE);
                                }
                            }
                        }).create().show();


            }
        });
    }

    public class deleteFavList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(mActivity.getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.DELETEFAVORITELIST();
                response = mJsonUtil.postMethod(stringurl,null,null,null,null,null,null,null,favHeader,null,null,null,null);
                OnePocketLog.d("favoritelsit delete resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("favoritelsit delete  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
        }
    }


    private void showProgress(String msg) {
        progress = ProgressDialog.show(mActivity, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }


    @Override public int getItemCount() {
        return favListItems.size();
    }

  /*  @Override public void onClick(final View v) {
        onItemClickListener.onItemClick(v, (ViewModel) v.getTag());
    }*/

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mStationName, mStationAddress , mfavDelete;
        public ImageView mFavNavigationImge;
        public LinearLayout mfavEmptyLayoutList,mlistCount;

        public ViewHolder(View itemView) {
            super(itemView);
            mStationName = (TextView) itemView.findViewById(R.id.favStationName);
            mStationAddress = (TextView) itemView.findViewById(R.id.favStationAddress);
            mFavNavigationImge = (ImageView) itemView.findViewById(R.id.favNavigation);
            mfavDelete = (TextView) itemView.findViewById(R.id.favDelete);
            mfavEmptyLayoutList = (LinearLayout)itemView.findViewById(R.id.favEmptyLayoutList);
            mlistCount = (LinearLayout)itemView.findViewById(R.id.listCount);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, ViewModel viewModel);

    }
}
