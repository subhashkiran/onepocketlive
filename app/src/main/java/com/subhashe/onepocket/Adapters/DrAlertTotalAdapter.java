/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subhashe.onepocket.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.subhashe.onepocket.Activities.DrAlertEventsMoreInfo;
import com.subhashe.onepocket.Core.DrTotalEventsModel;
import com.subhashe.onepocket.Core.ViewModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.OnePocketLog;

import java.util.HashMap;
import java.util.List;

public class DrAlertTotalAdapter  extends RecyclerView.Adapter<DrAlertTotalAdapter .ViewHolder> {

    private List<DrTotalEventsModel> drAlertTotalListItems;
    private Activity mActivity;
    private String startdate, enddate , starttime , endtime , signalname , signaltype , status;
    HashMap<String, String> moreInfoTotalData;
    int pos = 0;

    private OnItemClickListener onItemClickListener;
    Intent intent1=new Intent();
    ViewHolder mView ;


    public DrAlertTotalAdapter (Activity mActivity, List<DrTotalEventsModel> drAlertTotalListItems) {
        this.mActivity = mActivity;
        this.drAlertTotalListItems = drAlertTotalListItems;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_dralertlist, parent, false);
//        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    public void remove(int position ) {
//        int position = favListItems.indexOf(item);
        drAlertTotalListItems.remove(position);

        notifyItemRemoved(position);
        if(drAlertTotalListItems.size() == 0) {
//            mView.mlistCount.setVisibility(View.GONE);
            mView.mdrAlertEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override public void onBindViewHolder(final ViewHolder holder, int position) {
        final DrTotalEventsModel mDrtotalEventsModel = drAlertTotalListItems.get(position);
        pos = position;
        mView = holder;

        holder.mDrAlertStartDtTime.setText(mDrtotalEventsModel.getStartDate());
        holder.mDrAlertStatus.setText(mDrtotalEventsModel.getStatus());
        holder.mDrAlertSignalName.setText(mDrtotalEventsModel.getSignalName());

        OnePocketLog.d("drlistStartDate", mDrtotalEventsModel.getStartDate());
        OnePocketLog.d("drlistSignalName", mDrtotalEventsModel.getSignalName());
        OnePocketLog.d("drlistStatus", mDrtotalEventsModel.getStatus());

        holder.ll_dralert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pos =holder.getLayoutPosition() + 1;


                startdate = mDrtotalEventsModel.getStartDate();
                enddate = mDrtotalEventsModel.getEndDate();
                starttime = mDrtotalEventsModel.getStartTime();
                endtime = mDrtotalEventsModel.getEndTime();
                signalname = mDrtotalEventsModel.getSignalName();
                signaltype = mDrtotalEventsModel.getSignalType();
                status = mDrtotalEventsModel.getStatus();


                moreInfoTotalData = new HashMap<String, String>();
                moreInfoTotalData.put("STARTDATE", startdate);
                moreInfoTotalData.put("STARTTIME", enddate);
                moreInfoTotalData.put("ENDDATE", starttime);
                moreInfoTotalData.put("ENDTIME", endtime);
                moreInfoTotalData.put("SIGNALNAME", signalname);
                moreInfoTotalData.put("SIGNALTYPE", signaltype);
                moreInfoTotalData.put("STATUS", status);

                Intent intent = new Intent(mActivity, DrAlertEventsMoreInfo.class);
                intent.putExtra("moretotaldata",moreInfoTotalData);
                mActivity.startActivity(intent);


            }
        });

    }

    @Override
    public int getItemCount() {

        OnePocketLog.d("drtotallistsize", String.valueOf(drAlertTotalListItems.size()));

        return drAlertTotalListItems.size();

    }



    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mDrAlertStartDtTime,  mDrAlertSignalName, mDrAlertStatus ;
        public LinearLayout mdrAlertEmptyLayout , ll_dralert;

        public ViewHolder(View itemView) {
            super(itemView);

            mDrAlertStartDtTime = (TextView) itemView.findViewById(R.id.drAlertStartDtTime);
            mDrAlertSignalName = (TextView) itemView.findViewById(R.id.drAlertSignalName);
            mDrAlertStatus = (TextView) itemView.findViewById(R.id.drAlertStatus);
            mdrAlertEmptyLayout = (LinearLayout) itemView.findViewById(R.id.drAlertEmptyLayout);
            ll_dralert = (LinearLayout) itemView.findViewById(R.id.ll_dralerts);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, ViewModel viewModel);

    }
}
