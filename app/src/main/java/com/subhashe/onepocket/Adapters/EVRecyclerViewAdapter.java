package com.subhashe.onepocket.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;

import com.subhashe.onepocket.Activities.EVEditActivity;
import com.subhashe.onepocket.Core.EvModel;
import com.subhashe.onepocket.Core.EvModelAcc;
import com.subhashe.onepocket.Core.ViewModel;
import com.subhashe.onepocket.Core.Year;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EVRecyclerViewAdapter extends RecyclerView.Adapter<EVRecyclerViewAdapter.ViewHolder> {

    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String USER_ID = "USER_ID";

    private List<EvModel> mevListEdit;
    private EvModel evModel;
    private EvModelAcc evModelAcc;
    private Activity mActivity;
    private ProgressDialog progress;
    private String response;
    private String evHeader, evUpdateHeader;
    private int posDelete, posEdit;
    private String token;
    private JsonUtil mJsonUtil;
    private OnItemClickListener onItemClickListener;
    ViewHolder mView;
    private ConnectionDetector mConnectionDetector;
    private boolean firstFlag, firsttime, evAddError , evFlag;
    JSONObject jsonObject, addressObject;
    private boolean successFlag, successAlertFlag;
    PopupWindow popup;
    boolean editEVFlag = false;
    private String model, make, year, evYear , item2 , sItem;
    private long vehicleId;
    private int user_id;
    private long userId;
    private String mYearTemp, mMakeTemp, mModelTemp;
    LinearLayout ll_actionbar;

    int mSQid1, mQid1;
    ArrayList<Year> arraylistEditYear = new ArrayList<Year>();
    public static final String YEAR_ID = "YEAR_ID";
    public static final String FAST_SWITCH = "FAST_SWITCH";

    private Fragment fragment = null;
    HashMap<String, String> moreInfoData;
    HashMap<String, String> moreInfoYear;
    Switch dcFastSwitchEV;
    ImageView img_back;
    int spinnerPosition = 0;
    String[] SelectYears = new String[]{ "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010","2009","2008","2007","2006","2005"};

    public EVRecyclerViewAdapter(Activity mActivity, List<EvModel> mevListEdit) {
        this.mActivity = mActivity;
        this.mevListEdit = mevListEdit;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_evlistitems, parent, false);

        return new ViewHolder(v);
    }

    public void remove(int position) {

        mevListEdit.remove(position);
        new deleteEVList().execute();
        notifyItemRemoved(position);

        notifyItemRangeChanged(position, mevListEdit.size());

        notifyDataSetChanged();

        notifyItemRangeRemoved(0, position);

        OnePocketLog.d("EVLISTSIZE----------", String.valueOf(mevListEdit.size()));

        if (mevListEdit.size() == 0) {
            mView.evEmptyLayoutList.setVisibility(View.VISIBLE);
        }
    }

    public void edit(int evPosition) {

        mevListEdit.get(evPosition);

        notifyItemRangeChanged(evPosition, mevListEdit.size());

        notifyDataSetChanged();


        OnePocketLog.d("EVLISTEDITSIZE----------", String.valueOf(mevListEdit.size()));

    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        final EvModel evModel = mevListEdit.get(position);
        final EvModel evModel1 = mevListEdit.get(position);

        final int pos = position;
        mView = holder;

        evModelAcc = new EvModelAcc();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity.getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(ACC_USER_ID, 0);
        userId = prefs.getLong(USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

        mSQid1 = prefs.getInt(YEAR_ID, 0);

        OnePocketLog.d("YEarID", String.valueOf(mSQid1));

        mConnectionDetector = new ConnectionDetector(mActivity);

        mView.evlistCount.setVisibility(View.VISIBLE);

        final String temp = evModel.getModel();

        holder.tv_model.setText(evModel.getModel());
        holder.tv_make.setText(evModel.getMake());
        holder.tv_year.setText(evModel.getYear());

        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                posDelete = holder.getLayoutPosition();
                new AlertDialog.Builder(mActivity)
                        .setTitle(R.string.Alert)
                        .setMessage(R.string.evAlertDelete)
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {

                                evHeader = "{" + "\"year\":" + "\"" + evModel.getYear() + "\"" + "," + "\"model\":" + "\"" + evModel.getModel()
                                        + "\"" + "," + "\"make\":" + "\"" + evModel.getMake() + "\"" + "," + "\"vehicleId\":" + "\"" +
                                        evModel.getVehicleId() + "\"" + "}";


                                OnePocketLog.d("VehicleId----------", String.valueOf(evModel.getVehicleId()));

                                OnePocketLog.d("delete header----------", evHeader);

                                if (mevListEdit.size() > 0) {
                                    holder.evEmptyLayoutList.setVisibility(View.INVISIBLE);
                                    if (mevListEdit.size() == 1) {
                                        remove(0);
                                        holder.evEmptyLayoutList.setVisibility(View.VISIBLE);
                                    } else {
                                        remove(posDelete);
                                    }
                                } else if (mevListEdit.size() == 0) {
                                    holder.evEmptyLayoutList.setVisibility(View.VISIBLE);
                                } else if (mevListEdit.isEmpty()) {
                                    holder.evEmptyLayoutList.setVisibility(View.VISIBLE);
                                }
                            }
                        }).create().show();
            }
        });


        holder.tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(mActivity);

                posEdit =holder.getLayoutPosition();

                vehicleId = evModel.getVehicleId();

                make = evModel.getMake();
                model = evModel.getModel();
                year = evModel.getYear();

                moreInfoData = new HashMap<String, String>();
                moreInfoData.put("VEHICLEID", String.valueOf(vehicleId));
                moreInfoData.put("MAKE", make);
                moreInfoData.put("MODEL", model);
                moreInfoData.put("YEAR", year);

                Intent intent = new Intent(mActivity, EVEditActivity.class);
                intent.putExtra("moreevdata",moreInfoData);
                mActivity.startActivity(intent);

            }
        });

    }

    public class deleteEVList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(mActivity.getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.DELETEEVLIST();
                response = mJsonUtil.postMethod(stringurl, null, null, null, null, null, null, null, evHeader, null, null, null, null);

                OnePocketLog.d("evlist delete resp----------", response);

            } catch (Exception e) {
                OnePocketLog.d("evlist delete  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
        }
    }

    public void showAlertBox(final String msg) {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mActivity);
        if (!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(mActivity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                        evAddError = false;
                        if (msg.contains("EV updated successfully!")) {
                            mActivity.finish();
                            new KeyBoardHide().hideKeyBoard(mActivity);
                            /*Intent intent1 = new Intent(mActivity, EVListActivity.class);
                            mActivity.startActivity(intent1);*/
                            popup.dismiss();

                        }
                    }
                });

        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(mActivity.getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(mActivity, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }


    @Override
    public int getItemCount() {
        return mevListEdit.size();
    }


    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_model, tv_make, tv_year, tv_edit, tv_delete;
        public ImageView evImage;
        public LinearLayout evlistCount, evEmptyLayoutList;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_model = (TextView) itemView.findViewById(R.id.ev_list_model);
            tv_make = (TextView) itemView.findViewById(R.id.ev_list_make);
            tv_year = (TextView) itemView.findViewById(R.id.ev_list_year);

            tv_edit = (TextView) itemView.findViewById(R.id.ev_list_edit);
            tv_delete = (TextView) itemView.findViewById(R.id.ev_list_delete);

            evImage = (ImageView) itemView.findViewById(R.id.ev_list_img);
            evEmptyLayoutList = (LinearLayout) itemView.findViewById(R.id.evEmptyLayoutList);
            evlistCount = (LinearLayout) itemView.findViewById(R.id.ev_listCount);
        }
    }
    public interface OnItemClickListener {

        void onItemClick(View view, ViewModel viewModel);

    }
}