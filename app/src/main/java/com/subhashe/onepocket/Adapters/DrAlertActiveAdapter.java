/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subhashe.onepocket.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.subhashe.onepocket.Core.DrActiveModel;
import com.subhashe.onepocket.Core.ViewModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.JsonUtil;

import java.util.List;

public class DrAlertActiveAdapter extends RecyclerView.Adapter<DrAlertActiveAdapter.ViewHolder> {

    private List<DrActiveModel> drAlertActiveListItems;
    private Activity mActivity;
    private OnItemClickListener onItemClickListener;
    Intent intent1=new Intent();
    ViewHolder mView ;


    public DrAlertActiveAdapter(Activity mActivity, List<DrActiveModel> drAlertActiveListItems) {
        this.mActivity = mActivity;
        this.drAlertActiveListItems = drAlertActiveListItems;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_dralertactivelist, parent, false);
//        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    public void remove(int position ) {
//        int position = favListItems.indexOf(item);
        drAlertActiveListItems.remove(position);

        notifyItemRemoved(position);
        if(drAlertActiveListItems.size() == 0) {
//            mView.mlistCount.setVisibility(View.GONE);
            mView.mDrAlertActiveEmptyLayoutList.setVisibility(View.VISIBLE);
        }
    }

    @Override public void onBindViewHolder(final ViewHolder holder, int position) {
        final DrActiveModel mDrActiveModel = drAlertActiveListItems.get(position);
        final int pos = position;
        mView = holder;
       /* final String temp = mDrActiveModel.getStationAddModel().toString();
        holder.mStationName.setText(mDrActiveModel.getStationNameModel());

        String addAvailable = "";
        if(mDrActiveModel.getStationAddressModel().contains("null")){
            addAvailable = "Not Available";
        }else{
            addAvailable = mDrActiveModel.getStationAddressModel();
        }
        holder.mStationAddress.setText(addAvailable);*/

        holder.mDrAlertActiveName.setText(mDrActiveModel.getSignalName());
        holder.mDrAlertActiveNameStartDtTime.setText(mDrActiveModel.getStartDate());
        holder.mDrAlertActiveNameEndDtTime.setText(mDrActiveModel.getEndDate());
        holder.mDrAlertActiveNameSignalType.setText(mDrActiveModel.getSignalType());
    }

    @Override public int getItemCount() {
        return drAlertActiveListItems.size();
    }

  /*  @Override public void onClick(final View v) {
        onItemClickListener.onItemClick(v, (ViewModel) v.getTag());
    }*/

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mDrAlertActiveNameStartDtTime , mDrAlertActiveNameEndDtTime , mDrAlertActiveName, mDrAlertActiveNameSignalType;
        public LinearLayout mDrAlertActiveEmptyLayoutList;

        public ViewHolder(View itemView) {
            super(itemView);
            mDrAlertActiveName = (TextView) itemView.findViewById(R.id.drAlertActiveName);
            mDrAlertActiveNameStartDtTime = (TextView) itemView.findViewById(R.id.drAlertActiveStartDtTime);
            mDrAlertActiveNameEndDtTime = (TextView) itemView.findViewById(R.id.drAlertActiveEndDtTime);
            mDrAlertActiveNameSignalType = (TextView) itemView.findViewById(R.id.drAlertActiveSignalType);
            mDrAlertActiveEmptyLayoutList = (LinearLayout)itemView.findViewById(R.id.drAlertActiveEmptyLayout);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, ViewModel viewModel);

    }
}
