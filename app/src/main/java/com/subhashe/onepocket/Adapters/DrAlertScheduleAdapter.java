/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subhashe.onepocket.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.subhashe.onepocket.Core.DrActiveModel;
import com.subhashe.onepocket.Core.DrScheduleModel;
import com.subhashe.onepocket.Core.ViewModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;

import java.util.List;

public class DrAlertScheduleAdapter extends RecyclerView.Adapter<DrAlertScheduleAdapter.ViewHolder> {

    private List<DrScheduleModel> drAlertScheduleListItems;
    private Activity mActivity;

    private OnItemClickListener onItemClickListener;
    Intent intent1=new Intent();
    ViewHolder mView ;


    public DrAlertScheduleAdapter(Activity mActivity, List<DrScheduleModel> drAlertScheduleListItems) {
        this.mActivity = mActivity;
        this.drAlertScheduleListItems = drAlertScheduleListItems;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_dralertschedulelist, parent, false);
//        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    public void remove(int position ) {
//        int position = favListItems.indexOf(item);
        drAlertScheduleListItems.remove(position);

        notifyItemRemoved(position);
        if(drAlertScheduleListItems.size() == 0) {
//            mView.mlistCount.setVisibility(View.GONE);
            mView.mdrAlertScheduleEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override public void onBindViewHolder(final ViewHolder holder, int position) {
        final DrScheduleModel mDrScheduleModel = drAlertScheduleListItems.get(position);
        final int pos = position;
        mView = holder;
       /* final String temp = mDrActiveModel.getStationAddModel().toString();
        holder.mStationName.setText(mDrActiveModel.getStationNameModel());

        String addAvailable = "";
        if(mDrActiveModel.getStationAddressModel().contains("null")){
            addAvailable = "Not Available";
        }else{
            addAvailable = mDrActiveModel.getStationAddressModel();
        }
        holder.mStationAddress.setText(addAvailable);*/

        holder.mDrAlertScheduleStartDtTime.setText(mDrScheduleModel.getStartDate());
        holder.mDrAlertScheduleEndDtTime.setText(mDrScheduleModel.getEndDate());
        holder.mDrAlertScheduleSignalName.setText(mDrScheduleModel.getSignalName());
        holder.mDrAlertScheduleSignalType.setText(mDrScheduleModel.getSignalType());

    }


    @Override public int getItemCount() {

        return drAlertScheduleListItems.size();

    }

  /*  @Override public void onClick(final View v) {
        onItemClickListener.onItemClick(v, (ViewModel) v.getTag());
    }*/

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mDrAlertScheduleStartDtTime , mDrAlertScheduleEndDtTime , mDrAlertScheduleSignalName, mDrAlertScheduleSignalType;
        public LinearLayout mdrAlertScheduleEmptyLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            mDrAlertScheduleStartDtTime = (TextView) itemView.findViewById(R.id.drAlertScheduleStartDtTime);
            mDrAlertScheduleEndDtTime = (TextView) itemView.findViewById(R.id.drAlertScheduleEndDtTime);
            mDrAlertScheduleSignalName = (TextView) itemView.findViewById(R.id.drAlertScheduleSignalName);
            mDrAlertScheduleSignalType = (TextView) itemView.findViewById(R.id.drAlertScheduleSignalType);
            mdrAlertScheduleEmptyLayout = (LinearLayout)itemView.findViewById(R.id.drAlertScheduleEmptyLayout);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, ViewModel viewModel);

    }
}
