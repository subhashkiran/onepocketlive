package com.subhashe.onepocket.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.subhashe.onepocket.Core.DrCompletedModel;
import com.subhashe.onepocket.Core.DrScheduleModel;
import com.subhashe.onepocket.Core.ViewModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.OnePocketLog;

import java.util.List;

/**
 * Created by axxera on 07-07-2017.
 */

public class DrAlertCompletedAdapter extends RecyclerView.Adapter<DrAlertCompletedAdapter.ViewHolder> {

    private List<DrCompletedModel> drAlertCompletedListItems;
    private Activity mActivity;

    private DrAlertCompletedAdapter.OnItemClickListener onItemClickListener;
    Intent intent1=new Intent();
    DrAlertCompletedAdapter.ViewHolder mView ;


    public DrAlertCompletedAdapter(Activity mActivity, List<DrCompletedModel> drAlertCompletedListItems) {
        this.mActivity = mActivity;
        this.drAlertCompletedListItems = drAlertCompletedListItems;
    }

    public void setOnItemClickListener(DrAlertCompletedAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public DrAlertCompletedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_dralertcompletedlist, parent, false);
//        v.setOnClickListener(this);
        return new DrAlertCompletedAdapter.ViewHolder(v);
    }

    public void remove(int position ) {
//        int position = favListItems.indexOf(item);
        drAlertCompletedListItems.remove(position);

        notifyItemRemoved(position);
        if(drAlertCompletedListItems.size() == 0) {
//            mView.mlistCount.setVisibility(View.GONE);
            mView.mdrAlertCompletedEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override public void onBindViewHolder(final DrAlertCompletedAdapter.ViewHolder holder, int position) {
        final DrCompletedModel mDrCompletedModel = drAlertCompletedListItems.get(position);
        final int pos = position;
        mView = holder;
       /* final String temp = mDrActiveModel.getStationAddModel().toString();
        holder.mStationName.setText(mDrActiveModel.getStationNameModel());

        String addAvailable = "";
        if(mDrActiveModel.getStationAddressModel().contains("null")){
            addAvailable = "Not Available";
        }else{
            addAvailable = mDrActiveModel.getStationAddressModel();
        }
        holder.mStationAddress.setText(addAvailable);*/

        holder.mDrAlertCompletedStartDtTime.setText(mDrCompletedModel.getStartDate());
        holder.mDrAlertCompletedEndDtTime.setText(mDrCompletedModel.getEndDate());
        holder.mDrAlertCompletedSignalName.setText(mDrCompletedModel.getSignalName());
        holder.mDrAlertCompletedSignalType.setText(mDrCompletedModel.getSignalType());

    }


    @Override public int getItemCount() {

        return drAlertCompletedListItems.size();

    }

  /*  @Override public void onClick(final View v) {
        onItemClickListener.onItemClick(v, (ViewModel) v.getTag());
    }*/

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mDrAlertCompletedStartDtTime , mDrAlertCompletedEndDtTime , mDrAlertCompletedSignalName, mDrAlertCompletedSignalType;
        public LinearLayout mdrAlertCompletedEmptyLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            mDrAlertCompletedStartDtTime = (TextView) itemView.findViewById(R.id.drAlertCompletedStartDtTime);
            mDrAlertCompletedEndDtTime = (TextView) itemView.findViewById(R.id.drAlertCompletedEndDtTime);
            mDrAlertCompletedSignalName = (TextView) itemView.findViewById(R.id.drAlertCompletedSignalName);
            mDrAlertCompletedSignalType = (TextView) itemView.findViewById(R.id.drAlertCompletedSignalType);
            mdrAlertCompletedEmptyLayout = (LinearLayout)itemView.findViewById(R.id.drAlertCompletedEmptyLayout);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, ViewModel viewModel);

    }
}
