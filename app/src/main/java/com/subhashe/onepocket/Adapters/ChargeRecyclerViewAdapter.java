/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subhashe.onepocket.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.subhashe.onepocket.Activities.MoreInfoActivity;
import com.subhashe.onepocket.Core.FavModel;
import com.subhashe.onepocket.Core.StatModel;
import com.subhashe.onepocket.Core.ViewModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ChargeRecyclerViewAdapter extends RecyclerView.Adapter<ChargeRecyclerViewAdapter.ViewHolder> {

    private List<StatModel> mStatModelListItems;
    private Activity mActivity;
    private ProgressDialog progress;
    private JsonUtil mJsonUtil;
    private OnItemClickListener onItemClickListener;
    HashMap<String, String > moreInfoData;
    Intent intent1=new Intent();
    String timestring = "";
    String timestringmore = "";
    String timestring1 = "";
    String timestringmore1 = "";
    String temp = "";
    int pos = 0;
    int pos1 = 0;

    public ChargeRecyclerViewAdapter(Activity mActivity, List<StatModel> mStatModelListItems) {
        this.mActivity = mActivity;
        this.mStatModelListItems = mStatModelListItems;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_chargeactivityitems, parent, false);
//        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    public StatModel getItem(int position){
        return mStatModelListItems.get(position);
    }

    public void remove(int position ) {
//        int position = favListItems.indexOf(item);
        mStatModelListItems.remove(position);
        notifyItemRemoved(position);
    }

    @Override public void onBindViewHolder(final ViewHolder holder, final int position) {
        final StatModel statModel = mStatModelListItems.get(position);
//        pos = position;
        pos =holder.getLayoutPosition();
        pos1 = mStatModelListItems.get(position).getStatLocalId();

        String startdate = statModel.getChargeStart();//" Aug 28 2015  3:16PM"
        String[] parts;
        String[] parts1;

        String enddate = statModel.getChargeEnd();//" Sep 28 2015  5:47PM"
        if(startdate != "null"){
            startdate.replace("  "," ");
            parts = startdate.split(" ");
            timestring = parts[parts.length-1];
           timestringmore = parts[0]+ " " + parts[1]+ "   "+parts[2];
        }
        else{
            timestring = statModel.getChargeStart();

        }

        if(enddate != "null" || enddate == "0" ){
            enddate.replace("  "," ");
            parts1 = enddate.split(" ");
            timestring1 = parts1[parts1.length-1];
            timestringmore1 = parts1[0]+ " " + parts1[1]+ "   "+parts1[2];

        }else{
            timestring1 = statModel.getChargeEnd();
        }

        String free = "FREE";

        double spentCents = 100 * statModel.getSpent().doubleValue() ;
        if(spentCents <= 0) {
            holder.mPrice.setText(free);
            temp = free;
        }else{
            holder.mPrice.setText("$" + statModel.getSpent().toString());
            temp = "$" + statModel.getSpent().toString() ;
        }

        String hours = "";
        hours = convertHours(statModel.getTimeUsedPerHr());

        String minutes = "";
        minutes = formatHoursAndMinutes(statModel.getTimeUsedPerHr());

      /*  String hms = String.format("%02d:%02d:", TimeUnit.SECONDS.toHours(statModel.getTimeUsedPerHr().longValue()),
                TimeUnit.SECONDS.toMinutes(statModel.getTimeUsedPerHr().longValue()) % TimeUnit.HOURS.toMinutes(1));*/

        /*String hms = String.format("%02d:%02d:", TimeUnit.SECONDS.toMinutes(statModel.getTimeUsedPerHr().longValue()) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.SECONDS.toHours(statModel.getTimeUsedPerHr().longValue()));*/

        String hms = minutes;

        String tempAddress = "";
        if ((statModel.getStationAddress()).contains("null"))
        {
            tempAddress = "Not Available";
        }else
        {
            tempAddress = statModel.getStationAddress();
        }
        holder.mStaAddressCharge.setText(tempAddress);
        //holder.mStaAddressCharge.setText(statModel.getStationAddress());

        holder.mStaNameCharge.setText(statModel.getStationName());
        holder.mDateCharge.setText(statModel.getDate());
        holder.mHourCharge.setText(hms + "hrs");
        holder.mTimeCharge.setText(timestring+"-"+timestring1);

//        moreInfoData.put("MOREINFODATA",moreInfoArray);


        holder.mBottomLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos =holder.getLayoutPosition() + 1;
//                StatModel statModelMore1 = mStatModelListItems.get(pos -1);
//                StatModel statModelMore = getItem(pos - 1);
                StatModel statModelMore = new StatModel();
                int tempPos = pos -1;
                for(int test = 0; test < mStatModelListItems.size()  ; test++){
                    if(pos == mStatModelListItems.get(test).getStatLocalId()){
                        statModelMore = mStatModelListItems.get(test);
                    }
                }

                String startdate = statModelMore.getChargeStart();//" Aug 28 2015  3:16PM"
                String[] parts;
                String[] parts1;
                String Stringdate , Enddate;

                String enddate = statModelMore.getChargeEnd();//" Sep 28 2015  5:47PM"

                if(startdate != "null"){
                    parts = startdate.split(" ");

                    Stringdate = startdate.substring(0,13);

                    timestring = parts[parts.length-1];
                   // timestringmore = parts[0]+ " " + parts[1]+ "   " + parts[2] ;
                    timestringmore = Stringdate;


                }
                else{
                    String temp = " - ";
                    if(statModelMore.getChargeStart().contains("null")){
                        timestring = temp;
                    }else {
                        timestring = statModelMore.getChargeStart();
                    }
                    timestringmore = temp;

                    OnePocketLog.d("StartDate2" , timestringmore);
                }

                if(enddate != "null" || enddate == "0"){
                    parts1 = enddate.split(" ");

                    Enddate = enddate.substring(0,13);

                    timestring1 = parts1[parts1.length-1];
                    //timestringmore1 = parts1[0]+ " " + parts1[1]+ "   "+parts1[2];
                    timestringmore1 = Enddate;

                    OnePocketLog.d("EndDate1" , timestringmore1);

                }else{
                    String temp1 = " - ";
                    if(statModelMore.getChargeEnd().contains("null")){
                        timestring1 = temp1;
                    }else {
                        timestring1 = statModelMore.getChargeEnd();
                    }
                    timestringmore1 = temp1;

                    OnePocketLog.d("EndDate2" , timestringmore1);
                }

                String free = "FREE";
                String rate = "";
                double spentCents1 = 100 * statModelMore.getSpent().doubleValue() ;
                if(spentCents1 <= 0) {
                    temp = free;
                }else{
                    temp = "$" + statModelMore.getSpent().toString() ;
                }

                if(statModelMore.getRateKWPerTime().contains("null")){
                    rate = "0.0";
                }else{
                    rate = statModelMore.getRateKWPerTime();
                }

                String hours = "";
                hours = convertHours(statModelMore.getTimeUsedPerHr());

                /*String hms = String.format("%02d:%02d", TimeUnit.SECONDS.toHours(statModelMore.getTimeUsedPerHr().longValue()),
                        TimeUnit.SECONDS.toMinutes(statModelMore.getTimeUsedPerHr().longValue()) % TimeUnit.HOURS.toMinutes(1));*/

                String minutes = "";
                minutes = formatHoursAndMinutes(statModel.getTimeUsedPerHr());

      /*  String hms = String.format("%02d:%02d:", TimeUnit.SECONDS.toHours(statModel.getTimeUsedPerHr().longValue()),
                TimeUnit.SECONDS.toMinutes(statModel.getTimeUsedPerHr().longValue()) % TimeUnit.HOURS.toMinutes(1));*/

        /*String hms = String.format("%02d:%02d:", TimeUnit.SECONDS.toMinutes(statModel.getTimeUsedPerHr().longValue()) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.SECONDS.toHours(statModel.getTimeUsedPerHr().longValue()));*/

                String hms = minutes;

                moreInfoData = new HashMap<String,String>();
                moreInfoData.put("FINALCOST",temp);
                moreInfoData.put("STAADDRESS",statModelMore.getStationAddress());
                moreInfoData.put("STANAME",statModelMore.getStationName());
                moreInfoData.put("STARTDATE",timestringmore);
                moreInfoData.put("STARTTIME",timestring);
                moreInfoData.put("ENDDATE",timestringmore1);
                moreInfoData.put("ENDTIME",timestring1);
                moreInfoData.put("SESSION TIME",hms + "hrs");
                moreInfoData.put("LEVEL","Port-" + statModelMore.getPortId());
                moreInfoData.put("USED KWH",statModelMore.getKwUsed().toString() + " kWh");
                moreInfoData.put("RATE/KWH","$" +rate);

                Intent intent = new Intent(mActivity, MoreInfoActivity.class);
                intent.putExtra("moremap",moreInfoData);
                mActivity.startActivity(intent);

            }
        });

    }

    public static String formatHoursAndMinutes(double value) {

        String hours = Integer.toString((int) (value / 60));
        hours = hours.length() == 1 ? "0" + hours : hours;

        String minutes = Integer.toString((int) (value % 60));
        minutes = minutes.length() == 1 ? "0" + minutes : minutes;

        return hours + ":" + minutes;
    }

    public String convertHours(double value){
        int hours = (int) value / 3600;
        int remainder = (int) value - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;
       
        int[] ints = {hours , mins , secs};
        return hours + ":"+ mins;
    }

   /* public class deleteFavList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(mActivity.getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.DELETEFAVORITELIST();
                response = mJsonUtil.postMethod(stringurl,null,null,null,null,null,null,null,favHeader,null,null,null);
                OnePocketLog.d("favoritelsit delete resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("favoritelsit delete  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
        }
    }
*/

    private void showProgress(String msg) {
        progress = ProgressDialog.show(mActivity, "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }


    @Override public int getItemCount() {
        return mStatModelListItems.size();
    }

  /*  @Override public void onClick(final View v) {
        onItemClickListener.onItemClick(v, (ViewModel) v.getTag());
    }*/

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mPrice,mStaAddressCharge,mStaNameCharge;
        public TextView mDateCharge,mHourCharge,mTimeCharge,mMoreinfoCharge;
        public LinearLayout mBottomLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            mBottomLayout = (LinearLayout) itemView.findViewById(R.id.bottomlayout);
            mPrice = (TextView) itemView.findViewById(R.id.price);
            mStaAddressCharge = (TextView) itemView.findViewById(R.id.staAddressCharge);
            mStaNameCharge = (TextView) itemView.findViewById(R.id.staNameCharge);
            mDateCharge = (TextView) itemView.findViewById(R.id.DateCharge);
            mHourCharge = (TextView) itemView.findViewById(R.id.hourCharge);
            mTimeCharge = (TextView) itemView.findViewById(R.id.TimeCharge);
            mMoreinfoCharge = (TextView) itemView.findViewById(R.id.MoreinfoCharge);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, ViewModel viewModel);

    }
}
