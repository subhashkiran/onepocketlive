package com.subhashe.onepocket.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.subhashe.onepocket.Activities.EditProfile;
import com.subhashe.onepocket.Activities.LoginActivity;
import com.subhashe.onepocket.Core.EditProfileModel;
import com.subhashe.onepocket.Core.ProfileInfoModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Locale;

import static android.support.v7.app.ActionBar.DISPLAY_SHOW_HOME;

/**
 * Created by root on 28/1/16.
 */
public class ProfileFragment extends Fragment {

    private TextView firstName, lastName, tv_edit, addressLine1, addressLine2, country, state, city, zipcode, email, phoneNumber;
    Point p;
    int id = 1;
    EditText editTextAddress1, editTextAddress2, editTextCity, editTextState, editTextCountry, editTextZipcode, editTextPhone, editTextEmail;
    Button btnSubmit, close;
    private ProgressDialog progress;
    private ProfileInfoModel profileInfoModel;
    private EditProfileModel editProfileModel;
    private ConnectionDetector mConnectionDetector;
    private JsonUtil mJsonUtil;
    private String response, token;
    private int address_id;
    private boolean successFlag;
    JSONObject jsonObject, addressObject;
    JSONArray jsonArray;
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String ADDRESS_USER_ID = "ADDRESS_USER_ID";

    //    String mUrl = "http://192.168.168.141:8080/gridbot/services/address";
//    String token = "ali:1458803312170:33d05757ebebf6963533f877ed4fc272";
    android.support.v7.app.ActionBar mActionBar;
    StringBuffer buffer;
    String PhoneString;
    InputStream inputStream;
    String line = "";
    URL url;
    String finalJson;
    HttpURLConnection connection = null;
    BufferedReader reader = null;
    private Fragment fragment = null;

    private TextView mAccBalance;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor1;
    private boolean paymentPopup = false;
    private String getAccBal,mAmount;
    private int user_id,accBalence;
    public static final String ACC_BALENCE = "ACC_BALENCE";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";

    // fixed the bug in sprint-7 Ticket_Id-156 starts
    char[] acceptedChars = new char[]{',', ' ', '.'};
    char[] acceptedCharsNoSpace = new char[]{'-', '_', '.'};

    InputFilter filtertxtChar;

    // fixed the bug in sprint-7 Ticket_Id-156 end


   /* public ProfileFragment() {

    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profileInfoModel = new ProfileInfoModel();
        EditProfileModel editProfileModel = new EditProfileModel();
        mConnectionDetector = new ConnectionDetector(getActivity());

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        editor1 = prefs.edit();
        paymentPopup = prefs.getBoolean(PAYMENT_FLAG_ALERT, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Window w = getActivity().getWindow();
        w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.profile_view, container, false);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        final ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("My Profile");
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
            final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
            actionBar.setHomeAsUpIndicator(backArrow);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#48100f")));

        }
//        jsonUtil = new JsonUtil();

        firstName = (TextView) layout.findViewById(R.id.firstName);
        lastName = (TextView) layout.findViewById(R.id.lastName);

        addressLine1 = (TextView) layout.findViewById(R.id.addressLine1);
//        addressLine2 = (TextView) layout.findViewById(R.id.addressLine2);
//        country = (TextView) layout.findViewById(R.id.country);
        state = (TextView) layout.findViewById(R.id.state);
        city = (TextView) layout.findViewById(R.id.city);
        zipcode = (TextView) layout.findViewById(R.id.zipcode);
        email = (TextView) layout.findViewById(R.id.email);
        phoneNumber = (TextView) layout.findViewById(R.id.phoneNumber);

        mAccBalance = (TextView)layout.findViewById(R.id.accBalance);

        // fixed the bug in sprint-7 Ticket_Id-156 starts

        filtertxtChar = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetter(source.charAt(i)) && !(Character.isSpace(source.charAt(i))) &&
                            !new String(acceptedChars).contains(String.valueOf(source.charAt(i))) /*&&
                        new String(blockedCharSet).contains(String.valueOf(source.charAt(i)))*/) {
                        return "";
                    }
                }
                return null;
            }
        };

        // fixed the bug in sprint-7 Ticket_Id-156 end


        //tv_edit = (TextView) layout.findViewById(R.id.tvProfileViewEdit);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");
        address_id = prefs.getInt(ADDRESS_USER_ID, 0);
        if (mConnectionDetector.isOnline()) {
            new ProfileInformation().execute();
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
               /* int[] location = new int[2];
                tv_edit.getLocationOnScreen(location);

                //Initialize the Point with x, and y positions
                p = new Point();
                p.x = location[0];
                p.y = location[1];
                //Open popup window
                if (p != null)
                    showPopup(getActivity(), p);*/

                if (mConnectionDetector.isOnline()) {
                    Intent intent = new Intent(getActivity(), EditProfile.class);
                    getActivity().startActivity(intent);
//                getActivity().finish();
                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });

        return layout;

    }

    @Override
    public void onResume() {
        super.onResume();
        new BalenceCheck().execute();
    }

    public class BalenceCheck extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.USER_BALENCE_CHECK()+user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    JSONObject jObject = new JSONObject(response);
                    JSONArray jObject1 = jObject.getJSONArray("accountses");
                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp = jObject1.getJSONObject(i);
                        getAccBal = mTemp.getString("accountBalance");
                        accBalence = mTemp.getInt("accountBalance");


                        NumberFormat num = NumberFormat.getCurrencyInstance(Locale.US);
//                        BigDecimal bd = new BigDecimal(getAccBal.toString());
//                        mAmount = num.format(getAccBal);
                        mAccBalance.setText("");
                        mAccBalance.setText(num.format(new BigDecimal(getAccBal)));

                        try {
//                            accBalence = Integer.parseInt(getAccBal);
                            editor1.putInt(ACC_BALENCE, accBalence);
                        } catch(NumberFormatException e) {
                            System.out.println("parse value is not valid : " + e);
                        }
                        /*Fragment currentFragment = new PaymentsFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.detach(currentFragment);
                        fragmentTransaction.attach(currentFragment);
                        fragmentTransaction.commit();*/
                    }

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }


    public class ProfileInformation extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.USER_BALENCE_CHECK() + user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    insertProfile(response);

                    firstName.setText(profileInfoModel.getFirstName());
                    lastName.setText(profileInfoModel.getLastName());
                    addressLine1.setText(profileInfoModel.getAddressLine1());
//                    addressLine2.setText(profileInfoModel.getAddressLine2());
//                    country.setText(profileInfoModel.getCountry());
                    state.setText(profileInfoModel.getState());
                    city.setText(profileInfoModel.getCity());
                    zipcode.setText(profileInfoModel.getZipCode());
//                        email.setText(mTemp.getString("email"));
                    phoneNumber.setText(String.valueOf(profileInfoModel.getPhone()));
                    PhoneString = profileInfoModel.getPhone();

                  /*  JSONObject jObject = new JSONObject(response);
                    String email1 = jObject.getString("email");
                    email.setText(email1);

                    JSONArray jObject1 = jObject.getJSONArray("address");
                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp = jObject1.getJSONObject(i);
                        addressLine1.setText(mTemp.getString("addressLine1"));
                        addressLine2.setText(mTemp.getString("addressLine2"));
                        country.setText(mTemp.getString("country"));
                        state.setText(mTemp.getString("state"));
                        city.setText(mTemp.getString("city"));
                        zipcode.setText(mTemp.getString("zipCode"));
//                        email.setText(mTemp.getString("email"));
                        phoneNumber.setText(String.valueOf(mTemp.getLong("phone")));
                    }*/


                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    public class ProfileInformationUpdate extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                response = mJsonUtil.postMethod(OnePocketUrls.PROFILE_UPDATE(),null,null,null,profileInfoModel,null,null,null,
                        null,token,null,null,null);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                profileInfoModel.setPhone(PhoneString);
                showAlertBox(getString(R.string.servernotresponce));
            } else if(response.contains("Error Code-284")){
                profileInfoModel.setPhone(PhoneString);
                showAlertBox(getString(R.string.accountprofileedit));
            } else {
                try {
                    insertProfileUpdate(response);

                    firstName.setText(profileInfoModel.getFirstName());
                    lastName.setText(profileInfoModel.getLastName());
                    email.setText(profileInfoModel.getEmail());
                    phoneNumber.setText(profileInfoModel.getPhone());
                    addressLine1.setText(profileInfoModel.getAddressLine1());
//                    addressLine2.setText(profileInfoModel.getAddressLine2());
//                    country.setText(profileInfoModel.getCountry());
                    state.setText(profileInfoModel.getState());
                    city.setText(profileInfoModel.getCity());
                    zipcode.setText(profileInfoModel.getZipCode());
//                        email.setText(mTemp.getString("email"));
                    phoneNumber.setText(String.valueOf(profileInfoModel.getPhone()));

 //                    fixed the bug in sprint-7 Ticket_Id-156 starts
                    successFlag = true;
                    showAlertBox(getString(R.string.savedInfoSuccess));
//                    fixed the bug in sprint-7 Ticket_Id-156 end

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    /*private void addTextWatchers(){

        editTextAddress1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                profileInfoModel.setAddressLine1(editTextAddress1.getText().toString());
            }
        });
    }*/

    private void updateProfile() {
        // profileInfoModel.setFirstName(editProfileModel);
        profileInfoModel.setAddressLine1(editTextAddress1.getText().toString());
        profileInfoModel.setAddressLine2("");
        profileInfoModel.setCountry("UK");
        profileInfoModel.setState(editTextState.getText().toString());
        profileInfoModel.setCity(editTextCity.getText().toString());
        profileInfoModel.setZipCode(editTextZipcode.getText().toString());
//        profileInfoModel.setPhone(String.valueOf();
//        profileInfoModel.setPhone(Long.parseLong(editTextPhone.getText().toString()));
        profileInfoModel.setPhone(editTextPhone.getText().toString());
//        email.setText(editTextEmail.getText().toString());
    }

    private void insertProfile(String response) {
        try {
            JSONObject jObject = new JSONObject(response);
            String email1 = jObject.getString("email");
            email.setText(email1);
            JSONArray jObject1 = jObject.getJSONArray("address");

            for (int i = 0; i < jObject1.length(); i++) {
                JSONObject mTemp = jObject1.getJSONObject(i);
                profileInfoModel.setAddressLine1(mTemp.getString("addressLine1"));
                profileInfoModel.setAddressLine2(mTemp.getString("addressLine2"));
                profileInfoModel.setCountry(mTemp.getString("country"));
                profileInfoModel.setState(mTemp.getString("state"));
                profileInfoModel.setCity(mTemp.getString("city"));
                profileInfoModel.setZipCode(mTemp.getString("zipCode"));
                profileInfoModel.setId(address_id);
//                profileInfoModel.setPhone(mTemp.getLong("phone"));
                profileInfoModel.setPhone(mTemp.getString("phone"));
            }

        } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
        }
    }

    private void insertProfileUpdate(String response) {
        try {
            JSONObject jObject = new JSONObject(response);
//
            profileInfoModel.setAddressLine1(jObject.getString("addressLine1"));
            profileInfoModel.setAddressLine2(jObject.getString("addressLine2"));
//            profileInfoModel.setCountry(jObject.getString("country"));
            profileInfoModel.setState(jObject.getString("state"));
            profileInfoModel.setCity(jObject.getString("city"));
            profileInfoModel.setZipCode(jObject.getString("zipCode"));
            profileInfoModel.setId(address_id);
            profileInfoModel.setPhone(jObject.getString("phone"));
            PhoneString = "";
            PhoneString=jObject.getString("phone");
//            profileInfoModel.setPhone(jObject.getLong("phone"));

        } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
        }
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //fragment = new AccountFragmentTest();
                //getActivity().finish();
                //fragment = new AccountFragmentTest();
                getFragmentManager().popBackStack();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        if (!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

        /*AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        if(!successFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();*/
    }

    private void showPopup(final Activity context, Point p) {

        int popupWidth = 700;
        int popupHeight = 800;
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.ProfileEditLayout);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.profile_edit_view, viewGroup);

        editTextAddress1 = (EditText) layout.findViewById(R.id.editTextProfeditAdd1);
//        editTextAddress2 = (EditText) layout.findViewById(R.id.editTextProfeditAdd2);

        // fixed the bug in sprint-7 Ticket_Id-156 starts
        editTextCity = (EditText) layout.findViewById(R.id.editTextProfeditCity);
        editTextCity.setFilters(new InputFilter[]{filtertxtChar});


        editTextState = (EditText) layout.findViewById(R.id.editTextProfeditState);
        editTextState.setFilters(new InputFilter[]{filtertxtChar});

       /* editTextCountry = (EditText) layout.findViewById(R.id.editTextProfeditCountry);
        editTextCountry.setFilters(new InputFilter[]{filtertxtChar});*/
        // fixed the bug in sprint-7 Ticket_Id-156 end

        editTextZipcode = (EditText) layout.findViewById(R.id.editTextProfeditZipcode);
//        editTextEmail = (EditText) layout.findViewById(R.id.editTextProfeditEmail);
        editTextPhone = (EditText) layout.findViewById(R.id.editTextProfeditPhone);


        btnSubmit = (Button) layout.findViewById(R.id.btnSubmit);
        close = (Button) layout.findViewById(R.id.buttonprofeditcancel);

        // Creating the PopupWindow
       /* final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);*/
        final PopupWindow popup = new PopupWindow(layout, WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT, true);

        // Clear the default translucent background
//        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0/* p.x, p.y*/);

        editTextAddress1.setText(profileInfoModel.getAddressLine1());
//        editTextAddress2.setText(profileInfoModel.getAddressLine2());
        editTextCity.setText(profileInfoModel.getCity());
        editTextState.setText(profileInfoModel.getState());
//        editTextCountry.setText(profileInfoModel.getCountry());
        editTextZipcode.setText(profileInfoModel.getZipCode());
//        editTextEmail.setText(email.getText());
//        editTextPhone.setText(String.valueOf(profileInfoModel.getPhone()));
        editTextPhone.setText((profileInfoModel.getPhone()));

//        addTextWatchers();
        //        fixed the bug in sprint-7 Ticket_Id-156 starts
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTextAddress1.getText().toString().length() == 0) {
                    editTextAddress1.requestFocus();
                    showAlertBox(getString(R.string.enteraddews));
                } else if (editTextCity.getText().toString().length() == 0) {
                    editTextCity.requestFocus();
                    showAlertBox(getString(R.string.entercity));
                } else if (editTextState.getText().toString().length() == 0) {
                    editTextState.requestFocus();
                    showAlertBox(getString(R.string.enterstate));
                }/*else if (editTextCountry.getText().toString().length() == 0) {
                    editTextCountry.requestFocus();
                    showAlertBox(getString(R.string.entercountry));
                }*/ else if (editTextZipcode.getText().toString().length() == 0) {
                    editTextZipcode.requestFocus();
                    showAlertBox(getString(R.string.enterzip));
                } else if ((editTextZipcode.getText().toString().length() < 5) || (editTextZipcode.getText().toString().length() > 7)) {
                    editTextZipcode.requestFocus();
                    showAlertBox(getString(R.string.enterzipAlert));
                } else if (editTextPhone.getText().toString().length() == 0) {
                    editTextPhone.requestFocus();
//                    showAlertBox(getString(R.string.eneterphone));
//                    fixed the bug in sprint-10 Ticket_Id-225
                    showAlertBox(getString(R.string.eneterphoneNum));
//                    fixed the bug in sprint-10 Ticket_Id-225
                } else if ((editTextPhone.getText().toString().length() < 10) ||
                        (editTextPhone.getText().toString().length() > 10)) {
                    editTextPhone.requestFocus();
                    showAlertBox(getString(R.string.mobileNumberCount));
//                    DialogUtil("Password is too short");
                } else if (editTextPhone.getText().toString().contains("0000000000")) {
                    showAlertBox(getString(R.string.invalidmobile));
                } else {
//                    fixed the bug in sprint-7 Ticket_Id-114 starts
                    if (mConnectionDetector.isOnline()) {
                        updateProfile();
                        new ProfileInformationUpdate().execute();
                        popup.dismiss();
                    } else {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
//                    fixed the bug in sprint-7 Ticket_Id-114 end
                }
            }
        });

//        fixed the bug in sprint-7 Ticket_Id-156 end

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    /*class ProfileInformation extends AsyncTask<String, String, ProfileInfoModel> {

        ProfileInfoModel finalResult;

        @Override
        protected ProfileInfoModel doInBackground(String... params) {


            try {
                Log.d("ProfileInformation", "starts");
                url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("X-Auth-Token", token);
                Log.d("connection", "established" + connection);
                connection.connect();

                inputStream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                buffer = new StringBuffer();


                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                finalJson = buffer.toString();
                Log.d("finalJson", ":" + finalJson);

                ProfileInfoModel profileInfoModel = new ProfileInfoModel();

                jsonObject = new JSONObject(finalJson);
                Log.d("ProfileInformation", "Json Object Value " + jsonObject);

                profileInfoModel.setState(jsonObject.getString("state"));
                profileInfoModel.setCountry(jsonObject.getString("country"));
                profileInfoModel.setAddressLine1(jsonObject.getString("addressLine1"));
                profileInfoModel.setAddressLine2(jsonObject.getString("addressLine2"));
                profileInfoModel.setPhone(jsonObject.getLong("phone"));
                profileInfoModel.setCity(jsonObject.getString("city"));
                profileInfoModel.setZipCode(jsonObject.getString("zipCode"));
                profileInfoModel.setId(jsonObject.getInt("id"));


                return profileInfoModel;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ProfileInfoModel result) {
            super.onPostExecute(result);

            if (result != null) {
                finalResult = result;
            }
            Log.d("Profile Info model", "" + result);


            addressLine1.setText(result.getAddressLine1());
            addressLine2.setText(result.getAddressLine2());
            country.setText(result.getCountry());
            state.setText(result.getState());
            city.setText(result.getCity());
            zipcode.setText(result.getZipCode());
            email.setText("demo@axxerainc.com");
            phoneNumber.setText(String.valueOf(result.getPhone()));


            tv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    int[] location = new int[2];
                    tv_edit.getLocationOnScreen(location);

                    //Initialize the Point with x, and y positions
                    p = new Point();
                    p.x = location[0];
                    p.y = location[1];
                    //Open popup window
                    if (p != null)
                        showPopup(getActivity(), p);
                }
            });

        }

        private void showPopup(final Activity context, Point p) {

            int popupWidth = 700;
            int popupHeight = 800;
            // Inflate the popup_layout.xml
            LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.ProfileEditLayout);
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = layoutInflater.inflate(R.layout.profile_edit_view, viewGroup);

            editTextAddress1 = (EditText) layout.findViewById(R.id.editTextProfeditAdd1);
            editTextAddress2 = (EditText) layout.findViewById(R.id.editTextProfeditAdd2);
            editTextCity = (EditText) layout.findViewById(R.id.editTextProfeditCity);
            editTextState = (EditText) layout.findViewById(R.id.editTextProfeditState);
            editTextCountry = (EditText) layout.findViewById(R.id.editTextProfeditCountry);
            editTextZipcode = (EditText) layout.findViewById(R.id.editTextProfeditZipcode);
            editTextPhone = (EditText) layout.findViewById(R.id.editTextProfeditPhone);


            btnSubmit = (Button) layout.findViewById(R.id.btnSubmit);
            close = (Button) layout.findViewById(R.id.buttonprofeditcancel);

            // Creating the PopupWindow
            final PopupWindow popup = new PopupWindow(context);
            popup.setContentView(layout);
            popup.setWidth(popupWidth);
            popup.setHeight(popupHeight);
            popup.setFocusable(true);

            // Clear the default translucent background
            popup.setBackgroundDrawable(new BitmapDrawable());

            // Displaying the popup at the specified location, + offsets.
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0*//* p.x, p.y*//*);

            editTextAddress1.setText(finalResult.getAddressLine1());
            editTextAddress2.setText(finalResult.getAddressLine2());
            editTextCity.setText(finalResult.getCity());
            editTextState.setText(finalResult.getState());
            editTextCountry.setText(finalResult.getCountry());
            editTextZipcode.setText(finalResult.getZipCode());
            editTextPhone.setText(String.valueOf(finalResult.getPhone()));


            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();

                    popup.dismiss();
                }
            });

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                }
            });
        }
    }*/
}
