package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;

/**
 * Created by SubhashE on 3/2/2016.
 */
public class StatsFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private StatsPageAdapter adapter;
    private View rootView;
    private int pageIndex = 0;
    private ConnectionDetector mConnectionDetector;
//    fixed the bug in sprint-8 Ticket_Id-149 starts
//    private final String[] TITLES = { "Graphs", "Table Stats"};
    private final String[] TITLES = {"Graphs","Table Stats", "Account History"};
//    fixed the bug in sprint-8 Ticket_Id-149 end


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

   /* @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_main,container,false);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        final ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Stats");
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE );
        }

//        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mConnectionDetector = new ConnectionDetector(getActivity());
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
//        setupViewPager(viewPager);
        adapter = new StatsPageAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                try {
//                    fixed the bug in sprint-7 Ticket_Id-149  starts
                    if (mConnectionDetector.isOnline()) {
                        viewPager.setVisibility(View.VISIBLE);
                        viewPager.setCurrentItem(tab.getPosition());
                    }else {
                        viewPager.setVisibility(View.GONE);
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
//                    fixed the bug in sprint-7 Ticket_Id-149 end


//                    adapter.setPrimaryItem(null, pageIndex, fragment);
//                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.i("Error ", e + "");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return rootView;
    }

    public class StatsPageAdapter extends FragmentPagerAdapter {
        private Fragment fragment;
        FragmentManager fm;

        public StatsPageAdapter(FragmentManager fm) {

            super(fm);
            this.fm = fm;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            if (mConnectionDetector.isOnline()) {

//                fixed the bug in sprint-8 Ticket_Id-149 starts
                if (position == 0) {
                    fragment = new Graphs_statsFragment();
                } else if (position == 1) {
                    fragment = new TableStatsFragment();
                } else if (position == 2) {
                    fragment = new AccountsHistory();
                }

                /*if (position == 0) {
                    fragment = new Graphs_statsFragment();
                } else if (position == 1) {
                    fragment = new TableStatsFragment();
                }*/

//                fixed the bug in sprint-8 Ticket_Id-149 end
            } else {
                showAlertBox(getResources().getString(R.string.netNotAvl));
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO Auto-generated method stub
            if (fm.getFragments().contains(object))
                return POSITION_NONE;
            else
                return POSITION_UNCHANGED;
        }
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();*/
    }
}
