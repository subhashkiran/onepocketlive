package com.subhashe.onepocket.Fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.subhashe.onepocket.Adapters.RecyclerViewAdapter;
import com.subhashe.onepocket.Core.FavModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.subhashe.onepocket.Utils.OnePocketUrls.CONTACTUS_FLAG;

/**
 * Created by SubhashE on 3/2/2016.
 */
public class FavoriteListFragment extends Fragment {


    private View rootView;
    private RecyclerView mRecyclerView;
    private ProgressDialog progress;
    private RecyclerViewAdapter mMyAdapter;
    private List<FavModel> mFavList = new ArrayList<>();
    private LinearLayout favLayoutEmpty;
    private ConnectionDetector mConnectionDetector;
    private JsonUtil mJsonUtil;
    private String response;
    private int user_id;
    private Fragment fragment = null;
    public static final String FAVJSONSTRING = "FAVJSONSTRING";
    public static final String USER_ID = "USERID_PREFS_String";
    private HashMap<String, List<String>> outputFavData = new HashMap<String, List<String>>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Toast.makeText(getActivity(), "Contact Us Screen", Toast.LENGTH_SHORT).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_favoritelist, container, false);

        favLayoutEmpty = (LinearLayout)rootView.findViewById(R.id.favEmptyLayout);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        user_id = prefs.getInt(USER_ID, 0);

      /*  SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        SharedPreferences.Editor editor = pSharedPref.edit();
        String jsonString = pSharedPref.getString(FAVJSONSTRING,"");
        editor.commit();
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, List<String>>>(){}.getType();
        outputFavData = gson.fromJson(jsonString, type);*/
        new FavListGet().execute();


      /*  outputFavData = null;
        List<String> latlng = new ArrayList<String>();
        List<String> staName =  new ArrayList<String>();
        List<String> staAdd =  new ArrayList<String>();
//        FavModel mFavModel = new FavModel();

        if(outputFavData != null) {
            favLayoutEmpty.setVisibility(View.INVISIBLE);
            for (HashMap.Entry<String, List<String>> entry : outputFavData.entrySet()) {
                String key = entry.getKey();
                List<String> values = entry.getValue();


                   *//* HashSet<String> hashSet = new HashSet<String>();
                    hashSet.addAll(values);
                    values.clear();
                    values.addAll(hashSet);*//*

                if (key.equals("LatLng")) {
//                    values.add(temp.toString());
                    for (int i=0; i < values.size(); i++)
                    {
                        String temp = values.get(i).toString();
                        latlng.add(temp);
//                        mFavModel.setStationAddModel(temp);
                    }
                }

                if (key.equals("StationName")) {
                    for (int i=0; i < values.size(); i++) {
                        String temp = values.get(i).toString();
                        staName.add(temp);
                    }
                }

                if (key.equals("StationAdress")) {
                    for (int i=0; i < values.size(); i++) {
                        String temp = values.get(i).toString();
                        staAdd.add(temp);
                    }
                }

                System.out.println("Key = " + key);
                System.out.println("Values = " + values + "n");
            }

            int ii,jj,lat,sta;
            lat = 0;
            sta = 0;
            ii = latlng.size();
            jj = staName.size();
            while( ( lat < ii) && (sta < jj )){
                FavModel mFavModel = new FavModel();
                mFavModel.setIdFav(100 + lat);
                mFavModel.setStationAddModel(latlng.get(lat).toString());
                mFavModel.setStationNameModel(staName.get(sta).toString());
                mFavModel.setStationAddressModel(staAdd.get(sta).toString());
                mFavList.add(mFavModel);
                lat++;
                sta++;
            }
        }else{
            favLayoutEmpty.setVisibility(View.VISIBLE);
        }*/

//        toolbar= (Toolbar)rootView. findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);

       /* mMyAdapter = new RecyclerViewAdapter(getActivity(),mFavList);
        mRecyclerView.setAdapter(mMyAdapter);*/
        final ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("My Favorites");
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
        }

        return rootView;
    }

    @Override
    public void onResume() {

        if (mFavList.size() == 0)
        {
            favLayoutEmpty.setVisibility(View.VISIBLE);
        }
        super.onResume();
    }

    public class FavListGet extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String stringurl = OnePocketUrls.GETALLFAVORITELIST() + user_id;
                response = mJsonUtil.getMethod(stringurl,null,null);
                OnePocketLog.d("favoritelsit fragment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("favoritelsit fragment  error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            try {
                if(response != null || !response.contains("[]") || response != "") {

                    JSONArray temp = new JSONArray(response);

                    if (temp.length() == 0) {

                        favLayoutEmpty.setVisibility(View.VISIBLE);

                    } else if (temp != null) {

                        for (int i = 0; i < temp.length(); i++) {

                            favLayoutEmpty.setVisibility(View.INVISIBLE);

                            JSONObject e = temp.getJSONObject(i);
                            FavModel mFavModel = new FavModel();

                            mFavModel.setIdFav(e.getInt("FavId"));

                            OnePocketLog.d("FavIDStation" , String.valueOf(mFavModel.getIdFav()));


                            mFavModel.setStationAddressModel(e.getString("StationAddress"));
                            mFavModel.setStationAddModel(e.getString("StationGPS"));
                            mFavModel.setStationNameModel(e.getString("StationName"));

                           /* mFavModel.setLiveStatus(e.getString("LiveStatus"));
                            mFavModel.setConn1portType(e.getString("conn1portType"));
                            mFavModel.setConn2PortType(e.getString("conn2PortType"));
                            mFavModel.setVendingPrice1(e.getString("vendingPrice1"));
                           // mFavModel.ssetVendingPrice2(e.getString("vendingPrice2"));
                            mFavModel.setParkingPricePH(e.getString("parkingPricePH"));
                            mFavModel.setAdaStatus(e.getString("adaStatus"));*/

                            mFavModel.setUserId(user_id);
                            mFavList.add(mFavModel);

                        }

                    }
                    mMyAdapter = new RecyclerViewAdapter(getActivity(), mFavList);
                    mRecyclerView.setAdapter(mMyAdapter);
                }
            }catch (Exception e){

            }
        }
    }

    public void showAlertBox(final String msg) {

    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

}
