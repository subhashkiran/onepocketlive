package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.subhashe.onepocket.Activities.StatsActivity;
import com.subhashe.onepocket.Core.StatModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.NetworkChecker;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


/**
 * Created by root on 28/1/16.
 */
public class Graphs_statsFragment extends Fragment {

    String[] Graph_spinner = new String[] {"Monthly","Daily"};
    private Spinner spinner;
    ArrayAdapter<String> adapter;

    private ConnectionDetector mConnectionDetector;
    private ProgressDialog progress;
    private ProgressDialog progressDialog;
    private JsonUtil mJsonUtil;
    private String response,token;
    private int user_id;
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";


    String month,dateValue,year,monthValue,monthDateYearValue,dateString;
    ArrayList<String> monthDateYearlist,monthYearlist;
    double[] kwUsed,Spent;
    String[] monthDateYearArray,monthYearArray;
    double maxMonthsValue = 0,maxDaysValue=0;

    SimpleDateFormat dateFormat;
    Date date;
//
//    int id = 52;
//    String mUrl = "http://192.168.168.141:8080/gridbot/driver/stat";

    private View mChart;
    LinearLayout chartContainer;

//    fixed the bug in sprint-7 Ticket_Id-149 starts
    private BroadcastReceiver graphsBroadcastReceiver;
    boolean networkCheckerFlag;
    private static boolean firstConnect = true;
    private AlertDialog alertDialog;
    LinearLayout chartLinearLayout;
//    fixed the bug in sprint-7 Ticket_Id-149 end


    public Graphs_statsFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.graphs_view, container, false);
        mConnectionDetector = new ConnectionDetector(getActivity());
        chartContainer = (LinearLayout) layout.findViewById(R.id.chart);

        chartLinearLayout = (LinearLayout) layout.findViewById(R.id.chartLayout);

//        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        monthDateYearlist = new ArrayList<String>();
        monthYearlist = new ArrayList<String>();

        spinner =(Spinner)layout.findViewById(R.id.Graphsspiner);
        adapter = new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_spinner_item, Graph_spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(ACC_USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

//        fixed the bug in sprint-7 Ticket_Id-149 starts

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");

        graphsBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {

                if (firstConnect) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    networkCheckerFlag = NetworkChecker.getConnectivityStatusCheck(context);

//                    if (mConnectionDetector.isOnline() || networkCheckerFlag) {
                    if (mConnectionDetector.isOnline()) {

                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.cancel();
                        }
                        chartLinearLayout.setVisibility(View.VISIBLE);
                        new JSONTask().execute();


                    } else {
                        chartLinearLayout.setVisibility(View.GONE);
//                        Toast.makeText(getContext(), R.string.netNotAvl, Toast.LENGTH_SHORT).show();
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
                    firstConnect = false;
                }else{
                    firstConnect = true;
                }
            }
        };

        getActivity().registerReceiver(graphsBroadcastReceiver,filter);

       /* if (mConnectionDetector.isOnline()) {
            new JSONTask().execute();
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }*/

        return layout;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(graphsBroadcastReceiver);
    }

    //    fixed the bug in sprint-7 Ticket_Id-149 end
    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();
        /*AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();*/
    }


    public class JSONTask extends AsyncTask<String,String,List<StatModel>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.pleaseWait));

        /*    progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Connect to Server");
            progressDialog.setMessage("Please wait");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setProgress(0);
            progressDialog.show();*/
        }

        @Override
        protected List<StatModel> doInBackground(String... params) {

            try {
                String mUrl = OnePocketUrls.STATS_DRIVER() + user_id ;
//                response = mJsonUtil.postMethod(mUrl, null, null, null, null,null,null,null);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);


                ArrayList<StatModel> statList = new ArrayList<StatModel>();

                JSONArray temp = new JSONArray(response);

                for (int i = 0; i < temp.length(); i++) {
                    JSONObject e = temp.getJSONObject(i);

                    StatModel statModel = new StatModel();
                    Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                    statModel.setSessionId(e.getString("sessionId"));
                    statModel.setKwUsed(e.getDouble("kWUsed"));
                    statModel.setTimeUsedPerHr(e.getDouble("timeUsedPerHr"));
                    statModel.setChargeEnd(e.getString("chargeEnd"));
                    statModel.setRateKWPerTime(e.getString("rateKWPerTime"));
                    statModel.setSpent(e.getDouble("spent"));
                    statModel.setDate(e.getString("date"));
                    statModel.setPortId(e.getString("portId"));
                    statModel.setChargeStart(e.getString("chargeStart"));
                    statModel.setStationName(e.getString("stationName"));

                    statList.add(statModel);
                }
                return statList;


            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

        return null;

        }

        @Override
        protected void onPostExecute(final List<StatModel> result) {
            super.onPostExecute(result);
//            hideProgress();

//            progressDialog.dismiss();
//            fixed the bug in sprint-7 Ticket_Id-149 starts
            if (result == null || result.equals("")) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "No Data Found", Toast.LENGTH_SHORT).show();
//                fixed the bug in sprint-7 Ticket_Id-149 end
            } else {
                for (int i = 0; i < result.size(); i++) {
                    StatModel statModelResult = new StatModel();
                    statModelResult = result.get(i);

                    try {
                        dateString = statModelResult.getDate();
                        dateFormat = new SimpleDateFormat("MMM dd yyyy");
                        date = dateFormat.parse(dateString);

                        month = "" + android.text.format.DateFormat.format("MMM", date);
                        year = "" + android.text.format.DateFormat.format("yyyy", date);
                        dateValue = "" + android.text.format.DateFormat.format("dd",date);
                        monthDateYearValue = month + dateValue + year;
                        monthValue = month + year;

                        if (!monthDateYearlist.contains(monthDateYearValue)) {
                            monthDateYearlist.add(monthDateYearValue);
                        }
                        if (!monthYearlist.contains(monthValue)) {
                            monthYearlist.add(monthValue);
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

//                fixed the bug in sprint-7 Ticket_Id-149 starts
                String text = spinner.getSelectedItem().toString();
                if(text.equalsIgnoreCase("Daily")){
                    openChart(dataForDays(result), maxDaysValue + 4);
                }else if(text.equalsIgnoreCase("Monthly")){
                    openChart(dataForMonths(result), maxMonthsValue + 10);
                }
//                fixed the bug in sprint-7 Ticket_Id-149 end

                //String[] resultForChart =  dataForMonths(result);
//                openChart(dataForMonths(result), maxMonthsValue + 10);

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        Toast.makeText(parent.getContext(), "Please Wait", Toast.LENGTH_SHORT).show();
                        String item = parent.getItemAtPosition(position).toString();

                        if(item.equalsIgnoreCase("Daily")){
//                             Toast.makeText(parent.getContext(), "Please Wait", Toast.LENGTH_SHORT).show();
//                             String[] resultDaysArray = dataForDays(result);
                            openChart(dataForDays(result),maxDaysValue+4);

                        }else if(item.equalsIgnoreCase("Monthly")){
//                            Toast.makeText(parent.getContext(), "Please Wait", Toast.LENGTH_SHORT).show();
//                             String[] resultMonthsArray = dataForMonths(result);
                            openChart(dataForMonths(result),maxMonthsValue+10);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }


        public String[] dataForMonths(List<StatModel> result){

            monthYearArray = new String[monthYearlist.size()];
            monthYearlist.toArray(monthYearArray);

            kwUsed = new double[monthYearArray.length];
            Spent = new double[monthYearArray.length];

            for(int i=0;i<monthYearArray.length;i++){
                String dateInArray = monthYearArray[i];
                Double kwValue=0.0,spentValue=0.0;

                for(int j=0;j<result.size();j++){

                    StatModel statModel = result.get(j);

                    try {
                        dateString = statModel.getDate();
                        dateFormat = new SimpleDateFormat("MMM dd yyyy");
                        date = dateFormat.parse(dateString);

                        month = ""+android.text.format.DateFormat.format("MMM",date);
                        year = ""+android.text.format.DateFormat.format("yyyy", date);

                        if(dateInArray.substring(0,3).equalsIgnoreCase(month)&&dateInArray.substring(3,7).equalsIgnoreCase(year)){
                            kwValue+=statModel.getKwUsed();
                            spentValue+=statModel.getSpent();
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                kwUsed[i]=kwValue;
                Spent[i]=spentValue;
            }

            // for Y-Axis max length
            for(int n=0;n<kwUsed.length;n++){
                if(kwUsed[n]>maxMonthsValue){
                    maxMonthsValue = kwUsed[n];
                }
            }
            for(int n=0;n<Spent.length;n++){
                if(Spent[n]>maxMonthsValue){
                    maxMonthsValue = Spent[n];
                }
            }

            return monthYearArray;
            // for Y-Axis max length
        }

        public String[] dataForDays(List<StatModel> result){

            monthDateYearArray = new String[monthDateYearlist.size()];
            monthDateYearlist.toArray(monthDateYearArray);

            kwUsed = new double[monthDateYearArray.length];
            Spent = new double[monthDateYearArray.length];

            for(int i=0;i<monthDateYearArray.length;i++){
                String dateInArray = monthDateYearArray[i];
                Double kwValue=0.0,spentValue=0.0;

                for(int j=0;j<result.size();j++){

                    StatModel statModel = result.get(j);

                    try {
                        dateString = statModel.getDate();
                        dateFormat = new SimpleDateFormat("MMM dd yyyy");
                        date = dateFormat.parse(dateString);

                        month = ""+android.text.format.DateFormat.format("MMM",date);
                        dateValue = "" + android.text.format.DateFormat.format("dd",date);
                        year = ""+android.text.format.DateFormat.format("yyyy", date);

                        if(dateInArray.substring(0,3).equalsIgnoreCase(month)&&dateInArray.substring(3,5).equals(dateValue)&&dateInArray.substring(5,9).equalsIgnoreCase(year)){
                            kwValue+=statModel.getKwUsed();
                            spentValue+=statModel.getSpent();
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                kwUsed[i]=kwValue;
                Spent[i]=spentValue;
            }


            // for Y-Axis max length
            for(int n=0;n<kwUsed.length;n++){
                if(kwUsed[n]>maxDaysValue){
                    maxDaysValue = kwUsed[n];
                }
            }
            for(int n=0;n<Spent.length;n++){
                if(Spent[n]>maxDaysValue){
                    maxDaysValue = Spent[n];
                }
            }
            return monthDateYearArray;
            // for Y-Axis max length
        }
    }

    private void openChart(String[] resultArray,double maxValue){

        // Creating an XYSeries for Spent
        XYSeries spentSeries = new XYSeries("$Spent");
        // Creating an XYSeries for kwUsed
        XYSeries kwUsedSeries = new XYSeries("KW Used");
        // Adding data to KwUsed and Expense Spent
        for(int i=0;i<resultArray.length;i++){
            kwUsedSeries.add(i,kwUsed[i]);
            spentSeries.add(i,Spent[i]);
        }

        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        // Adding KwUsed Series to the dataset
        dataset.addSeries(kwUsedSeries);
        // Adding SpentSeries Series to dataset
        dataset.addSeries(spentSeries);

        // Creating XYSeriesRenderer to customize kwUsedSeries
        XYSeriesRenderer kwUsedRenderer = new XYSeriesRenderer();
        kwUsedRenderer.setColor(Color.rgb(139, 140, 140)); //color of the graph set to cyan(144, 179, 194)
        kwUsedRenderer.setFillPoints(true);
        kwUsedRenderer.setLineWidth(4f);
        kwUsedRenderer.setDisplayChartValues(true);
        //setting chart value distance
        kwUsedRenderer.setDisplayChartValuesDistance(10);
        //setting line graph point style to circle
        kwUsedRenderer.setPointStyle(PointStyle.CIRCLE);
        //setting stroke of the line chart to solid
        kwUsedRenderer.setStroke(BasicStroke.SOLID);

        kwUsedRenderer.setFillBelowLine(true);
        kwUsedRenderer.setFillBelowLineColor(10);

        // Creating XYSeriesRenderer to customize spendSeries
        XYSeriesRenderer spentRenderer = new XYSeriesRenderer();
        spentRenderer.setColor(Color.rgb(144, 179, 194));
        spentRenderer.setFillPoints(true);
        spentRenderer.setLineWidth(4f);
        spentRenderer.setDisplayChartValues(true);
        spentRenderer.setDisplayChartValuesDistance(10);
        //setting line graph point style to circle
        spentRenderer.setPointStyle(PointStyle.SQUARE);
        //setting stroke of the line chart to solid
        spentRenderer.setStroke(BasicStroke.SOLID);

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(0);
        multiRenderer.setChartTitle("KW Used & Spent Chart");
        multiRenderer.setLabelsColor(Color.rgb(75, 75, 75));

        //        multiRenderer.setXTitle("Year 2014");
        //        multiRenderer.setYTitle("Amount in Dollars");

        /***
         * Customizing graphs
         */

        //setting text size of the title
        multiRenderer.setChartTitleTextSize(32);
        //setting text size of the axis title
        multiRenderer.setAxisTitleTextSize(10);
        //setting text size of the graph lable
        multiRenderer.setLabelsTextSize(20);
        //setting zoom buttons visiblity
        multiRenderer.setZoomButtonsVisible(false);
        //setting pan enablity which uses graph to move on both axis
        multiRenderer.setPanEnabled(false, false);
        //setting click false on graph
        multiRenderer.setClickEnabled(true);
        //setting zoom to false on both axis
        multiRenderer.setZoomEnabled(true, true);
        //setting lines to display on y axis
        multiRenderer.setShowAxes(true);
                    /*multiRenderer.setShowGridY(true);
            //setting lines to display on x axis
                    multiRenderer.setShowGridX(true);*/
        //setting legend to fit the screen size
        multiRenderer.setFitLegend(true);
        //setting displaying line on grid
        multiRenderer.setShowGrid(true);
        //setting zoom to false
        multiRenderer.setZoomEnabled(true);
        //setting external zoom functions to false
        multiRenderer.setExternalZoomEnabled(false);
        //setting displaying lines on graph to be formatted(like using graphics)
        multiRenderer.setAntialiasing(true);
        //setting to in scroll to false
        multiRenderer.setInScroll(true);
        //setting to set legend height of the graph
        multiRenderer.setLegendHeight(30);
        //setting x axis label align
        multiRenderer.setXLabelsAlign(Paint.Align.CENTER);
        //setting X-axis text color
        multiRenderer.setXLabelsColor(Color.rgb(75, 75, 75));
        //setting y axis label to align
        multiRenderer.setYLabelsAlign(Paint.Align.RIGHT);
        //setting X-axis text color
        multiRenderer.setYLabelsColor(0, Color.rgb(75, 75, 75));
        //setting text style
        //multiRenderer.setTextTypeface("Roboto-Regular", Typeface.NORMAL);
        //setting no of values to display in y axis
        multiRenderer.setYLabels(10);
        // setting y axis max value, Since i'm using static values inside the graph so i'm setting y max value to 15000.
        // if you use dynamic values then get the max y value and set here
        multiRenderer.setYAxisMax(maxValue);
        //setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMin(-0.5);
        multiRenderer.setXAxisMax(resultArray.length);
        //Setting background color of the graph to transparent
        multiRenderer.setBackgroundColor(Color.TRANSPARENT);
        //Setting margin color of the graph to transparent
        multiRenderer.setMarginsColor(getActivity().getResources().getColor(R.color.transparent_background));
        multiRenderer.setApplyBackgroundColor(true);

        multiRenderer.setScale(2f);
        //setting x axis point size
        multiRenderer.setPointSize(4f);
        //setting the margin size for the graph in the order top, left, bottom, right
        multiRenderer.setMargins(new int[]{50, 30, 100, 30});
        //setting x-axis text of veritical view
        multiRenderer.setXLabelsAngle(280);

        for (int i = 0; i < resultArray.length;i++){
//                multiRenderer.addXTextLabel(i, resultArray[i]);
            multiRenderer.addXTextLabel(i, resultArray[i]+"            ");
        }
        multiRenderer.addSeriesRenderer(kwUsedRenderer);
        multiRenderer.addSeriesRenderer(spentRenderer);

        chartContainer.removeAllViews();
        //drawing chart
        mChart = ChartFactory.getLineChartView(this.getContext(), dataset, multiRenderer);
        //adding the view to the linearlayout
        chartContainer.addView(mChart);

    }

}





