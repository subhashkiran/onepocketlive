package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.subhashe.onepocket.Activities.BarCodeScannerActivity;
import com.subhashe.onepocket.Activities.EditProfile;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;

/**
 * Created by SubhashE on 3/2/2016.
 */
public class SettingsFragment extends Fragment {

    private View rootView;
    private LinearLayout editProfile,barcodeScanner;
//    fixed the bug in sprint-7 Reopened Ticket_Id-194 starts
    private ConnectionDetector mConnectionDetector;
//    fixed the bug in sprint-7 Reopened Ticket_Id-194 end
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Toast.makeText(getActivity(), "Settings Screen", Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_settings,container,false);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        final ActionBar actionBar = activity.getSupportActionBar();
//        fixed the bug in sprint-7 Reopened Ticket_Id-194 starts
        mConnectionDetector = new ConnectionDetector(getActivity());
//        fixed the bug in sprint-7 Reopened Ticket_Id-194 end
        if (actionBar != null) {
            actionBar.setTitle("Settings");
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE );
        }

        editProfile = (LinearLayout)rootView.findViewById(R.id.editProfileText);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fixed the bug in sprint-7 Reopened Ticket_Id-194 starts
                if (mConnectionDetector.isOnline()) {
                    Intent intent = new Intent(getActivity(), EditProfile.class);
                    getActivity().startActivity(intent);
//                getActivity().finish();
                }else{
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
//                fixed the bug in sprint-7 Reopened Ticket_Id-194 end
            }
        });

       /* barcodeScanner = (LinearLayout)rootView.findViewById(R.id.barCodeScanner);
        barcodeScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BarCodeScannerActivity.class);
                getActivity().startActivity(intent);
            }
        });
*/
        return rootView;
    }

    public void showAlertBox(final String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

        /*AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();*/
    }
}
