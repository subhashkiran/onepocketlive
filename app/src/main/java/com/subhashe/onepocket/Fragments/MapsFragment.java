/*
package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.subhashe.onepocket.Activities.GPSTracker;
import com.subhashe.onepocket.Adapters.ClearableAutoCompleteTextView;
import com.subhashe.onepocket.Adapters.GooglePlacesAutocompleteAdapter;
import com.subhashe.onepocket.Core.LatLang;
import com.subhashe.onepocket.Core.MapModel;
import com.subhashe.onepocket.Core.MyItem;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

*/
/**
 * Created by SubhashE on 1/21/2016.
 *//*

public class MapsFragment extends Fragment implements OnMapReadyCallback, AdapterView.OnItemClickListener,
        ClusterManager.OnClusterItemInfoWindowClickListener<MyItem>{
       //    public static final String JSON_URL = "http://192.168.168.141:8080/gridbot/maps";
    public static final String CONN2_PORT_TYPE = "conn2portType";
    public static final String STATION = "station";
    public static final String CONN1_PORT_TYPE = "conn1portType";
    public static final String STATION_STATUS = "stationStatus";
    public static final String CONN2_PORT_LEVEL = "conn2portLevel";
    public static final String CONN1_PORT_LEVEL = "conn1portLevel";
    public static final String PORT_QUANTITY = "portQuantity";

    private GoogleMap mMap;
    Marker marker1,marker2;
    private View rootView;
    FragmentManager myFragmentManager;
    SupportMapFragment mySupportMapFragment;
    private JsonUtil mJsonUtil;
    private ConnectionDetector mConnectionDetector;
    Button mMapView,mSatelliteView;
    ImageButton imageButton,searchButton;
    GPSTracker gpsTracker;
    boolean singleMarker = false;
    boolean searchLocationMarker = false;
    HashMap<String,MapModel> hashMapData;
    String response;
    private CameraPosition position;
    private LatLng currentLoc;
    private GooglePlacesAutocompleteAdapter mGooglePlaceAdapter;
    LayoutInflater layoutInflater;
    View v;
    String searchString = null;
    private ClusterManager<LatLang> mClusterManager;
    private LatLang clickedClusterItem;
    private Circle circle;
    private CircleOptions circleOptions;
    private static final String LOG_TAG = "MapSearch";


    //------------ make your specific key ------------
    private static final String API_KEY = "AIzaSyBE64USHAOPxQbSPl4jS78_zIDXRFgtDYY";
//    private static final String BROWSER_API_KEY = "AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
    // private static final String API_KEY = "AIzaSyAU9ShujnIg3IDQxtPr7Q1qOvFVdwNmWc4";
    // private static final String API_KEY = "AIzaSyA5ngSBpnoj2JYvjTuMrCrgJpaQ3wXFBSc";
    // map search data ends


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView  = inflater.inflate(R.layout.activity_maps,container,false);
        */
/*LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.search_bar, null);*//*

        final ActionBar actionBar =  ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_SHOW_HOME );
        try{
            initilizeMap(rootView);
            intitilizeActionbar(v);
            actionBar.setCustomView(v);

        }catch (Exception e) {
            e.printStackTrace();
        }
        return rootView ;
    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter  {
        private final View myContentsView;

        MyCustomAdapterForItems() {
            myContentsView = getActivity().getLayoutInflater().inflate(
                    R.layout.info_window, null);
        }
        @Override
        public View getInfoWindow(Marker marker) {

          */
/*  TextView tvTitle = ((TextView) myContentsView
                    .findViewById(R.id.txtTitle));
            TextView tvSnippet = ((TextView) myContentsView
                    .findViewById(R.id.txtSnippet));

            tvTitle.setText(clickedClusterItem.getTitle());
            tvSnippet.setText(clickedClusterItem.getSnippet());*//*


            return myContentsView;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }

    @Override
    public void onClusterItemInfoWindowClick(MyItem myItem) {

    }

    private void initilizeMap(View view){
        try{
            if(mMap == null){
                SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);

               */
/* AutoCompleteTextView autoCompView = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);

                autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter(this.getActivity(), R.layout.search_list));
                autoCompView.setOnItemClickListener(this);*//*


                mMapView = (Button)rootView.findViewById(R.id.mapview);
                mMapView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                });
                mSatelliteView = (Button)rootView.findViewById(R.id.satileteView);
                mSatelliteView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    }
                });
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void intitilizeActionbar(View v){
        final ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        // the view that contains the new clearable autocomplete text view
        final ClearableAutoCompleteTextView searchBox =  (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);

        // start with the text view hidden in the action bar
        searchBox.setVisibility(View.INVISIBLE);
        searchIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleSearch(false);
            }
        });

        searchBox.setOnClearListener(new ClearableAutoCompleteTextView.OnClearListener() {

            @Override
            public void onClear() {
                toggleSearch(true);
            }
        });
        searchBox.setAdapter(mGooglePlaceAdapter);
        searchBox.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // handle clicks on search resaults here
                searchString = (String) adapterView.getItemAtPosition(position);

                //keyboard closing window code starts 19-Feb-2014

                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                //keyboard closing window code ends 19-Feb-2014

                //  Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

                if (searchLocationMarker) {
                    marker2.remove();
                }
                List<Address> addresList = null;
                Geocoder geocoder = new Geocoder(getActivity());
                try {
                    addresList = geocoder.getFromLocationName(searchString, 3);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Address address = addresList.get(0);

                LatLng locationInfo = new LatLng(address.getLatitude(), address.getLongitude());
                initCamera(locationInfo);
                moveToNewLocation(locationInfo);
                marker2 = mMap.addMarker(new MarkerOptions().position(locationInfo).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title(searchString));
                // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationInfo, 1));
                searchLocationMarker = true;
            }

        });
    }

    protected void toggleSearch(boolean reset) {
      */
/*  LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.search_bar, null);*//*


        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);
        ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        if (reset) {
            // hide search box and show search icon
            searchBox.setText("");
            searchBox.setVisibility(View.GONE);
            searchIcon.setVisibility(View.VISIBLE);
            // hide the keyboard
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
        } else {
            // hide search icon and show search box
            searchIcon.setVisibility(View.GONE);
            searchBox.setVisibility(View.VISIBLE);
            searchBox.requestFocus();
            // show the keyboard
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(searchBox, InputMethodManager.SHOW_FORCED);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConnectionDetector = new ConnectionDetector(this.getActivity());
        mGooglePlaceAdapter =  new GooglePlacesAutocompleteAdapter(this.getActivity(), R.layout.search_list);
        layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = layoutInflater.inflate(R.layout.search_bar, null);
        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.actionbaritems, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
           */
/* case R.id.action_search:
                // search action

                return true;*//*

            */
/*case R.id.action_gpslocater:
                if(mConnectionDetector.isOnline()) {
                    LocationFound();
                }else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
                return true;*//*

        }

        return super.onOptionsItemSelected(item);
    }

    public void LocationFound(){
        boolean locationUpdate = true;
        gpsTracker = new GPSTracker(getActivity().getApplicationContext());

        Log.d("marker", " : " + singleMarker);
        if (singleMarker) {
            marker1.remove();
            marker1 = null;
        }
        if (gpsTracker.canGetLocation) {
            Log.d("gps tracker", " : " + gpsTracker.canGetLocation);
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

            currentLoc = new LatLng(latitude, longitude);
//            marker1 = mMap.addMarker(new MarkerOptions().position(currentLoc));
            marker1 = mMap.addMarker(new MarkerOptions().position(currentLoc).
                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).
                    title(getAddressFromLatLng(currentLoc)));
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 18));
            initCamera(currentLoc);
            moveToNewLocation(currentLoc);
            singleMarker = true;

        } else {
           showSettingsAlert();
        }
    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getActivity().startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    private void initCamera(LatLng temp) {
        position = CameraPosition.builder()
                .target(temp)
                .zoom(4f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();
    }

    private void moveToNewLocation(LatLng temp) {
//        mMap.addMarker(new MarkerOptions().position(temp).title(getAddressFromLatLng(temp)));
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);
        if(circle!=null){
            circle.remove();
        }
        circleOptions = new CircleOptions().center(temp)
                .radius(100).strokeColor(Color.parseColor("#696464")).fillColor(0x40ff0000).strokeWidth(4);
        circle = mMap.addCircle(circleOptions);
//        mMap.addCircle(new CircleOptions().center(temp).radius(100).strokeColor(Color.parseColor("#696464")).fillColor(0x40ff0000).strokeWidth(4));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLoc));
    }

    private String getAddressFromLatLng(LatLng latLng) {
        Geocoder geocoder = new Geocoder(this.getActivity());

        String address = "";
        try {
            address = geocoder
                    .getFromLocation(latLng.latitude, latLng.longitude, 1)
                    .get(0).getAddressLine(0);
        } catch (IOException e) {
        }

        return address;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mClusterManager = new ClusterManager<>(this.getActivity(), mMap);
        mClusterManager.setRenderer(new CustomRenderer<LatLang>(this.getActivity(), mMap, mClusterManager));
        if(mConnectionDetector.isOnline()) {
            new GoogleMapsTask().execute();
        }else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }
    }

    public class GoogleMapsTask extends AsyncTask<String, String, List<MapModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected List<MapModel> doInBackground(String... params) {

            try {
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(OnePocketUrls.GOOGLEMAPS_URL(),null,null);
                OnePocketLog.d("singup resp----------", response);
                int count = 0;
                ArrayList<MapModel> mapArrayList = new ArrayList<MapModel>();
                hashMapData = new HashMap<String,MapModel>();

                JSONArray temp = new JSONArray(response);
                Log.d("~~~~~~~~~~~~~", "m");
                Log.d("MainActivity.java", "temp : " + temp);

                for (int i = 0; i < temp.length(); i++) {
                    // for(int i=0;i<5;i++){
                    JSONObject e = temp.getJSONObject(i);
                    MapModel mapModel = new MapModel();
                    Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                    JSONObject data = e.getJSONObject("data");
                    Log.d("Json Object ", "data " + data);

                    // mapModel.setId(i);
                    mapModel.setConn2PortType((String) data.getString(CONN2_PORT_TYPE));
                    mapModel.setStation((String) data.getString(STATION));
                    mapModel.setConn1portType((String) data.getString(CONN1_PORT_TYPE));
                    mapModel.setStationStatus((String) data.getString(STATION_STATUS));
                    mapModel.setConn2portLevel((String) data.getString(CONN2_PORT_LEVEL));
                    mapModel.setConn1portLevel((String) data.getString(CONN1_PORT_LEVEL));
                    mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));



                    LatLang latLang = new LatLang();

                    if ((!e.getJSONArray("latLng").get(0).equals(null)) && (!e.getJSONArray("latLng").get(1).equals(null))) {
                        latLang.setLattitude((Double) e.getJSONArray("latLng").get(0));
                        latLang.setLongitude((Double) e.getJSONArray("latLng").get(1));

                    } else {
                        latLang.setLattitude(1.1);
                        latLang.setLongitude(1.1);

                    }
                    mapModel.setLatLang(latLang);

                    mapArrayList.add(mapModel);
                    Log.d("map arraylist " + count, " : " + mapArrayList);
                    count++;
                }
                Log.d("map arraylist", " : " + mapArrayList);
                Log.d("map ArrayList size :", " " + mapArrayList.size());


                return mapArrayList;
            } catch (Exception e) {
                OnePocketLog.d("singup error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<MapModel> result) {
            super.onPostExecute(result);

            //To do need the set the data to list
            //   LatLng mapDisplay;
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());

            for (int i = 0; i < result.size(); i++) {
                MapModel mMapModel = new MapModel();
                mMapModel = result.get(i);

                LatLng latlng = new LatLng(mMapModel.getLatLang().getLattitude(), mMapModel.getLatLang().getLongitude());
                LatLang latLang = new LatLang(latlng.latitude,latlng.longitude);

                MarkerOptions markerOptions = new MarkerOptions().position(latlng);

                Marker marker = mMap.addMarker(markerOptions);
                mClusterManager.addItem(latLang);
                // mMapModel.setMarkerId(marker.getId());
                hashMapData.put(marker.getId(), mMapModel);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
                mMap.setOnCameraChangeListener(mClusterManager);
//                mMap.setOnMarkerClickListener(mClusterManager);
                mMap.getUiSettings().setMapToolbarEnabled(false);

            }
*/
/*
            mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<LatLang>() {
                @Override
                public boolean onClusterClick(Cluster<LatLang> cluster) {
                    return false;
                }
            });*//*

           */
/* mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if(!mConnectionDetector.isOnline()) {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
                }
            });*//*


            */
/*mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    Log.d("clicked", "view");
                    view = null;
                    for (Map.Entry m : hashMapData.entrySet()) {
                        MapModel mm = new MapModel();
                        mm = (MapModel) m.getValue();

                        *//*
*/
/*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*//*
*/
/*
                        if ((marker.getId()).equals(m.getKey())) {
                            Log.d("markerId", " " + marker.getId());
                            Log.d("data", " " + m.getKey());

                            view = getActivity().getLayoutInflater().inflate(R.layout.map_info_layout, null);
                            final LinearLayout mInfo_window = (LinearLayout) view.findViewById(R.id.info_window);
                            TextView stationName = (TextView) view.findViewById(R.id.stationName);
                            //  TextView stationAddress = (TextView) view.findViewById(R.id.stationAddress);
                            TextView chargers = (TextView) view.findViewById(R.id.chargers);
                            TextView levelType1 = (TextView) view.findViewById(R.id.levelType1);
                            TextView levelType2 = (TextView) view.findViewById(R.id.levelType2);

                            stationName.setText(mm.getStation());
                            // stationAddress.setText(infoWindowModel.getStationAddress());
                            chargers.setText(mm.getPortQuantity());
                            levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                            levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                        }
                    }

                    Log.d("data", " view " + view);
                    return view;
                }
            });
*//*

            //information window code starts
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    Log.d("clicked", "view");
                    view = null;
                    for (Map.Entry m : hashMapData.entrySet()) {
                        MapModel mm = new MapModel();
                        mm = (MapModel) m.getValue();

                        */
/*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*//*

                        if ((marker.getId()).equals(m.getKey())) {
                            Log.d("markerId", " " + marker.getId());
                            Log.d("data", " " + m.getKey());

                            view = getActivity().getLayoutInflater().inflate(R.layout.map_info_layout, null);
                            final LinearLayout mInfo_window = (LinearLayout) view.findViewById(R.id.info_window);
                            TextView stationName = (TextView) view.findViewById(R.id.stationName);
                            //  TextView stationAddress = (TextView) view.findViewById(R.id.stationAddress);
//                            TextView chargers = (TextView) view.findViewById(R.id.chargers);
                            */
/*TextView levelType1 = (TextView) view.findViewById(R.id.levelType1);
                            TextView levelType2 = (TextView) view.findViewById(R.id.levelType2);*//*


                            stationName.setText(mm.getStation());
                            // stationAddress.setText(infoWindowModel.getStationAddress());
//                            chargers.setText(mm.getPortQuantity());
                          */
/*  levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                            levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());*//*


//                            Log.d("closeButton", "clicked " + closewindow);
                        }
                    }

                    Log.d("data", " view " + view);
                    return view;
                }
            });

            mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<LatLang>() {
                @Override
                public boolean onClusterItemClick(LatLang latLang) {
                    clickedClusterItem = latLang;
                    return false;
                }
            });

                    //information window code ends
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        View view = getActivity().getLayoutInflater().inflate(R.layout.map_info_layout, null);
                        final LinearLayout mInfo_window = (LinearLayout) view.findViewById(R.id.info_window);
                        ImageButton closewindow = (ImageButton) view.findViewById(R.id.closewindow);

                        @Override
                        public void onInfoWindowClick(Marker marker) {

                            if (marker.isVisible()) {
                                marker.setVisible(false);
                            }
                   */
/* closewindow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mInfo_window.setVisibility(View.INVISIBLE);
                        }
                    });*//*

                        }
                    });
        }
    }

    public class CustomRenderer<T extends ClusterItem> extends DefaultClusterRenderer<T>
    {
        public CustomRenderer(Context context, GoogleMap map, ClusterManager<T> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<T> cluster) {
            //start clustering if at least 2 items overlap
            return cluster.getSize() > 2;
        }
    }

    //map search code starts

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        searchString = (String) adapterView.getItemAtPosition(position);

        //keyboard closing window code starts 19-Feb-2014

        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

        //keyboard closing window code ends 19-Feb-2014

        //  Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

        if (searchLocationMarker) {
            marker2.remove();
        }
        List<Address> addresList = null;
        Geocoder geocoder = new Geocoder(this.getActivity());
        try {
            addresList = geocoder.getFromLocationName(searchString, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Address address = addresList.get(0);

        LatLng locationInfo = new LatLng(address.getLatitude(), address.getLongitude());
        initCamera(locationInfo);
        moveToNewLocation(locationInfo);
        marker2 = mMap.addMarker(new MarkerOptions().position(locationInfo).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title(searchString));
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationInfo, 1));
        searchLocationMarker = true;

    }



    public void showAlertBox(final String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               */
/* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*//*


        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

        */
/*AlertDialog alertDialog = new AlertDialog.Builder(
                this.getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();*//*

    }

    // map search code ends
}
*/
