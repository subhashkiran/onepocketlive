package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import com.subhashe.onepocket.Core.StatModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.NetworkChecker;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 28/1/16.
 */
public class TableStatsFragment extends Fragment {

    TextView stationNameTv, portTv, sessionIdTv, dateTv, chargeStartTv, chargeEndTv, TimeUsedPerHourTv, rateKwByTimeTv, spentTv, kwUsedTv;
    TableLayout tl1;
    TableRow tr1;
    View view,horizontalbar;;
    private ConnectionDetector mConnectionDetector;
    private ProgressDialog progress;
    private JsonUtil mJsonUtil;
    private String response,token;
    private int user_id;
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
//    ArrayList<StatModel> statList = new ArrayList<StatModel>();

//    fixed the bug in sprint-7 Ticket_Id-149 starts
 /*   private BroadcastReceiver tableBroadcastReceiver;
    boolean networkCheckerFlag;
    private static boolean firstConnect = true;
    private AlertDialog alertDialog;*/
//    fixed the bug in sprint-7 Ticket_Id-149 end

    public TableStatsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.tablestats_view, container, false);

        tl1=(TableLayout)view.findViewById(R.id.TablestatsTable);
        tl1.setColumnStretchable(0,true);
        tl1.setColumnStretchable(1,true);
        tl1.setColumnStretchable(2,true);
        tl1.setColumnStretchable(3,true);
        tl1.setColumnStretchable(4,true);
        tl1.setColumnStretchable(5,true);
        tl1.setColumnStretchable(6,true);
        tl1.setColumnStretchable(7,true);
       /* tl1.setColumnStretchable(8,true);
        tl1.setColumnStretchable(9, true);*/
        mConnectionDetector = new ConnectionDetector(getActivity());

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(ACC_USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

//        fixed the bug in sprint-7 Ticket_Id-149 starts

       /* IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");

        tableBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {

                if (firstConnect) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    networkCheckerFlag = NetworkChecker.getConnectivityStatusCheck(context);

//                    if (mConnectionDetector.isOnline() || networkCheckerFlag) {
                    if (mConnectionDetector.isOnline()) {

                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.cancel();
                        }
                        tl1.setVisibility(View.VISIBLE);
                        new StatsTable().execute();


                    } else {
                        tl1.setVisibility(View.GONE);
//                        Toast.makeText(getContext(), R.string.netNotAvl, Toast.LENGTH_SHORT).show();
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
                    firstConnect = false;
                }else{
                    firstConnect = true;
                }
            }
        };

        getActivity().registerReceiver(tableBroadcastReceiver,filter);*/

//        fixed the bug in sprint-7 Ticket_Id-149 end

      /*  if (mConnectionDetector.isOnline()) {
            new StatsTable().execute();
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }*/
//    fixed the bug in sprint-7 Ticket_Id-149 starts

        if (mConnectionDetector.isOnline()) {
            new StatsTable().execute();
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }

//    fixed the bug in sprint-7 Ticket_Id-149 end
//        new StatsTable().execute("http://192.168.168.141:8080/gridbot/services/admin/stat");

        return view;
    }


    //    fixed the bug in sprint-7 Ticket_Id-149 starts
    /*@Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(tableBroadcastReceiver);
    }*/
//    fixed the bug in sprint-7 Ticket_Id-149 end

    public class StatsTable extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.STATS_DRIVER() + user_id ;
//                response = mJsonUtil.postMethod(mUrl, null, null, null, null,null,null,null);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);


            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            ArrayList<StatModel> statList = new ArrayList<StatModel>();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if(response.contains("Already Requested GridKey") || response.contains("not yet processed")){
                showAlertBox(getString(R.string.gridkeyalready));
            }else if(response.contains("have already requested") || response.contains("maximum gridkey")){
                showAlertBox(getString(R.string.gridkeymax));
            }else{
                try{
                    JSONArray temp = new JSONArray(response);
                    Log.d("~~~~~~~~~~~~~", "m");
                    Log.d("MainActivity.java", "temp : " + temp);

                    for (int i = 0; i < temp.length(); i++) {
                        // for(int i=0;i<5;i++){
                        JSONObject e = temp.getJSONObject(i);

                        StatModel statModel = new StatModel();
                        Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                        //JSONObject data = e.getJSONObject("sessionId");
                        //(String) data.getString(CONN2_PORT_TYPE)

                        statModel.setSessionId(e.getString("sessionId"));
                        statModel.setKwUsed(e.getDouble("kWUsed"));
                        statModel.setTimeUsedPerHr(e.getDouble("timeUsedPerHr"));
                        statModel.setChargeEnd(e.getString("chargeEnd"));
                        statModel.setRateKWPerTime(e.getString("rateKWPerTime"));
                        statModel.setSpent(e.getDouble("spent"));
                        statModel.setDate(e.getString("date"));
                        statModel.setPortId(e.getString("portId"));
                        statModel.setChargeStart(e.getString("chargeStart"));
                        statModel.setStationName(e.getString("stationName"));

                        statList.add(statModel);
                    }

                    for (int i = 0; i < statList.size(); i++) {
                        StatModel statModel = new StatModel();
                        statModel = statList.get(i);

                        tr1 = new TableRow(getActivity());
                        horizontalbar = new View(getActivity());

                        stationNameTv = new TextView(getActivity());
                        portTv = new TextView(getActivity());
                        sessionIdTv = new TextView(getActivity());
                        dateTv = new TextView(getActivity());
                        chargeStartTv = new TextView(getActivity());
                        chargeEndTv = new TextView(getActivity());
                        TimeUsedPerHourTv = new TextView(getActivity());
                        rateKwByTimeTv = new TextView(getActivity());
                        spentTv = new TextView(getActivity());
                        kwUsedTv = new TextView(getActivity());

                        stationNameTv.setText(statModel.getStationName());
                        //tv1.setGravity(Gravity.CENTER);
                        //tv1.layout(5, 5, 5, 5);
//                        stationNameTv.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1.1f));
                        stationNameTv.setLayoutParams(new TableRow.LayoutParams(0, 100, 1.0f));

                        portTv.setText(statModel.getPortId());
//                        portTv.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.4f));
                        portTv.setLayoutParams(new TableRow.LayoutParams(0, 100, 0.5f));

                        sessionIdTv.setText(statModel.getSessionId());
//                        sessionIdTv.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.6f));
                        sessionIdTv.setLayoutParams(new TableRow.LayoutParams(0, 100, 0.8f));

                        dateTv.setText(statModel.getDate());
//                        dateTv.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.5f));
                        dateTv.setLayoutParams(new TableRow.LayoutParams(0, 100, 0.8f));

                        chargeStartTv.setText(statModel.getChargeStart());
//                        chargeStartTv.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.8f));
                        chargeStartTv.setLayoutParams(new TableRow.LayoutParams(0, 100, 1.1f));

                        chargeEndTv.setText(statModel.getChargeEnd());
//                        chargeEndTv.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1));
                        chargeEndTv.setLayoutParams(new TableRow.LayoutParams(0, 100, 1.1f));

                        TimeUsedPerHourTv.setText(String.valueOf(statModel.getTimeUsedPerHr()));
//                        TimeUsedPerHourTv.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.5f));
                        TimeUsedPerHourTv.setLayoutParams(new TableRow.LayoutParams(0, 100, 0.5f));

                        rateKwByTimeTv.setText(String.valueOf(statModel.getRateKWPerTime()));
                        rateKwByTimeTv.setGravity(Gravity.CENTER);
//                        rateKwByTimeTv.setLayoutParams(new TableRow.LayoutParams(0,android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1));
                        rateKwByTimeTv.setLayoutParams(new TableRow.LayoutParams(0,100, 1));

                        kwUsedTv.setText(String.valueOf(statModel.getKwUsed()));
                        kwUsedTv.setGravity(Gravity.CENTER);
//                        kwUsedTv.setLayoutParams(new TableRow.LayoutParams(0,android.view.ViewGroup.LayoutParams.WRAP_CONTENT, .8f));
                        kwUsedTv.setLayoutParams(new TableRow.LayoutParams(0,100, .8f));

                        spentTv.setText(String.valueOf("$" + statModel.getSpent()));
                        spentTv.setGravity(Gravity.CENTER);
//                        spentTv.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, .5f));
                        spentTv.setLayoutParams(new TableRow.LayoutParams(0, 100, .5f));


                        horizontalbar.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
                        horizontalbar.setBackgroundColor(Color.rgb(51, 51, 51));

                        tr1.addView(stationNameTv);
                        tr1.addView(portTv);
                        tr1.addView(sessionIdTv);
                        tr1.addView(dateTv);
                        tr1.addView(chargeStartTv);
                        tr1.addView(chargeEndTv);
                        tr1.addView(TimeUsedPerHourTv);
                        tr1.addView(rateKwByTimeTv);
                        tr1.addView(kwUsedTv);
                        tr1.addView(spentTv);

                        tl1.addView(tr1);
                        tl1.addView(horizontalbar);
                    }


                }catch (Exception e){

                }
            }
        }
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();
        
        /*AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();*/
    }
}





