package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.Circle;
import com.androidmapsextensions.CircleOptions;
import com.androidmapsextensions.ClusterGroup;
import com.androidmapsextensions.ClusteringSettings;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.SupportMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.ClusterManager;
import com.subhashe.onepocket.Activities.GPSTracker;
import com.subhashe.onepocket.Activities.MapMarkerInfoActivity;
import com.subhashe.onepocket.Adapters.ClearableAutoCompleteTextView;
import com.subhashe.onepocket.Adapters.GooglePlacesAutocompleteAdapter;
import com.subhashe.onepocket.Core.Constants;
import com.subhashe.onepocket.Core.ImageClusterOptionsProvider;
import com.subhashe.onepocket.Core.LatLang;
import com.subhashe.onepocket.Core.LatLangAll;
import com.subhashe.onepocket.Core.MapListModel;
import com.subhashe.onepocket.Core.MapModel;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;

import com.subhashe.onepocket.Core.MapModelAll;
import com.subhashe.onepocket.DataBase.DbHelper;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.NetworkChecker;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.subhashe.onepocket.R.drawable.station_available;

//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.Circle;
//import com.google.android.gms.maps.model.CircleOptions;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.subhashe.onepocket.Core.ClusteringSettings;

/**
 * Created by SubhashE on 1/21/2016.
 */
public class MapsFragmentAllStations extends BaseFragment implements  AdapterView.OnItemClickListener{
    //    public static final String JSON_URL = "http://192.168.168.141:8080/gridbot/maps";
    private static final double[] CLUSTER_SIZES = new double[]{180, 160, 144, 120, 96};
    public static final String CONN2_PORT_TYPE = "conn2portType";
    public static final String STATION = "station";
    public static final String CONN1_PORT_TYPE = "conn1portType";
    public static final String STATION_STATUS = "stationStatus";
    public static final String CONN2_PORT_LEVEL = "conn2portLevel";
    public static final String CONN1_PORT_LEVEL = "conn1portLevel";
    public static final String PORT_QUANTITY = "portQuantity";

    private SupportMapFragment mapFragment;
    //    private GoogleMap mMap;
    Marker marker1,marker2;
    private View rootView;
    FragmentManager myFragmentManager;
    SupportMapFragment mySupportMapFragment;
    private JsonUtil mJsonUtil;
    private ConnectionDetector mConnectionDetector;
    Button mMapView,mSatelliteView;
    //    FrameLayout mBottomLayout;
    private LinearLayout mViewA,mViewB;
    LinearLayout.LayoutParams mViewAget;
    LinearLayout.LayoutParams mViewBget;
    LinearLayout layoutStatus;
    FloatingActionButton mFab;
    TextView stationName,stationAddress,chargers,levelType1,levelType2,city , liveStatus;
    ImageButton imageButton,searchButton;
    GPSTracker gpsTracker;
    boolean singleMarker = false;
    boolean searchLocationMarker = false;
    Marker marker = null;
    Marker markerTell = null;
    List<Marker> markerList = new ArrayList<Marker>();
    List<Marker> markerListTell = new ArrayList<Marker>();
    HashMap<String,MapModel> hashMapData;
    HashMap<String,MapModelAll> hashMapDataAll;
    String response,responseAll,markerCheck;
    String chargerKey = null;
    private CameraPosition position;
    ProgressDialog progressDialog;
    private LatLng currentLoc,mLatLng;
    private GooglePlacesAutocompleteAdapter mGooglePlaceAdapter;
    LayoutInflater layoutInflater;
    List<MapListModel> mapListData;
    private DbHelper dbHelper;
    View v;
    String searchString = null;
    AlertDialog alertDialog;
    boolean searchLocation;
    private ClusterManager<LatLang> mClusterManager;
    private LatLang clickedClusterItem;
    private Circle circle;
    private CircleOptions circleOptions;
    private List<Marker> declusterifiedMarkers;
    private static final String LOG_TAG = "MapSearch";
    private static final double LN2 = 0.6931471805599453;
    private static final int WORLD_PX_HEIGHT = 256;
    private static final int WORLD_PX_WIDTH = 256;
    private static final int ZOOM_MAX = 21;

    private BroadcastReceiver ReceivefrmSERVICE;
    private boolean firstCHeck,networkCheckerFlag,reCheckFlag,alertFlag;
    //------------ make your specific key ------------
    private static final String API_KEY = "AIzaSyBE64USHAOPxQbSPl4jS78_zIDXRFgtDYY";
//    private static final String BROWSER_API_KEY = "AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
    // private static final String API_KEY = "AIzaSyAU9ShujnIg3IDQxtPr7Q1qOvFVdwNmWc4";
    // private static final String API_KEY = "AIzaSyA5ngSBpnoj2JYvjTuMrCrgJpaQ3wXFBSc";
    // map search data ends
    public static final String JSON_ARRAY_NAME = "fuel_stations";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView  = inflater.inflate(R.layout.activity_maps,container,false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        /*LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.search_bar, null);*/
        final ActionBar actionBar =  ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_SHOW_HOME );
        hashMapDataAll = new HashMap<String, MapModelAll>();
        reCheckFlag = false;
        markerCheck = "";
        alertDialog = new AlertDialog.Builder(
                this.getActivity()).create();
        try{
            initilizeMap(rootView);
            initFab();
            intitilizeActionbar(v);
            actionBar.setCustomView(v);

        }catch (Exception e) {
            e.printStackTrace();
        }
        return rootView ;
    }

    private void initFab() {
        mFab = (FloatingActionButton)rootView.findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(rootView, "FAB Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void initilizeMap(View view){
        try{
            mMapView = (Button)rootView.findViewById(R.id.mapview);
           /* if (mMapView.getViewTreeObserver().isAlive()) {
                mMapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressLint("NewApi") // We check which build version we are using.
                    @Override
                    public void onGlobalLayout() {

                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            mMapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            mMapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
//                        myMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                    }});
            }*/
//            mBottomLayout = (FrameLayout)rootView.findViewById(R.id.bottomFragment);

            mViewB = (LinearLayout)rootView.findViewById(R.id.viewB);
            mViewA = (LinearLayout)rootView.findViewById(R.id.viewA);
            stationName = (TextView) rootView.findViewById(R.id.stationName);
            stationAddress = (TextView) rootView.findViewById(R.id.stationAddress);
            liveStatus = (TextView) rootView.findViewById(R.id.stationLiveStatus);

            layoutStatus  = (LinearLayout)rootView.findViewById(R.id.station_layout_status);
//            chargers = (TextView) rootView.findViewById(R.id.chargers);
            city = (TextView) rootView.findViewById(R.id.city);
          /*  levelType1 = (TextView) rootView.findViewById(R.id.levelType1);
            levelType2 = (TextView) rootView.findViewById(R.id.levelType2);*/

         /*   mBottomLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getActivity(), "Bottom layout clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(),MapMarkerInfoActivity.class);
                    intent.putExtra("hashMap", hashMapData);
                    intent.putExtra("chargerMkey",chargerKey);
                    startActivity(intent);
                }
            }); */
            mViewB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getActivity(), "Bottom layout clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(),MapMarkerInfoActivity.class);
                    intent.putExtra("hashMap", hashMapData);
                    intent.putExtra("sendListData", (Serializable) mapListData);
                    intent.putExtra("chargerMkey",chargerKey);
                    intent.putExtra("markerCheck",markerCheck);
                    startActivity(intent);
                }
            });
            mMapView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            });
            mSatelliteView = (Button)rootView.findViewById(R.id.satileteView);
            mSatelliteView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                }
            });
           /* if(mMap == null){
                FragmentManager fm = getChildFragmentManager();
                mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
              *//*  SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager()
                        .findFragmentById(R.id.map);*//*
//                mapFragment.getMapAsync(this);
               *//* if(mapFragment != null){
                    mapFragment = createMapFragment();
                }*//*

                mMap =mapFragment.getExtendedMap();
                if(mMap != null){
                    setUpMap();
                }

                mMapView = (Button)rootView.findViewById(R.id.mapview);
                mMapView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                });
                mSatelliteView = (Button)rootView.findViewById(R.id.satileteView);
                mSatelliteView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    }
                });
            }*/
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    protected SupportMapFragment createMapFragment() {
        return SupportMapFragment.newInstance();
    }

    public void intitilizeActionbar(View v) {
        final ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        // the view that contains the new clearable autocomplete text view
        final ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);

        // start with the text view hidden in the action bar
//        mBottomLayout.setVisibility(View.INVISIBLE);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width=display.getWidth();
        int height=display.getHeight();

        mViewAget = (LinearLayout.LayoutParams) mViewA.getLayoutParams();
        mViewBget = (LinearLayout.LayoutParams) mViewB.getLayoutParams();

        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width+mViewBget.width,mViewAget.height+mViewBget.height));

        //mFab.setVisibility(View.INVISIBLE);
        mViewB.setVisibility(View.INVISIBLE);
        searchBox.setVisibility(View.INVISIBLE);
        searchIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleSearch(false);
            }
        });

        searchBox.setOnClearListener(new ClearableAutoCompleteTextView.OnClearListener() {

            @Override
            public void onClear() {
                toggleSearch(true);
            }
        });

        searchBox.setAdapter(mGooglePlaceAdapter);
        searchBox.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // handle clicks on search resaults here
                searchString = (String) adapterView.getItemAtPosition(position);
                searchLocation = true;
                //keyboard closing window code starts 19-Feb-2014

                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                //keyboard closing window code ends 19-Feb-2014

                //  Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

                if (searchLocationMarker) {
                    marker2.remove();
                }
                List<Address> addresList = null;
                Geocoder geocoder = new Geocoder(getActivity());
                try {
                    addresList = geocoder.getFromLocationName(searchString, 3);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Address address = addresList.get(0);

                LatLng locationInfo = new LatLng(address.getLatitude(), address.getLongitude());
                initCamera(locationInfo);
                moveToNewLocation(locationInfo);
                marker2 = mMap.addMarker(new MarkerOptions().position(locationInfo).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(searchString));
                // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationInfo, 1));
                searchLocationMarker = true;
            }

        });
    }

    protected void toggleSearch(boolean reset) {
      /*  LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.search_bar, null);*/

        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);
        ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        if (mConnectionDetector.isOnline()) {
            if (reset) {
                // hide search box and show search icon
                searchBox.setText("");
                searchBox.setVisibility(View.GONE);
                searchIcon.setVisibility(View.VISIBLE);
                // hide the keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
            } else {
                // hide search icon and show search box
                searchIcon.setVisibility(View.GONE);
                searchBox.setVisibility(View.VISIBLE);
                searchBox.requestFocus();
                // show the keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchBox, InputMethodManager.SHOW_FORCED);
            }
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConnectionDetector = new ConnectionDetector(this.getActivity());
        mGooglePlaceAdapter =  new GooglePlacesAutocompleteAdapter(this.getActivity(), R.layout.search_list);
        layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = layoutInflater.inflate(R.layout.search_bar, null);
        dbHelper  = new DbHelper(getContext());

        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.actionbaritems, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
           /* case R.id.action_search:
                // search action

                return true;*/
            /*case R.id.action_gpslocater:
                if(mConnectionDetector.isOnline()) {
                    LocationFound();
                }else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;*/
        }

        return super.onOptionsItemSelected(item);
    }


    public void LocationFound(){
        boolean locationUpdate = true;
        gpsTracker = new GPSTracker(getActivity().getApplicationContext());

        Log.d("marker", " : " + singleMarker);
        if (singleMarker) {
            if (marker1 != null) {
                marker1.remove();
//                marker1 = null;
            }
        }
        if (gpsTracker.canGetLocation) {
            Log.d("gps tracker", " : " + gpsTracker.canGetLocation);
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

            mViewB.setVisibility(View.INVISIBLE);
            mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));

            currentLoc = new LatLng(latitude, longitude);
//            marker1 = mMap.addMarker(new MarkerOptions().position(currentLoc));

            String locationString = getAddressFromLatLng(currentLoc);
            if(locationString.contains("Please try again")){
                Toast.makeText(getActivity(), "Location service failed. Please try again", Toast.LENGTH_SHORT).show();
            }else{
                marker1 = mMap.addMarker(new MarkerOptions().position(currentLoc).
                        icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).
                        title(locationString));


//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 18));
                initCamera(currentLoc);
                moveToNewLocation(currentLoc);
                singleMarker = true;
            }
        } else {
            showSettingsAlert();
        }
    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getActivity().startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    private void initCamera(LatLng temp) {
//        if (searchLocation) {
        position = CameraPosition.builder()
                .target(temp)
                .zoom(12f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();
        searchLocation = false;
     /*   } else {
            position = CameraPosition.builder()
                    .target(temp)
                    .zoom(15f)
                    .bearing(0.0f)
                    .tilt(0.0f)
                    .build();
        }*/
    }

    private void moveToNewLocation(LatLng temp) {
//        mMap.addMarker(new MarkerOptions().position(temp).title(getAddressFromLatLng(temp)));
//        float zoom = mMap.getMinZoomLevelNotClustered(marker1);
        mViewB.setVisibility(View.INVISIBLE);
        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);
        if(circle!=null){
            circle.remove();
        }
        circleOptions = new CircleOptions().center(temp)
                .radius(1000).strokeColor(Color.parseColor("#696464")).fillColor(Color.TRANSPARENT).strokeWidth(4);
        circle = mMap.addCircle(circleOptions);
//        mMap.addCircle(new CircleOptions().center(temp).radius(1000).strokeColor(Color.parseColor("#696464")).fillColor(Color.parseColor("lightgray")).strokeWidth(4));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLoc));
    }

    private String getAddressFromLatLng(LatLng latLng) {
        Geocoder geocoder = new Geocoder(this.getActivity());

//        Location address;
        String returnAdress = "Please try again";
        try {
            List<Address> address  = geocoder
                    .getFromLocation(latLng.latitude, latLng.longitude, 1);
            if(address != null && (address.size()!= 0)){
                Address fetchedAddress = address.get(0);
                if(fetchedAddress != null) {
                    returnAdress = fetchedAddress.getAddressLine(0);
                }
               /* for(int i=0; i<fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(.getAddressLine(i)).append("\n");
                }*/

            }

        } catch (IOException e) {
        }

        return returnAdress;
    }

    protected void setUpBroadCast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");

        ReceivefrmSERVICE = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                networkCheckerFlag = NetworkChecker.getConnectivityStatusCheck(context);

                if(mConnectionDetector.isOnline() && networkCheckerFlag) {
//            if(hashMapData== null && hashMapDataAll == null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        if(!reCheckFlag) {
                            reCheckFlag = true;
                            new GoogleMapsTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            new GoogleMapsTaskAllStations().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                        }
                    } else {
                        new GoogleMapsTask().execute();
                    }
//            }
                }else {
                  /*  if(!alertDialog.isShowing()) {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }*/
                    Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                }
            }
        };

        getActivity().registerReceiver(ReceivefrmSERVICE, filter);
    }

    protected void setUpMap(){
        if(!firstCHeck){
            if(!networkCheckerFlag){
               networkCheckerFlag = true;
            }/*else{
                networkCheckerFlag = false;
            }*/
            firstCHeck = true;
        }


    }
   /* @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        mClusterManager = new ClusterManager<>(this.getActivity(), mMap);
//        mClusterManager.setRenderer(new CustomRenderer<LatLang>(this.getActivity(), mMap, mClusterManager));
        if(mConnectionDetector.isOnline()) {
            new GoogleMapsTask().execute();
        }else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }
    }*/

    public class GoogleMapsTask extends AsyncTask<String, String, List<MapModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected List<MapModel> doInBackground(String... params) {

            try {
                if(mConnectionDetector.isOnline() && networkCheckerFlag) {

//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(OnePocketUrls.GOOGLEMAPS_URL(),null,null);
                OnePocketLog.d("singup resp----------", response);
                int count = 0;
                ArrayList<MapModel> mapArrayList = new ArrayList<MapModel>();
                hashMapData = new HashMap<String,MapModel>();

                JSONArray temp = new JSONArray(response);
                Log.d("~~~~~~~~~~~~~", "m");
                Log.d("MainActivity.java", "temp : " + temp);

                for (int i = 0; i < temp.length(); i++) {
                    // for(int i=0;i<5;i++){
                    JSONObject e = temp.getJSONObject(i);
                    MapModel mapModel = new MapModel();
                    Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                    JSONObject data = e.getJSONObject("data");
                    Log.d("Json Object ", "data " + data);

                    // mapModel.setId(i);
                    mapModel.setConn2PortType((String) data.getString(CONN2_PORT_TYPE));
                    mapModel.setStationName((String) data.getString(STATION));
                    mapModel.setConn1portType((String) data.getString(CONN1_PORT_TYPE));
                    mapModel.setStationStatus((String) data.getString(STATION_STATUS));
                    mapModel.setConn2portLevel((String) data.getString(CONN2_PORT_LEVEL));
                    mapModel.setConn1portLevel((String) data.getString(CONN1_PORT_LEVEL));
                    mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));



                    LatLang latLang = new LatLang();

                    if ((!e.getJSONArray("latLng").get(0).equals(null)) && (!e.getJSONArray("latLng").get(1).equals(null))) {
                        latLang.setLattitude((Double) e.getJSONArray("latLng").get(0));
                        latLang.setLongitude((Double) e.getJSONArray("latLng").get(1));

                    } else {
                        latLang.setLattitude(1.1);
                        latLang.setLongitude(1.1);

                    }
                    mapModel.setLatLang(latLang);

                    mapArrayList.add(mapModel);
                    Log.d("map arraylist " + count, " : " + mapArrayList);
                    count++;
                }
                Log.d("map arraylist", " : " + mapArrayList);
                Log.d("map ArrayList size :", " " + mapArrayList.size());

                return mapArrayList;

                }else{
                    Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                OnePocketLog.d("singup error----------", e.toString());
//                showAlertBox(getResources().getString(R.string.netNotAvl));
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<MapModel> result) {
            super.onPostExecute(result);

            //To do need the set the data to list
            //   LatLng mapDisplay;
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                for (int i = 0; i < result.size(); i++) {
                    MapModel mMapModel = new MapModel();
                    mMapModel = result.get(i);

                    LatLng latlng = new LatLng(mMapModel.getLatLang().getLattitude(), mMapModel.getLatLang().getLongitude());
                    LatLang latLang = new LatLang(latlng.latitude, latlng.longitude);

                    MarkerOptions markerOptions = new MarkerOptions().position(latlng);

                    Marker marker = mMap.addMarker(markerOptions);

                    hashMapData.put(marker.getId(), mMapModel);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
                    mMap.getUiSettings().setMapToolbarEnabled(true);
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                }
            }else{
                Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if (marker.isCluster()) {
                        if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                            declusterify(marker);
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                    return false;
                }
            });
            //information window code starts
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    Log.d("clicked", "view");
                    view = null;
                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                        for (Map.Entry m : hashMapData.entrySet()) {
                            MapModel mm = new MapModel();
                            mm = (MapModel) m.getValue();


                          /*  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.clear();
                            editor1.putString(CHARGER_ID, m.getKey().toString());
                            editor1.commit();*/
                        /*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*/
                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
                                chargerKey = m.getKey().toString();
//                                mBottomLayout.setVisibility(View.VISIBLE);

                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width , mViewAget.height,mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height,mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);

                                stationName.setText(mm.getStationName());
                                stationAddress.setText(mm.getStationAddress());
                                Log.d("stationAdd", " " + mm.getStationAddress());

                                liveStatus.setText(mm.getLiveStatus());

                                Log.d("stationLiveStatus", " " + mm.getLiveStatus());
                                String statusMsg = liveStatus.toString();
                                if (statusMsg.contains("Available"))
                                {
                                    layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
                                }
                                else if(statusMsg.equals("In-Use"))
                                {
                                    layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
                                }

                                markerCheck="";
                                markerCheck = "Type1";

//                                chargers.setText(mm.getPortQuantity());
                                city.setText("");
                                levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                                levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    Log.d("data", " view " + view);
                    return view;
                }
            });

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng position) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    } else {
                        mViewB.setVisibility(View.INVISIBLE);
                        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));
                    }
                }
            });

            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
//                        mMap.animateCamera(CameraUpdateFactory.zoomTo(0.0f));
//                        mBottomLayout.setVisibility(View.INVISIBLE);
                    }
                }
            });
            //information window code ends
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }

                    if (!mConnectionDetector.isOnline()) {
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    public class GoogleMapsTaskAllStations extends AsyncTask<String, String, List<MapModelAll>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setCancelable(false);
//            progressDialog.setTitle("Connecting to Server");
//            progressDialog.setMessage("This process can take a few seconds. Please wait....");
                progressDialog.setMessage("Please wait....");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.show();
            }else{
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        protected List<MapModelAll> doInBackground(String... params) {

          /*  try {
                responseAll = mJsonUtil.getMethod(OnePocketUrls.GOOGLEMAPS_ALL_STATIONS_URL(),null,null);
                OnePocketLog.d("GOOGLEMAPS_ALL_STATIONS_URL----------", responseAll);

                ArrayList<MapModelAll> mapArrayList = new ArrayList<MapModelAll>();
                hashMapDataAll = new HashMap<String, MapModelAll>();


                JsonFactory factory = new MappingJsonFactory();

                Log.d("Json factory: ", "" + factory);

                JsonParser jsonParser = factory.createParser(responseAll);

                Log.d("Json Parser: ", "" + jsonParser);

                JsonToken token;
                token = jsonParser.nextToken();
                Log.d("Json token: ", "" + token);
               *//* ArrayList objectArray = new ArrayList();*//*

                if (token != JsonToken.START_OBJECT) {
                    System.out.println("Error: root should be object: quiting.");
                }

                while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
                    String fieldName = jsonParser.getCurrentName();
                    Log.d("fieldName: ", "" + fieldName);
                    // move from field name to field value
                    token = jsonParser.nextToken();
                    Log.d("token: ", "" + token);
                    if(fieldName == null){
                        return null;
                    }
                    if (fieldName.equals(Constants.JSON_ARRAY_NAME)) {
                        if (token == JsonToken.START_ARRAY) {
                            // For each of the records in the array
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                // read the record into a tree model,
                                // this moves the parsing position to the end of it
                                JsonNode node = jsonParser.readValueAsTree();
                                Log.d("node: ", "" + node);

                                MapModelAll mapModel = new MapModelAll();

                                mapModel.setAccess_days_time(node.get(Constants.ACCESS_DAYS_TIME).textValue());
                                mapModel.setCards_accepted(node.get(Constants.CARDS_ACCEPTED).textValue());
                                mapModel.setDate_last_confirmed(node.get(Constants.DATE_LAST_CONFIRMED).textValue());
                                mapModel.setExpected_date(node.get(Constants.EXPECTED_DATE).textValue());
                                mapModel.setFuel_type_code(node.get(Constants.FUEL_TYPE_CODE).textValue());
                                mapModel.setId(node.get(Constants.ID).intValue());
                                mapModel.setGroups_with_access_code(node.get(Constants.GROUPS_WITH_ACCESS_CODE).textValue());

                                mapModel.setOpen_date(node.get(Constants.OPEN_DATE).textValue());
                                mapModel.setOwner_type_code(node.get(Constants.OWNER_TYPE_CODE).textValue());
                                mapModel.setStatus_code(node.get(Constants.STATUS_CODE).textValue());
                                mapModel.setStation_name(node.get(Constants.STATION_NAME).textValue());
                                mapModel.setStation_phone(node.get(Constants.STATION_PHONE).textValue());
                                mapModel.setUpdated_at(node.get(Constants.UPDATED_AT).textValue());
                                mapModel.setGeocode_status(node.get(Constants.GEOCODE_STATUS).textValue());

                                LatLangAll latLang = new LatLangAll();
                                latLang.setLatitude(node.get(Constants.LATITUDE).doubleValue());
                                latLang.setLongitude(node.get(Constants.LONGITUDE).doubleValue());
                                mapModel.setLatLang(latLang);

                                mapModel.setCity(node.get(Constants.CITY).textValue());
                                mapModel.setIntersection_directions(node.get(Constants.INTERSECTION_DIRECTION).textValue());
                                mapModel.setPlus4(node.get(Constants.PLUS4).textValue());
                                mapModel.setState(node.get(Constants.STATE).textValue());
                                mapModel.setStreet_address(node.get(Constants.STREET_ADDRESS).textValue());
                                mapModel.setZip(node.get(Constants.ZIP).textValue());
                                mapModel.setBd_blends(node.get(Constants.BD_BLENDS).textValue());
                                mapModel.setE85_blender_pump(node.get(Constants.E85_BLENDER_PUMP).textValue());


                                //added code starts on 9-3-16
                                List connectorList = new ArrayList();

                                Log.d("Ev Size", " : " + node.get(Constants.EV_CONNECTOR_TYPES).size());
                                for(int count=0;count<node.get(Constants.EV_CONNECTOR_TYPES).size();count++) {
                                    connectorList.add( node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                    Log.d("Ev node type", " : " + node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                }

                                mapModel.setConnectorTypesList(connectorList);
                                //added code ends on 9-3-16

                                mapModel.setEv_connector_types(node.get(Constants.EV_CONNECTOR_TYPES).textValue());
                                mapModel.setEv_dc_fast_num(node.get(Constants.EV_DC_FAST_NUM).intValue());
                                mapModel.setEv_level1_evse_num(node.get(Constants.EV_LEVEL1_EVSE_NUM).intValue());
                                mapModel.setEv_level2_evse_num(node.get(Constants.EV_LEVEL2_EVSE_NUM).intValue());
                                mapModel.setEv_network(node.get(Constants.EV_NETWORK).textValue());
                                mapModel.setEv_network_web(node.get(Constants.EV_NETWORK_WEB).textValue());
                                mapModel.setEv_other_evse(node.get(Constants.EV_OTHER_EVSE).textValue());
                                mapModel.setHy_status_link(node.get(Constants.HY_STATUS_LINK).textValue());
                                mapModel.setLpg_primary(node.get(Constants.LPG_PRIMARY).textValue());
                                mapModel.setNg_fill_type_code(node.get(Constants.NG_FILL_TYPE_CODE).textValue());
                                mapModel.setNg_psi(node.get(Constants.NG_PSI).textValue());
                                mapModel.setNg_vehicle_class(node.get(Constants.NG_VEHICLE_CLASS).textValue());


                                mapArrayList.add(mapModel);
                                //  dbHelper.addStationDetails(mapModel);


                            }
                        } else {
                            System.out.println("Error: records should be an array: skipping.");
                            jsonParser.skipChildren();
                        }
                    } else {
                        System.out.println("Unprocessed property: " + fieldName);
                        jsonParser.skipChildren();
                    }
                }

                return mapArrayList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;*/
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                ArrayList<MapModelAll> dbList = dbHelper.getAllStationRecords();

                Log.d("mapModel list size", ":" + dbList.size());

                return dbList;
            }else{
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
               /* if(!alertDialog.isShowing()) {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }*/
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<MapModelAll> result) {
            super.onPostExecute(result);

            if(result == null){
                return;
            }
            progressDialog.dismiss();
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());

            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                // int i;
                for (int i = 0; i < result.size(); i++) {
                    MapModelAll mMapModel = new MapModelAll();
                    mMapModel = result.get(i);
                    // MyMarker myMarker = new MyMarker();
                    LatLng latlng = new LatLng(mMapModel.getLattitude(), mMapModel.getLongitude());

                    MarkerOptions markerOptions = new MarkerOptions().position(latlng);

                    marker = mMap.addMarker(markerOptions);
                    markerList.add(marker);

                    hashMapDataAll.put(marker.getId(), mMapModel);
                }
            }else{
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if (marker.isCluster()) {
                        declusterify(marker);
                        return true;
                    }
                    return false;
                }
            });

//            progressDialog.dismiss();
            // Log.d("Total Markers ", ":" + i);

            // information window code starts

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    view = null;
                    Log.d("declusterifiedMarkers", "" + declusterifiedMarkers);

                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                        for (Map.Entry m : hashMapDataAll.entrySet()) {
                            MapModelAll mm = new MapModelAll();
                            mm = (MapModelAll) m.getValue();

                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
//                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());

                                chargerKey = m.getKey().toString();
                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                ;

                                mapListData = new ArrayList<MapListModel>();

                                MapListModel mapListModel = new MapListModel();

                                mapListModel.setAccess_days_time(mm.getAccess_days_time());
                                mapListModel.setFuel_type_code(mm.getFuel_type_code());
                                mapListModel.setGroups_with_access_code(mm.getGroups_with_access_code());
                                mapListModel.setStation_name(mm.getStation_name());
                                mapListModel.setStation_phone(mm.getStation_phone());
                                mapListModel.setStatus_code(mm.getStatus_code());
                                mapListModel.setCity(mm.getCity());
                                mapListModel.setState(mm.getState());
                                mapListModel.setStreet_address(mm.getStreet_address());
                                mapListModel.setZip(mm.getZip());
                                mapListModel.setBd_blends(mm.getBd_blends());
                                mapListModel.setE85_blender_pump(mm.getE85_blender_pump());
                                markerCheck="";
                                markerCheck = "Type2";

                               /* List connectorList = null;
                                StringBuffer sb = new StringBuffer();
                                // Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());
                                for (int count = 0; count < mm.getConnectorTypesList().size() ; count++) {
                                    Log.d("get data", ": " + mm.getConnectorTypesList().get(count));
                                    sb.append(mm.getConnectorTypesList().get(count) + "  ");
                                    // connectorList = new ArrayList();
                                    //   connectorList.add(mm.getConnectorTypesList().get(count));
                                }
                                sb.toString();
                                Log.d("sb value", " : " + sb.toString());

                                Log.d("cL at fragment", ": " + connectorList);*/
                                //   mapListModel.setConnectorTypesList(connectorList);
                                Log.d("cL at after", ": " + mapListModel.getConnectorTypesList());
                                mapListModel.setEv_connector_types(mm.getEv_connector_types());
                                // mapLis.setEv_connector_types(mm.getEv_connector_types());
                                mapListModel.setEv_dc_fast_num(mm.getEv_dc_fast_num());
                                mapListModel.setEv_level1_evse_num(mm.getEv_level1_evse_num());
                                mapListModel.setEv_level2_evse_num(mm.getEv_level2_evse_num());
                                mapListModel.setEv_network(mm.getEv_network());
                                mapListModel.setEv_network_web(mm.getEv_network_web());
                                mapListModel.setEv_other_evse(mm.getEv_other_evse());
                                mapListModel.setHy_status_link(mm.getHy_status_link());
                                mapListModel.setLpg_primary(mm.getLpg_primary());
                                mapListModel.setNg_fill_type_code(mm.getNg_fill_type_code());
                                mapListModel.setNg_psi(mm.getNg_psi());
                                mapListModel.setNg_vehicle_class(mm.getNg_vehicle_class());

                                mapListData.add(mapListModel);

                                Log.d("MapModelList", ": " + mapListData.get(0));

                            /*for(int i= 0;i<mapListData.size();i++) {
                                MapListModel mapListModel1 = new MapListModel();
                                mapListModel1 = mapListData.get(i);
                                Log.d("MLD getStatus_code", "" + mapListModel1.getStatus_code());
                                Log.d("MLD getStation_name", "" + mapListModel1.getStation_name());
                                Log.d("MLD getCity", "" + mapListModel1.getCity());
                                Log.d("MLD connector_types", "" + mapListModel1.getEv_connector_types());
                                Log.d("MLD level1_evse_num", "" + mapListModel1.getEv_level1_evse_num());
                                Log.d("MLD level2_evse_num", "" + mapListModel1.getEv_level2_evse_num());
                            }*/

                                stationName.setText(mm.getStation_name());
                                stationAddress.setText(mm.getStation_address());
                                liveStatus.setText(mm.getLive_status());
                                //    statusCode.setText(mm.getStatus_code());
                                city.setText(mm.getCity());
                                //   evconnectortypes.setText(mm.getEv_connector_types());
                                //   evlevel1evsenum.setText(mm.getEv_level1_evse_num());
                                // evlevel2evsenum.setText(mm.getEv_level1_evse_num()+"/"+mm.getEv_level2_evse_num());
                                //  evconnectortypes.setText((String) mm.getConnectorTypesList().get(0));

                               /* //add code starts 9-3-16
                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());
                                for (int count = 0; count < mm.getConnectorTypesList().size(); count++) {
                                    //  String temp = (String) mm.getConnectorTypesList().get(count);
                                    Log.d("get data", ": " + mm.getConnectorTypesList().get(count));
                                }
                                //add code starts 9-3-16*/

                           /* Log.d("getStation_name", ":" + mm.getStation_name());
                            Log.d("getCity", ":" + mm.getCity());
                            Log.d("getEv_connector_types",":"+ mm.getEv_connector_types());
                            Log.d("getEv_level1_evse_num", ":" + mm.getEv_level1_evse_num());
                            Log.d("getEv_level2_evse_num",":"+ mm.getEv_level2_evse_num());*/
                            }
                        }
                        //
                        for (Map.Entry m : hashMapData.entrySet()) {
                            MapModel mm = new MapModel();
                            mm = (MapModel) m.getValue();


                          /*  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.clear();
                            editor1.putString(CHARGER_ID, m.getKey().toString());
                            editor1.commit();*/
                        /*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*/
                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
                                chargerKey = m.getKey().toString();
//                                mBottomLayout.setVisibility(View.VISIBLE);

                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
//                                String temp = initStationName(mm.getLatLang());

                                stationName.setText(mm.getStationName());
                                 stationAddress.setText(mm.getStationAddress());
                                liveStatus.setText(mm.getLiveStatus());
//                                chargers.setText(mm.getPortQuantity());
                                city.setText("null");
                                levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                                levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    return view;
                }
            });
            // information window code end

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }
                }
            });
            Log.d("Info window success ", "");


            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {


                @Override
                public void onCameraChange(CameraPosition cameraPosition) {

                    //  Toast.makeText(getContext(), "Please Wait...", Toast.LENGTH_SHORT).show();

                    LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

                   /* Log.d("LatlngbOunds",":"+bounds);
                    Log.d("markerList",":"+markerList);
                    Log.d("markerList size",":"+markerList.size());*/

                    for (Marker marker1 :markerList) {
                        if (bounds.contains(marker1.getPosition())) {
                            marker1.setVisible(true);
                        } else {
                            marker1.setVisible(false);
                        }
                    }
                    for (Marker markerT :markerListTell) {
                        if (bounds.contains(markerT.getPosition())) {
                            markerT.setVisible(true);
                        } else {
                            markerT.setVisible(false);
                        }
                    }
                }
            });
    }}

    private String initStationName(LatLang latLang){

        double lat = latLang.getLattitude();
        double lon = latLang.getLongitude();
        mLatLng = new LatLng(lat, lon);

        String locationString = getAddressFromLatLng(mLatLng);
        return locationString;
    }

    private void declusterify(Marker cluster) {
//        clusterifyMarkers();
        if (mConnectionDetector.isOnline() && networkCheckerFlag) {
            Toast.makeText(getContext(), "Please Wait....", Toast.LENGTH_SHORT).show();
            declusterifiedMarkers = cluster.getMarkers();
//        LatLng clusterPosition = cluster.getPosition();
//        double distance = calculateDistanceBetweenMarkers();
//        double currentDistance = -declusterifiedMarkers.size() / 2 * distance;
    /*    for (Marker marker : declusterifiedMarkers) {
            marker.setData(marker.getPosition());
            marker.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
            LatLng newPosition = new LatLng(marker.getPosition().latitude,marker.getPosition().longitude*//*clusterPosition.latitude, clusterPosition.longitude + currentDistance*//*);
            marker.animatePosition(newPosition);
//            currentDistance += distance;
        }*/
//        mBottomLayout.setVisibility(View.INVISIBLE);
            mViewB.setVisibility(View.INVISIBLE);
            mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));
            LatLngBounds.Builder builder = LatLngBounds.builder();
//        List<Marker> val= checkDupMarkers(declusterifiedMarkers);
            Set set = new HashSet();
            List newList = new ArrayList<Marker>();
            for (Iterator iter = declusterifiedMarkers.iterator(); iter.hasNext(); ) {
                Marker element = (Marker) iter.next();
                if (set.add(element.getPosition()))
                    newList.add(element);
            }
            declusterifiedMarkers.clear();
            declusterifiedMarkers.addAll(newList);

     /*   HashSet<Marker> listToSet = new HashSet<Marker>(declusterifiedMarkers);
        List<Marker> listWithoutDuplicates = new ArrayList<Marker>(listToSet);*/

            for (Marker marker : declusterifiedMarkers) {
//            if(!marker.equals(marker1)) {
                builder.include(marker.getPosition());
//            }
            }
            final LatLngBounds bounds = builder.build();
        /*int zoomLevel;
        zoomLevel = getBoundsZoomLevel(bounds,);*/
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(adjustBoundsForMaxZoomLevel(bounds),
                    getResources().getDimensionPixelSize(R.dimen.padding)));
        }else{
            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }
//        mMap.animateCamera(CameraUpdateFactory.zoomBy(zoomLevel));

        /*if(mMap.getCameraPosition().zoom > 4.0f){
           mMap.animateCamera(CameraUpdateFactory.zoom);
        }*/
    }

  /*  private List<Marker> checkDupMarkers( List<Marker> decluster){
        Set<Marker> s = new TreeSet<Marker>(new Comparator<Marker>() {
            @Override
            public int compare(Marker lhs, Marker rhs) {
                if(lhs.getId().equalsIgnoreCase(rhs.getId())){
                    return 0;
                }
                return 1;
            }
        });
        s.addAll(decluster);
        return decluster;
    }*/

    private LatLngBounds adjustBoundsForMaxZoomLevel(LatLngBounds bounds) {
        LatLng sw = bounds.southwest;
        LatLng ne = bounds.northeast;
        double deltaLat = Math.abs(sw.latitude - ne.latitude);
        double deltaLon = Math.abs(sw.longitude - ne.longitude);

        final double zoomN = 0.005; // minimum zoom coefficient
        if (deltaLat < zoomN) {
            sw = new LatLng(sw.latitude - (zoomN - deltaLat / 2), sw.longitude);
            ne = new LatLng(ne.latitude + (zoomN - deltaLat / 2), ne.longitude);
            bounds = new LatLngBounds(sw, ne);
        }
        else if (deltaLon < zoomN) {
            sw = new LatLng(sw.latitude, sw.longitude - (zoomN - deltaLon / 2));
            ne = new LatLng(ne.latitude, ne.longitude + (zoomN - deltaLon / 2));
            bounds = new LatLngBounds(sw, ne);
        }

        return bounds;
    }

    public int getBoundsZoomLevel(LatLngBounds bounds, int mapWidthPx, int mapHeightPx){

        LatLng ne = bounds.northeast;
        LatLng sw = bounds.southwest;

        double latFraction = (latRad(ne.latitude) - latRad(sw.latitude)) / Math.PI;

        double lngDiff = ne.longitude - sw.longitude;
        double lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        double latZoom = zoom(mapHeightPx, WORLD_PX_HEIGHT, latFraction);
        double lngZoom = zoom(mapWidthPx, WORLD_PX_WIDTH, lngFraction);

        int result = Math.min((int)latZoom, (int)lngZoom);
        return Math.min(result, ZOOM_MAX);
    }

    private double latRad(double lat) {
        double sin = Math.sin(lat * Math.PI / 180);
        double radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }
    private double zoom(int mapPx, int worldPx, double fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / LN2);
    }


    /*private void clusterifyMarkers() {
        if (declusterifiedMarkers != null) {
            for (Marker marker : declusterifiedMarkers) {
                LatLng position = marker.getPosition();
                marker.setPosition(position);
                marker.setClusterGroup(ClusterGroup.DEFAULT);
            }
            declusterifiedMarkers = null;
        }
    }*/

    void updateClustering(){
        if (mMap == null) {
            return;
        }
        ClusteringSettings clusteringSettings = new ClusteringSettings();
        clusteringSettings.clusterOptionsProvider(new ImageClusterOptionsProvider(getResources()));

        double clusterSize = CLUSTER_SIZES[2];
        clusteringSettings.clusterSize(clusterSize);
        mMap.setClustering(clusteringSettings);
    }
    //map search code starts

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        searchString = (String) adapterView.getItemAtPosition(position);

        //keyboard closing window code starts 19-Feb-2014

        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

        //keyboard closing window code ends 19-Feb-2014

        //  Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

        if (searchLocationMarker) {
            if(marker2 != null){
                marker2.remove();
            }
        }
        List<Address> addresList = null;
        Geocoder geocoder = new Geocoder(this.getActivity());
        try {
            addresList = geocoder.getFromLocationName(searchString, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Address address = addresList.get(0);

        LatLng locationInfo = new LatLng(address.getLatitude(), address.getLongitude());
        initCamera(locationInfo);
        moveToNewLocation(locationInfo);
        marker2 = mMap.addMarker(new MarkerOptions().position(locationInfo).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title(searchString));
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationInfo, 1));
        searchLocationMarker = true;

    }



    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

        /*alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        getActivity().unregisterReceiver(ReceivefrmSERVICE);
        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);
        if(searchIcon != null){
            searchIcon.setVisibility(View.GONE);
        }

        if(searchBox != null){
            searchBox.setVisibility(View.GONE);
        }
    }
// map search code ends
}
