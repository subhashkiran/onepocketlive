package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import com.subhashe.onepocket.Core.AccHistoryModel;
import com.subhashe.onepocket.Core.GridKeyModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AccountsHistory extends Fragment {

    View view;
    TableLayout tableLayout;
    TableRow tableRow;
    View horizontalbar;
//    fixed the bug in sprint-8 Ticket_Id-167 starts
    TextView date, creditAmount, debitAmount, currentBalance, stationId,comment;
//    fixed the bug in sprint-8 Ticket_Id-167 end
    private int user_id;
    private String response, gridkeyHeader, token;
    private String user_name;
    private ConnectionDetector mConnectionDetector;
    private ProgressDialog progress;
    private JsonUtil mJsonUtil;
//    ArrayList<AccHistoryModel> historyList = new ArrayList<AccHistoryModel>();

    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USERNAME_ID = "USERNAME_ID_PREFS_String";

//    int id=60;
//    String url = "http://192.168.168.141:8080/gridbot/driver/accHistory";

    public AccountsHistory() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_accounts_history, container, false);

        tableLayout = (TableLayout) view.findViewById(R.id.accountsHistoryTable);
        tableLayout.setColumnStretchable(0, true);
        tableLayout.setColumnStretchable(1, true);
        tableLayout.setColumnStretchable(2, true);
        tableLayout.setColumnStretchable(3, true);
//        fixed the bug in sprint-8 Ticket_Id-167 starts
        tableLayout.setColumnStretchable(4, true);
        tableLayout.setColumnStretchable(5, true);
//        fixed the bug in sprint-8 Ticket_Id-167 end
        mConnectionDetector = new ConnectionDetector(getActivity());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(ACC_USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_name = prefs.getString(USERNAME_ID, "NOT FOUND");

        if (mConnectionDetector.isOnline()) {
            new AccHistoryTable().execute();
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }


        return view;
    }


    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        historyList.clear();
//        historyList = null;
    }

    public void showAlertBox(final String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();
        /*AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();*/
    }

    public class AccHistoryTable extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.ACCOUNT_ACCOUNT_HISTORY() + user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);


            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            ArrayList<AccHistoryModel> historyList = new ArrayList<AccHistoryModel>();
            try {
                JSONArray temp = new JSONArray(response);

//                fixed the bug in sprint-8 Ticket_Id-167 starts
                for(int i=0;i<temp.length();i++){
                    JSONObject jsonObject = temp.getJSONObject(i);
                    AccHistoryModel accHistoryModel = new AccHistoryModel();

                    // Long timestamp = jsonObject.getLong("createTimestamp");
                    // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //  Date date = new Date(timestamp);
                    //  sdf.format(netDate);
                    //  accHistoryModel.setDate(dateFormat.format(date));

                    accHistoryModel.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(jsonObject.getLong("CreateTimestamp"))));


                    //if(null!=jsonObject.get("amtCredit")||jsonObject.get("amtCredit")!="") {
                    if(!jsonObject.isNull("AmtCredit")){
                        accHistoryModel.setAmtCredit(BigDecimal.valueOf(jsonObject.getDouble("AmtCredit")));
                    }else{
                        accHistoryModel.setAmtCredit(BigDecimal.valueOf(0));
                    }

                    if(!jsonObject.isNull("AmtDebit")){
                        accHistoryModel.setAmtDebit(BigDecimal.valueOf(jsonObject.getDouble("AmtDebit")));
                    }else{
                        accHistoryModel.setAmtDebit(BigDecimal.valueOf(0));
                    }

                    if(!jsonObject.isNull("CurrentBalance")){
                        accHistoryModel.setCurrentBalance(BigDecimal.valueOf(jsonObject.getDouble("CurrentBalance")));
                    }else{
                        accHistoryModel.setCurrentBalance(BigDecimal.valueOf(0));
                    }

                    if(!jsonObject.isNull("StationId")){
                        accHistoryModel.setStationId(jsonObject.getString("StationId"));
                    }else{
                        accHistoryModel.setStationId("");
                    }

                    if(!jsonObject.isNull("Comment")){
                        accHistoryModel.setComment(jsonObject.getString("Comment"));
                    }else{
                        accHistoryModel.setComment("");
                    }


//                    fixed the bug in sprint-8 Ticket_Id-167 starts end
                    historyList.add(accHistoryModel);
                }

                /*for (int i = 0; i < temp.length(); i++) {
                    JSONObject jsonObject = temp.getJSONObject(i);
                    AccHistoryModel accHistoryModel = new AccHistoryModel();

                    // Long timestamp = jsonObject.getLong("createTimestamp");
                    // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //  Date date = new Date(timestamp);
                    //  sdf.format(netDate);
                    //  accHistoryModel.setDate(dateFormat.format(date));

                    accHistoryModel.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(jsonObject.getLong("CreateTimestamp"))));
                    accHistoryModel.setAmtCredit(jsonObject.getInt("AmtCredit"));
                    accHistoryModel.setAmtDebit(jsonObject.getInt("AmtDebit"));
                    accHistoryModel.setCurrentBalance(jsonObject.getInt("CurrentBalance"));
                    accHistoryModel.setStationId(jsonObject.getString("StationId"));

                    historyList.add(accHistoryModel);
                }*/

                for (int i = 0; i < historyList.size(); i++) {
                    AccHistoryModel accHistoryModel = new AccHistoryModel();
                    accHistoryModel = historyList.get(i);

                    tableRow = new TableRow(getActivity());
                    horizontalbar = new View(getActivity());

                    date = new TextView(getActivity());
                    creditAmount = new TextView(getActivity());
                    debitAmount = new TextView(getActivity());
                    currentBalance = new TextView(getActivity());
//                    fixed the bug in sprint-8 Ticket_Id-167 starts
                    stationId = new TextView(getActivity());
//                    fixed the bug in sprint-8 Ticket_Id-167 end
                    comment = new TextView(getActivity());



                    date.setText(String.valueOf(accHistoryModel.getDate()));
                    date.setTextSize(16);
                    date.setLayoutParams(new TableRow.LayoutParams(0, 100, 4f));

                    creditAmount.setText("$" + String.valueOf(accHistoryModel.getAmtCredit()));
                    creditAmount.setTextSize(16);
                    creditAmount.setLayoutParams(new TableRow.LayoutParams(0, 100, 2));
//                    creditAmount.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 2));

                    debitAmount.setText("$" + String.valueOf(accHistoryModel.getAmtDebit()));
                    debitAmount.setTextSize(16);
                    debitAmount.setLayoutParams(new TableRow.LayoutParams(0, 100, 2));
//                    debitAmount.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 2));

                    currentBalance.setText("$" + String.valueOf(accHistoryModel.getCurrentBalance()));
                    currentBalance.setTextSize(16);
                    currentBalance.setLayoutParams(new TableRow.LayoutParams(0, 100, 2));
//                    currentBalance.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 2));

//                    fixed the bug in sprint-8 Ticket_Id-167 starts
                    if (accHistoryModel.getStationId() == null)
                        accHistoryModel.setStationId("");

//                    stationId.setText("87654321");
                    stationId.setText(accHistoryModel.getStationId());
                    stationId.setTextSize(16);
                    stationId.setLayoutParams(new TableRow.LayoutParams(0, 100, 2));
//                    stationId.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 2));
//                    fixed the bug in sprint-8 Ticket_Id-167 end

                    comment.setText(accHistoryModel.getComment());
                    comment.setTextSize(16);
                    comment.setLayoutParams(new TableRow.LayoutParams(0, 100, 5f));
//                    comment.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 3f));

                    horizontalbar.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
                    horizontalbar.setBackgroundColor(Color.rgb(51, 51, 51));

                    tableRow.addView(date);
                    tableRow.addView(creditAmount);
                    tableRow.addView(debitAmount);
                    tableRow.addView(currentBalance);
//                    fixed the bug in sprint-8 Ticket_Id-167 starts
                    tableRow.addView(stationId);
//                    fixed the bug in sprint-8 Ticket_Id-167 end

                    tableRow.addView(comment);

                    tableLayout.addView(tableRow);
                    tableLayout.addView(horizontalbar);
                }

            } catch (Exception e) {

            }
        }
    }
}
