package com.subhashe.onepocket.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.Circle;
import com.androidmapsextensions.CircleOptions;
import com.androidmapsextensions.ClusterGroup;
import com.androidmapsextensions.ClusteringSettings;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.Polyline;
import com.androidmapsextensions.PolylineOptions;
import com.androidmapsextensions.SupportMapFragment;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.ClusterManager;
import com.subhashe.onepocket.Activities.CreditCardPayActivity;
import com.subhashe.onepocket.Activities.FullScannerActivity;
import com.subhashe.onepocket.Activities.GPSTracker;
import com.subhashe.onepocket.Activities.LoginActivity;
import com.subhashe.onepocket.Activities.MapMarkerInfoActivity;
import com.subhashe.onepocket.Activities.NavigationDrawerActivity;
import com.subhashe.onepocket.Adapters.ClearableAutoCompleteTextView;
import com.subhashe.onepocket.Adapters.GooglePlacesAutocompleteAdapter;
import com.subhashe.onepocket.Core.Constants;
import com.subhashe.onepocket.Core.FavModel;
import com.subhashe.onepocket.Core.ImageClusterOptionsProvider;
import com.subhashe.onepocket.Core.LatLang;
import com.subhashe.onepocket.Core.MapListModel;
import com.subhashe.onepocket.Core.MapModel;
import com.subhashe.onepocket.Core.MapModelAll;
import com.subhashe.onepocket.Core.MapModelFilters;
import com.subhashe.onepocket.Core.PlaceJSONParser;
import com.subhashe.onepocket.Core.RouteLatLngModel;
import com.subhashe.onepocket.Core.RouteModel;
import com.subhashe.onepocket.Core.UserFilterData;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.DataBase.DbHelper;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.NetworkChecker;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;
import com.subhashe.onepocket.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import static android.content.Context.MODE_PRIVATE;
import static com.paypal.android.sdk.dc.k;


public class MapsFragmentComplete extends BaseFragment implements AdapterView.OnItemClickListener,
        RoutingListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    public static final String LIVE_STATUS = "LiveStatus";
    public static final String CONN2_PORT_TYPE = "conn2portType";
    public static final String STATIONNAME = "stationName";
    public static final String STATIONID = "stationId";
    public static final String CONN1_PORT_TYPE = "conn1portType";
    public static final String STATION_STATUS = "StationStatus";
    public static final String STATION_ADDRESS = "StationAddress";
    public static final String CONN2_PORT_LEVEL = "conn2portLevel";
    public static final String CONN1_PORT_LEVEL = "conn1portLevel";
    public static final String PORT_QUANTITY = "portQuantity";
    public static final String VENDINGPRICE1 = "vendingprice1";
    public static final String VENDINGPRICE2 = "vendingprice2";
    public static final String STATIONADASTATUS = "stationADAStatus";
    public static final String PARKINGPRICEPH = "parkingPricePH";
    public static final String VENDINGPRICEUNIT1 = "vendingPriceUnit1";
    public static final String Notes = "notes";
    public static final String OPENTIME = "OpenTime";
    public static final String CLOSETIME = "CloseTime";
    public static final String FAVLATRETURN = "FAVLATRETURN";
    public static final String FAVLATRETURNFLAG = "FAVLATRETURNFLAG";
    public static final String FAST_SWITCH = "FAST_SWITCH";
    public static final String PAYMENT_FLAG = "PAYMENT_FLAG";
    public static final String STATUSSWITCH = "STATUSSWITCH";
    public static final String PRICESWITCH = "PRICESWITCH";
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String USER_REG_ID = "USERID_REG_String";
    //    private static final String BROWSER_API_KEY = "AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
    // private static final String API_KEY = "AIzaSyAU9ShujnIg3IDQxtPr7Q1qOvFVdwNmWc4";
    // private static final String API_KEY = "AIzaSyA5ngSBpnoj2JYvjTuMrCrgJpaQ3wXFBSc";
    // map search data ends
    public static final String JSON_ARRAY_NAME = "fuel_stations";
    public static final String ACC_BALENCE = "ACC_BALENCE";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";
    public static final String EXISTING_PAYMENT_FLAG_ALERT = "EXISTING_PAYMENT_FLAG_ALERT";
    //    public static final String JSON_URL = "http://192.168.168.141:8080/gridbot/maps";
    private static final double[] CLUSTER_SIZES = new double[]{180, 160, 144, 120, 96};
    private static final int ZBAR_CAMERA_PERMISSION = 1;
    private static final int[] COLORS = new int[]{R.color.primary_dark, R.color.account_number_blue, R.color.blue, R.color.accent, R.color.primary_dark_material_light};
    private static final LatLngBounds AXXERA_HYD = new LatLngBounds(new LatLng(17.2168886, 78.1599217),
            new LatLng(17.6078088, 78.6561694));
    private static final String LOG_TAG = "MapSearch";
    private static final double LN2 = 0.6931471805599453;
    private static final int WORLD_PX_HEIGHT = 256;
    private static final int WORLD_PX_WIDTH = 256;
    private static final int ZOOM_MAX = 21;
    //    private SharedPreferences prefs;
//    private SharedPreferences.Editor editor1;
    //------------ make your specific key ------------
    private static final String KEY3 = "AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
    //    protected com.google.android.gms.maps.GoogleMap map,mGoogleMap;
    protected LatLng start, currentLatLang;
    protected LatLng end;
    //    @InjectView(R.id.send)
//    ImageView send;
    protected GoogleApiClient mGoogleApiClient;
    ArrayAdapter<String> adapter;
    String[] mPlaceType = null;
    String[] mPlaceTypeName = null;
    String nearestUrl = null;
    String tripPlanerString = "";
    ArrayList<Marker> markerArrayList = new ArrayList<Marker>();
    Polyline polyline;
    PolylineOptions polyOptions;
    //    EditText yourLocation;
    LatLng yourLocationLatLng;
    LatLng destinationLocationLatLng;
    LinearLayout showStartTextView, showDestinationTextView, showStartAutoCompleteView, showDestinationAutoCompleteView;
    //TextView yourLocation, destinationLocation;
    TextView yourLocation;
    EditText destinationLocation;
    boolean currentLocationFlag = true, destinationLocationFlag = true;
    InputMethodManager inputMethodManager;
    Double destinationLattitude, destinationLongitude;
    //    @InjectView(R.id.start)
    AutoCompleteTextView starting;
//    com.google.android.gms.maps.GoogleMap googleMapForPolylines;

    //    private static final LatLngBounds BOUNDS_JAMAICA = new LatLngBounds(new LatLng(-57.965341647205726, 144.9987719580531),
//            new LatLng(72.77492067739843, -9.998857788741589));
    //    @InjectView(R.id.destination)
    AutoCompleteTextView destination;
    ImageView img_back;
    boolean flag = false;
    Button send;
    double mLatitude = 0;
    double mLongitude = 0;
    GPSTracker gpsTracker1;
    double yourLocationLatitude = 0;
    double yourLocationLongitude = 0;
    //    Button checkRoute;
    TextView routeId1, routeDistance1, routeDuration1, routeColor1, routeId2, routeDistance2, routeDuration2, routeColor2, routeId3, routeDistance3, routeDuration3, routeColor3;
    //    private GoogleMap mMap;
    Marker marker1, marker2;
    FragmentManager myFragmentManager;
    SupportMapFragment mySupportMapFragment;
    JsonFactory factory;
    JsonToken token;
    JsonParser jsonParser;
    String fieldName;
    JSONArray jsonArray;
    int jsonArrayLength;
    int jsonLength;
    StringBuffer buffer;
    String line = "";
    JSONObject jsonObject;
    String finalJson = "";
    //    fixed the bug in sprint-7 Ticket_Id-147 starts
    ArrayList<MapModelFilters> mapArrayListFilter = new ArrayList<MapModelFilters>();
    //    fixed the bug in sprint-7 Ticket_Id-147 end
    Button mMapView, mSatelliteView;
    ImageButton currentLocationMap;
    TableLayout tableLayout;
    TableRow tableRow;
    //    fixed the bug in sprint-7 Reopened Ticket_Id-197 starts
    TextView routeId, routeDistance, routeDuration, routeColor, space;
    ArrayList<RouteLatLngModel> routeLatLngList;
    ArrayList<Marker> routeMarkerList = new ArrayList<Marker>();
    ArrayList<Marker> markerPointsList = new ArrayList<Marker>();
    Marker markerPointA, markerPointB;
    JSONArray jRoutes = null;
    //    fixed the bug in sprint-7 Reopened Ticket_Id-197 end
    // viewB = bottomFragment;  viewC = nearestPlacesFragment;  viewD=searchPlacesFragment
    JSONArray jLegs = null;
    JSONArray jSteps = null;
    JSONObject jObject = null;
    double routelattitude, routeLongitude;
    int selectedPosition;
    String spinnerValueType, destinationString;
    String spinnerTypeValueFromChargerInfo, favLatLng;
    LatLng startLatLangValueFromChargerInfo, endLatLangValueFromChargerInfo;
    TextView searchItem;
    ActionBar actionBar;
    ImageButton backNavigation;
    ImageView backImg;
    ArrayList<Marker> searchMarkerList = new ArrayList<>();
    ActionBarDrawerToggle actionBarDrawerToggle;
    // fixed the bug in sprint-7 Ticket_Id-161 starts
    LinearLayout.LayoutParams mViewAget, mViewBget, mViewCget, mViewDget, mViewEget;
    // fixed the bug in sprint-7 Ticket_Id-161 end
    FloatingActionButton mFab;
    TextView stationName, stationAddress, liveStatus, chargers, levelType1, levelType2, mstationStatus, mConnectorType, portQuantity, vendingprice1, vendingprice2;
    ImageButton imageButton, searchButton;
    LinearLayout layoutStatus;
    GPSTracker gpsTracker;
    boolean singleMarker = false;
    boolean searchLocationMarker = false;
    boolean favFlag;
    Marker marker = null;
    Marker markerTell = null;
    List<Marker> markerList = new ArrayList<Marker>();
    List<Marker> markerListTell = new ArrayList<Marker>();
    List<Marker> markerListFilter = new ArrayList<Marker>();
    ArrayList<MapModelAll> dbList;
    ArrayList<MapModelAll> dbFilterList = new ArrayList<MapModelAll>();
    HashMap<String, MapModel> hashMapData;
    HashMap<String, MapModelAll> hashMapDataAll;
    //    fixed the bug in sprint-7 Ticket_Id-147 starts
    HashMap<String, MapModelFilters> hashMapFilter;
    //    fixed the bug in sprint-7 Ticket_Id-147 end
    String response, responseAll, markerCheck;
    String filterString, statusString, priceString, connectorTypeString, networkString;
    String chargerKey = null;
    ProgressDialog progressDialog, progressDialog2, progressDialogTellus;
    LayoutInflater layoutInflater, layoutInflaterSwitch, layoutTripPlanner;
    List<MapListModel> mapListData;
    List<MapModel> mapListTellus;
    //    fixed the bug in sprint-7 Ticket_Id-147 starts
    List<MapModelFilters> mapListDataFilter;
    List<FavModel> favModel;
    View v;
    String searchString = null;
    AlertDialog alertDialog;
    boolean searchLocation, tripPlannerEnable, filterEnable;
    boolean filterFlag, dcFastFlag, dcSwitchFlag;
    SharedPreferences.Editor editor1, editor2;
    boolean priceSwitchEnableFlag;
    ArrayList<MapModelFilters> freeStationRecordsList = new ArrayList<>();
    Menu myMenu;
    Marker selectedMarker = null;
    LinearLayout tripPlannerLayout, chargerDetailsLayout;
    LinearLayout.LayoutParams tripPlannerLayoutParams;
    Intent intent1 = new Intent();
    boolean homePressed = true, doubleBackToExitPressedOnce = false;
    /* route map*/
    private Spinner spinner;
    private Fragment fragment = null;
    private FragmentManager fragmentManager;
    private LatLng locationInfo;
    private PlaceAutoCompleteAdapter mAdapter;
    private ProgressDialog progressDialog1;
    private ProgressDialog progress;
    private ArrayList<Polyline> polylines = new ArrayList<>();
    private Class<?> mClss;
    /*route map end*/
    private SupportMapFragment mapFragment;
    private View rootView, layout;
    private JsonUtil mJsonUtil;
    private ProgressDialog progDailog, progressDialogueFilters;
    private Button filterSubmit, filterCancel;
    private LinearLayout viewGroup, DcFastSwitchLinear, DcFastComboSwitchLinear, TeslaSwitchLinaear;
    private Switch statusSwitch, priceSwitch, leve1Switch, leve2Switch/*, dcFastSwitch, dcFastComboSwitch, teslaSwitch,
            CPSwitch, BlkSwitch, SCSwitch, evgoSwitch, AVSwitch, EVCSwitch, EVSESwitch, GLSwitch, OPCSwitch, RASwitch, SPSwitch*/;
    private ConnectionDetector mConnectionDetector;
    private URL url;
    private InputStream stream;
    private UserFilterData mUserFilterData;
    //public static final String LEVE1SWITCH = "LEVE1SWITCH";
    //public static final String LEVE2SWITCH = "LEVE2SWITCH";
   /* public static final String DCFASTSWITCH = "DCFASTSWITCH";
    public static final String DCFASTCOMBOSWITCH = "DCFASTCOMBOSWITCH";
    public static final String TESLASWITCH = "TESLASWITCH";
    public static final String CPSWITCH = "CPSWITCH";
    public static final String BLKSWITCH = "BLKSWITCH";
    public static final String SCSWITCH = "SCSWITCH";
    public static final String EVGOSWITCH = "EVGOSWITCH";
    public static final String AVSWITCH = "AVSWITCH";
    public static final String EVCSWITCH = "EVCSWITCH";
    public static final String EVSESWITCH = "EVSESWITCH";
    public static final String GLSWITCH = "GLSWITCH";
    public static final String OPCSWITCH = "OPCSWITCH";
    public static final String RASWITCH = "RASWITCH";
    public static final String SPSWITCH = "SPSWITCH";*/
    private ArrayList<MapModelAll> mapArrayList;
    //    FrameLayout mBottomLayout;
    private LinearLayout mViewA, mViewB;
    private LinearLayout viewC, viewD, viewE;
    private ImageView searchIcon;
    private ClearableAutoCompleteTextView searchBox;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private CameraPosition position;
    private LatLng currentLoc, mLatLng;
    private GooglePlacesAutocompleteAdapter mGooglePlaceAdapter;
    //    fixed the bug in sprint-7 Ticket_Id-147 end
    private DbHelper dbHelper;
    private ClusterManager<LatLang> mClusterManager;
    private LatLang clickedClusterItem;
    private Circle circle;
    private CircleOptions circleOptions;
    private List<Marker> declusterifiedMarkers;
    private boolean checkUser = false;
    private boolean searchFlag = false;
    private int user_id;
    private BroadcastReceiver ReceivefrmSERVICE;
    private boolean firstCHeck, networkCheckerFlag, reCheckFlag, alertFlag, mFirstFlag;
    private boolean isTaskRoot;
    private int accBalance;
    private boolean paymentPopup = false;
    private boolean existingPaymentPopup = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_maps, container, false);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        /*LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.search_bar, null);*/
        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_SHOW_HOME);
        actionBar.show();
        hashMapDataAll = new HashMap<String, MapModelAll>();
//        fixed the bug in sprint-7 Ticket_Id-147 starts
        hashMapFilter = new HashMap<String, MapModelFilters>();
//        fixed the bug in sprint-7 Ticket_Id-147 end
        routeLatLngList = new ArrayList<RouteLatLngModel>();
        progressDialog2 = new ProgressDialog(getActivity());
        mUserFilterData = new UserFilterData();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        editor1 = prefs.edit();
        /*editor1.putBoolean(PAYMENT_FLAG,true);
        editor1.commit();*/

        checkUser = prefs.getBoolean(PAYMENT_FLAG, false);
        user_id = prefs.getInt(USER_ID, 0);

        paymentPopup = prefs.getBoolean(PAYMENT_FLAG_ALERT, false);

        accBalance = prefs.getInt(ACC_BALENCE, 0);

        MarkerOptions markerOptions = new MarkerOptions();

        /*if (prefs.getBoolean(FAST_SWITCH, false)) {
            dcFastFlag = true;
        } else {
            dcFastFlag = false;
        }*/

        new KeyBoardHide().hideKeyBoard(getActivity());

        if (paymentPopup) {

            showPaymentAlertBox(getString(R.string.newUserPaymentPopUp));
        }

        getActivity().setResult(5, intent1);

        filterString = statusString = priceString = connectorTypeString = networkString = "";
        connectorTypeString = "ev_connector_type=";
        networkString = "ev_network=";

        reCheckFlag = false;
        markerCheck = "";
        alertDialog = new AlertDialog.Builder(
                this.getActivity()).create();
        try {
            initilizeMap(rootView);

            initFab();
            mFab.setVisibility(View.GONE);
            intitilizeActionbar(v);
            actionBar.setCustomView(v);
            initilizeSwitch();

        } catch (Exception e) {
            e.printStackTrace();
        }

//        SwitchFilterFlags(); // uncomment if filters want to work;

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(null);

        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        Log.d("request code", " : " + requestCode);
        Log.d("result code", " : " + resultCode);
        if (resultCode == 7) {

            spinnerTypeValueFromChargerInfo = intent.getStringExtra("spinnerTypeValueFromChargerInfo");

            Bundle bundle = intent.getParcelableExtra("bundle");
            startLatLangValueFromChargerInfo = bundle.getParcelable("startLatLangValueFromChargerInfo");
            endLatLangValueFromChargerInfo = bundle.getParcelable("endLatLangValueFromChargerInfo");


            Log.d("spinnerType", "ValueFromChargerInfo : " + spinnerTypeValueFromChargerInfo);
            Log.d("startLatLang", "ValueFromChargerInfo : " + startLatLangValueFromChargerInfo);
            Log.d("endLatLangValue", " FromChargerInfo  : " + endLatLangValueFromChargerInfo);


            start = startLatLangValueFromChargerInfo;
            end = endLatLangValueFromChargerInfo;
            spinnerValueType = spinnerTypeValueFromChargerInfo;

            routingExecution();
        }

        if (resultCode == 8) {
            Bundle bundle = intent.getBundleExtra("bundle");
            favLatLng = bundle.getString("favLatLng", "");
            favFlag = bundle.getBoolean("FAVFLAG");
            Log.d("favLatLng ", favLatLng);
        }
    }

   /* @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);

        if(requestCode == 7){

            spinnerTypeValueFromChargerInfo = intent.getStringExtra("spinnerTypeValueFromChargerInfo");

            Bundle bundle = intent.getParcelableExtra("bundle");
            startLatLangValueFromChargerInfo = bundle.getParcelable("startLatLangValueFromChargerInfo");
            endLatLangValueFromChargerInfo = bundle.getParcelable("endLatLangValueFromChargerInfo");

            Log.d("startLatLang","ValueFromChargerInfo : "+startLatLangValueFromChargerInfo);
            Log.d("endLatLangValue"," FromChargerInfo  : "+endLatLangValueFromChargerInfo);


            start = startLatLangValueFromChargerInfo;
            end = endLatLangValueFromChargerInfo;
            routingExecution();
            *//*intent.putExtra("spinnerTypeValueFromChargerInfo",spinnerValueType);
            intent.putExtra("startLatLangValueFromChargerInfo",start);
            intent.putExtra("endLatLangValueFromChargerInfo",end);*//*
        }
    }*/

    private void SwitchFilterFlags() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        if (checkUser) {
//            Log.d("new user check", " : " + checkUser);
//            Toast.makeText(getContext(), "New User", Toast.LENGTH_SHORT).show();

            editor1.putBoolean(STATUSSWITCH, false);
            statusSwitch.setChecked(false);

            editor1.putBoolean(PRICESWITCH, false);
            priceSwitch.setChecked(false);
/*
            editor1.putBoolean(LEVE1SWITCH, false);
            leve1Switch.setChecked(false);

            editor1.putBoolean(LEVE2SWITCH, false);
            leve2Switch.setChecked(false);*/

            checkUser = false;
            editor1.putBoolean(PAYMENT_FLAG, false);
            editor1.putInt(USER_REG_ID, user_id);
            editor1.commit();

        } else {
            if (prefs.getBoolean(STATUSSWITCH, false)) {
                statusSwitch.setChecked(true);
                filterFlag = true;
            } else {
                statusSwitch.setChecked(false);
            }

            if (prefs.getBoolean(PRICESWITCH, false)) {
                priceSwitch.setChecked(true);
                filterFlag = true;
            } else {
                priceSwitch.setChecked(false);
            }

           /* if (prefs.getBoolean(LEVE1SWITCH, false)) {
                leve1Switch.setChecked(true);
                filterFlag = true;
            } else {
                leve1Switch.setChecked(false);
            }

            if (prefs.getBoolean(LEVE2SWITCH, false)) {
                leve2Switch.setChecked(true);
                filterFlag = true;
            } else {
                leve2Switch.setChecked(false);
            }*/
        }
    }

    public void showPaymentAlertBox(final String msg) {

        android.app.AlertDialog.Builder alertDialogBuilderPayment = new android.app.AlertDialog.Builder(getActivity());
        //alertDialogBuilderPayment.setTitle(R.string.Alert);
        //alertDialogBuilderPayment.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilderPayment
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Go to My Wallet", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent intent = new Intent(getActivity(), CreditCardPayActivity.class);
                        startActivity(intent);

                    }
                });
        alertDialogBuilderPayment
                .setNegativeButton("No,Thanks", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (paymentPopup) {
                            //showAlertBox(getString(R.string.paymentPop2));
                            paymentPopup = false;
                            editor1.putBoolean(PAYMENT_FLAG_ALERT, false);
                            editor1.putInt(USER_REG_ID, user_id);
                            editor1.commit();
                            dialog.cancel();
                        }

                    }
                });

        final android.app.AlertDialog alertDialogPayment = alertDialogBuilderPayment.create();
        alertDialogPayment.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialogPayment.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialogPayment.show();

    }

   /* public void showExistingPaymentAlertBox(final String msg) {

        android.app.AlertDialog.Builder alertDialogBuilderExistingPayment = new android.app.AlertDialog.Builder(getActivity());
        //alertDialogBuilderPayment.setTitle(R.string.Alert);
        //alertDialogBuilderPayment.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilderExistingPayment
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton( "Go to My Wallet",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        Intent intent = new Intent(getActivity() , CreditCardPayActivity.class);
                        startActivity(intent);

                    }
                });
        alertDialogBuilderExistingPayment
                .setNegativeButton("No,Thanks",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        dialog.cancel();
                       *//* if(!existingPaymentPopup){
                            //showAlertBox(getString(R.string.paymentPop2));
                            editor1.putString(EXISTING_PAYMENT_FLAG_ALERT, "NOT FOUND" );
                            editor1.putBoolean(getString(R.string.existingUserPaymentPopUp1), Boolean.TRUE);
                            //editor2.putInt(USER_REG_ID, user_id);
                            editor1.commit();
                            dialog.cancel();
                        }
*//*
                    }
                });

        final android.app.AlertDialog alertDialogExistingPayment = alertDialogBuilderExistingPayment.create();
        alertDialogExistingPayment.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialogExistingPayment.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialogExistingPayment.show();
    }
*/

    public void currentLocationLatLng() {
        gpsTracker = new GPSTracker(getContext());

        if (gpsTracker.canGetLocation) {
            yourLocationLatitude = gpsTracker.getLatitude();
            yourLocationLongitude = gpsTracker.getLongitude();
            yourLocationLatLng = new LatLng(yourLocationLatitude, yourLocationLongitude);

            if (yourLocationLatLng != null) {
                start = currentLatLang = yourLocationLatLng;

                String locationString = getAddressFromLatLng(currentLatLang);
                marker1 = mMap.addMarker(new MarkerOptions().position(currentLatLang).
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.curlocedit)
                                /*BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)*/).
                        title(locationString));
                marker1.setClusterGroup(ClusterGroup.DEFAULT);

                initCamera(currentLatLang);
                moveToNewLocation(currentLatLang);
            }
        } else {
            gpsTracker.showSettingsAlert();
        }
    }


    private void initFab() {
        mFab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        drawerLayout = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);

//        mFab.setBackgroundDrawable(new ColorDrawable(Color.parseColor0("#520E0C")));
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSearch(true);
//                actionBar.hide();
             /*   actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar,
                        R.string.openDrawer, R.string.closeDrawer) {

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                        super.onDrawerClosed(drawerView);
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                        super.onDrawerOpened(drawerView);
                    }
                };
                actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu_back);
                actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                actionBarDrawerToggle.syncState();
//                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_back);
//                actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP);*/

               /* Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
//                Uri gmmIntentUri = Uri.parse("https://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&key=AIzaSyB_hjWnhkM7L_xZiCuyni0oNgIcwqedQUc");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }*/
              /*  Uri gmmIntentUri = Uri.parse("geo:0,0?q=1600 Amphitheatre Parkway, Mountain+View, California");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);*/
                double lat = 0;
                double lang = 0;
                if (searchFlag) {
                    lat = locationInfo.latitude;
                    lang = locationInfo.longitude;
                } else {
                    lat = mapListTellus.get(0).getLattitude();
                    lang = mapListTellus.get(0).getLongitude();
                }
                String parseString = "google.navigation:q=" + lat + "," + lang;
                Uri gmmIntentUri = Uri.parse(parseString);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");

                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(getContext(), "Google maps is disabled,Please enable it.", Toast.LENGTH_SHORT).show();
                }
                mFab.setVisibility(View.VISIBLE);
                if (searchFlag) {
                    viewE.setVisibility(View.VISIBLE);
                } else {
                    viewE.setVisibility(View.GONE);
                }
//                startActivity(mapIntent);

                /*mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                viewC.setLayoutParams(new LinearLayout.LayoutParams(mViewCget.width, mViewCget.height, mViewCget.weight));
                viewC.setVisibility(View.VISIBLE);
                viewE.setVisibility(View.VISIBLE);
                RouteFound();*/
//                Snackbar.make(rootView, "FAB Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
    }
    /*private void initFab() {
        mFab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        drawerLayout = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);


//        mFab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#520E0C")));
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSearch(true);
//                actionBar.hide();
             *//*   actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar,
                        R.string.openDrawer, R.string.closeDrawer) {

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                        super.onDrawerClosed(drawerView);
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                        super.onDrawerOpened(drawerView);
                    }
                };
                actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu_back);
                actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                actionBarDrawerToggle.syncState();
//                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_back);
//                actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP);*//*
                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                viewC.setLayoutParams(new LinearLayout.LayoutParams(mViewCget.width, mViewCget.height, mViewCget.weight));
                viewC.setVisibility(View.VISIBLE);
                viewE.setVisibility(View.VISIBLE);
                RouteFound();
//                Snackbar.make(rootView, "FAB Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });

    }*/


    private void initilizeSwitch() {

        viewGroup = (LinearLayout) getActivity().findViewById(R.id.ProfileEditLayout);
        layoutInflaterSwitch = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        fixed the bug in sprint-10 Ticket_Id-214 starts
        layout = layoutInflater.inflate(R.layout.sortfiltertellus, viewGroup, false);
//        fixed the bug in sprint-10 Ticket_Id-214 end

        filterSubmit = (Button) layout.findViewById(R.id.filterSubmit);
        filterCancel = (Button) layout.findViewById(R.id.filterCancel);

       /* DcFastSwitchLinear = (LinearLayout) layout.findViewById(R.id.DcFastSwitchLinear);
        DcFastComboSwitchLinear = (LinearLayout) layout.findViewById(R.id.DcFastComboSwitchLinear);
        TeslaSwitchLinaear = (LinearLayout) layout.findViewById(R.id.TeslaSwitchLinaear);*/
        statusSwitch = (Switch) layout.findViewById(R.id.StatusSwitch);
        priceSwitch = (Switch) layout.findViewById(R.id.PriceSwitch);
        /*leve1Switch = (Switch) layout.findViewById(R.id.Level1Switch);
        leve2Switch = (Switch) layout.findViewById(R.id.Level2Switch);*/
        /*dcFastSwitch = (Switch) layout.findViewById(R.id.DcFastSwitch);
        dcFastComboSwitch = (Switch) layout.findViewById(R.id.DcFastComboSwitch);
        teslaSwitch = (Switch) layout.findViewById(R.id.TeslaSwitch);
        CPSwitch = (Switch) layout.findViewById(R.id.CharPointSwitch);
        BlkSwitch = (Switch) layout.findViewById(R.id.blinkSwitch);
        SCSwitch = (Switch) layout.findViewById(R.id.semchargeSwitch);
        evgoSwitch = (Switch) layout.findViewById(R.id.eVgoSwitch);
        AVSwitch = (Switch) layout.findViewById(R.id.AeroVironmentSwitch);
        EVCSwitch = (Switch) layout.findViewById(R.id.EVConnectSwitch);
        EVSESwitch = (Switch) layout.findViewById(R.id.EVSELLCWebNetSwitch);
        GLSwitch = (Switch) layout.findViewById(R.id.GreenlotsSwitch);
        OPCSwitch = (Switch) layout.findViewById(R.id.OpConnectSwitch);
        RASwitch = (Switch) layout.findViewById(R.id.RechargeAccessSwitch);
        SPSwitch = (Switch) layout.findViewById(R.id.ShorepowerSwitch);*/

       /* if (!dcFastFlag) {
            DcFastSwitchLinear.setVisibility(View.GONE);
            DcFastComboSwitchLinear.setVisibility(View.GONE);
            TeslaSwitchLinaear.setVisibility(View.GONE);
        } else {*/
//        DcFastSwitchLinear.setVisibility(View.VISIBLE);
        //DcFastComboSwitchLinear.setVisibility(View.VISIBLE);
        //TeslaSwitchLinaear.setVisibility(View.VISIBLE);
//        }
    }


    private void initilizeMap(View view) {
        try {
            // added for nearest places on 13Apr2016 starts

//            currentLocationLatLng();

            inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


            tripPlannerLayout = (LinearLayout) rootView.findViewById(R.id.tripPlannerLayout);

            tripPlannerLayoutParams = (LinearLayout.LayoutParams) tripPlannerLayout.getLayoutParams();
            tripPlannerLayout.setLayoutParams(new LinearLayout.LayoutParams(tripPlannerLayoutParams.width, tripPlannerLayoutParams.height, tripPlannerLayoutParams.weight));

            showStartAutoCompleteView = (LinearLayout) rootView.findViewById(R.id.showStartAutoCompleteView);
            showStartAutoCompleteView.setVisibility(View.GONE);

            showStartTextView = (LinearLayout) rootView.findViewById(R.id.showStartTextView);
            showStartTextView.setVisibility(View.VISIBLE);

            /*showDestinationAutoCompleteView = (LinearLayout) rootView.findViewById(R.id.showDestinationAutoCompleteView);
            showDestinationAutoCompleteView.setVisibility(View.GONE);*/

            /*showDestinationTextView = (LinearLayout) rootView.findViewById(R.id.showDestinationTextView);
            showDestinationTextView.setVisibility(View.VISIBLE);*/

            yourLocation = (TextView) rootView.findViewById(R.id.yourLocation);
            //destinationLocation = (EditText) rootView.findViewById(R.id.destinationLocation);


            starting = (AutoCompleteTextView) rootView.findViewById(R.id.start);
//        yourLocation = (EditText) rootView.findViewById(R.id.yourLocation);
            destination = (AutoCompleteTextView) rootView.findViewById(R.id.destination);
//        send = (ImageView) rootView.findViewById(R.id.send);
            send = (Button) rootView.findViewById(R.id.send);

            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Util.Operations.isOnline(getContext())) {
                        viewC.setVisibility(View.GONE);
                        viewE.setVisibility(View.GONE);
                        mFab.setVisibility(View.GONE);
                        actionBar.show();

                        new KeyBoardHide().hideKeyBoard(getActivity());

//                    Toast.makeText(getContext(), "route clicked", Toast.LENGTH_SHORT).show();
                        route();

                    } else {
                        // fixed the bug in sprint-7 Ticket_Id-161 starts
                        Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        // fixed the bug in sprint-7 Ticket_Id-161 end
                    }
                }
            });

            // call nearest palces fragment and spinner
            // Array of place types
            mPlaceType = getResources().getStringArray(R.array.place_type);
            // Array of place type names
            mPlaceTypeName = getResources().getStringArray(R.array.place_type_name);

            spinner = (Spinner) rootView.findViewById(R.id.placeTypeSpinner);
            adapter = new ArrayAdapter<String>(this.getActivity(), R.layout.simple_spinner_item, mPlaceTypeName);
            adapter.setDropDownViewResource(R.layout.custom_spinner_item);
            spinner.setAdapter(adapter);

            backImg = (ImageView) rootView.findViewById(R.id.tripplanner_back_arrow);

            mMapView = (Button) rootView.findViewById(R.id.mapview);
           /* if (mMapView.getViewTreeObserver().isAlive()) {
                mMapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressLint("NewApi") // We check which build version we are using.
                    @Override
                    public void onGlobalLayout() {

                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            mMapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            mMapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
//                        myMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                    }});
            }*/
//            mBottomLayout = (FrameLayout)rootView.findViewById(R.id.bottomFragment);
            mViewB = (LinearLayout) rootView.findViewById(R.id.viewB);
            mViewB.setVisibility(View.GONE);
//            mViewB = (LinearLayout) rootView.findViewById(R.id.viewB);
            mViewA = (LinearLayout) rootView.findViewById(R.id.viewA);
            viewC = (LinearLayout) rootView.findViewById(R.id.viewC);
//            viewC.setVisibility(View.GONE);
            viewD = (LinearLayout) rootView.findViewById(R.id.viewD);
            viewE = (LinearLayout) rootView.findViewById(R.id.viewE);
//            viewD.setVisibility(View.GONE);

            stationName = (TextView) rootView.findViewById(R.id.stationName);

            stationAddress = (TextView) rootView.findViewById(R.id.stationAddress);
            liveStatus = (TextView) rootView.findViewById(R.id.stationLiveStatus);
            layoutStatus = (LinearLayout) rootView.findViewById(R.id.station_layout_status);
//            chargers = (TextView) rootView.findViewById(R.id.chargers);
//            mstationStatus = (TextView) rootView.findViewById(R.id.stationStatus);
            mConnectorType = (TextView) rootView.findViewById(R.id.connectorTypeText);
            //vendingprice1 = (TextView) rootView.findViewById(R.id.vendingprice1);
            portQuantity = (TextView) rootView.findViewById(R.id.portQuantity);
//            vendingprice2 = (TextView) rootView.findViewById(R.id.vendingprice2);

            /*levelType1 = (TextView) rootView.findViewById(R.id.levelType1);
            levelType2 = (TextView) rootView.findViewById(R.id.levelType2);*/

         /*   mBottomLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getActivity(), "Bottom layout clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(),MapMarkerInfoActivity.class);
                    intent.putExtra("hashMap", hashMapData);
                    intent.putExtra("chargerMkey",chargerKey);
                    startActivity(intent);
                }
            }); */

            viewC.setVisibility(View.GONE);
            mViewB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getActivity(), "Bottom layout clicked", Toast.LENGTH_SHORT).show();
                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                        Intent intent = new Intent(getActivity(), MapMarkerInfoActivity.class);
                        if (mapListTellus != null) {
                            intent.putExtra("sendListTellus", (Serializable) mapListTellus);
                            intent.putExtra("tellusmarker", true);
                        }
                        intent.putExtra("sendListData", (Serializable) mapListData);
                        intent.putExtra("sendListDataFilter", (Serializable) mapListDataFilter); //markerListFilter
                        intent.putExtra("chargerMkey", chargerKey);
                        intent.putExtra("markerCheck", markerCheck);
//                        startActivity(intent);
                        startActivityForResult(intent, 7);
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                }
            });


//            backNavigation1 = (ImageButton) rootView.findViewById(R.id.backNavigation1);
            backNavigation = (ImageButton) rootView.findViewById(R.id.backNavigation);

            backNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewC.setVisibility(View.GONE);
                    viewE.setVisibility(View.VISIBLE);
                    mFab.setVisibility(View.GONE);
                    actionBar.show();
                }
            });
          /*  backNavigation1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewC.setVisibility(View.GONE);
                    actionBar.show();
                }
            });*/

            tableLayout = (TableLayout) view.findViewById(R.id.routeSearchTableLayout);
            tableLayout.setColumnStretchable(0, true);
            tableLayout.setColumnStretchable(1, true);
            tableLayout.setColumnStretchable(2, true);
            tableLayout.setColumnStretchable(3, true);
            tableLayout.setColumnStretchable(4, true);


            mMapView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            });
            mSatelliteView = (Button) rootView.findViewById(R.id.satileteView);
//            mSatelliteView.setVisibility(View.INVISIBLE);
            mSatelliteView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                }
            });

            currentLocationMap = (ImageButton) rootView.findViewById(R.id.currentLocationMap);
            currentLocationMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mConnectionDetector.isOnline()) {
                        LocationFound();

                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                }
            });
           /* if(mMap == null){
                FragmentManager fm = getChildFragmentManager();
                mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
              *//*  SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager()
                        .findFragmentById(R.id.map);*//*
//                mapFragment.getMapAsync(this);
               *//* if(mapFragment != null){
                    mapFragment = createMapFragment();
                }*//*

                mMap =mapFragment.getExtendedMap();
                if(mMap != null){
                    setUpMap();
                }

                mMapView = (Button)rootView.findViewById(R.id.mapview);
                mMapView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                });
                mSatelliteView = (Button)rootView.findViewById(R.id.satileteView);
                mSatelliteView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    }
                });
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // fixed the bug in sprint-7 Ticket_Id-161 starts
    public void disableAllViews() {

        if (selectedMarker != null) {
            selectedMarker.remove();
        }

        mViewB.setVisibility(View.GONE);
        viewC.setVisibility(View.GONE);
        viewD.setVisibility(View.GONE);
        viewE.setVisibility(View.GONE);
        mFab.setVisibility(View.GONE);
        searchFlag = false;
        actionBar.show();
        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));

        resetMyPositionButton();

    }

    public void clickMarkerViews(Marker marker) {
//        fixed the bug in sprint-7 Ticket_Id-147 starts

        selectedMarkerDisplay(marker);

        if (markerListTell.contains(marker) || markerList.contains(marker) || markerListFilter.contains(marker)) {
            Log.d("markerList tellus", " success :  " + markerList);
            viewD.setVisibility(View.GONE);
            viewC.setVisibility(View.GONE);
            viewE.setVisibility(View.GONE);
            mFab.setVisibility(View.VISIBLE);
            mViewB.setVisibility(View.VISIBLE);
            if (searchFlag) {
                searchFlag = false;
            }
        }
//        fixed the bug in sprint-7 Ticket_Id-147 end
        if (routeMarkerList.contains(marker) || markerPointsList.contains(marker)) {
            Log.d("routeMarkerList tellus", " success :  " + routeMarkerList);
            viewC.setVisibility(View.GONE);
            mViewB.setVisibility(View.GONE);
            viewE.setVisibility(View.GONE);
            mFab.setVisibility(View.GONE);
            mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
            viewD.setLayoutParams(new LinearLayout.LayoutParams(mViewDget.width, mViewDget.height, mViewDget.weight));
            viewD.setVisibility(View.VISIBLE);
        }

        if (searchMarkerList.contains(marker)) {
            viewC.setVisibility(View.GONE);
            viewD.setVisibility(View.GONE);
            mViewB.setVisibility(View.GONE);
            actionBar.show();
            searchFlag = true;

            mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
            viewE.setLayoutParams(new LinearLayout.LayoutParams(mViewEget.width, mViewEget.height, mViewEget.weight));
            viewE.setVisibility(View.VISIBLE);
            mFab.setVisibility(View.VISIBLE);
//            resetMyToolBar();
        }

    }


    public void selectedMarkerDisplay(Marker marker) {

        if (selectedMarker != null) {
            selectedMarker.remove();
        }
        MarkerOptions mOptions;
        LatLng selectedMarkerLatLang = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);

//        selectedMarker =  mMap.addMarker(new MarkerOptions().position(marker.getPosition()).icon(BitmapDescriptorFactory.defaultMarker()));

//        MarkerOptions mOptions = new MarkerOptions().position(marker.getPosition());

        //filters click marker code starts

        OnePocketLog.d("ZOOMIN", selectedMarkerLatLang.toString());

        if (markerListFilter.contains(marker) || freeStationRecordsList.contains(marker)) {
            for (Map.Entry m : hashMapFilter.entrySet()) {
                MapModelFilters mMapModelFreeRecords = new MapModelFilters();
                mMapModelFreeRecords = (MapModelFilters) m.getValue();

                if ((marker.getId()).equals(m.getKey())) {

                    if (mMapModelFreeRecords.getStatus_code().contains("E")) {
                        if (mMapModelFreeRecords.getEv_connector_types().contains("CHADEMO") ||
                                mMapModelFreeRecords.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModelFreeRecords.getEv_connector_types().contains("TESLA")) {

                            /*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.green_fc));*/

                            mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));


                        } /*else {
                            mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.available_green));

                            *//*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));*//*
                        }*/
                    } else if (mMapModelFreeRecords.getStatus_code().contains("T") ||
                            mMapModelFreeRecords.getStatus_code().contains("P")) {
                        if (mMapModelFreeRecords.getEv_connector_types().contains("CHADEMO") ||
                                mMapModelFreeRecords.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModelFreeRecords.getEv_connector_types().contains("TESLA")) {

                            mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray_fc));

                            /*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));*/

                        } else {
                            mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));

                            /*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));*/
                        }
                    } else {

                        mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));

                        /*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));*/
                    }


                    // selectedMarker = mMap.addMarker(mOptions);
                }
            }
        }

        //filters click marker code end


        //gov site api click marker code starts

        if (markerList.contains(marker)) {
            for (Map.Entry m : hashMapDataAll.entrySet()) {
                MapModelAll mMapModel = new MapModelAll();
                mMapModel = (MapModelAll) m.getValue();

                if ((marker.getId()).equals(m.getKey())) {


                    if (mMapModel.getStatus_code().contains("E")) {
                        if (mMapModel.getEv_connector_types().contains("CHADEMO") ||
                                mMapModel.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModel.getEv_connector_types().contains("TESLA")) {

                            mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.green_fc));

                            /*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));*/

                        } else {
                            /*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.available_green));*/
                            /*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));*/
                        }
                    } else if (mMapModel.getStatus_code().contains("T") ||
                            mMapModel.getStatus_code().contains("P")) {
                        if (mMapModel.getEv_connector_types().contains("CHADEMO") ||
                                mMapModel.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModel.getEv_connector_types().contains("TESLA")) {

                            mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray_fc));

                           /* mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));*/

                        } else {
                            mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));

                            /*mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));*/
                        }
                    } else {

                        mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                       /* mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));*/

                    }

                    //selectedMarker = mMap.addMarker(mOptions);
                }
            }

        }
        //gov site api click marker code end


        //tellus click marker code starts
        if (markerListTell.contains(marker)) {
            for (Map.Entry m : hashMapData.entrySet()) {
                MapModel mMapModel = new MapModel();
                mMapModel = (MapModel) m.getValue();

                if ((marker.getId()).equals(m.getKey())) {

                    if (mMapModel.getLiveStatus().contains("Available")) {
                        mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.green));
                    } else if (mMapModel.getLiveStatus().contains("In-Use")) {
                        mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                    } else {
                        mOptions = new MarkerOptions().position(selectedMarkerLatLang).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));
                    }

                    selectedMarker = mMap.addMarker(mOptions);
                }
            }
        }


        //tellus click marker code end


    }

    public void displayInfoWindow(Marker marker) {

        if (mConnectionDetector.isOnline() && networkCheckerFlag) {

//            for tellus marker info starts
            if (markerListTell.contains(marker)) {
                for (Map.Entry m : hashMapData.entrySet()) {
                    MapModel mm = new MapModel();
                    mm = (MapModel) m.getValue();

                    if ((marker.getId()).equals(m.getKey())) {
                        Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getKey());
                        chargerKey = m.getKey().toString();

                        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                        mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                        mViewB.setVisibility(View.VISIBLE);
                        viewC.setVisibility(View.GONE);
                        viewD.setVisibility(View.GONE);


                        mapListTellus = new ArrayList<MapModel>();

                        MapModel mapListModelTellus = new MapModel();

                        mapListModelTellus.setMarkerId(mm.getMarkerId());
                        mapListModelTellus.setPortQuantity(mm.getPortQuantity());
                        mapListModelTellus.setLiveStatus(mm.getLiveStatus());
                        mapListModelTellus.setConn2PortType(mm.getConn2PortType());
                        mapListModelTellus.setStationName(mm.getStationName());
                        mapListModelTellus.setStationID(mm.getStationID());
                        mapListModelTellus.setConn1portType(mm.getConn1portType());
                        mapListModelTellus.setStationStatus(mm.getStationStatus());
                        mapListModelTellus.setParkingPricePH(mm.getParkingPricePH());
                        mapListModelTellus.setStationADAStatus(mm.getStationADAStatus());

                        mapListModelTellus.setStationAddress(mm.getStationAddress());

                        /*OnePocketLog.d("PORTQUANTITY" , mm.getPortQuantity());

                        String noOfPorts = mm.getPortQuantity() ;

                        OnePocketLog.d("NOOFPORTS" , noOfPorts);*/

                       /* if(noOfPorts.equals("1"))
                        {
                            data.put("stationId",stations[0]);
                            data.put("StationStatus", stations[2]);
                            data.put("portQuantity", stations[3]);
                            data.put("stationName", stations[4]);
                            data.put("stationADAStatus", stations[5]);
                            data.put("conn1portLevel", stations[7]==null?0:stations[7]);
                            data.put("vendingprice1", stations[12]==null?0:stations[12]);
                            data.put("vendingPriceUnit1", stations[14]==null?0:stations[14]);
                            data.put("FreeTrail", stations[16]);
                            data.put("StationAddress", stations[17]);
                            data.put("parkingPricePH", stations[18]);
                            data.put("LiveStatus", stations[19]);
                            data.put("latLng", latLng);
                            stationMap.put("latLng", latLng);
                            stationMap.put("data", data );
                            stationList.add(stationMap);

                        }*/

                        mapListModelTellus.setConn2portLevel(mm.getConn2portLevel());

                        mapListModelTellus.setConn1portLevel(mm.getConn1portLevel());
                        mapListModelTellus.setPortQuantity(mm.getPortQuantity());
                        mapListModelTellus.setLattitude(mm.getLattitude());
                        mapListModelTellus.setLongitude(mm.getLongitude());
                        mapListModelTellus.setLatLang(mm.getLatLang());
                        mapListModelTellus.setVendingPrice1(mm.getVendingPrice1());
                        mapListModelTellus.setVendingPrice2(mm.getVendingPrice2());
                        mapListModelTellus.setNotes(mm.getNotes());

                        mapListModelTellus.setOpenTime(mm.getOpenTime());
                        mapListModelTellus.setCloseTime(mm.getCloseTime());

                        OnePocketLog.d("OpenTIME111", mm.getOpenTime());
                        OnePocketLog.d("CloseTIME111", mm.getCloseTime());

                        OnePocketLog.d("OpenTIME222", mapListModelTellus.getOpenTime());
                        OnePocketLog.d("CloseTIME222", mapListModelTellus.getCloseTime());

                       /* double lat = mm.getLattitude();
                        mapListModelTellus.setLattitude(lat);
                        double lang = mm.getLongitude();
                        mapListModelTellus.setLongitude(lang);*/

                        mapListTellus.add(mapListModelTellus);
                        Log.d("MapModelList", ": " + mapListTellus.get(0));

                        stationName.setText(mm.getStationName());

                        String addAvailable = "";
                        if (mm.getStationAddress().contains("null")) {
                            addAvailable = "Not Available";
                        } else {
                            addAvailable = mm.getStationAddress();
                        }
                        stationAddress.setText(addAvailable);

                        //stationAddress.setText(mm.getStationAddress());

                        String noOfPorts = mm.getPortQuantity();
                        int noOfPortsInteger = Integer.parseInt(noOfPorts);

                        if (noOfPortsInteger == 1) {
                            portQuantity.setText(mm.getPortQuantity() + " port");
                        } else if (noOfPortsInteger == 2) {
                            portQuantity.setText(mm.getPortQuantity() + " ports");
                        }

                        liveStatus.setText(mm.getLiveStatus());

                        Log.d("stationLiveStatus", " " + mm.getLiveStatus());
                        String statusMsg = "";
                        if (mm.getLiveStatus().contains("Available")) {
                            layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
                        } else if (mm.getLiveStatus().contains("In-Use")) {
                            layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
                        }
                        /*if (statusMsg.contains("Available"))
                        {
                            layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
                        }
                        else if(statusMsg.equals("In-Use"))
                        {
                            layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
                        }*/


                        markerCheck = "";
                        markerCheck = "Type1";
                        //vendingprice1.setText(mm.getVendingPrice1());
                        String tempAdd = "";
                       /* if(mm.getStationAddress().contains("null")){
                            tempAdd = "Not Available";
                        }else{
                            tempAdd = mm.getStationAddress();
                        }*/
                        //mConnectorType.setText(mm.getConn1portLevel() + "," + mm.getConn2portLevel());
                        //mConnectorType.setText(mm.getConn1portType() + "," + mm.getConn2PortType());
                       /* levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                        levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());*/


                    }
                }
            }

//            for tellus marker info end


//            for gov site marker info starts
            if (markerList.contains(marker)) {
                for (Map.Entry m : hashMapDataAll.entrySet()) {
                    MapModelAll mm = new MapModelAll();
                    mm = (MapModelAll) m.getValue();

                    if ((marker.getId()).equals(m.getKey())) {
                        Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getKey());

                        chargerKey = m.getKey().toString();
                        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                        mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                        mViewB.setVisibility(View.VISIBLE);
                        viewC.setVisibility(View.GONE);
                        viewD.setVisibility(View.GONE);

                        mapListData = new ArrayList<MapListModel>();

                        MapListModel mapListModel = new MapListModel();


                        mapListModel.setStation_name(mm.getStation_name());
                        mapListModel.setFuel_type_code(mm.getFuel_type_code());
                        mapListModel.setEv_network(mm.getEv_network());
                        mapListModel.setCards_accepted(mm.getCards_accepted());
                        mapListModel.setEv_connector_types(mm.getEv_connector_types());
                        mapListModel.setAccess_days_time(mm.getAccess_days_time());
                        mapListModel.setStreet_address(mm.getStreet_address());
                        mapListModel.setCity(mm.getCity());
                        mapListModel.setState(mm.getState());
                        mapListModel.setZip(mm.getZip());
                        mapListModel.setIntersection_directions(mm.getIntersection_directions());
                        mapListModel.setLattitude(mm.getLattitude());
                        mapListModel.setLongitude(mm.getLongitude());

                        /*mapListModel.setAccess_days_time(mm.getAccess_days_time());
                        mapListModel.setFuel_type_code(mm.getFuel_type_code());
                        mapListModel.setGroups_with_access_code(mm.getGroups_with_access_code());
                        mapListModel.setStation_name(mm.getStation_name());
                        mapListModel.setStation_phone(mm.getStation_phone());
                        mapListModel.setStatus_code(mm.getStatus_code());
                        mapListModel.setLattitude(mm.getLattitude());
                        mapListModel.setLongitude(mm.getLongitude());
                        mapListModel.setCity(mm.getCity());
                        mapListModel.setState(mm.getState());
                        mapListModel.setStreet_address(mm.getStreet_address());
                        mapListModel.setZip(mm.getZip());
                        mapListModel.setBd_blends(mm.getBd_blends());
                        mapListModel.setE85_blender_pump(mm.getE85_blender_pump());*/
                        markerCheck = "";
                        markerCheck = "Type1";


                       /* Log.d("cL at after", ": " + mapListModel.getConnectorTypesList());
                        mapListModel.setEv_connector_types(mm.getEv_connector_types());
                        // mapListModel.setEv_connector_types(mm.getEv_connector_types());
                        mapListModel.setEv_dc_fast_num(mm.getEv_dc_fast_num());
                        mapListModel.setEv_level1_evse_num(mm.getEv_level1_evse_num());
                        mapListModel.setEv_level2_evse_num(mm.getEv_level2_evse_num());
                        mapListModel.setEv_network(mm.getEv_network());
                        mapListModel.setEv_network_web(mm.getEv_network_web());
                        mapListModel.setEv_other_evse(mm.getEv_other_evse());
                        mapListModel.setHy_status_link(mm.getHy_status_link());
                        mapListModel.setLpg_primary(mm.getLpg_primary());
                        mapListModel.setNg_fill_type_code(mm.getNg_fill_type_code());
                        mapListModel.setNg_psi(mm.getNg_psi());
                        mapListModel.setNg_vehicle_class(mm.getNg_vehicle_class());*/

                        mapListData.add(mapListModel);

                        Log.d("MapModelList", ": " + mapListData.get(0));


                        stationName.setText(mm.getStation_name());

                        stationAddress.setText(mm.getStation_address());
                        liveStatus.setText(mm.getLive_status());

                        Log.d("stationLiveStatus", " " + mm.getLive_status());
                        String statusMsg = liveStatus.toString();

                        if (mm.getLive_status().contains("Available")) {
                            layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
                        } else if (mm.getLive_status().contains("In-Use")) {
                            layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
                        }

                        //    statusCode.setText(mm.getStatus_code());
//                        city.setText(mm.getCity());

                    }
                }
            }
//            for gov site marker info ends

//            fixed the bug in sprint-7 Ticket_Id-147 starts
//            for filters site marker info starts
            if (markerListFilter.contains(marker) || freeStationRecordsList.contains(marker)) {
                for (Map.Entry m : hashMapFilter.entrySet()) {
                    MapModelFilters mm = new MapModelFilters();
                    mm = (MapModelFilters) m.getValue();

                    if ((marker.getId()).equals(m.getKey())) {
                        Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getKey());
//                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());

                        chargerKey = m.getKey().toString();
                        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                        mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                        mViewB.setVisibility(View.VISIBLE);
                        viewC.setVisibility(View.GONE);
                        viewD.setVisibility(View.GONE);

                        mapListDataFilter = new ArrayList<MapModelFilters>();

                        MapModelFilters mapListModel = new MapModelFilters();

                        mapListModel.setStation_name(mm.getStation_name());
                        mapListModel.setFuel_type_code(mm.getFuel_type_code());
                        mapListModel.setEv_network(mm.getEv_network());
                        mapListModel.setCards_accepted(mm.getCards_accepted());
                        mapListModel.setEv_connector_types(mm.getEv_connector_types());
                        mapListModel.setAccess_days_time(mm.getAccess_days_time());
                        mapListModel.setStreet_address(mm.getStreet_address());
                        mapListModel.setCity(mm.getCity());
                        mapListModel.setState(mm.getState());
                        mapListModel.setZip(mm.getZip());
                        mapListModel.setIntersection_directions(mm.getIntersection_directions());
                        mapListModel.setLattitude(mm.getLattitude());
                        mapListModel.setLongitude(mm.getLongitude());

                        /*mapListModel.setAccess_days_time(mm.getAccess_days_time());
                        mapListModel.setFuel_type_code(mm.getFuel_type_code());
                        mapListModel.setGroups_with_access_code(mm.getGroups_with_access_code());
                        mapListModel.setStation_name(mm.getStation_name());
                        mapListModel.setStation_phone(mm.getStation_phone());
                        mapListModel.setStatus_code(mm.getStatus_code());
                        mapListModel.setCity(mm.getCity());
                        mapListModel.setState(mm.getState());
                        mapListModel.setStreet_address(mm.getStreet_address());
                        mapListModel.setZip(mm.getZip());
                        mapListModel.setBd_blends(mm.getBd_blends());
                        mapListModel.setE85_blender_pump(mm.getE85_blender_pump());*/
                        markerCheck = "";
                        markerCheck = "Type1";

                       /* mapListModel.setLattitude(mm.getLattitude());
                        mapListModel.setLongitude(mm.getLongitude());
                        mapListModel.setEv_connector_types(mm.getEv_connector_types());
                        mapListModel.setEv_dc_fast_num(mm.getEv_dc_fast_num());
                        mapListModel.setEv_level1_evse_num(mm.getEv_level1_evse_num());
                        mapListModel.setEv_level2_evse_num(mm.getEv_level2_evse_num());
                        mapListModel.setEv_network(mm.getEv_network());
                        mapListModel.setEv_network_web(mm.getEv_network_web());
                        mapListModel.setEv_other_evse(mm.getEv_other_evse());
                        mapListModel.setHy_status_link(mm.getHy_status_link());
                        mapListModel.setLpg_primary(mm.getLpg_primary());
                        mapListModel.setNg_fill_type_code(mm.getNg_fill_type_code());
                        mapListModel.setNg_psi(mm.getNg_psi());
                        mapListModel.setNg_vehicle_class(mm.getNg_vehicle_class());*/

                        mapListDataFilter.add(mapListModel);

                        Log.d("MapModelList", ": " + mapListDataFilter.get(0));


                        stationName.setText(mm.getStation_name());

                        stationAddress.setText(mm.getStation_address());
                        liveStatus.setText(mm.getStation_live_status());

                        Log.d("stationLiveStatus", " " + mm.getStation_live_status());
                        String statusMsg = liveStatus.toString();

                        if (mm.getStation_live_status().contains("Available")) {
                            layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_available));
                        } else if (mm.getStation_live_status().contains("In-Use")) {
                            layoutStatus.setBackground(getResources().getDrawable(R.drawable.station_unavailable));
                        }
                        //    statusCode.setText(mm.getStatus_code());
//                        city.setText(mm.getCity());

                    }
                }
            }

//            fixed the bug in sprint-7 Ticket_Id-147 end


//            for gov site marker info ends


        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }

    }

    public void cameraChangeInMap() {
        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
//        Log.d("Bounds value"," : "+bounds);

        if ((mMap.getCameraPosition().zoom) <= 15) {

            OnePocketLog.d("CAMERAPOSITION", bounds.toString());

            if (selectedMarker != null) {
                selectedMarker.remove();
                mFab.setVisibility(View.GONE);
            }

            mViewB.setVisibility(View.GONE);

            updateClustering();
            clusterifyMarkers();
        }
        if ((mMap.getCameraPosition().zoom) >= 15) {

            displayAllMarkers();

            //OnePocketLog.d("CAMERAPOSITION123" , bounds.toString());
        }

        for (Marker marker1 : markerList) {
            if (bounds.contains(marker1.getPosition())) {
                marker1.setVisible(true);
                //OnePocketLog.d("CAMERAPOSITION1234" , bounds.toString());
            } else {
                marker1.setVisible(false);
            }
        }

        for (Marker markerT : markerListTell) {
            if (bounds.contains(markerT.getPosition())) {
                markerT.setVisible(true);
                OnePocketLog.d("CAMERAPOSITION1234456", bounds.toString());
            } else {
                markerT.setVisible(false);
            }
        }

        for (Marker markerF : markerListFilter) {
            if (bounds.contains(markerF.getPosition())) {
                markerF.setVisible(true);
            } else {
                markerF.setVisible(false);
            }
        }

    }

// fixed the bug in sprint-7 Ticket_Id-161 end

    public void routeMapDisplay() {

//        polylines = new ArrayList<>();
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        MapsInitializer.initialize(getContext());
        mGoogleApiClient.connect();

        mAdapter = new PlaceAutoCompleteAdapter(getContext(), android.R.layout.simple_list_item_1,
                mGoogleApiClient, AXXERA_HYD, null);
        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        mAdapter.setBounds(bounds);

        startAndDestinationPoints();
       /* if (currentLocationFlag) {

            if (Build.VERSION.SDK_INT >= 23 &&
                    ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                    CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(mLatitude, mLongitude));
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

                    mMap.moveCamera(center);
                    mMap.animateCamera(zoom);

                    int selectedPosition = spinner.getSelectedItemPosition();
                    String type = mPlaceType[selectedPosition];

                    nearestUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + mLatitude + "," + mLongitude + "&radius=1000&types=" + type + "&sensor=true&key=AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
                    new PlacesTask().execute(nearestUrl);

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });


            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                    CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(mLatitude, mLongitude));
//              CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude()));
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

                    mMap.moveCamera(center);
                    mMap.animateCamera(zoom);

                    int selectedPosition = spinner.getSelectedItemPosition();
                    String type = mPlaceType[selectedPosition];

                    nearestUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + mLatitude + "," + mLongitude + "&radius=1000&types=" + type + "&sensor=true&key=AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
                    new PlacesTask().execute(nearestUrl);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });

        }*/


    }

    public void startAndDestinationPoints() {

//        fixed the bug in sprint-7 Ticket_Id-166 starts
        /*if (Util.Operations.isOnline(getContext())) {
            destination.setAdapter(mAdapter);
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }*/

        if (!currentLocationFlag) {

            if (Util.Operations.isOnline(getContext())) {
                starting.setAdapter(mAdapter);
                starting.requestFocus();

            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }

//            fixed the bug in sprint-7 Ticket_Id-166 end

            starting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    //keyboard closing window code starts


                    //inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                    //keyboard closing window code ends

                    final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                    final String placeId = String.valueOf(item.placeId);
                    Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);


                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                            .getPlaceById(mGoogleApiClient, placeId);
                    placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (!places.getStatus().isSuccess()) {
                                // Request did not complete successfully
                                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                                places.release();
                                return;
                            }
                            // Get the Place object from the buffer.
                            final Place place = places.get(0);

                            start = place.getLatLng();
                        }
                    });

                }
            });

            starting.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int startNum, int before, int count) {
                    if (start != null) {
                        start = null;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        if (!destinationLocationFlag) {

            if (Util.Operations.isOnline(getContext())) {
                destination.setAdapter(mAdapter);
                destination.requestFocus();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }

            destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    //keyboard closing window code starts
                    //inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                    //keyboard closing window code ends

                    final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                    final String placeId = String.valueOf(item.placeId);
                    Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);


                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                            .getPlaceById(mGoogleApiClient, placeId);
                    placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (!places.getStatus().isSuccess()) {
                                // Request did not complete successfully
                                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                                places.release();
                                return;
                            }
                            // Get the Place object from the buffer.
                            final Place place = places.get(0);

                            end = place.getLatLng();
                        }
                    });

                }
            });


            destination.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    if (end != null) {
                        end = null;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            destination.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    destination.showDropDown();
                    return false;
                }
            });
        }
    }

    public void checkDestination() {

        destinationString = destination.toString();

        if (destinationString.contains("Choose a destination...")) {
            actionBar.hide();
            viewC.setVisibility(View.VISIBLE);
            viewE.setVisibility(View.VISIBLE);
            mFab.setVisibility(View.GONE);
            Toast.makeText(getContext(), "Please choose a destination point", Toast.LENGTH_SHORT).show();

        }
    }

    public void checkSpinnerForPlaces() {

        selectedPosition = spinner.getSelectedItemPosition();
        spinnerValueType = mPlaceType[selectedPosition];

        if (spinnerValueType.contains("findnearby")) {
            actionBar.hide();
            viewC.setVisibility(View.VISIBLE);
            viewE.setVisibility(View.VISIBLE);
            mFab.setVisibility(View.GONE);
            Toast.makeText(getContext(), "Please select Find Nearby type", Toast.LENGTH_SHORT).show();

        }
    }

    //    fixed the bug in sprint-7 Ticket_Id-168 starts
    public void route() {

        if (start == null || end == null) {
            if (!currentLocationFlag) {
                if (start == null) {
                    actionBar.show();
                    viewC.setVisibility(View.VISIBLE);
                    if (starting.getText().length() < 0) {
//                        viewC.setVisibility(View.VISIBLE);
                        starting.setError("Choose location from dropdown.");
                        actionBar.hide();
                    } else {
//                        starting.setError("Please choose a starting");
                        Toast.makeText(getContext(), "Please choose a starting point", Toast.LENGTH_SHORT).show();
                        actionBar.hide();
                    }
                }
            }

            if (!destinationLocationFlag) {
                if (end == null) {
                    viewC.setVisibility(View.VISIBLE);
                    actionBar.show();
                    if (destination.getText().length() < 0) {
//                    viewC.setVisibility(View.VISIBLE);
                        destination.setError("Choose location from dropdown.");
                        actionBar.hide();
                    } else {
//                    destination.setError("Please choose a destination");
                        Toast.makeText(getContext(), "Please choose a destination point", Toast.LENGTH_SHORT).show();
                        actionBar.hide();
                    }
                }
            }
           /* else{

                checkSpinnerForPlaces();
            }*/
            //}

            selectedPosition = spinner.getSelectedItemPosition();
            spinnerValueType = mPlaceType[selectedPosition];

            Log.d("spiner Type vallue", " : " + spinnerValueType);
            Log.d("start vallue", " : " + start);
            Log.d("destination vallue", " : " + end);

//            Intent intent1=new Intent();
            intent1.putExtra("spinnerTypeValueFromChargerInfo", spinnerValueType);

            Bundle args = new Bundle();
            args.putParcelable("startLatLangValueFromChargerInfo", start);
            args.putParcelable("endLatLangValueFromChargerInfo", end);

            intent1.putExtra("bundle", args);

           /* else{

                checkSpinnerForPlaces();
            }

            selectedPosition = spinner.getSelectedItemPosition();
            spinnerValueType = mPlaceType[selectedPosition];

            Log.d("spiner Type vallue", " : " + spinnerValueType);
            Log.d("start vallue", " : " + start);
            Log.d("destination vallue", " : " + end);

//            Intent intent1=new Intent();
            intent1.putExtra("spinnerTypeValueFromChargerInfo", spinnerValueType);

            Bundle args = new Bundle();
            args.putParcelable("startLatLangValueFromChargerInfo", start);
            args.putParcelable("endLatLangValueFromChargerInfo", end);

            intent1.putExtra("bundle", args);

            /*intent.putExtra("startLatLangValueFromChargerInfo",start);
            intent.putExtra("endLatLangValueFromChargerInfo",end);*/

            //setResult(7,intent1);
            //onBackPressed();
//            finish();
        } else {

            // checkSpinnerForPlaces();

            selectedPosition = spinner.getSelectedItemPosition();
            spinnerValueType = mPlaceType[selectedPosition];

            OnePocketLog.d("SelectedPosition", String.valueOf(selectedPosition));

            OnePocketLog.d("SelectedPositionValueType", spinnerValueType);


            routingExecution();

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        setKeyListenerOnView(getView());
    }

    public void setKeyListenerOnView(View v) {

        OnePocketLog.d("KEYListener", "1");

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:

                        new AlertDialog.Builder(getActivity())
                                //.setTitle("Really Exit?")
                                .setMessage("Do you want to logout?")
                                .setNegativeButton(android.R.string.no, null)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface arg0, int arg1) {
                                        getActivity().finish();
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                    }
                                }).create().show();
                        break;
                }

                return true;
            }
        });
    }


    public void onBackPressed() {


      /*  FragmentManager manager = getFragmentManager();

        if (manager.getBackStackEntryCount() > 1) {
            // If there are back-stack entries, leave the FragmentActivity
            // implementation take care of them.
            manager.popBackStack();

        } else {
            // Otherwise, ask user if he wants to leave :)
            new AlertDialog.Builder(getActivity())
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            // MainActivity.super.onBackPressed();
                            getActivity().finish();
                            getActivity().moveTaskToBack(true);
                        }
                    }).create().show();
        }*/
    }

    public void routingExecution() {

        progressDialog = ProgressDialog.show(getContext(), "Please wait.",
                "Fetching route information.", true);

        routeLatLngList.clear();

        clearPolyLinesAndMarkersAB();

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(start, end)
                .build();
        routing.execute();

    }

    //    fixed the bug in sprint-7 Ticket_Id-168 starts
    public void clearPolyLinesAndMarkersAB() {

        if (routeMarkerList != null) {
            for (int i = 0; i < routeMarkerList.size(); i++) {
                Marker markerRemove = routeMarkerList.get(i);
                markerRemove.remove();
            }
            routeMarkerList.clear();
        }

        if (markerPointA != null && markerPointB != null && polyline != null) {
            markerPointA.remove();
            markerPointB.remove();

            for (int i = 0; i < polylines.size(); i++) {
                polyline = polylines.get(i);
                polyline.remove();
            }
            polylines.clear();
        }
    }

    // fixed the bug in sprint-7 Ticket_Id-161 starts
    @Override
    public void onRoutingFailure(RouteException e) {
        // The Routing request failed
        progressDialog.dismiss();
        if (e != null) {
            Toast.makeText(getContext(), "No Route Found", Toast.LENGTH_LONG).show();
//            Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    //    fixed the bug in sprint-7 Ticket_Id-168 ends

    @Override
    public void onRoutingStart() {
        // The Routing Request starts
    }
    // fixed the bug in sprint-7 Ticket_Id-161 ends

    //    fixed the bug in sprint-7 Ticket_Id-168 starts
    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        progressDialog.dismiss();

        Double routeDistanceValue = 0.0;

        //      added code by praveen on 20April2016 starts

        ArrayList<RouteModel> routeArrayList = new ArrayList<>();

        //      added code by praveen on 20April2016 end


        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

        mMap.moveCamera(center);

        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            RouteModel routeModel = new RouteModel();

            polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);

            routeModel.setRouteId(i + 1);
//            routeModel.setDistance((route.get(i).getDistanceValue() / 1000));

            // fixed the bug in sprint-7 Ticket_Id-153 starts

//            fixed the bug in sprint-10 Ticket_Id-219
            DecimalFormat df = new DecimalFormat("0.#");   // 0.### for 3 digits
//            fixed the bug in sprint-10 Ticket_Id-219
            routeModel.setDistance(Double.parseDouble(df.format(route.get(i).getDistanceValue() / 1609.344)));
            // fixed the bug in sprint-7 Ticket_Id-153 ends

            routeModel.setDuration((route.get(i).getDurationValue() / 60));

            routeArrayList.add(routeModel);


            if (routeDistanceValue < routeModel.getDistance()) {
                routeDistanceValue = routeModel.getDistance();
            }
//            route.get(i).getLatLgnBounds()
//            Toast.makeText(getContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
            Log.d("", "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue());
        }

        if (routeDistanceValue > 1000) {

            for (int i = 0; i < polylines.size(); i++) {
                polyline = polylines.get(i);
                polyline.remove();
            }
            polylines.clear();

            Toast.makeText(getContext(), "No Route Found", Toast.LENGTH_SHORT).show();

        } else {

            if (!spinnerValueType.contains("findnearby")) {

                new RoutesJsonTask().execute("https://maps.googleapis.com/maps/api/directions/json?origin=" +
                        start.latitude + "," + start.longitude + "&destination=" + end.latitude + "," + end.longitude +
                        "&sensor=false&mode=driving&alternatives=true&key=" + KEY3);
            }

            //      added code by praveen on 20April2016 starts

            if (routeArrayList != null) {
                viewD.setVisibility(View.VISIBLE);
                mViewB.setVisibility(View.GONE);

                searchBox.setVisibility(View.GONE);
                searchIcon.setVisibility(View.VISIBLE);
               /* myMenu.findItem(R.id.action_sortFilter).setVisible(true);
                myMenu.findItem(R.id.action_barcodeScannerH).setVisible(true);*/
                searchBox.setText("");

                tableLayout.removeAllViews();
                for (int i = 0; i < routeArrayList.size(); i++) {

                    int colorIndex = i % COLORS.length;

                    RouteModel routeModelDisplay = new RouteModel();
                    routeModelDisplay = routeArrayList.get(i);

                    tableRow = new TableRow(getActivity());
//                    fixed the bug in sprint-7 Reopened Ticket_Id-197 starts
                    routeColor = new TextView(getActivity());
                    routeId = new TextView(getActivity());
                    space = new TextView(getActivity());
                    routeDistance = new TextView(getActivity());
                    routeDuration = new TextView(getActivity());

                   /* routeColor.setText("||||||||||||");
                    routeColor.setTextColor(getResources().getColor(COLORS[colorIndex]));*/
                    routeColor.setBackgroundColor(getResources().getColor(COLORS[colorIndex]));
                    routeId.setText(String.valueOf(routeModelDisplay.getRouteId()));
                    space.setText("");
                    // fixed the bug in sprint-7 Ticket_Id-153 starts
                    routeDistance.setText(String.valueOf(routeModelDisplay.getDistance() + " miles"));
                    // fixed the bug in sprint-7 Ticket_Id-153 ends
//                    routeDuration.setText(String.valueOf(routeModelDisplay.getDuration() + " Min"));

//                    fixed the bug in sprint-10 Ticket_Id-219
                    String startTime = "00:00";
                    int minutes = (int) routeModelDisplay.getDuration();
                    int h = minutes / 60 + Integer.parseInt(startTime.substring(0, 1));
                    int m = minutes % 60 + Integer.parseInt(startTime.substring(3, 4));
                    String newtime = h + " hr. " + m + " min.";

                    routeDuration.setText(newtime);
//                     fixed the bug in sprint-10 Ticket_Id-219 end
                    tableRow.addView(routeId);
                    tableRow.addView(routeColor);
                    tableRow.addView(space);
                    tableRow.addView(routeDistance);
                    tableRow.addView(routeDuration);

                    tableRow.setPadding(0, 0, 0, 10);

//                    fixed the bug in sprint-7 Reopened Ticket_Id-197 end
                    tableLayout.addView(tableRow);
                }

            }
            //      added code by praveen on 20April2016 ends

            // Start marker
            MarkerOptions options = new MarkerOptions();
            options.position(start);
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.bluepin));
            markerPointA = mMap.addMarker(options);
            markerPointsList.add(markerPointA);


              /*  Uri gmmIntentUri = Uri.parse("geo:0,0?q=restaurants");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);*/

            // End marker
            options = new MarkerOptions();
            options.position(end);
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.graypin));
            markerPointB = mMap.addMarker(options);
            markerPointsList.add(markerPointB);

            for (Marker arrayListData : markerPointsList) {
                arrayListData.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
            }
        }

    }

    @Override
    public void onRoutingCancelled() {
        Log.i(LOG_TAG, "Routing was cancelled.");
    }

//    fixed the bug in sprint-7 Ticket_Id-168 end

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.v(LOG_TAG, connectionResult.toString());
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    protected SupportMapFragment createMapFragment() {
        return SupportMapFragment.newInstance();
    }

    public void intitilizeActionbar(View v) {
        searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        // the view that contains the new clearable autocomplete text view
        searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);

        // start with the text view hidden in the action bar
//        mBottomLayout.setVisibility(View.INVISIBLE);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        mViewAget = (LinearLayout.LayoutParams) mViewA.getLayoutParams();
        mViewBget = (LinearLayout.LayoutParams) mViewB.getLayoutParams();
        mViewCget = (LinearLayout.LayoutParams) viewC.getLayoutParams();

        // fixed the bug in sprint-7 Ticket_Id-161 starts
        mViewDget = (LinearLayout.LayoutParams) viewD.getLayoutParams();
        mViewEget = (LinearLayout.LayoutParams) viewE.getLayoutParams();
        // fixed the bug in sprint-7 Ticket_Id-161 end
        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));

        mFab.setVisibility(View.INVISIBLE);
        mViewB.setVisibility(View.INVISIBLE);
        searchBox.setVisibility(View.INVISIBLE);
        searchFlag = false;
        Drawable drawable = searchBox.getBackground(); // get current EditText drawable
        drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP); // change the drawable color

        if (Build.VERSION.SDK_INT > 16) {
            searchBox.setBackground(drawable); // set the new drawable to EditText
        } else {
            searchBox.setBackgroundDrawable(drawable); // use setBackgroundDrawable because setBackground required API 16
        }

        searchIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleSearch(false);
            }
        });

        searchBox.setOnClearListener(new ClearableAutoCompleteTextView.OnClearListener() {

            @Override
            public void onClear() {
                toggleSearch(true);
            }
        });

        searchBox.setAdapter(mGooglePlaceAdapter);
        searchBox.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // handle clicks on search resaults here

                searchString = "";
//                searchMarkerList = new ArrayList<Marker>();
                searchItem = (TextView) rootView.findViewById(R.id.searchItem);
//                searchItemPart2 = (TextView) rootView.findViewById(R.id.searchItemPart2);
                searchString = (String) adapterView.getItemAtPosition(position);
                searchLocation = true;
                //keyboard closing window code starts 19-Feb-2014

                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                //keyboard closing window code ends 19-Feb-2014

                //  Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

                if (searchLocationMarker) {
                    if (marker2 != null) {
                        marker2.remove();
                    }
                }

                List<Address> addresList = null;
                Geocoder geocoder = new Geocoder(getActivity());
                try {
                    addresList = geocoder.getFromLocationName(searchString, 3);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (addresList.size() != 0) {
                    Address address = addresList.get(0);
                    if (address != null) {
                        locationInfo = new LatLng(address.getLatitude(), address.getLongitude());
                        initCamera(locationInfo);
                        moveToNewLocation(locationInfo);
                        searchFlag = true;
                    }
                }

                if (locationInfo != null) {
                    end = locationInfo;
                }
                /*String[] parts = searchString.split(", ", 2);
                String searchStringPart1 = parts[0];
                String searchStringPart2 = parts[1];*/

                searchItem.setText(searchString);
                searchBox.setSelection(0);

//                searchItemPart2.setText(searchStringPart2);

                searchMarkerList.clear();

                if (locationInfo != null) {
                    marker2 = mMap.addMarker(new MarkerOptions().position(locationInfo).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))/*.title(searchString)*/);
                    marker2.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
                    searchMarkerList.add(marker2);
                }


                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
//                        resetMyToolBar();
                        if (marker.isCluster()) {
                            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                                declusterify(marker);
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                            }
                            return true;
                        } else {
                            clickMarkerViews(marker);
                            return false;
                        }
                    }
                });

                mViewB.setVisibility(View.GONE);
                viewC.setVisibility(View.GONE);
                viewD.setVisibility(View.GONE);

                //mFab.setVisibility(View.VISIBLE);

                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                viewE.setLayoutParams(new LinearLayout.LayoutParams(mViewEget.width, mViewEget.height, mViewEget.weight));
                viewE.setVisibility(View.VISIBLE);

                // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationInfo, 1));
                searchLocationMarker = true;
            }

        });
    }

    protected void toggleSearch(boolean reset) {
      /*  LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.search_bar, null);*/

//        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_back);
//        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);
        ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);

        if (mConnectionDetector.isOnline()) {
            if (reset) {
                // hide search box and show search icon
               /* myMenu.findItem(R.id.action_sortFilter).setVisible(true);
                myMenu.findItem(R.id.action_barcodeScannerH).setVisible(true);*/
                searchBox.setText("");
                searchBox.setVisibility(View.GONE);
                searchIcon.setVisibility(View.VISIBLE);
//                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
                mFab.setVisibility(View.GONE);
                viewE.setVisibility(View.GONE);

                // hide the keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);

            } else {
                // hide search icon and show search box
               /* myMenu.findItem(R.id.action_sortFilter).setVisible(false);
                myMenu.findItem(R.id.action_barcodeScannerH).setVisible(false);*/

                if (singleMarker) {
                    if (marker1 != null) {
                        marker1.remove();
//                marker1 = null;
                    }
                }
                searchIcon.setVisibility(View.GONE);
                searchBox.setVisibility(View.VISIBLE);
                searchBox.requestFocus();
                // show the keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchBox, InputMethodManager.SHOW_FORCED);
            }
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConnectionDetector = new ConnectionDetector(this.getActivity());
        mGooglePlaceAdapter = new GooglePlacesAutocompleteAdapter(this.getActivity(), R.layout.search_list);
        layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = layoutInflater.inflate(R.layout.search_bar, null);
        dbHelper = new DbHelper(getContext());

        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        SharedPreferences.Editor editor = pSharedPref.edit();
        favLatLng = pSharedPref.getString(FAVLATRETURN, "");
        favFlag = pSharedPref.getBoolean(FAVLATRETURNFLAG, false);

        editor.commit();
        Log.d("favLatLng ", favLatLng);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.actionbaritems, menu);
        myMenu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

          /*  case R.id.fragment_menu_item:
                // search action

                return true;*/
          /*  case R.id.action_gpslocater:
                if (mConnectionDetector.isOnline()) {
                    LocationFound();
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;*/

             /* case R.id.action_barcodeScanner:
                if (mConnectionDetector.isOnline()) {
                    launchActivity(FullScannerActivity.class);
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;*/
            case R.id.action_trip_planner:
                viewC.setVisibility(View.GONE);
                viewD.setVisibility(View.GONE);
                mViewB.setVisibility(View.GONE);

                if (mConnectionDetector.isOnline()) {
                    tripPlannerEnable = true;
                    new KeyBoardHide().hideKeyBoard(getActivity());
                    //TripPlannerPopUp(getActivity());
                    RouteFound();

                    destinationLocationFlag = false;
                    end = null;
                    destination.setText("");
                    routeMapDisplay();


                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.action_sortFilter:
                viewC.setVisibility(View.GONE);
                viewD.setVisibility(View.GONE);
                mViewB.setVisibility(View.GONE);
                SwitchFilterFlags();
                if (mConnectionDetector.isOnline()) {
                    filterEnable = true;

                    new KeyBoardHide().hideKeyBoard(getActivity());
                    mFab.setVisibility(View.GONE);
                    SortFilterPopUp(getActivity());

                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.action_barcodeScannerH:
                if (mConnectionDetector.isOnline()) {
                    launchActivity(FullScannerActivity.class);
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;
           /* case R.id.action_routeFilter:
                if (mConnectionDetector.isOnline()) {
                    mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                    viewC.setLayoutParams(new LinearLayout.LayoutParams(mViewCget.width, mViewCget.height, mViewCget.weight));
                    viewC.setVisibility(View.VISIBLE);
                    RouteFound();
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;*/
        }

        return super.onOptionsItemSelected(item);
    }

    public void launchActivity(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClss = clss;
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA}, ZBAR_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(getActivity(), clss);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZBAR_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mClss != null) {
                        Intent intent = new Intent(getActivity(), mClss);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(getActivity(), "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    public void RouteFound() {

        actionBar.hide();
        mViewB.setVisibility(View.GONE);
        viewD.setVisibility(View.GONE);
        mFab.setVisibility(View.GONE);
        viewE.setVisibility(View.GONE);
        currentLocationFlag = true;
        destinationLocationFlag = true;
        // destinationLocation.setText(searchString);
        destination.setText("");

        spinner.setAdapter(adapter);

        currentLocationLatLng();
        if (locationInfo != null) {
            end = locationInfo;
        }

        viewC.setVisibility(View.VISIBLE);
        tripPlanerString = getAddressFromLatLng(currentLatLang);
        showStartTextView.setVisibility(View.VISIBLE);
        yourLocation.setText(tripPlanerString);
        showStartAutoCompleteView.setVisibility(View.GONE);
        //showDestinationTextView.setVisibility(View.VISIBLE);
        //showDestinationAutoCompleteView.setVisibility(View.VISIBLE);

        routeMapDisplay();

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(getActivity());
                viewC.setVisibility(View.GONE);
                viewE.setVisibility(View.VISIBLE);
                mFab.setVisibility(View.VISIBLE);
                actionBar.show();
                mFab.setVisibility(View.GONE);

                searchBox.setVisibility(View.GONE);
                searchIcon.setVisibility(View.VISIBLE);
               /* myMenu.findItem(R.id.action_sortFilter).setVisible(true);
                myMenu.findItem(R.id.action_barcodeScannerH).setVisible(true);*/
                searchBox.setText("");

                fragment = new MapsFragmentComplete();
                initFragment();


            }
        });
/*
        destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destinationLocationFlag = false;
                //showDestinationTextView.setVisibility(View.VISIBLE);
                end = null;
                destination.setText("");
                //showDestinationAutoCompleteView.setVisibility(View.VISIBLE);
                routeMapDisplay();
            }
        });*/

        yourLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLocationFlag = false;
                start = null;

                showStartTextView.setVisibility(View.GONE);
                starting.setText(tripPlanerString);
//                destination.setText("");

                showStartAutoCompleteView.setVisibility(View.VISIBLE);
                routeMapDisplay();
            }
        });

    }

    public void SortFilterPopUp(final Activity context) {

        final PopupWindow popup = new PopupWindow(layout, WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT, true);

        popup.showAtLocation(layout, Gravity.CENTER, 10, 10);

        filterSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //fragment = new MapsFragmentComplete();

                popup.dismiss();
                switchListeners();
//                fixed the bug in sprint-7 Ticket_Id-160 starts
//                fixed the bug in sprint-8 Ticket_Id-201 starts
                if (filterFlag) {

                    mMap.clear();
                    markerListFilter.clear();
                    markerListTell.clear();
                    markerList.clear();
                    freeStationRecordsList.clear();

                    new FilterListGet().execute();

//                    new GoogleMapsSortList().execute();
//                    displaySortFilters();
                   /* if (priceSwitchEnableFlag) {
                        new FreeStationsAsyncTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                        *//*if (freeStationRecords != null) {
                            displayFreeStationRecords(freeStationRecords);
                        }*//*
                    } else {
                        new GetJsonString().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                        new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                    }*/

                } else {
                    mMap.clear();
                    markerListFilter.clear();
                    markerListTell.clear();
                    markerList.clear();

                    new GoogleMapsTask().execute();

                 /*   new GoogleMapsTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                    new GoogleMapsTaskAllStations().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);*/
                }
//                fixed the bug in sprint-8 Ticket_Id-201 end
//                fixed the bug in sprint-7 Ticket_Id-160 end
            }
        });

        filterCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                //fragment = new MapsFragmentComplete();
            }
        });
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

   /* public void TripPlannerPopUp(final Activity context) {

        final PopupWindow popup = new PopupWindow(layout, WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT, true);

        popup.showAtLocation(layout, Gravity.BOTTOM, 10, 10);

        RouteFound();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.Operations.isOnline(getContext())) {
                    viewC.setVisibility(View.GONE);
                    viewE.setVisibility(View.GONE);
                    mFab.setVisibility(View.GONE);
                    actionBar.show();

                    new KeyBoardHide().hideKeyBoard(getActivity());

//                    Toast.makeText(getContext(), "route clicked", Toast.LENGTH_SHORT).show();
                    route();

                } else {
                    // fixed the bug in sprint-7 Ticket_Id-161 starts
                    Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                    // fixed the bug in sprint-7 Ticket_Id-161 end
                }
            }
        });

    }*/

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    private void initFilterData() {
        if (statusSwitch.isChecked()) {
            mUserFilterData.setAvailability(true);
        } else {
            mUserFilterData.setAvailability(false);
        }

        if (priceSwitch.isChecked()) {
            mUserFilterData.setPrice(true);
        } else {
            mUserFilterData.setPrice(false);
        }

       /* if(leve1Switch.isChecked()){
            mUserFilterData.setLevel1(true);
        }else{
            mUserFilterData.setLevel1(false);
        }

        if(leve2Switch.isChecked()){
            mUserFilterData.setLevel2(true);
        }else{
            mUserFilterData.setLevel2(false);
        }*/
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            Log.d("Download url", "Places Task");
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
//            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }
    /*private void displaySortFilters(){
        List<Marker> sortedMarkerList = new ArrayList<Marker>();
        if (markerListFilter != null) {
            Log.d("free station records", "length : " + freeStationRecordsList.size());

            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                for (int i = 0; i < markerListFilter.size(); i++) {
                    MapModel mMapModel = new MapModel();
                    mMapModel = markerListFilter.get(i);
                    if (statusSwitch.isChecked()) {
                        if(markerListFilter.)
                    }
                }
            }
        }

    }*/

    private void switchListeners() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        SharedPreferences.Editor editor1 = prefs.edit();

        filterString = "";
        if (connectorTypeString.contains("")) {
            connectorTypeString = "ev_connector_type=";
        }

        if (networkString.contains("")) {
            networkString = "ev_network=";
        }
        if (mConnectionDetector.isOnline() && networkCheckerFlag) {

            if (statusSwitch.isChecked()) {
                statusSwitch.setChecked(true);
                editor1.putBoolean(STATUSSWITCH, true);
                statusString = "status=E";
            } else {
                statusSwitch.setChecked(false);
                editor1.putBoolean(STATUSSWITCH, false);
                statusString = "";
            }

            if (priceSwitch.isChecked()) {
                priceSwitch.setChecked(true);
                editor1.putBoolean(PRICESWITCH, true);
                priceString = "cards_accepted=all";
            } else {
                priceSwitch.setChecked(false);
                editor1.putBoolean(PRICESWITCH, false);
                priceString = "";
            }

          /*  if (leve1Switch.isChecked()) {
                leve1Switch.setChecked(true);
                editor1.putBoolean(LEVE1SWITCH, true);
                connectorTypeString += "NEMA515,NEMA520,NEMA1450,";
            } else {
                leve1Switch.setChecked(false);
                editor1.putBoolean(LEVE1SWITCH, false);
                connectorTypeString += "";
            }

            if (leve2Switch.isChecked()) {
                leve2Switch.setChecked(true);
                editor1.putBoolean(LEVE2SWITCH, true);
                connectorTypeString += "J1772,";
            } else {
                leve2Switch.setChecked(false);
                editor1.putBoolean(LEVE2SWITCH, false);
                connectorTypeString += "";
            }*/


            filterString = "&" + statusString + "&" + priceString + "&" + connectorTypeString + "&" + networkString;
            filterString.replaceAll(" ", "%20");
            priceSwitchEnableFlag = false;

            if (!statusSwitch.isChecked() && priceSwitch.isChecked()
                /*&& !dcFastSwitch.isChecked() && !dcFastComboSwitch.isChecked() &&
                    !teslaSwitch.isChecked() && !CPSwitch.isChecked() && !BlkSwitch.isChecked() &&
                    !SCSwitch.isChecked() && !evgoSwitch.isChecked() && !AVSwitch.isChecked() &&
                    !EVCSwitch.isChecked() && !EVSESwitch.isChecked() && !GLSwitch.isChecked() &&
                    !OPCSwitch.isChecked() && !RASwitch.isChecked() && !SPSwitch.isChecked()*/) {

                filterFlag = true;
                priceSwitchEnableFlag = true;
               /* freeStationRecords.clear();
                freeStationRecords = dbHelper.getFreeStationRecords();

                Log.d("free station records", " : " + freeStationRecords.size());*/
               /* if(freeStationRecords!=null) {
                    Log.d("free station records", "length : " + freeStationRecords.size());
                    for (int i = 0; i < freeStationRecords.size(); i++) {

                            MapModelAll mMapModelFreeRecords = new MapModelAll();
                            mMapModelFreeRecords = freeStationRecords.get(i);
                            // MyMarker myMarker = new MyMarker();
                            Log.d("card type "," : " + mMapModelFreeRecords.getCards_accepted());
                            Log.d("records "," : " + i);

                    }
                }*/

            } else if (!statusSwitch.isChecked() && !priceSwitch.isChecked()
                   /* && !dcFastSwitch.isChecked() && !dcFastComboSwitch.isChecked() &&
                    !teslaSwitch.isChecked() && !CPSwitch.isChecked() && !BlkSwitch.isChecked() &&
                    !SCSwitch.isChecked() && !evgoSwitch.isChecked() && !AVSwitch.isChecked() &&
                    !EVCSwitch.isChecked() && !EVSESwitch.isChecked() && !GLSwitch.isChecked() &&
                    !OPCSwitch.isChecked() && !RASwitch.isChecked() && !SPSwitch.isChecked()*/) {
                filterFlag = false;
                filterString = "";
                filterString = "https://api.data.gov/nrel/alt-fuel-stations/v1.json?fuel_type=ELEC&api_key=1vWB8MccC1alVSuFO29CxE4nq5Y3cmqgLLhy1BGL";
            } else {
                filterFlag = true;
            }
            editor1.commit();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }

    }

    public void LocationFound() {

        OnePocketLog.d("Location clicked" , "OnCLick");

        boolean locationUpdate = true;
        gpsTracker = new GPSTracker(getActivity().getApplicationContext());

        Log.d("marker", " : " + singleMarker);
        if (singleMarker) {
            if (marker1 != null) {
                marker1.remove();
//                marker1 = null;
            }
        }
        if (gpsTracker.canGetLocation) {
            Log.d("gps tracker", " : " + gpsTracker.canGetLocation);
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

            mViewB.setVisibility(View.INVISIBLE);
            mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));

            //currentLoc = new LatLng(latitude, longitude);

            if (!favFlag) {

                currentLoc = new LatLng(latitude, longitude);

            } else {

                String latlongtemp = favLatLng.replace("lat/lng:", "");
                String latlongtemp1 = latlongtemp.replace("(", "");
                String latlongtemp2 = latlongtemp1.replace(")", "");

                String[] latlong = latlongtemp2.split(",");
                double lat = Double.parseDouble(latlong[0]);
                double longt = Double.parseDouble(latlong[1]);
                currentLoc = new LatLng(lat, longt);

            }

//            marker1 = mMap.addMarker(new MarkerOptions().position(currentLoc));
            String locationString = getAddressFromLatLng(currentLoc);
            if (locationString.contains("Please try again")) {
                Toast.makeText(getActivity(), "Location service failed. Please try again", Toast.LENGTH_SHORT).show();
            } else {
                if (!favFlag) {
                    marker1 = mMap.addMarker(new MarkerOptions().position(currentLoc).
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.curlocedit)
                                /*BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)*/).
                            title(locationString));
                    marker1.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
                }

//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 18));

                    initCamera(currentLoc);
                    moveToNewLocation(currentLoc);
                    singleMarker = true;
            }
        } else {
            showSettingsAlert();
//            showSettingsAlert();
        }
    }

    public void showSettingsAlert() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("GPS Settings");
        alertDialogBuilder
                .setMessage("GPS is not enabled. Do you want to go to settings menu?")
                .setCancelable(false)
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getActivity().startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                if (alertDialog != null) {
                  /*  alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.primary));*/
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#520E0C"));
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#520E0C"));
                }
            }
        });
        alertDialog.show();

        /*AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getActivity().startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();*/
    }

    private void initCamera(LatLng temp) {
//        if (searchLocation) {
        position = CameraPosition.builder()
                .target(temp)
                .zoom(14f) //changed 15f to 14f for default zoom position
                .bearing(0.0f)
                .tilt(0.0f)
                .build();
        searchLocation = false;
     /*   } else {
            position = CameraPosition.builder()
                    .target(temp)
                    .zoom(15f)
                    .bearing(0.0f)
                    .tilt(0.0f)
                    .build();
        }*/
    }

    private void moveToNewLocation(LatLng temp) {
//        mMap.addMarker(new MarkerOptions().position(temp).title(getAddressFromLatLng(temp)));
//        float zoom = mMap.getMinZoomLevelNotClustered(marker1);
        mViewB.setVisibility(View.INVISIBLE);
        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);
       /* if (!favFlag) {
            if (circle != null) {
                circle.remove();
            }
       *//* circleOptions = new CircleOptions().center(temp)
                .radius(500).strokeColor(Color.parseColor("#696464")).fillColor(Color.TRANSPARENT).strokeWidth(4);*//*

            circleOptions = new CircleOptions().center(temp)
                    .radius(250).strokeColor(Color.parseColor("#1a88ff")).fillColor(Color.TRANSPARENT).strokeWidth(3);

            circle = mMap.addCircle(circleOptions);

        }*/

//        mMap.addCircle(new CircleOptions().center(temp).radius(1000).strokeColor(Color.parseColor("#696464")).fillColor(Color.parseColor("lightgray")).strokeWidth(4));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLoc));
    }

    private String getAddressFromLatLng(LatLng latLng) {
        Geocoder geocoder = new Geocoder(this.getActivity());

//        Location address;
        String returnAdress = "Please try again";
        try {
            List<Address> address = geocoder
                    .getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (address != null && (address.size() != 0)) {
                Address fetchedAddress = address.get(0);
                if (fetchedAddress != null) {
                    returnAdress = fetchedAddress.getAddressLine(0);
                }
               /* for(int i=0; i<fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(.getAddressLine(i)).append("\n");
                }*/

            }

        } catch (IOException e) {
        }

        return returnAdress;
    }

    protected void setUpBroadCast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");

        ReceivefrmSERVICE = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                networkCheckerFlag = NetworkChecker.getConnectivityStatusCheck(context);

                if (mConnectionDetector.isOnline() && networkCheckerFlag) {
//            if(hashMapData== null && hashMapDataAll == null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                       /* if (!reCheckFlag) {
                            reCheckFlag = true;

                            if (filterFlag) {
                                switchListeners();
                                if (priceSwitchEnableFlag) {
                                    new FreeStationsAsyncTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                                    *//*if (freeStationRecords != null) {
                                        displayFreeStationRecords(freeStationRecords);
                                    }*//*
                                } else {
                                    new GetJsonString().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                                    new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                                }
                            } else {
                                new GoogleMapsTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                                new GoogleMapsTaskAllStations().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            }
                        }*/
                        if (!mFirstFlag) {
                            new GoogleMapsTask().execute();
//                            currentLocationLatLng();
                            LocationFound();

                            mFirstFlag = true;

                        }
                    } else {
                        new GoogleMapsTask().execute();
                    }
//            }
                } else {
                  /*  if(!alertDialog.isShowing()) {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }*/
                    Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                }
            }
        };

        getActivity().registerReceiver(ReceivefrmSERVICE, filter);
    }

    protected void setUpMap() {
        if (!firstCHeck) {
            if (!networkCheckerFlag) {
                networkCheckerFlag = true;
            }/*else{
                networkCheckerFlag = false;
            }*/
            firstCHeck = true;
        }


    }

    private void resetMyPositionButton() {

        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
//        Fragment fragment = ( (SupportMapFragment) getSupportFragmentManager().findFragmentById( R.id.map ) );
        ViewGroup v1 = (ViewGroup) mapFragment.getView();
        ViewGroup v2 = (ViewGroup) v1.getChildAt(0);
        ViewGroup v3 = (ViewGroup) v2.getChildAt(2);
        View position = (View) v3.getChildAt(0);
        int positionWidth = position.getLayoutParams().width;
        int positionHeight = position.getLayoutParams().height;

        //lay out position button
        RelativeLayout.LayoutParams positionParams = new RelativeLayout.LayoutParams(positionWidth, positionHeight);
        int margin = positionWidth / 5;
        positionParams.setMargins(0, 0, 30, 220);
        positionParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        positionParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        position.setLayoutParams(positionParams);
    }

    private void resetMyToolBar() {

        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);

        ViewGroup v1 = (ViewGroup) mapFragment.getView();
        Log.d("Child Count", " : " + v1.getChildCount());

        for (int i = 0; i < v1.getChildCount(); i++) {
            View view = v1.getChildAt(i);
        }
        ViewGroup v2 = (ViewGroup) v1.getChildAt(0);
        ViewGroup v3 = (ViewGroup) v2.getChildAt(1);
//        ViewGroup v4 = (ViewGroup) v3.getChildAt(1);
        View position = (View) v3.getChildAt(0);

        int positionWidth = position.getLayoutParams().width;
        int positionHeight = position.getLayoutParams().height;

        //lay out position button
        RelativeLayout.LayoutParams positionParams = new RelativeLayout.LayoutParams(positionWidth, positionHeight);
        int margin = positionWidth / 5;
        positionParams.setMargins(0, 0, 30, 100);
        positionParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        positionParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        position.setLayoutParams(positionParams);


        /*FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
//        Fragment fragment = ( (SupportMapFragment) getSupportFragmentManager().findFragmentById( R.id.map ) );
        ViewGroup v1 = (ViewGroup) mapFragment.getView();
        ViewGroup v2 = (ViewGroup) v1.getChildAt(0);
        ViewGroup v3 = (ViewGroup) v2.getChildAt(1);
        View position = (View) v3.getChildAt(0);
        int positionWidth = position.getLayoutParams().width;
        int positionHeight = position.getLayoutParams().height;

        //lay out position button
        RelativeLayout.LayoutParams positionParams = new RelativeLayout.LayoutParams(positionWidth, positionHeight);
        int margin = positionWidth / 5;
        positionParams.setMargins(0, 0, 30, 100);
        positionParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        positionParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        position.setLayoutParams(positionParams);*/
    }

    /* public class GoogleMapsSortList extends AsyncTask<String, String, List<MapModel>> {
         @Override
         protected void onPreExecute() {
             super.onPreExecute();
 //            fixed the bug in sprint-7 Ticket_Id-159 starts
            *//* progressDialogTellus = new ProgressDialog(getActivity());
            progressDialogTellus.setCancelable(false);
            progressDialogTellus.setMessage("Please wait....");
            progressDialogTellus.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialogTellus.setProgress(0);
            progressDialogTellus.show();*//*
//            fixed the bug in sprint-7 Ticket_Id-159 end
        }

        @Override
        protected List<MapModel> doInBackground(String... params) {

            try {
                if (mConnectionDetector.isOnline() && networkCheckerFlag) {

//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                    response = mJsonUtil.getMethod(OnePocketUrls.GOOGLEMAPS_URL(), null, null);
                    OnePocketLog.d("singup resp----------", response);
                    int count = 0;
                    ArrayList<MapModel> mapArrayList = new ArrayList<MapModel>();

                    hashMapData = new HashMap<String, MapModel>();

                    JSONArray temp = new JSONArray(response);
                    Log.d("~~~~~~~~~~~~~", "m");
                    Log.d("MainActivity.java", "temp : " + temp);

                    for (int i = 0; i < temp.length(); i++) {
                        // for(int i=0;i<5;i++){
                        JSONObject e = temp.getJSONObject(i);
                        MapModel mapModel = new MapModel();
                        Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                        JSONObject data = e.getJSONObject("data");
                        Log.d("Json Object ", "data " + data);

                        // mapModel.setId(i);
                        mapModel.setLiveStatus((String) data.getString(LIVE_STATUS));
                        mapModel.setConn2PortType((String) data.getString(CONN2_PORT_TYPE));
                        mapModel.setStationName((String) data.getString(STATIONNAME));
                        mapModel.setConn1portType((String) data.getString(CONN1_PORT_TYPE));
                        mapModel.setStationStatus((String) data.getString(STATION_STATUS));
                        mapModel.setStationAddress((String) data.getString(STATION_ADDRESS));
                        mapModel.setConn2portLevel((String) data.getString(CONN2_PORT_LEVEL));
                        mapModel.setConn1portLevel((String) data.getString(CONN1_PORT_LEVEL));
                        mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));
//                        mapModel.setVendingPrice("\u00A2" +(String) data.getString(VENDINGPRICE));
                        mapModel.setVendingPrice1((String) "$" + data.getString(VENDINGPRICE1) + " kWh");
                        mapModel.setVendingPrice2((String) "$" + data.getString(VENDINGPRICE2) + " kWh");
                        mapModel.setStationADAStatus((String) data.getString(STATIONADASTATUS));
                        mapModel.setParkingPricePH((String) data.getString(PARKINGPRICEPH));
                        mapModel.setNotes((String) data.getString(Notes));


                        LatLang latLang = new LatLang();

                        if ((!e.getJSONArray("latLng").get(0).equals(null)) && (!e.getJSONArray("latLng").get(1).equals(null))) {
                            latLang.setLattitude((Double) e.getJSONArray("latLng").get(0));
                            latLang.setLongitude((Double) e.getJSONArray("latLng").get(1));

                        } else {
                            latLang.setLattitude(1.1);
                            latLang.setLongitude(1.1);

                        }
                        mapModel.setLatLang(latLang);

                        mapModel.setLattitude(mapModel.getLatLang().getLattitude());
                        mapModel.setLongitude(mapModel.getLatLang().getLongitude());

                        mapArrayList.add(mapModel);
                        Log.d("map arraylist " + count, " : " + mapArrayList);
                        count++;
                    }
                    Log.d("map arraylist", " : " + mapArrayList);
                    Log.d("map ArrayList size :", " " + mapArrayList.size());

                    return mapArrayList;

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
//                fixed the bug in sprint-7 Ticket_Id-159 starts
                OnePocketLog.d("singup error----------", e.toString());
//                showAlertBox(getResources().getString(R.string.netNotAvl));
//                progDailog.dismiss();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialogTellus != null) {
                            progressDialogTellus.dismiss();
                        }
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                });
//                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
//                fixed the bug in sprint-7 Ticket_Id-159 end
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<MapModel> result) {
            super.onPostExecute(result);

            if (result == null) {
                return;
            }
//            fixed the bug in sprint-7 Ticket_Id-159 starts
            if (progressDialogTellus != null && progressDialogTellus.isShowing()) {
                progressDialogTellus.dismiss();
                progressDialogTellus = null;
            }
//            fixed the bug in sprint-7 Ticket_Id-159 end
            //To do need the set the data to list
            //   LatLng mapDisplay;
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                for (int i = 0; i < result.size(); i++) {
                    MapModel mMapModel = new MapModel();
                    mMapModel = result.get(i);

                    LatLng latlng = new LatLng(mMapModel.getLattitude(), mMapModel.getLongitude());
                    LatLang latLang = new LatLang(latlng.latitude, latlng.longitude);
                    MarkerOptions markerOptions = new MarkerOptions();

                    if (statusSwitch.isChecked()) {
                        if (mMapModel.getLiveStatus().equals("Available")) {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.available_green));
                        }
                    } else {
                        if (mMapModel.getLiveStatus().contains("In-Use")) {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                        }
                    }

                    if (priceSwitch.isChecked()) {
                        if (!mMapModel.getVendingPrice1().contains("0.000")) {
                            if (statusSwitch.isChecked()) {
                                if (mMapModel.getLiveStatus().equals("Available")) {
                                    markerOptions = new MarkerOptions().position(latlng).
                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.available_green));
                                }
                            } else {
                                if (mMapModel.getLiveStatus().contains("In-Use")) {
                                    markerOptions = new MarkerOptions().position(latlng).
                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                                }
                            }
                        } else {
                            if (statusSwitch.isChecked()) {
                                if (mMapModel.getLiveStatus().equals("Available")) {
                                    markerOptions = new MarkerOptions().position(latlng).
                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.green));
                                }
                            } else {
                                if (mMapModel.getLiveStatus().contains("In-Use")) {
                                    markerOptions = new MarkerOptions().position(latlng).
                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                                }
                            }
                        }
                    }

                    *//*if(leve1Switch.isChecked()){

                    }

                    if(leve2Switch.isChecked()){

                    }*//*


                    Marker marker = mMap.addMarker(markerOptions);
                    markerListTell.add(marker);
                    hashMapData.put(marker.getId(), mMapModel);
//                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
                    mMap.getUiSettings().setMapToolbarEnabled(false);
//                    resetMyToolBar();
//                    mMap.getUiSettings().setZoomControlsEnabled(true);
//                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
//                    mMap.setMyLocationEnabled(true);
//                    resetMyPositionButton();
                }
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();

            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {

                    disableAllViews();

                    gpsTracker = new GPSTracker(getActivity().getApplicationContext());

                    if (!gpsTracker.canGetLocation) {
                        showSettingsAlert();
//                        mMap.getUiSettings().setScrollGesturesEnabled(false);
//                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
//                        map.getUiSettings().setMyLocationButtonEnabled(false);
//                        mMap.setMyLocationEnabled(false);
                    }

                    if (!mConnectionDetector.isOnline()) {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                                Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }
            });

            //            fixed the bug in sprint-7 Ticket_Id-161 starts
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

//                    resetMyToolBar();
                    if (marker.isCluster()) {
                        if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                            declusterify(marker);
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    } else {
                        clickMarkerViews(marker);
                        return false;
                    }


                    *//*if (markerListTell.contains(marker)) {
                        Log.d("markerList tellus", " success :  " + markerList);
                        viewD.setVisibility(View.GONE);
                        viewC.setVisibility(View.GONE);
                        mViewB.setVisibility(View.VISIBLE);
                    }
                    if (routeMarkerList.contains(marker) || markerPointsList.contains(marker)) {
                        Log.d("routeMarkerList tellus", " success :  " + routeMarkerList);
                        viewD.setVisibility(View.VISIBLE);
                        viewC.setVisibility(View.GONE);
                        mViewB.setVisibility(View.GONE);
                    }

                    return false;*//*
                }
            });
            //information window code starts
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    Log.d("clicked", "view");
                    view = null;
                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {

                        displayInfoWindow(marker);
                        *//*for (Map.Entry m : hashMapData.entrySet()) {
                            MapModel mm = new MapModel();
                            mm = (MapModel) m.getValue();


                          *//**//*  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.clear();
                            editor1.putString(CHARGER_ID, m.getKey().toString());
                            editor1.commit();*//**//*
                        *//**//*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*//**//*
                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
                                chargerKey = m.getKey().toString();
//                                mBottomLayout.setVisibility(View.VISIBLE);

                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                viewC.setVisibility(View.GONE);
                                viewD.setVisibility(View.GONE);

                                stationName.setText(mm.getStation());
                                markerCheck = "";
                                markerCheck = "Type1";
                                // stationAddress.setText(infoWindowModel.getStationAddress());
//                                chargers.setText(mm.getPortQuantity());
                                city.setText("");
                                levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                                levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                            }
                        }*//*
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    Log.d("data", " view " + view);
                    return view;
                }
            });


            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng position) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    } else {
                        disableAllViews();

                    }
                }
            });

            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
//                        mMap.animateCamera(CameraUpdateFactory.zoomTo(0.0f));
//                        mBottomLayout.setVisibility(View.INVISIBLE);
                    }

                    cameraChangeInMap();
                   *//* LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;


                    if ((mMap.getCameraPosition().zoom) <= 15) {
                        updateClustering();
                        clusterifyMarkers();
                    }

                    if ((mMap.getCameraPosition().zoom) >= 15) {
                        displayAllMarkers();
                    }

                    for (Marker marker1 : markerList) {
                        if (bounds.contains(marker1.getPosition())) {
                            marker1.setVisible(true);
                        } else {
                            marker1.setVisible(false);
                        }
                    }*//*
                }
            });

            //            fixed the bug in sprint-7 Ticket_Id-161 end
            //information window code ends
            *//*mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }

                    if (!mConnectionDetector.isOnline()) {
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                }
            });*//*
        }
    }

*/
    private void clusterifyMarkers() {

        if (declusterifiedMarkers != null) {
            for (Marker marker : declusterifiedMarkers) {

                LatLng position = marker.getPosition();
                marker.setPosition(position);

                OnePocketLog.d("POSITION OF CLUSTER", String.valueOf(marker));

                marker.setClusterGroup(ClusterGroup.DEFAULT);
            }
            declusterifiedMarkers = null;
        }
    }

    private void displayAllMarkers() {

        if (mMap == null) {
            return;
        }
        ClusteringSettings clusteringSettings = new ClusteringSettings();
        clusteringSettings.enabled(false);
        mMap.setClustering(clusteringSettings);
    }

    public void displayFreeStationRecords(ArrayList<MapModelFilters> freeStationRecords) {

        if (freeStationRecords != null) {
            Log.d("free station records", "length : " + freeStationRecords.size());

            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                for (int i = 0; i < freeStationRecords.size(); i++) {

                    MapModelFilters mMapModelFreeRecords = new MapModelFilters();
                    mMapModelFreeRecords = freeStationRecords.get(i);
                    // MyMarker myMarker = new MyMarker();
                    Log.d("card type ", " : " + mMapModelFreeRecords.getCards_accepted());
                    Log.d("records ", " : " + i);

                    LatLng latlng = new LatLng(mMapModelFreeRecords.getLattitude(), mMapModelFreeRecords.getLongitude());

                    MarkerOptions markerOptions = new MarkerOptions().position(latlng);


                    if (mMapModelFreeRecords.getStatus_code().contains("E")) {
                        if (mMapModelFreeRecords.getEv_connector_types().contains("CHADEMO") ||
                                mMapModelFreeRecords.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModelFreeRecords.getEv_connector_types().contains("TESLA")) {
//                            if (dcFastFlag) {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.green_fc));
//                                dcSwitchFlag = false;
//                            } else {
//                                dcSwitchFlag = true;
//                            }
                        } /*else {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.available_green));
                        }*/
                    } else if (mMapModelFreeRecords.getStatus_code().contains("T") ||
                            mMapModelFreeRecords.getStatus_code().contains("P")) {
                        if (mMapModelFreeRecords.getEv_connector_types().contains("CHADEMO") ||
                                mMapModelFreeRecords.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModelFreeRecords.getEv_connector_types().contains("TESLA")) {
//                            if (dcFastFlag) {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray_fc));
//                                dcSwitchFlag = false;
//                            } else {
//                                dcSwitchFlag = true;
//                            }
                        } else {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));
                        }
                    } else {

                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                    }

                    if (!dcSwitchFlag) {
                        marker = mMap.addMarker(markerOptions);
                        markerListFilter.add(marker);
//                        mMap.setMyLocationEnabled(true);
                        hashMapFilter.put(marker.getId(), mMapModelFreeRecords);
                    }
//                    resetMyPositionButton();
                    mMap.getUiSettings().setMapToolbarEnabled(false);
//                    mMap.getUiSettings().setMapToolbarEnabled(true);
//                    resetMyToolBar();

                }
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }


            updateClustering();

            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    disableAllViews();

                    gpsTracker = new GPSTracker(getActivity().getApplicationContext());

                    if (!gpsTracker.canGetLocation) {
                        showSettingsAlert();
//                        mMap.getUiSettings().setScrollGesturesEnabled(false);
//                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
//                        mMap.setMyLocationEnabled(false);
                    }

                    if (!mConnectionDetector.isOnline()) {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                                Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }
            });

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    if (marker.isCluster()) {
                        declusterify(marker);
                        return true;
                    } else {
                        clickMarkerViews(marker);
                        return false;
                    }
                }
            });

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng position) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    } else {
                        disableAllViews();
                    }
                }
            });


            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    view = null;
                    Log.d("declusterifiedMarkers", "" + declusterifiedMarkers);

                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {

                        displayInfoWindow(marker);
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    return view;
                }
            });

            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {


                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    cameraChangeInMap();

                }
            });

        }

    }

    public void initFragment() {
        if (fragment != null) {
            fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frameLayout, fragment).addToBackStack(null).commit();

            new KeyBoardHide().hideKeyBoard(getActivity());
            // update selected item and title, then close the drawer
//                mDrawerList.setItemChecked(position, true);
//                mDrawerList.setSelection(position);
//                mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.d("NavigationDrActivity", "Error  creating fragment");
        }
    }

    private String initStationName(LatLang latLang) {

        double lat = latLang.getLattitude();
        double lon = latLang.getLongitude();
        mLatLng = new LatLng(lat, lon);

        String locationString = getAddressFromLatLng(mLatLng);
        return locationString;
    }



   /* @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        mClusterManager = new ClusterManager<>(this.getActivity(), mMap);
//        mClusterManager.setRenderer(new CustomRenderer<LatLang>(this.getActivity(), mMap, mClusterManager));
        if(mConnectionDetector.isOnline()) {
            new GoogleMapsTask().execute();
        }else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }
    }*/

    private void declusterify(Marker cluster) {
        clusterifyMarkers();
        if (mConnectionDetector.isOnline() && networkCheckerFlag) {
            declusterifiedMarkers = cluster.getMarkers();
//        LatLng clusterPosition = cluster.getPosition();
//        double distance = calculateDistanceBetweenMarkers();
//        double currentDistance = -declusterifiedMarkers.size() / 2 * distance;
    /*    for (Marker marker : declusterifiedMarkers) {
            marker.setData(marker.getPosition());
            marker.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
            LatLng newPosition = new LatLng(marker.getPosition().latitude,marker.getPosition().longitude*//*clusterPosition.latitude, clusterPosition.longitude + currentDistance*//*);
            marker.animatePosition(newPosition);
//            currentDistance += distance;
        }*/
//        mBottomLayout.setVisibility(View.INVISIBLE);
            mViewB.setVisibility(View.INVISIBLE);
            viewC.setVisibility(View.GONE);
            viewD.setVisibility(View.GONE);
            mFab.setVisibility(View.GONE);
            mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));

            resetMyPositionButton();

            if (declusterifiedMarkers.size() == 1) {
                LatLng clusterPosition = cluster.getPosition();
                for (Marker marker : declusterifiedMarkers) {
                    marker.setData(marker.getPosition());
                    marker.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
                    LatLng singleMarkerPosition = new LatLng(clusterPosition.latitude, clusterPosition.longitude);
                    marker.animatePosition(singleMarkerPosition);
//                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(singleMarkerPosition, 18));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(singleMarkerPosition, 23));
                }
            } else {
                Toast.makeText(getContext(), "Please Wait....", Toast.LENGTH_SHORT).show();
                LatLngBounds.Builder builder = LatLngBounds.builder();
//        List<Marker> val= checkDupMarkers(declusterifiedMarkers);
                Set set = new HashSet();
                double max = .999999;
                double min = 1.000001;

                List newList = new ArrayList<Marker>();
                for (Iterator iter = declusterifiedMarkers.iterator(); iter.hasNext(); ) {
                    Marker element = (Marker) iter.next();
                    if (set.add(element.getPosition())) {
                        newList.add(element);
                    } else {
                        LatLng selDuplicate = new LatLng(element.getPosition().latitude * (Math.random() * (max - min) + min),
                                element.getPosition().longitude * (Math.random() * (max - min) + min));
                        element.setPosition(selDuplicate);
                        newList.add(element);
                    }
                }
                declusterifiedMarkers.clear();
                declusterifiedMarkers.addAll(newList);

     /*   HashSet<Marker> listToSet = new HashSet<Marker>(declusterifiedMarkers);
        List<Marker> listWithoutDuplicates = new ArrayList<Marker>(listToSet);*/

                for (Marker marker : declusterifiedMarkers) {
//            if(!marker.equals(marker1)) {
                    builder.include(marker.getPosition());
//            }
                }
                final LatLngBounds bounds = builder.build();
        /*int zoomLevel;
        zoomLevel = getBoundsZoomLevel(bounds,);*/
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(adjustBoundsForMaxZoomLevel(bounds),
                        getResources().getDimensionPixelSize(R.dimen.padding)));
            }
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }
//        mMap.animateCamera(CameraUpdateFactory.zoomBy(zoomLevel));

        /*if(mMap.getCameraPosition().zoom > 4.0f){
           mMap.animateCamera(CameraUpdateFactory.zoom);
        }*/
    }

    private LatLngBounds adjustBoundsForMaxZoomLevel(LatLngBounds bounds) {
        LatLng sw = bounds.southwest;
        LatLng ne = bounds.northeast;
        double deltaLat = Math.abs(sw.latitude - ne.latitude);
        double deltaLon = Math.abs(sw.longitude - ne.longitude);

        final double zoomN = 0.005; // minimum zoom coefficient
        if (deltaLat < zoomN) {
            sw = new LatLng(sw.latitude - (zoomN - deltaLat / 2), sw.longitude);
            ne = new LatLng(ne.latitude + (zoomN - deltaLat / 2), ne.longitude);
            bounds = new LatLngBounds(sw, ne);
        } else if (deltaLon < zoomN) {
            sw = new LatLng(sw.latitude, sw.longitude - (zoomN - deltaLon / 2));
            ne = new LatLng(ne.latitude, ne.longitude + (zoomN - deltaLon / 2));
            bounds = new LatLngBounds(sw, ne);
        }

        return bounds;
    }

    public int getBoundsZoomLevel(LatLngBounds bounds, int mapWidthPx, int mapHeightPx) {

        LatLng ne = bounds.northeast;
        LatLng sw = bounds.southwest;

        double latFraction = (latRad(ne.latitude) - latRad(sw.latitude)) / Math.PI;

        double lngDiff = ne.longitude - sw.longitude;
        double lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        double latZoom = zoom(mapHeightPx, WORLD_PX_HEIGHT, latFraction);
        double lngZoom = zoom(mapWidthPx, WORLD_PX_WIDTH, lngFraction);

        int result = Math.min((int) latZoom, (int) lngZoom);
        return Math.min(result, ZOOM_MAX);
    }

    private double latRad(double lat) {
        double sin = Math.sin(lat * Math.PI / 180);
        double radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }

    /* filter async task*/
//    fixed the bug in sprint-7 Ticket_Id-147 starts

    private double zoom(int mapPx, int worldPx, double fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / LN2);
    }

    void updateClustering() {
        if (mMap == null) {
            return;
        }
        ClusteringSettings clusteringSettings = new ClusteringSettings();
        clusteringSettings.clusterOptionsProvider(new ImageClusterOptionsProvider(getResources()));

        double clusterSize = CLUSTER_SIZES[1];
        clusteringSettings.clusterSize(clusterSize);
        mMap.setClustering(clusteringSettings);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        searchString = (String) adapterView.getItemAtPosition(position);

        //keyboard closing window code starts 19-Feb-2014

        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

        //keyboard closing window code ends 19-Feb-2014

        //  Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

        if (searchLocationMarker) {
            if (marker2 != null) {
                marker2.remove();
            }
        }
        List<Address> addresList = null;
        Geocoder geocoder = new Geocoder(this.getActivity());
        try {
            addresList = geocoder.getFromLocationName(searchString, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Address address = addresList.get(0);

        locationInfo = new LatLng(address.getLatitude(), address.getLongitude());
        initCamera(locationInfo);
        moveToNewLocation(locationInfo);
        marker2 = mMap.addMarker(new MarkerOptions().position(locationInfo).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title(searchString));
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationInfo, 1));
        searchLocationMarker = true;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        SharedPreferences.Editor editor = pSharedPref.edit();

        editor.putString(FAVLATRETURN, "");
        editor.putBoolean(FAVLATRETURNFLAG, false);
        editor.commit();

        ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        getActivity().unregisterReceiver(ReceivefrmSERVICE);
        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);
        if (searchIcon != null) {
            searchIcon.setVisibility(View.GONE);
        }

        if (searchBox != null) {
            searchBox.setVisibility(View.GONE);
        }
    }
//    fixed the bug in sprint-7 Ticket_Id-147 end
    /*filter async task end*/

    public class FilterListGet extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                initFilterData();
                String stringurl = OnePocketUrls.GETFILTERLIST();
                response = mJsonUtil.postMethod(stringurl, null, null, null, null, null, null, null, null, null, mUserFilterData, null, null);
                OnePocketLog.d("Register resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("Register error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
        }
    }

  /*  private List<Marker> checkDupMarkers( List<Marker> decluster){
        Set<Marker> s = new TreeSet<Marker>(new Comparator<Marker>() {
            @Override
            public int compare(Marker lhs, Marker rhs) {
                if(lhs.getId().equalsIgnoreCase(rhs.getId())){
                    return 0;
                }
                return 1;
            }
        });
        s.addAll(decluster);
        return decluster;
    }*/

    /**
     * A class, to download Google Places
     */
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        //      added code by praveen on 20April2016 starts
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog2.setCancelable(false);
            progressDialog2.setMessage("Please wait....");
            progressDialog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog2.setProgress(0);
            progressDialog2.show();
        }
        //      added code by praveen on 20April2016 ends


        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try {
                Log.d("Background Task", "Places Task");
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result) {
            Log.d("onPost", "Places Task");
            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            new ParserTask().execute(result);

        }

    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {


        JSONObject jObject;


        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                Log.d("Background Task", "parser Task");
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String, String>> list) {

            double lat = 0, lng = 0;

//            fixed the bug in sprint-7 Ticket_Id-168 starts
            if (list == null) {
                Toast.makeText(getContext(), "Didn't get complete data, check internet connection", Toast.LENGTH_SHORT).show();
//            fixed the bug in sprint-7 Ticket_Id-168 end
            } else {

                Marker marker;

                // Clears all the existing markers

                //      removed code by praveen on 20April2016 starts

               /* if (markerArrayList != null) {
                    for (int i = 0; i < markerArrayList.size(); i++) {
                        Marker markerRemove = markerArrayList.get(i);
                        markerRemove.remove();
                    }
                    markerArrayList.clear();
                }*/

                //      removed code by praveen on 20April2016 ends


           /* Marker marker = null;
            marker.remove();*/

                Log.d("onPostExecute", "parser Task");
                for (int i = 0; i < list.size(); i++) {

                    // Creating a marker
                    MarkerOptions markerOptions = new MarkerOptions();

                    // Getting a place from the places list
                    HashMap<String, String> hmPlace = list.get(i);

                    // Getting latitude of the place
                    lat = Double.parseDouble(hmPlace.get("lat"));

                    // Getting longitude of the place
                    lng = Double.parseDouble(hmPlace.get("lng"));

                    // Getting name
                    String name = hmPlace.get("place_name");

                    // Getting vicinity
                    String vicinity = hmPlace.get("vicinity");


                    LatLng latLng = new LatLng(lat, lng);
                    // Setting the position for the marker
                    markerOptions.position(latLng);

                    // Setting the title for the marker.
                    //This will be displayed on taping the marker
                    markerOptions.title(name + " : " + vicinity);

                    // Placing a marker on the touched position
//                    marker = googleMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.atmmarker)));

                    //      updated code by praveen on 21April2016 starts
                    if (lat != start.latitude && lng != start.longitude && lat != end.latitude && lng != end.longitude) {
//                        fixed the bug in sprint-7 Reopened Ticket_Id-197 starts
                       /* if (spinnerValueType.contains("atm")) {
                            marker = mMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                            routeMarkerList.add(marker);
                        }*/

                        //OnePocketLog.d("SPINNERVALUETYPE" , spinnerValueType);

//                        fixed the bug in sprint-7 Reopened Ticket_Id-197 end

                        if (spinnerValueType.contains("restaurant")) {
//                            marker = mMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                            marker = mMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.restuarent)));
                            routeMarkerList.add(marker);

                        }
                        if (spinnerValueType.contains("cafe")) {
//                            marker = mMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
                            marker = mMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.coffeshop)));
                            routeMarkerList.add(marker);
                        }
                        if (spinnerValueType.contains("shopping_mall")) {
//                            marker = mMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
                            marker = mMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.shop)));
                            routeMarkerList.add(marker);
                        }
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);
                        mMap.animateCamera(zoom);
//                        markerArrayList.add(marker);
                    }
                    //      updated code by praveen on 21April2016 starts
                }

                for (Marker arrayListData : routeMarkerList) {
                    arrayListData.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
                }
            }
            //      added code by praveen on 20April2016 starts
            progressDialog2.dismiss();

            //      added code by praveen on 20April2016 ends

            /*routeMarkersMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
//                    viewD.setVisibility((View.VISIBLE));
                    viewC.setVisibility(View.GONE);
                    viewB.setVisibility(View.GONE);
                    return true;
                }
            });

            routeMarkersMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoContents(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoWindow(Marker marker) {
                    Log.d("routeMarkersMap"," displayed");
                    return null;
                }
            });*/

        }
    }

    public class RoutesJsonTask extends AsyncTask<String, String, List<RouteLatLngModel>> {

        @Override
        protected List<RouteLatLngModel> doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();

                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                String finalJson = buffer.toString();

                Log.d("final Json", " : " + finalJson);

                jObject = new JSONObject(finalJson);

                jRoutes = jObject.getJSONArray("routes");

                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {

                            RouteLatLngModel routeLatLngModel = new RouteLatLngModel();

                            routelattitude = (Double) ((JSONObject) ((JSONObject) jSteps.get(k)).get("start_location")).get("lat");
                            routeLongitude = (Double) ((JSONObject) ((JSONObject) jSteps.get(k)).get("start_location")).get("lng");

                            routeLatLngModel.setLatitude(routelattitude);
                            routeLatLngModel.setLongitude(routeLongitude);

                            routeLatLngList.add(routeLatLngModel);
                        }
                    }
                }

                return routeLatLngList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<RouteLatLngModel> routeLatLngModelsList) {
            super.onPostExecute(routeLatLngModelsList);
            if (routeLatLngModelsList == null || routeLatLngModelsList.equals("")) {
                Toast.makeText(getContext(), "No Data Found", Toast.LENGTH_SHORT).show();
            } else {

                Log.d("RouteLatLngList", " : " + routeLatLngModelsList.size());
                for (int i = 0; i < routeLatLngModelsList.size(); i++) {

                    RouteLatLngModel routeLatLngModel = routeLatLngModelsList.get(i);

                    Log.d("lattitude " + i, " : " + routeLatLngModel.getLatitude());
                    Log.d("longitude " + i, " : " + routeLatLngModel.getLongitude());

                    nearestUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + routeLatLngModel.getLatitude() + "," +
                            routeLatLngModel.getLongitude() + "&radius=400&types=" + spinnerValueType + "&sensor=true&key=" + KEY3;

                    new PlacesTask().execute(nearestUrl);

                    OnePocketLog.d("nearsetUrl" , nearestUrl);
                }


                Log.d("End of App Execution", "");

            }
        }
    }

    public class GoogleMapsTask extends AsyncTask<String, String, List<MapModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            fixed the bug in sprint-7 Ticket_Id-159 starts
           /* progressDialogTellus = new ProgressDialog(getActivity());
            progressDialogTellus.setCancelable(false);
            progressDialogTellus.setMessage("Please wait....");
            progressDialogTellus.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialogTellus.setProgress(0);
            progressDialogTellus.show();*/
//            fixed the bug in sprint-7 Ticket_Id-159 end
        }

        @Override
        protected List<MapModel> doInBackground(String... params) {

            try {
                if (mConnectionDetector.isOnline() && networkCheckerFlag) {

//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                    response = mJsonUtil.getMethod(OnePocketUrls.GOOGLEMAPS_URL(), null, null);
                    OnePocketLog.d("GoogleMapsResponse----------", response);
                    int count = 0;
                    ArrayList<MapModel> mapArrayList = new ArrayList<MapModel>();

                    hashMapData = new HashMap<String, MapModel>();

                    JSONArray temp = new JSONArray(response);
                    Log.d("~~~~~~~~~~~~~", "m");
                    Log.d("MainActivity.java", "temp : " + temp);

                    for (int i = 0; i < temp.length(); i++) {
                        // for(int i=0;i<5;i++){
                        JSONObject e = temp.getJSONObject(i);
                        MapModel mapModel = new MapModel();
                        Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                        JSONObject data = e.getJSONObject("data");
                        Log.d("Json Object ", "data " + data);

                        // mapModel.setId(i);
                        mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));
                      /*  mapModel.setLiveStatus((String) data.getString(LIVE_STATUS));
                        mapModel.setConn2PortType((String) data.getString(CONN2_PORT_TYPE));
                        mapModel.setStation((String) data.getString(STATION));
                        mapModel.setStationID((String) data.getString(STATIONID));
                        mapModel.setConn1portType((String) data.getString(CONN1_PORT_TYPE));
                        mapModel.setStationStatus((String) data.getString(STATION_STATUS));
                        mapModel.setStationAddress((String) data.getString(STATION_ADDRESS));
                        mapModel.setConn2portLevel((String) data.getString(CONN2_PORT_LEVEL));
                        mapModel.setConn1portLevel((String) data.getString(CONN1_PORT_LEVEL));
                        mapModel.setConn1portLevel((String) data.getString(CONN1_PORT_LEVEL));
                        mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));
//                        mapModel.setVendingPrice("\u00A2" +(String) data.getString(VENDINGPRICE));
                        mapModel.setVendingPrice1((String) "$" + data.getString(VENDINGPRICE1) + " kWh");
                        mapModel.setVendingPrice2((String) "$" + data.getString(VENDINGPRICE2) + " kWh");*/

                        String noOfPorts = data.getString(PORT_QUANTITY);
                        int noOfPortsIntegerValue = Integer.parseInt(noOfPorts);

                        if (noOfPortsIntegerValue == 1) {
                            mapModel.setLiveStatus((String) data.getString(LIVE_STATUS));
                            mapModel.setStationName((String) data.getString(STATIONNAME));
                            mapModel.setStationID((String) data.getString(STATIONID));

                            mapModel.setStationAddress((String) data.getString(STATION_ADDRESS));
                            mapModel.setConn1portLevel((String) data.getString(CONN1_PORT_LEVEL));
                            mapModel.setVendingPriceUnit1((String) data.getString(VENDINGPRICEUNIT1));

                            String noOfvendingPriceUnit1 = data.getString(VENDINGPRICEUNIT1);
                            int noOfvendingPriceUnit1IntegerValue = Integer.parseInt(noOfvendingPriceUnit1);

                            if (noOfvendingPriceUnit1IntegerValue == 1) {
                                mapModel.setVendingPrice1((String) "$" + data.getString(VENDINGPRICE1) + " Hr");
                            } else if (noOfvendingPriceUnit1IntegerValue == 2) {
                                mapModel.setVendingPrice1((String) "$" + data.getString(VENDINGPRICE1) + " kWh");
                            }
                            //mapModel.setVendingPrice1((String) "$" + data.getString(VENDINGPRICE1) + "kWh");
                            mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));
                            mapModel.setStationStatus((String) data.getString(STATION_STATUS));
                            mapModel.setStationADAStatus((String) data.getString(STATIONADASTATUS));
                            mapModel.setParkingPricePH((String) data.getString(PARKINGPRICEPH));
                            mapModel.setNotes((String) data.getString(Notes));
                            mapModel.setOpenTime((String) data.getString(OPENTIME));
                            mapModel.setCloseTime((String) data.getString(CLOSETIME));

                        } else if (noOfPortsIntegerValue == 2) {
                            mapModel.setStationAddress((String) data.getString(STATION_ADDRESS));
                            mapModel.setStationName((String) data.getString(STATIONNAME));
                            mapModel.setConn1portLevel((String) data.getString(CONN1_PORT_LEVEL));
                            mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));
                            mapModel.setStationADAStatus((String) data.getString(STATIONADASTATUS));
                            mapModel.setParkingPricePH((String) data.getString(PARKINGPRICEPH));

                            mapModel.setLiveStatus((String) data.getString(LIVE_STATUS));
                            mapModel.setStationID((String) data.getString(STATIONID));
                            // mapModel.setConn2PortType((String) data.getString(CONN2_PORT_TYPE));
                            mapModel.setConn2portLevel((String) data.getString(CONN2_PORT_LEVEL));
                            mapModel.setStationStatus((String) data.getString(STATION_STATUS));
                            mapModel.setVendingPriceUnit1((String) data.getString(VENDINGPRICEUNIT1));
                            mapModel.setNotes((String) data.getString(Notes));

                            mapModel.setOpenTime((String) data.getString(OPENTIME));
                            mapModel.setCloseTime((String) data.getString(CLOSETIME));

                            /*Vending Price 2*/
                            String noOfvendingPriceUnit1 = data.getString(VENDINGPRICEUNIT1);
                            int noOfvendingPriceUnit1IntegerValue = Integer.parseInt(noOfvendingPriceUnit1);

                            if (noOfvendingPriceUnit1IntegerValue == 1) {
                                mapModel.setVendingPrice2((String) "$" + data.getString(VENDINGPRICE2) + " Hr");

                            } else if (noOfvendingPriceUnit1IntegerValue == 2) {
                                mapModel.setVendingPrice2((String) "$" + data.getString(VENDINGPRICE2) + " kWh");
                            }
                            /*Vending Price 2*/

                            /*Vending Price 1*/

                            String noOfvendingPriceUnit2 = data.getString(VENDINGPRICEUNIT1);
                            int noOfvendingPriceUnit1IntegerValue2 = Integer.parseInt(noOfvendingPriceUnit2);

                            if (noOfvendingPriceUnit1IntegerValue2 == 1) {
                                mapModel.setVendingPrice1((String) "$" + data.getString(VENDINGPRICE1) + " Hr");
                            } else if (noOfvendingPriceUnit1IntegerValue2 == 2) {
                                mapModel.setVendingPrice1((String) "$" + data.getString(VENDINGPRICE1) + " kWh");
                            }

                            /*Vending Price 1*/

                            //mapModel.setConn1portType((String) data.getString(CONN1_PORT_TYPE));
                            //mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));
//                        mapModel.setVendingPrice("\u00A2" +(String) data.getString(VENDINGPRICE));
                        }

                        //OnePocketLog.d("STATION" , data.getString(STATION));

                        LatLang latLang = new LatLang();

                        if ((!e.getJSONArray("latLng").get(0).equals(null)) && (!e.getJSONArray("latLng").get(1).equals(null))) {
                            latLang.setLattitude((Double) e.getJSONArray("latLng").get(0));
                            latLang.setLongitude((Double) e.getJSONArray("latLng").get(1));

                        } else {
                            latLang.setLattitude(1.1);
                            latLang.setLongitude(1.1);

                        }
                        mapModel.setLatLang(latLang);

                        mapModel.setLattitude(mapModel.getLatLang().getLattitude());
                        mapModel.setLongitude(mapModel.getLatLang().getLongitude());

                        mapArrayList.add(mapModel);
                        Log.d("map arraylist " + count, " : " + mapArrayList);
                        count++;
                    }
                    Log.d("map arraylist", " : " + mapArrayList);
                    Log.d("map ArrayList size :", " " + mapArrayList.size());

                    return mapArrayList;

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
//                fixed the bug in sprint-7 Ticket_Id-159 starts
                OnePocketLog.d("GoogleMapsResponseError----------", e.toString());
//                showAlertBox(getResources().getString(R.string.netNotAvl));
//                progDailog.dismiss();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialogTellus != null) {
                            progressDialogTellus.dismiss();
                        }
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                });
//                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
//                fixed the bug in sprint-7 Ticket_Id-159 end
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<MapModel> result) {
            super.onPostExecute(result);

            if (result == null) {
                return;
            }
//            fixed the bug in sprint-7 Ticket_Id-159 starts
            if (progressDialogTellus != null && progressDialogTellus.isShowing()) {
                progressDialogTellus.dismiss();
                progressDialogTellus = null;
            }
//            fixed the bug in sprint-7 Ticket_Id-159 end
            //To do need the set the data to list
            //   LatLng mapDisplay;
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                for (int i = 0; i < result.size(); i++) {
                    MapModel mMapModel = new MapModel();
                    mMapModel = result.get(i);

                    LatLng latlng = new LatLng(mMapModel.getLattitude(), mMapModel.getLongitude());
                    LatLang latLang = new LatLang(latlng.latitude, latlng.longitude);
                    MarkerOptions markerOptions;
                   /*if (mMapModel.getStationStatus().contains("StationIdleAllOk")) {
                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.green));
                    } else if (mMapModel.getStationStatus().contains("InSession") ||
                            mMapModel.getStationStatus().contains("ChargingInitiated")) {
                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                    } else {

                        OnePocketLog.d("STATIONZOOMIN" , latLang.toString());

                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));
                    }*/

                    if (mMapModel.getLiveStatus().contains("Available")) {
                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.green));
                    } else if (mMapModel.getLiveStatus().contains("In-Use")) {
                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                    } else {
                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));
                    }


                    Marker marker = mMap.addMarker(markerOptions);
                    markerListTell.add(marker);
                    hashMapData.put(marker.getId(), mMapModel);
//                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
                    mMap.getUiSettings().setMapToolbarEnabled(false);
//                    resetMyToolBar();
//                    mMap.getUiSettings().setZoomControlsEnabled(true);
//                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
//                    mMap.setMyLocationEnabled(true);
//                    resetMyPositionButton();
                }
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();

            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {

                    disableAllViews();

                    gpsTracker = new GPSTracker(getActivity().getApplicationContext());

                    if (!gpsTracker.canGetLocation) {
                        showSettingsAlert();
//                        mMap.getUiSettings().setScrollGesturesEnabled(false);
//                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
//                        map.getUiSettings().setMyLocationButtonEnabled(false);
//                        mMap.setMyLocationEnabled(false);
                    }

                    if (!mConnectionDetector.isOnline()) {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                                Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }
            });

            //            fixed the bug in sprint-7 Ticket_Id-161 starts
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

//                    resetMyToolBar();
                    if (marker.isCluster()) {
                        if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                            declusterify(marker);
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    } else {
                        clickMarkerViews(marker);
                        return false;
                    }


                    /*if (markerListTell.contains(marker)) {
                        Log.d("markerList tellus", " success :  " + markerList);
                        viewD.setVisibility(View.GONE);
                        viewC.setVisibility(View.GONE);
                        mViewB.setVisibility(View.VISIBLE);
                    }
                    if (routeMarkerList.contains(marker) || markerPointsList.contains(marker)) {
                        Log.d("routeMarkerList tellus", " success :  " + routeMarkerList);
                        viewD.setVisibility(View.VISIBLE);
                        viewC.setVisibility(View.GONE);
                        mViewB.setVisibility(View.GONE);
                    }

                    return false;*/
                }
            });
            //information window code starts
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    Log.d("clicked", "view");
                    view = null;
                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {

                        displayInfoWindow(marker);
                        /*for (Map.Entry m : hashMapData.entrySet()) {
                            MapModel mm = new MapModel();
                            mm = (MapModel) m.getValue();


                          *//*  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.clear();
                            editor1.putString(CHARGER_ID, m.getKey().toString());
                            editor1.commit();*//*
                        *//*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*//*
                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
                                chargerKey = m.getKey().toString();
//                                mBottomLayout.setVisibility(View.VISIBLE);

                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                viewC.setVisibility(View.GONE);
                                viewD.setVisibility(View.GONE);

                                stationName.setText(mm.getStation());
                                markerCheck = "";
                                markerCheck = "Type1";
                                // stationAddress.setText(infoWindowModel.getStationAddress());
//                                chargers.setText(mm.getPortQuantity());
                                city.setText("");
                                levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                                levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                            }
                        }*/
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    Log.d("data", " view " + view);
                    return view;
                }
            });


            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng position) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    } else {
                        disableAllViews();

                    }
                }
            });

            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
//                        mMap.animateCamera(CameraUpdateFactory.zoomTo(0.0f));
//                        mBottomLayout.setVisibility(View.INVISIBLE);
                    }

                    cameraChangeInMap();
                   /* LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;


                    if ((mMap.getCameraPosition().zoom) <= 15) {
                        updateClustering();
                        clusterifyMarkers();
                    }

                    if ((mMap.getCameraPosition().zoom) >= 15) {
                        displayAllMarkers();
                    }

                    for (Marker marker1 : markerList) {
                        if (bounds.contains(marker1.getPosition())) {
                            marker1.setVisible(true);
                        } else {
                            marker1.setVisible(false);
                        }
                    }*/
                }
            });

            //            fixed the bug in sprint-7 Ticket_Id-161 end
            //information window code ends
            /*mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }

                    if (!mConnectionDetector.isOnline()) {
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                }
            });*/
        }
    }


    /*private void clusterifyMarkers() {
        if (declusterifiedMarkers != null) {
            for (Marker marker : declusterifiedMarkers) {
                LatLng position = marker.getPosition();
                marker.setPosition(position);
                marker.setClusterGroup(ClusterGroup.DEFAULT);
            }
            declusterifiedMarkers = null;
        }
    }*/

    public class FreeStationsAsyncTask extends AsyncTask<String, String, List<MapModelFilters>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Please wait....");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.show();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected List<MapModelFilters> doInBackground(String... params) {

            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                freeStationRecordsList.clear();
                freeStationRecordsList = dbHelper.getFreeStationRecords();

                Log.d("free station records", " : " + freeStationRecordsList.size());
                return freeStationRecordsList;
            } else {
//                fixed the bug in sprint-7 Ticket_Id-159 starts
//                    progDailog.dismiss();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<MapModelFilters> result) {
            super.onPostExecute(result);

            if (result == null) {
                return;
            }
//            fixed the bug in sprint-7 Ticket_Id-159 starts

//            fixed the bug in sprint-7 Ticket_Id-159 end
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());


            if (freeStationRecordsList != null) {
                Log.d("free station records", "length : " + freeStationRecordsList.size());

                if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                    for (int i = 0; i < freeStationRecordsList.size(); i++) {

                        MapModelFilters mMapModelFreeRecords = new MapModelFilters();
                        mMapModelFreeRecords = freeStationRecordsList.get(i);
                        // MyMarker myMarker = new MyMarker();
                        Log.d("card type ", " : " + mMapModelFreeRecords.getCards_accepted());
                        Log.d("records ", " : " + i);

                        LatLng latlng = new LatLng(mMapModelFreeRecords.getLattitude(), mMapModelFreeRecords.getLongitude());

                        MarkerOptions markerOptions = new MarkerOptions().position(latlng);


                        if (mMapModelFreeRecords.getStatus_code().contains("E")) {
                            if (mMapModelFreeRecords.getEv_connector_types().contains("CHADEMO") ||
                                    mMapModelFreeRecords.getEv_connector_types().contains("J1772COMBO") ||
                                    mMapModelFreeRecords.getEv_connector_types().contains("TESLA")) {
//                            if (dcFastFlag) {
                                markerOptions = new MarkerOptions().position(latlng).
                                        icon(BitmapDescriptorFactory.fromResource(R.drawable.green_fc));
//                                dcSwitchFlag = false;
//                            } else {
//                                dcSwitchFlag = true;
//                            }
                            } /*else {
                                markerOptions = new MarkerOptions().position(latlng).
                                        icon(BitmapDescriptorFactory.fromResource(R.drawable.available_green));
                            }*/
                        } else if (mMapModelFreeRecords.getStatus_code().contains("T") ||
                                mMapModelFreeRecords.getStatus_code().contains("P")) {
                            if (mMapModelFreeRecords.getEv_connector_types().contains("CHADEMO") ||
                                    mMapModelFreeRecords.getEv_connector_types().contains("J1772COMBO") ||
                                    mMapModelFreeRecords.getEv_connector_types().contains("TESLA")) {
//                            if (dcFastFlag) {
                                markerOptions = new MarkerOptions().position(latlng).
                                        icon(BitmapDescriptorFactory.fromResource(R.drawable.gray_fc));
//                                dcSwitchFlag = false;
//                            } else {
//                                dcSwitchFlag = true;
//                            }
                            } else {
                                markerOptions = new MarkerOptions().position(latlng).
                                        icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));
                            }
                        } else {

                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                        }

                        if (!dcSwitchFlag) {
                            marker = mMap.addMarker(markerOptions);
                            markerListFilter.add(marker);
//                            mMap.setMyLocationEnabled(true);
                            hashMapFilter.put(marker.getId(), mMapModelFreeRecords);
                        }
//                        resetMyPositionButton();
                        mMap.getUiSettings().setMapToolbarEnabled(false);
//                    mMap.getUiSettings().setMapToolbarEnabled(true);
//                    resetMyToolBar();

                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                }


                updateClustering();

                mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        disableAllViews();

                        gpsTracker = new GPSTracker(getActivity().getApplicationContext());

                        if (!gpsTracker.canGetLocation) {
                            showSettingsAlert();
//                            mMap.getUiSettings().setScrollGesturesEnabled(false);
//                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
//                        mMap.setMyLocationEnabled(false);
                        }

                        if (!mConnectionDetector.isOnline()) {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                                    Toast.LENGTH_SHORT).show();
                        }
                        return false;
                    }
                });

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        if (marker.isCluster()) {
                            declusterify(marker);
                            return true;
                        } else {
                            clickMarkerViews(marker);
                            return false;
                        }
                    }
                });

                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng position) {
                        if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                        } else {
                            disableAllViews();
                        }
                    }
                });


                mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    View view;

                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {

                        view = null;
                        Log.d("declusterifiedMarkers", "" + declusterifiedMarkers);

                        if (mConnectionDetector.isOnline() && networkCheckerFlag) {

                            displayInfoWindow(marker);
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                        }

                        return view;
                    }
                });

                mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {


                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        cameraChangeInMap();
                    }
                });

            }

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }

        }
    }
    //map search code starts

    public class GetJsonString extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(getActivity());
            progDailog.setMessage("Loading... Please Wait");
//            progDailog.getWindow().setGravity(Gravity.BOTTOM);
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected String doInBackground(String... url1) {

            //public String jsonString(String url1){

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                String jsonurl = Constants.JSON_URL + filterString;
                if (mConnectionDetector.isOnline()) {
                    url = new URL(jsonurl.replaceAll(" ", "%20"));
//                    url = new URL(Constants.DUP_JSON_URL);
                } else {
                    return "network";
                }
                if (mConnectionDetector.isOnline()) {
                    connection = (HttpURLConnection) url.openConnection();
                    //connection.connect();
                } else {
                    return "network";
                }

                if (mConnectionDetector.isOnline()) {
                    connection.connect();
                } else {
                    return "network";
                }

                if (mConnectionDetector.isOnline()) {
                    stream = connection.getInputStream();
                } else {
                    return "network";
                }

                if (mConnectionDetector.isOnline()) {
                    reader = new BufferedReader(new InputStreamReader(stream));
                } else {
                    return "network";
                }

                if (mConnectionDetector.isOnline()) {
                    buffer = new StringBuffer();
                } else {
                    return "network";
                }
                //crash
                while ((line = reader.readLine()) != null) {
                    if (mConnectionDetector.isOnline()) {
                        buffer.append(line);
                        finalJson = buffer.toString();
                    } else {
                        return "network";
                    }
                }

                try {
                    if (mConnectionDetector.isOnline()) {
                        jsonObject = new JSONObject(finalJson);
                        jsonArray = jsonObject.getJSONArray(Constants.JSON_ARRAY_NAME);
                        jsonArrayLength = jsonArray.length();

                        Log.d("Json Array Length", " : " + jsonArrayLength);
                    } else {
                        return "network";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return finalJson;

        }
    }


  /*  public void showAlertBox(final String msg) {

        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }*/

    public class GetJsonTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialogueFilters = new ProgressDialog(getActivity());
            progressDialogueFilters.setMessage("Loading... Please Wait");
//            progDailog.getWindow().setGravity(Gravity.BOTTOM);
            progressDialogueFilters.setIndeterminate(false);
            progressDialogueFilters.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialogueFilters.setCancelable(false);
            progressDialogueFilters.show();
        }

        @Override
        protected String doInBackground(String... params) {

            mapArrayListFilter.clear();
            try {

                if (mConnectionDetector.isOnline()) {
                    factory = new MappingJsonFactory();
                } else {
                    return "network";
                }

                Log.d("Json factory: ", "" + factory);
                if (mConnectionDetector.isOnline()) {
                    if (factory != null) {
                        jsonParser = factory.createParser(finalJson);
                    } else {
                        return "network";
                    }
                    Log.d("Json Parser: ", "" + jsonParser);
                } else {
                    return "network";
                }


                if (mConnectionDetector.isOnline()) {
                    if (jsonParser != null) {
                        token = jsonParser.nextToken();
                    } else {
                        return "network";
                    }
                } else {
                    return "network";
                }

                Log.d("Json token: ", "" + token);
                //* ArrayList objectArray = new ArrayList();*//*

                if (token != JsonToken.START_OBJECT) {
                    if (mConnectionDetector.isOnline()) {
                        System.out.println("Error: root should be object: quiting.");
                    } else {
                        return "network";
                    }
                }
                while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
                    if (mConnectionDetector.isOnline()) {

                        // move from field name to field value
                        if (jsonParser != null) {
                            fieldName = jsonParser.getCurrentName();
                            Log.d("fieldName: ", "" + fieldName);
                            token = jsonParser.nextToken();
                        } else {
                            return "network";
                        }
                        Log.d("token: ", "" + token);

                        if (fieldName != null) {
                            if (fieldName.equals(Constants.JSON_ARRAY_NAME)) {
                                if (token != null) {
                                    if (token == JsonToken.START_ARRAY) {
                                        if (mConnectionDetector.isOnline()) {

                                            // For each of the records in the array
                                            Long startTime = System.currentTimeMillis();
                                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {

                                                if (mConnectionDetector.isOnline()) {
                                                    // read the record into a tree model,
                                                    // this moves the parsing position to the end of it
                                                    JsonNode node = jsonParser.readValueAsTree();
                                                    Log.d("node: ", "" + node);

                                                    MapModelFilters mapModel = new MapModelFilters();

                                                    mapModel.setAccess_days_time(node.get(Constants.ACCESS_DAYS_TIME).textValue());
                                                    mapModel.setCards_accepted(node.get(Constants.CARDS_ACCEPTED).textValue());
                                                    mapModel.setDate_last_confirmed(node.get(Constants.DATE_LAST_CONFIRMED).textValue());
                                                    mapModel.setExpected_date(node.get(Constants.EXPECTED_DATE).textValue());
                                                    mapModel.setFuel_type_code(node.get(Constants.FUEL_TYPE_CODE).textValue());
                                                    mapModel.setId(node.get(Constants.ID).intValue());
                                                    mapModel.setGroups_with_access_code(node.get(Constants.GROUPS_WITH_ACCESS_CODE).textValue());

                                                    mapModel.setOpen_date(node.get(Constants.OPEN_DATE).textValue());
                                                    mapModel.setOwner_type_code(node.get(Constants.OWNER_TYPE_CODE).textValue());
                                                    mapModel.setStatus_code(node.get(Constants.STATUS_CODE).textValue());
                                                    mapModel.setStation_name(node.get(Constants.STATION_NAME).textValue());
                                                    mapModel.setStation_phone(node.get(Constants.STATION_PHONE).textValue());
                                                    mapModel.setUpdated_at(node.get(Constants.UPDATED_AT).textValue());
                                                    mapModel.setGeocode_status(node.get(Constants.GEOCODE_STATUS).textValue());

                                                    mapModel.setLattitude(node.get(Constants.LATITUDE).doubleValue());
                                                    mapModel.setLongitude(node.get(Constants.LONGITUDE).doubleValue());

                                                    /*LatLangAll latLang = new LatLangAll();
                                                    latLang.setLatitude(node.get(Constants.LATITUDE).doubleValue());
                                                    latLang.setLongitude(node.get(Constants.LONGITUDE).doubleValue());
                                                    mapModel.setLatLang(latLang);*/

                                                    mapModel.setCity(node.get(Constants.CITY).textValue());
                                                    mapModel.setIntersection_directions(node.get(Constants.INTERSECTION_DIRECTION).textValue());
                                                    mapModel.setPlus4(node.get(Constants.PLUS4).textValue());
                                                    mapModel.setState(node.get(Constants.STATE).textValue());
                                                    mapModel.setStreet_address(node.get(Constants.STREET_ADDRESS).textValue());
                                                    mapModel.setZip(node.get(Constants.ZIP).textValue());
                                                    mapModel.setBd_blends(node.get(Constants.BD_BLENDS).textValue());
                                                    mapModel.setE85_blender_pump(node.get(Constants.E85_BLENDER_PUMP).textValue());


                                                    List connectorList = new ArrayList();

                                                    for (int count = 0; count < node.get(Constants.EV_CONNECTOR_TYPES).size(); count++) {
                                                        connectorList.add(node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                                    }

                                                    mapModel.setConnectorTypesList(connectorList);

                                                    StringBuffer sb = new StringBuffer();
                                                    for (int count = 0; count < mapModel.getConnectorTypesList().size(); count++) {
                                                        sb.append(mapModel.getConnectorTypesList().get(count) + " \t");
                                                    }


                                                    mapModel.setEv_connector_types(sb.toString());
                                                    mapModel.setEv_dc_fast_num(node.get(Constants.EV_DC_FAST_NUM).intValue());
                                                    mapModel.setEv_level1_evse_num(node.get(Constants.EV_LEVEL1_EVSE_NUM).intValue());
                                                    mapModel.setEv_level2_evse_num(node.get(Constants.EV_LEVEL2_EVSE_NUM).intValue());
                                                    mapModel.setEv_network(node.get(Constants.EV_NETWORK).textValue());
                                                    mapModel.setEv_network_web(node.get(Constants.EV_NETWORK_WEB).textValue());
                                                    mapModel.setEv_other_evse(node.get(Constants.EV_OTHER_EVSE).textValue());
                                                    mapModel.setHy_status_link(node.get(Constants.HY_STATUS_LINK).textValue());
                                                    mapModel.setLpg_primary(node.get(Constants.LPG_PRIMARY).textValue());
                                                    mapModel.setNg_fill_type_code(node.get(Constants.NG_FILL_TYPE_CODE).textValue());
                                                    mapModel.setNg_psi(node.get(Constants.NG_PSI).textValue());
                                                    mapModel.setNg_vehicle_class(node.get(Constants.NG_VEHICLE_CLASS).textValue());


                                                    mapArrayListFilter.add(mapModel);

                                                } else {
                                                    return "network";
                                                }
                                            }

//                                            dbHelper.bulkInsert(mapArrayList);

                                            Long endTime = System.currentTimeMillis();
                                            Log.d("Total Time", ":" + (endTime - startTime));
                                        } else {
                                            return "network";
                                        }
                                    } else {
                                        System.out.println("Error: records should be an array: skipping.");
                                        jsonParser.skipChildren();
                                    }
                                } else {
                                    return "network";
                                }
                            } else {
                                System.out.println("Unprocessed property: " + fieldName);
                                jsonParser.skipChildren();
                            }
                        } else {
                            return "network";
                        }
                    } else {
                        return "network";
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return finalJson;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progDailog.dismiss();
            progressDialogueFilters.dismiss();

            if (result.equals("network") || result.equalsIgnoreCase("null") || result.equals("")) {
                Toast.makeText(getActivity(), R.string.netNotAvl, Toast.LENGTH_SHORT).show();
//                showAlertBox(getResources().getString(R.string.netNotAvl));
            }

//            mapArrayListFilter
            if ((markerList.size() > 0) && (markerListTell.size() > 0)) {
                mMap.clear();
            }
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                // int i;
                for (int i = 0; i < mapArrayListFilter.size(); i++) {
                    MapModelFilters mMapModel = new MapModelFilters();
                    mMapModel = mapArrayListFilter.get(i);
                    // MyMarker myMarker = new MyMarker();
                    LatLng latlng = new LatLng(mMapModel.getLattitude(), mMapModel.getLongitude());

                    MarkerOptions markerOptions = new MarkerOptions().position(latlng);

                    if (mMapModel.getStatus_code().contains("E")) {
                        if (mMapModel.getEv_connector_types().contains("CHADEMO") ||
                                mMapModel.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModel.getEv_connector_types().contains("TESLA")) {
//                            if (dcFastFlag) {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.green_fc));
//                                dcSwitchFlag = false;
//                            } else {
//                                dcSwitchFlag = true;
//                            }
                        } /*else {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.available_green));
                        }*/
                    } else if (mMapModel.getStatus_code().contains("T") ||
                            mMapModel.getStatus_code().contains("P")) {
                        if (mMapModel.getEv_connector_types().contains("CHADEMO") ||
                                mMapModel.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModel.getEv_connector_types().contains("TESLA")) {
//                            if (dcFastFlag) {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray_fc));
//                                dcSwitchFlag = false;
//                            } else {
//                                dcSwitchFlag = true;
//                            }
                        } else {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));
                        }
                    } else {

                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                    }

                    if (!dcSwitchFlag) {
                        marker = mMap.addMarker(markerOptions);
                        markerListFilter.add(marker);
//                        mMap.setMyLocationEnabled(true);
                        hashMapFilter.put(marker.getId(), mMapModel);
                    }
//                    resetMyPositionButton();
                    mMap.getUiSettings().setMapToolbarEnabled(false);
//                    mMap.getUiSettings().setMapToolbarEnabled(true);
//                    resetMyToolBar();

                }
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();


            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    disableAllViews();

                    gpsTracker = new GPSTracker(getActivity().getApplicationContext());

                    if (!gpsTracker.canGetLocation) {
                        showSettingsAlert();
//                        mMap.getUiSettings().setScrollGesturesEnabled(false);
//                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
//                        mMap.setMyLocationEnabled(false);
                    }

                    if (!mConnectionDetector.isOnline()) {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                                Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }
            });

//            fixed the bug in sprint-7 Ticket_Id-161 starts

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    if (marker.isCluster()) {
                        declusterify(marker);
                        return true;
                    } else {
                        clickMarkerViews(marker);
                        return false;
                    }

                   /* if (markerList.contains(marker)) {
                        Log.d("markerList gov ", " success :  " + markerList);
                        viewD.setVisibility(View.GONE);
                        viewC.setVisibility(View.GONE);
                        mViewB.setVisibility(View.VISIBLE);
                    }
                    if (routeMarkerList.contains(marker) || markerPointsList.contains(marker)) {
                        Log.d("routeMarkerList gov", " success :  " + routeMarkerList);
                        viewD.setVisibility(View.VISIBLE);
                        viewC.setVisibility(View.GONE);
                        mViewB.setVisibility(View.GONE);
                    }

                    return false;*/
                }
            });


            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng position) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    } else {
                        disableAllViews();
                    }
                }
            });

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    view = null;
                    Log.d("declusterifiedMarkers", "" + declusterifiedMarkers);

                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {

                        displayInfoWindow(marker);
                        /*for (Map.Entry m : hashMapFilter.entrySet()) {
                            MapModelAll mm = new MapModelAll();
                            mm = (MapModelAll) m.getValue();

                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
//                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());

                                chargerKey = m.getKey().toString();
                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                viewC.setVisibility(View.GONE);
                                viewD.setVisibility(View.GONE);

                                mapListDataFilter = new ArrayList<MapListModel>();

                                MapListModel mapListModel = new MapListModel();

                                mapListModel.setAccess_days_time(mm.getAccess_days_time());
                                mapListModel.setFuel_type_code(mm.getFuel_type_code());
                                mapListModel.setGroups_with_access_code(mm.getGroups_with_access_code());
                                mapListModel.setStation_name(mm.getStation_name());
                                mapListModel.setStation_phone(mm.getStation_phone());
                                mapListModel.setStatus_code(mm.getStatus_code());
                                mapListModel.setCity(mm.getCity());
                                mapListModel.setState(mm.getState());
                                mapListModel.setStreet_address(mm.getStreet_address());
                                mapListModel.setZip(mm.getZip());
                                mapListModel.setBd_blends(mm.getBd_blends());
                                mapListModel.setE85_blender_pump(mm.getE85_blender_pump());
                                markerCheck = "";
                                markerCheck = "Type2";


                                mapListModel.setEv_connector_types(mm.getEv_connector_types());
                                mapListModel.setEv_dc_fast_num(mm.getEv_dc_fast_num());
                                mapListModel.setEv_level1_evse_num(mm.getEv_level1_evse_num());
                                mapListModel.setEv_level2_evse_num(mm.getEv_level2_evse_num());
                                mapListModel.setEv_network(mm.getEv_network());
                                mapListModel.setEv_network_web(mm.getEv_network_web());
                                mapListModel.setEv_other_evse(mm.getEv_other_evse());
                                mapListModel.setHy_status_link(mm.getHy_status_link());
                                mapListModel.setLpg_primary(mm.getLpg_primary());
                                mapListModel.setNg_fill_type_code(mm.getNg_fill_type_code());
                                mapListModel.setNg_psi(mm.getNg_psi());
                                mapListModel.setNg_vehicle_class(mm.getNg_vehicle_class());

                                mapListDataFilter.add(mapListModel);

                                Log.d("MapModelList", ": " + mapListDataFilter.get(0));


                                stationName.setText(mm.getStation_name());
                                //    statusCode.setText(mm.getStatus_code());
                                city.setText(mm.getCity());

                            }
                        }*/
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    return view;
                }
            });
            // information window code end

            /*mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }
                }
            });*/


            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {


                @Override
                public void onCameraChange(CameraPosition cameraPosition) {

                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
//                        mMap.animateCamera(CameraUpdateFactory.zoomTo(0.0f));
//                        mBottomLayout.setVisibility(View.INVISIBLE);
                    }

                    cameraChangeInMap();
                    /*LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

                    if ((mMap.getCameraPosition().zoom) <= 15) {
                        updateClustering();
                        clusterifyMarkers();
                    }

                    if ((mMap.getCameraPosition().zoom) >= 15) {
                        displayAllMarkers();
                    }

                    for (Marker markerT : markerListFilter) {
                        if (bounds.contains(markerT.getPosition())) {
                            markerT.setVisible(true);
                        } else {
                            markerT.setVisible(false);
                        }
                    }*/
                }
            });

//            fixed the bug in sprint-7 Ticket_Id-161 end

        }
    }
// map search code ends

     /*  public class GoogleMapsTaskAllStations extends AsyncTask<String, String, List<MapModelAll>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setCancelable(false);
//            progressDialog.setTitle("Connecting to Server");
//            progressDialog.setMessage("This process can take a few seconds. Please wait....");
                progressDialog.setMessage("Please wait....");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.show();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected List<MapModelAll> doInBackground(String... params) {

          *//*  try {
                responseAll = mJsonUtil.getMethod(OnePocketUrls.GOOGLEMAPS_ALL_STATIONS_URL(),null,null);
                OnePocketLog.d("GOOGLEMAPS_ALL_STATIONS_URL----------", responseAll);

                ArrayList<MapModelAll> mapArrayList = new ArrayList<MapModelAll>();
                hashMapDataAll = new HashMap<String, MapModelAll>();


                JsonFactory factory = new MappingJsonFactory();

                Log.d("Json factory: ", "" + factory);

                JsonParser jsonParser = factory.createParser(responseAll);

                Log.d("Json Parser: ", "" + jsonParser);

                JsonToken token;
                token = jsonParser.nextToken();
                Log.d("Json token: ", "" + token);
               *//**//* ArrayList objectArray = new ArrayList();*//**//*

                if (token != JsonToken.START_OBJECT) {
                    System.out.println("Error: root should be object: quiting.");
                }

                while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
                    String fieldName = jsonParser.getCurrentName();
                    Log.d("fieldName: ", "" + fieldName);
                    // move from field name to field value
                    token = jsonParser.nextToken();
                    Log.d("token: ", "" + token);
                    if(fieldName == null){
                        return null;
                    }
                    if (fieldName.equals(Constants.JSON_ARRAY_NAME)) {
                        if (token == JsonToken.START_ARRAY) {
                            // For each of the records in the array
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                // read the record into a tree model,
                                // this moves the parsing position to the end of it
                                JsonNode node = jsonParser.readValueAsTree();
                                Log.d("node: ", "" + node);

                                MapModelAll mapModel = new MapModelAll();

                                mapModel.setAccess_days_time(node.get(Constants.ACCESS_DAYS_TIME).textValue());
                                mapModel.setCards_accepted(node.get(Constants.CARDS_ACCEPTED).textValue());
                                mapModel.setDate_last_confirmed(node.get(Constants.DATE_LAST_CONFIRMED).textValue());
                                mapModel.setExpected_date(node.get(Constants.EXPECTED_DATE).textValue());
                                mapModel.setFuel_type_code(node.get(Constants.FUEL_TYPE_CODE).textValue());
                                mapModel.setId(node.get(Constants.ID).intValue());
                                mapModel.setGroups_with_access_code(node.get(Constants.GROUPS_WITH_ACCESS_CODE).textValue());

                                mapModel.setOpen_date(node.get(Constants.OPEN_DATE).textValue());
                                mapModel.setOwner_type_code(node.get(Constants.OWNER_TYPE_CODE).textValue());
                                mapModel.setStatus_code(node.get(Constants.STATUS_CODE).textValue());
                                mapModel.setStation_name(node.get(Constants.STATION_NAME).textValue());
                                mapModel.setStation_phone(node.get(Constants.STATION_PHONE).textValue());
                                mapModel.setUpdated_at(node.get(Constants.UPDATED_AT).textValue());
                                mapModel.setGeocode_status(node.get(Constants.GEOCODE_STATUS).textValue());

                                LatLangAll latLang = new LatLangAll();
                                latLang.setLatitude(node.get(Constants.LATITUDE).doubleValue());
                                latLang.setLongitude(node.get(Constants.LONGITUDE).doubleValue());
                                mapModel.setLatLang(latLang);

                                mapModel.setCity(node.get(Constants.CITY).textValue());
                                mapModel.setIntersection_directions(node.get(Constants.INTERSECTION_DIRECTION).textValue());
                                mapModel.setPlus4(node.get(Constants.PLUS4).textValue());
                                mapModel.setState(node.get(Constants.STATE).textValue());
                                mapModel.setStreet_address(node.get(Constants.STREET_ADDRESS).textValue());
                                mapModel.setZip(node.get(Constants.ZIP).textValue());
                                mapModel.setBd_blends(node.get(Constants.BD_BLENDS).textValue());
                                mapModel.setE85_blender_pump(node.get(Constants.E85_BLENDER_PUMP).textValue());


                                //added code starts on 9-3-16
                                List connectorList = new ArrayList();

                                Log.d("Ev Size", " : " + node.get(Constants.EV_CONNECTOR_TYPES).size());
                                for(int count=0;count<node.get(Constants.EV_CONNECTOR_TYPES).size();count++) {
                                    connectorList.add( node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                    Log.d("Ev node type", " : " + node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                }

                                mapModel.setConnectorTypesList(connectorList);
                                //added code ends on 9-3-16

                                mapModel.setEv_connector_types(node.get(Constants.EV_CONNECTOR_TYPES).textValue());
                                mapModel.setEv_dc_fast_num(node.get(Constants.EV_DC_FAST_NUM).intValue());
                                mapModel.setEv_level1_evse_num(node.get(Constants.EV_LEVEL1_EVSE_NUM).intValue());
                                mapModel.setEv_level2_evse_num(node.get(Constants.EV_LEVEL2_EVSE_NUM).intValue());
                                mapModel.setEv_network(node.get(Constants.EV_NETWORK).textValue());
                                mapModel.setEv_network_web(node.get(Constants.EV_NETWORK_WEB).textValue());
                                mapModel.setEv_other_evse(node.get(Constants.EV_OTHER_EVSE).textValue());
                                mapModel.setHy_status_link(node.get(Constants.HY_STATUS_LINK).textValue());
                                mapModel.setLpg_primary(node.get(Constants.LPG_PRIMARY).textValue());
                                mapModel.setNg_fill_type_code(node.get(Constants.NG_FILL_TYPE_CODE).textValue());
                                mapModel.setNg_psi(node.get(Constants.NG_PSI).textValue());
                                mapModel.setNg_vehicle_class(node.get(Constants.NG_VEHICLE_CLASS).textValue());


                                mapArrayList.add(mapModel);
                                //  dbHelper.addStationDetails(mapModel);


                            }
                        } else {
                            System.out.println("Error: records should be an array: skipping.");
                            jsonParser.skipChildren();
                        }
                    } else {
                        System.out.println("Unprocessed property: " + fieldName);
                        jsonParser.skipChildren();
                    }
                }

                return mapArrayList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;*//*
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
//                ArrayList<MapModelAll> dbList = dbHelper.getAllStationRecords();
                dbList = dbHelper.getAllStationRecords();
                Log.d("mapModel list size", ":" + dbList.size());
               *//* if (filterEnable) {
                    filterEnable = false;
                    if (markerList.size() > 0) {
                        markerList.clear();
                    }
                    return dbFilterList;
                } else {
                    return dbList;
                }*//*
                return dbList;

            } else {
//                fixed the bug in sprint-7 Ticket_Id-159 starts
//                    progDailog.dismiss();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                });
//                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
               *//* if(!alertDialog.isShowing()) {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }*//*
//                fixed the bug in sprint-7 Ticket_Id-159 end
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<MapModelAll> result) {
            super.onPostExecute(result);

            if (result == null) {
                return;
            }
//            fixed the bug in sprint-7 Ticket_Id-159 starts
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
//            fixed the bug in sprint-7 Ticket_Id-159 end
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());

            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                // int i;
                for (int i = 0; i < result.size(); i++) {
                    MapModelAll mMapModel = new MapModelAll();
                    mMapModel = result.get(i);
                    // MyMarker myMarker = new MyMarker();
                    LatLng latlng = new LatLng(mMapModel.getLattitude(), mMapModel.getLongitude());

                    MarkerOptions markerOptions = new MarkerOptions().position(latlng);

                    if (mMapModel.getStatus_code().contains("E")) {
                        if (mMapModel.getEv_connector_types().contains("CHADEMO") ||
                                mMapModel.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModel.getEv_connector_types().contains("TESLA")) {
//                            if (dcFastFlag) {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.green_fc));
//                                dcSwitchFlag = false;
//                            } else {
//                                dcSwitchFlag = true;
//                            }
                        } else {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.available_green));
                        }
                    } else if (mMapModel.getStatus_code().contains("T") ||
                            mMapModel.getStatus_code().contains("P")) {
                        if (mMapModel.getEv_connector_types().contains("CHADEMO") ||
                                mMapModel.getEv_connector_types().contains("J1772COMBO") ||
                                mMapModel.getEv_connector_types().contains("TESLA")) {
//                            if (dcFastFlag) {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray_fc));
//                                dcSwitchFlag = false;
//                            } else {
//                                dcSwitchFlag = true;
//                            }
                        } else {
                            markerOptions = new MarkerOptions().position(latlng).
                                    icon(BitmapDescriptorFactory.fromResource(R.drawable.gray));
                        }
                    } else {

                        markerOptions = new MarkerOptions().position(latlng).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.blue));
                    }

                    if (!dcSwitchFlag) {
                        marker = mMap.addMarker(markerOptions);
                        markerList.add(marker);
                        hashMapDataAll.put(marker.getId(), mMapModel);
                    }
//                    mMap.setMyLocationEnabled(true);
//                    resetMyPositionButton();

                    mMap.getUiSettings().setMapToolbarEnabled(false);
//                    mMap.getUiSettings().setMapToolbarEnabled(true);
//                    resetMyToolBar();
                }
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();

            mViewB.setVisibility(View.GONE);


            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    disableAllViews();

                    gpsTracker = new GPSTracker(getActivity().getApplicationContext());

                    if (!gpsTracker.canGetLocation) {
                        showSettingsAlert();
//                        mMap.getUiSettings().setScrollGesturesEnabled(false);
//                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
//                        mMap.setMyLocationEnabled(false);
                    }

                    if (!mConnectionDetector.isOnline()) {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                                Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }
            });

//            fixed the bug in sprint-7 Ticket_Id-161 starts
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
//                    resetMyToolBar();
                    if (marker.isCluster()) {
                        declusterify(marker);
                        return true;
                    } else {
                        clickMarkerViews(marker);
                        return false;
                    }

                   *//* if (markerList.contains(marker)) {
                        Log.d("markerList ", " success :  " + markerList);
                        viewD.setVisibility(View.GONE);
                        viewC.setVisibility(View.GONE);
                        mViewB.setVisibility(View.VISIBLE);
                    }

                    mViewB.setVisibility(View.VISIBLE);

                    if (routeMarkerList.contains(marker) || markerPointsList.contains(marker)) {
                        Log.d("routeMarkerList ", " success :  " + routeMarkerList);
                        viewD.setVisibility(View.VISIBLE);
                        viewC.setVisibility(View.GONE);
                        mViewB.setVisibility(View.GONE);
                    }

                    return false;*//*
                }
            });

//            progressDialog.dismiss();
            // Log.d("Total Markers ", ":" + i);

            // information window code starts

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    view = null;
                    Log.d("declusterifiedMarkers", "" + declusterifiedMarkers);

                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {

                        displayInfoWindow(marker);
                       *//* for (Map.Entry m : hashMapDataAll.entrySet()) {
                            MapModelAll mm = new MapModelAll();
                            mm = (MapModelAll) m.getValue();

                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
//                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());

                                chargerKey = m.getKey().toString();
                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                viewC.setVisibility(View.GONE);
                                viewD.setVisibility(View.GONE);

                                mapListData = new ArrayList<MapListModel>();

                                MapListModel mapListModel = new MapListModel();

                                mapListModel.setAccess_days_time(mm.getAccess_days_time());
                                mapListModel.setFuel_type_code(mm.getFuel_type_code());
                                mapListModel.setGroups_with_access_code(mm.getGroups_with_access_code());
                                mapListModel.setStation_name(mm.getStation_name());
                                mapListModel.setStation_phone(mm.getStation_phone());
                                mapListModel.setStatus_code(mm.getStatus_code());
                                mapListModel.setCity(mm.getCity());
                                mapListModel.setState(mm.getState());
                                mapListModel.setStreet_address(mm.getStreet_address());
                                mapListModel.setZip(mm.getZip());
                                mapListModel.setBd_blends(mm.getBd_blends());
                                mapListModel.setE85_blender_pump(mm.getE85_blender_pump());
                                markerCheck = "";
                                markerCheck = "Type2";


                                Log.d("cL at after", ": " + mapListModel.getConnectorTypesList());
                                mapListModel.setEv_connector_types(mm.getEv_connector_types());
                                // mapListModel.setEv_connector_types(mm.getEv_connector_types());
                                mapListModel.setEv_dc_fast_num(mm.getEv_dc_fast_num());
                                mapListModel.setEv_level1_evse_num(mm.getEv_level1_evse_num());
                                mapListModel.setEv_level2_evse_num(mm.getEv_level2_evse_num());
                                mapListModel.setEv_network(mm.getEv_network());
                                mapListModel.setEv_network_web(mm.getEv_network_web());
                                mapListModel.setEv_other_evse(mm.getEv_other_evse());
                                mapListModel.setHy_status_link(mm.getHy_status_link());
                                mapListModel.setLpg_primary(mm.getLpg_primary());
                                mapListModel.setNg_fill_type_code(mm.getNg_fill_type_code());
                                mapListModel.setNg_psi(mm.getNg_psi());
                                mapListModel.setNg_vehicle_class(mm.getNg_vehicle_class());

                                mapListData.add(mapListModel);

                                Log.d("MapModelList", ": " + mapListData.get(0));


                                stationName.setText(mm.getStation_name());
                                //    statusCode.setText(mm.getStatus_code());
                                city.setText(mm.getCity());

                            }
                        }*//*
                        //
                        *//*for (Map.Entry m : hashMapData.entrySet()) {
                            MapModel mm = new MapModel();
                            mm = (MapModel) m.getValue();


                          *//**//*  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.clear();
                            editor1.putString(CHARGER_ID, m.getKey().toString());
                            editor1.commit();*//**//*
                        *//**//*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*//**//*
                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
                                chargerKey = m.getKey().toString();
//                                mBottomLayout.setVisibility(View.VISIBLE);

                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
//                                String temp = initStationName(mm.getLatLang());

                                stationName.setText(mm.getStation());
                                // stationAddress.setText(infoWindowModel.getStationAddress());
//                                chargers.setText(mm.getPortQuantity());
                                city.setText("null");
                                levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                                levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                            }
                        }*//*
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    return view;
                }
            });
            // information window code end

         *//*   mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }
                }
            });*//*

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng position) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    } else {
                        disableAllViews();
                    }
                }
            });

            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {


                @Override
                public void onCameraChange(CameraPosition cameraPosition) {


                    cameraChangeInMap();
                    *//*LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

                   *//**//* Log.d("LatlngbOunds",":"+bounds);
                    Log.d("markerList",":"+markerList);
                    Log.d("markerList size",":"+markerList.size());*//**//*


                    if ((mMap.getCameraPosition().zoom) <= 15) {
                        updateClustering();
                        clusterifyMarkers();
                    }

                    if ((mMap.getCameraPosition().zoom) >= 15) {
                        displayAllMarkers();
                    }

                    for (Marker marker1 : markerList) {
                        if (bounds.contains(marker1.getPosition())) {
                            marker1.setVisible(true);
                        } else {
                            marker1.setVisible(false);
                        }
                    }
                    for (Marker markerT : markerListTell) {
                        if (bounds.contains(markerT.getPosition())) {
                            markerT.setVisible(true);
                        } else {
                            markerT.setVisible(false);
                        }
                    }*//*
                }
            });

//            fixed the bug in sprint-7 Ticket_Id-161 end
        }
    }*/
}




