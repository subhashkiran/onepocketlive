package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.subhashe.onepocket.Activities.MoreInfoActivity;
import com.subhashe.onepocket.Adapters.ChargeRecyclerViewAdapter;
import com.subhashe.onepocket.Adapters.RecyclerViewAdapter;
import com.subhashe.onepocket.Core.DayAxisValueFormatter;
import com.subhashe.onepocket.Core.DayAxisValueFormatterWeekly;
import com.subhashe.onepocket.Core.MyAxisValueFormatter;
import com.subhashe.onepocket.Core.MyAxisValueFormatterDay;
import com.subhashe.onepocket.Core.StatGraphModel;
import com.subhashe.onepocket.Core.StatModel;
import com.subhashe.onepocket.Core.XYMarkerView;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by SubhashE on 3/2/2016.
 */
public class ChargeActivityFragment extends Fragment implements OnChartValueSelectedListener {
    private View rootView;
    private ConnectionDetector mConnectionDetector;
    protected BarChart mChart;
    private IAxisValueFormatter xAxisFormatter ,xAxisFormatterWeekly;
    private XAxis xAxis;
    private boolean monthlyFlag,spentFlag,kWhFlag;
    private TextView mMoreinfoCharge, mWeekylText, mMonthlyText,mSpent,mSpentRect,mkwh,mkwhRect;
    protected Typeface mTfLight;
    private LinearLayout chargeLayoutEmpty, textInfo, mWeekly, mMonthly,mSpentLabel,mkWhLabel;
    private ChargeRecyclerViewAdapter mMyAdapter;
    private ProgressDialog progress;
    private JsonUtil mJsonUtil;
    private String response, response1,response2, token;
    private int user_id;
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    ArrayList<StatModel> statList = new ArrayList<StatModel>();
    ArrayList<StatGraphModel> statGraphList = new ArrayList<StatGraphModel>();
    ArrayList<StatGraphModel> statGraphListMonthly = new ArrayList<StatGraphModel>();
    RecyclerView mRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

   /* @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.chargeactivity, container, false);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        final ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("My Charging Activity");
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
        }

//        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mConnectionDetector = new ConnectionDetector(getActivity());
        mMoreinfoCharge = (TextView) rootView.findViewById(R.id.MoreinfoCharge);

        mSpent = (TextView) rootView.findViewById(R.id.spent);
        mSpentRect = (TextView) rootView.findViewById(R.id.spentRect);
        mkwh = (TextView) rootView.findViewById(R.id.kwh);
        mkwhRect = (TextView) rootView.findViewById(R.id.kwhRect);

        chargeLayoutEmpty = (LinearLayout) rootView.findViewById(R.id.chargingInfoEmpty);
        mSpentLabel = (LinearLayout) rootView.findViewById(R.id.spentLabel);
        mkWhLabel = (LinearLayout) rootView.findViewById(R.id.kWhLabel);
        textInfo = (LinearLayout) rootView.findViewById(R.id.textInfo);
        mWeekly = (LinearLayout) rootView.findViewById(R.id.weeklyText);
        mMonthly = (LinearLayout) rootView.findViewById(R.id.monthlyText);
        mWeekylText = (TextView) rootView.findViewById(R.id.chargeweekly);
        mMonthlyText = (TextView) rootView.findViewById(R.id.chargeMonthly);


        mSpentLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpent.setTextColor(getResources().getColor(R.color.kwh));
                mSpentRect.setBackground(getResources().getDrawable(R.drawable.button_kwh));

               /* mkwh.setTextColor(getResources().getColor(R.color.gray));
                mkwhRect.setBackground(getResources().getDrawable(R.drawable.button_gray));*/
                spentFlag = true;
                kWhFlag = false;
                mChart.clear();
                if(monthlyFlag){
                    setDataSpent(3,100);
                }else {
                    setDataSpent(6, 100);
                }
            }
        });

        mkWhLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mkwh.setTextColor(getResources().getColor(R.color.kwspent));
                mkwhRect.setBackground(getResources().getDrawable(R.drawable.button_spent));

               /* mSpent.setTextColor(getResources().getColor(R.color.gray));
                mSpentRect.setBackground(getResources().getDrawable(R.drawable.button_gray));*/

                kWhFlag = true;
                spentFlag = false;
                mChart.clear();
                if(monthlyFlag){
                    setData(3,100);
                }else {
                    setData(6, 100);
                }
            }
        });

        mWeekylText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWeekly.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                mWeekylText.setTextColor(getResources().getColor(R.color.white));
                mMonthly.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                mMonthlyText.setTextColor(getResources().getColor(R.color.appColor));

                mkwh.setTextColor(getResources().getColor(R.color.kwspent));
                mkwhRect.setBackground(getResources().getDrawable(R.drawable.button_spent));

                /*mSpent.setTextColor(getResources().getColor(R.color.gray));
                mSpentRect.setBackground(getResources().getDrawable(R.drawable.button_gray));*/

                xAxis.setValueFormatter(xAxisFormatter);
                monthlyFlag = false;
                if (mConnectionDetector.isOnline()) {
                    new ChargeActivityList().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                    new ChargeActivityWeeklyList().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });

        mMonthlyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWeekly.setBackground(getResources().getDrawable(R.drawable.frgt_layout_unselected));
                mWeekylText.setTextColor(getResources().getColor(R.color.appColor));
                mMonthly.setBackground(getResources().getDrawable(R.drawable.frgt_layout_selected));
                mMonthlyText.setTextColor(getResources().getColor(R.color.white));

                mkwh.setTextColor(getResources().getColor(R.color.kwspent));
                mkwhRect.setBackground(getResources().getDrawable(R.drawable.button_spent));

               /* mSpent.setTextColor(getResources().getColor(R.color.gray));
                mSpentRect.setBackground(getResources().getDrawable(R.drawable.button_gray));*/

                xAxis.setValueFormatter(xAxisFormatterWeekly);

                monthlyFlag = true;
                if (mConnectionDetector.isOnline()) {
                    new ChargeActivityMonthlyList().execute();
                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }

            }
        });
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(ACC_USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

        if (mConnectionDetector.isOnline()) {
            new ChargeActivityList().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            new ChargeActivityWeeklyList().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewCharge);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);

        textInfo.setVisibility(View.INVISIBLE);
        mChart = (BarChart) rootView.findViewById(R.id.chart1);
//        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        //mChart.setPinchZoom(false);

        mChart.setTouchEnabled(false);

        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);

        xAxisFormatter = new DayAxisValueFormatter(mChart);
        xAxisFormatterWeekly = new DayAxisValueFormatterWeekly(mChart);

        IAxisValueFormatter customX = new MyAxisValueFormatter();
        xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(null);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setTypeface(mTfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(null);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.NONE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
        // l.setExtra(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });
        // l.setCustom(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });

       /* XYMarkerView mv = new XYMarkerView(getActivity(), xAxisFormatter);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart*/

//        setData(6, 100);

       /* mMoreinfoCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MoreInfoActivity.class);
                startActivity(intent);
            }
        });
*/
        return rootView;
    }


    public class ChargeActivityList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.STATS_DRIVER() + user_id;
//                response = mJsonUtil.postMethod(mUrl, null, null, null, null,null,null,null);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);


            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            hideProgress();
//            ArrayList<StatModel> statList = new ArrayList<StatModel>();
            /* if(response.contains("") || response.contains("[]")){
                 showAlertBox(getString(R.string.servernotresponce));
             }
             else*/
            if (response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response.contains("Already Requested GridKey") || response.contains("not yet processed")) {
                showAlertBox(getString(R.string.gridkeyalready));
            } else if (response.contains("have already requested") || response.contains("maximum gridkey")) {
                showAlertBox(getString(R.string.gridkeymax));
            } else {
                try {
                    JSONArray temp = new JSONArray(response);
                    Log.d("~~~~~~~~~~~~~", "m");
                    Log.d("MainActivity.java", "temp : " + temp);

                        if (temp.length()==0){

                            chargeLayoutEmpty.setVisibility(View.VISIBLE);
                        }
                        else if (temp != null) {

                            for (int i = 0; i < temp.length(); i++) {
                                // for(int i=0;i<5;i++){
                                JSONObject e = temp.getJSONObject(i);

                                StatModel statModel = new StatModel();
                                Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                                //JSONObject data = e.getJSONObject("sessionId");
                                //(String) data.getString(CONN2_PORT_TYPE)
                                statModel.setStatLocalId(i + 1);
                                statModel.setSessionId(e.getString("sessionId"));
                                statModel.setKwUsed(e.getDouble("kWUsed"));
                                statModel.setTimeUsedPerHr(e.getDouble("timeUsedPerHr"));
                                statModel.setChargeEnd(e.getString("chargeEnd"));
                                statModel.setRateKWPerTime(e.getString("rateKWPerTime"));
                                statModel.setSpent(e.getDouble("spent"));
                                statModel.setDate(e.getString("date"));
                                statModel.setPortId(e.getString("portId"));
                                statModel.setChargeStart(e.getString("chargeStart"));
                                statModel.setStationName(e.getString("stationName"));
                                statModel.setStationAddress(e.getString("stationAddress"));

                                statList.add(statModel);
                            }
                        }
                } catch (Exception e) {

                }
            }
            mMyAdapter = new ChargeRecyclerViewAdapter(getActivity(), statList);
            mRecyclerView.setAdapter(mMyAdapter);
        }
    }


    public class ChargeActivityWeeklyList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                String mUrl = OnePocketUrls.STATS_WEEKLY_DRIVER() + user_id;
//                response = mJsonUtil.postMethod(mUrl, null, null, null, null,null,null,null);
                response1 = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("chargingactivityweekly", response1);


            } catch (Exception e) {
                OnePocketLog.d("chargingactivityweeklyerror----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
//            ArrayList<StatModel> statList = new ArrayList<StatModel>();
            /* if(response.contains("") || response.contains("[]")){
                 showAlertBox(getString(R.string.servernotresponce));
             }
             else*/
            if (response1 == "[]") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    JSONArray temp = new JSONArray(response1);
                    Log.d("~~~~~~~~~~~~~", "m");
                    Log.d("MainActivity.java", "temp : " + temp);
                    statGraphList.clear();
                    for (int i = 0; i < temp.length(); i++) {
                        // for(int i=0;i<5;i++){
                        JSONObject e = temp.getJSONObject(i);

                        StatGraphModel mStatGraphModel = new StatGraphModel();
                        Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                        //JSONObject data = e.getJSONObject("sessionId");
                        //(String) data.getString(CONN2_PORT_TYPE)

                        mStatGraphModel.setKwUsed(e.getDouble("kWUsed"));
                        mStatGraphModel.setSpent(e.getDouble("spent"));
                        mStatGraphModel.setDate(e.getString("date"));

                        statGraphList.add(mStatGraphModel);
                    }
                } catch (Exception e) {

                }
                mChart.clear();
                setData(6, 100);
            }
        }
    }

    public class ChargeActivityMonthlyList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                String mUrl = OnePocketUrls.STATS_MONTHLY_DRIVER() + user_id;
//                response = mJsonUtil.postMethod(mUrl, null, null, null, null,null,null,null);
                response2 = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("chargingactvitymonthly", response2);


            } catch (Exception e) {
                OnePocketLog.d("chargingactvitymonthlyerror----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
//            ArrayList<StatModel> statList = new ArrayList<StatModel>();
            /* if(response.contains("") || response.contains("[]")){
                 showAlertBox(getString(R.string.servernotresponce));
             }
             else*/
            if (response2 == "[]") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    JSONArray temp = new JSONArray(response2);
                    Log.d("~~~~~~~~~~~~~", "m");
                    Log.d("MainActivity.java", "temp : " + temp);
                    statGraphListMonthly.clear();
                    for (int i = 0; i < temp.length(); i++) {
                        // for(int i=0;i<5;i++){
                        JSONObject e = temp.getJSONObject(i);

                        StatGraphModel mStatGraphModel = new StatGraphModel();
                        Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                        //JSONObject data = e.getJSONObject("sessionId");
                        //(String) data.getString(CONN2_PORT_TYPE)

                        mStatGraphModel.setKwUsed(e.getDouble("kWUsed"));
                        mStatGraphModel.setSpent(e.getDouble("spent"));
                        mStatGraphModel.setDate(e.getString("date"));

                        statGraphListMonthly.add(mStatGraphModel);
                    }
                } catch (Exception e) {

                }
                mChart.clear();
                setData(3, 100);
            }
        }
    }


    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public String convertToDate(String value) {
        String temp = "";
        String sta = "Jan 01 2017";
        long days = 0;

        SimpleDateFormat format1 = new SimpleDateFormat("MMM dd yyyy");
        try {
            Date dt1 = format1.parse(value);
            Date dt2 = format1.parse(sta);

            DateFormat format2 = new SimpleDateFormat("dd");
//            temp = format2.format(dt1);
            days = (dt1.getTime() - dt2.getTime())/(24 *60*60*1000);
        }catch (Exception e){}
        return  String.valueOf(days + 1);
    }

    private void setData(int count, float range) {

        float start = 1f;

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        ArrayList<BarEntry> yVals1Monthly = new ArrayList<BarEntry>();

        /*for (int i = (int) start; i < start + count + 1; i++) {
            float mult = (range + 1);
            float val = (float) (Math.random() * mult);
            yVals1.add(new BarEntry(i, val));
        }*/

        if(!monthlyFlag) {
            int value = statGraphList.size();
            int j = 0;
            String finalDay="";
            for (int i = (int) start; i < value + 1; i++) {
                j = i - 1;
                finalDay = convertToDate(statGraphList.get(j).getDate().toString());
               /* String input_date = statGraphList.get(j).getDate().toString();
                SimpleDateFormat format1=new SimpleDateFormat("MMM dd yyyy");
                try {
                    Date dt1 = format1.parse(input_date);
                    DateFormat format2=new SimpleDateFormat("dd");
                    finalDay=format2.format(dt1);
                }catch (Exception e){}*/
                yVals1.add(new BarEntry(Float.parseFloat(finalDay), statGraphList.get(j).getKwUsed().floatValue()));
            }
        }else{
            int value = statGraphListMonthly.size();
            int j = 0;
            for (int i = (int) start; i < value + 1; i++) {
                j = i - 1;
                yVals1Monthly.add(new BarEntry(i, statGraphListMonthly.get(j).getKwUsed().floatValue()));
            }
        }

        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            if(!monthlyFlag) {
                set1.setValues(yVals1);
            }else{
                set1.setValues(yVals1Monthly);
            }
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            if(!monthlyFlag) {
                set1 = new BarDataSet(yVals1, " ");
            }else{
                set1 = new BarDataSet(yVals1Monthly, " ");
            }
            set1.setColors(ColorTemplate.MATERIAL_KWH);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTypeface(mTfLight);
            data.setBarWidth(0.9f);
            mChart.setData(data);
        }
    }

    private void setDataSpent(int count, float range) {

        float start = 1f;

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        ArrayList<BarEntry> yVals1Monthly = new ArrayList<BarEntry>();

        /*for (int i = (int) start; i < start + count + 1; i++) {
            float mult = (range + 1);
            float val = (float) (Math.random() * mult);
            yVals1.add(new BarEntry(i, val));
        }*/

        if(!monthlyFlag) {
            int value = statGraphList.size();
            int j = 0;
            String finalDay="";
            for (int i = (int) start; i < value + 1; i++) {
                j = i - 1;
                finalDay = convertToDate(statGraphList.get(j).getDate().toString());
                yVals1.add(new BarEntry(Float.parseFloat(finalDay), statGraphList.get(j).getSpent().floatValue()));
            }
        }else{
            int value = statGraphListMonthly.size();
            int j = 0;
            for (int i = (int) start; i < value + 1; i++) {
                j = i - 1;
                yVals1Monthly.add(new BarEntry(i, statGraphListMonthly.get(j).getSpent().floatValue()));
            }
        }

        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            if(!monthlyFlag) {
                set1.setValues(yVals1);
            }else{
                set1.setValues(yVals1Monthly);
            }
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            if(!monthlyFlag) {
                set1 = new BarDataSet(yVals1, " ");
            }else{
                set1 = new BarDataSet(yVals1Monthly, " ");
            }
            set1.setColors(ColorTemplate.MATERIAL_SPENT);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTypeface(mTfLight);
            data.setBarWidth(0.9f);
            mChart.setData(data);
        }

    }

    protected RectF mOnValueSelectedRectF = new RectF();

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;

        RectF bounds = mOnValueSelectedRectF;
        mChart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = mChart.getPosition(e, YAxis.AxisDependency.LEFT);

        Log.i("bounds", bounds.toString());
        Log.i("position", position.toString());

        Log.i("x-index",
                "low: " + mChart.getLowestVisibleX() + ", high: "
                        + mChart.getHighestVisibleX());

        MPPointF.recycleInstance(position);
    }

    @Override
    public void onNothingSelected() {

    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();*/
    }
}
