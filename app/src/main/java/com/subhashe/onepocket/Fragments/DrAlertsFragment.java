package com.subhashe.onepocket.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.subhashe.onepocket.Activities.DRAlertsActivity;
import com.subhashe.onepocket.Adapters.RecyclerViewAdapter;
import com.subhashe.onepocket.Core.FavModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by SubhashE on 3/2/2016.
 */
public class DrAlertsFragment extends Fragment {


    private View rootView;
    private LinearLayout drAlertNotification;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Toast.makeText(getActivity(), "Contact Us Screen", Toast.LENGTH_SHORT).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_notificatons, container, false);

        drAlertNotification = (LinearLayout)rootView.findViewById(R.id.drAlertNotifications);


//        toolbar= (Toolbar)rootView. findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

       /* mMyAdapter = new RecyclerViewAdapter(getActivity(),mFavList);
        mRecyclerView.setAdapter(mMyAdapter);*/
        final ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Notifications");
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
        }

        drAlertNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DRAlertsActivity.class);
                getActivity().startActivity(intent);
            }
        });
        return rootView;
    }

}
