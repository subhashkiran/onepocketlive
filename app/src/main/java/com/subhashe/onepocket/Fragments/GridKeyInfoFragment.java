package com.subhashe.onepocket.Fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.subhashe.onepocket.Core.GridKeyModel;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by root on 28/1/16.
 */
public class GridKeyInfoFragment extends Fragment {

    Point p1, p2;
    Button butGetGKCard, butGK1;
    NumberPicker nopicker;
    int pickerValue;
    View view;
    View layout;
    Button close;
    private String response,gridkeyHeader,gKDeactiveHeader,token;
    private int user_id,accBalence;
    private String user_name,rfidText;
    private ConnectionDetector mConnectionDetector;
    private ProgressDialog progress;
    private JsonUtil mJsonUtil;
    int id = 1;
    TableLayout tableLayout;
//    TableRow tableRow;
    TextView cardNumber, status,getCardNumber;
    ArrayList<GridKeyModel> gridKeyListTemp = new ArrayList<GridKeyModel>();
    int charObjId = 0;
    Button requestBut;
    View horizontalbar;
    PopupWindow popup;
    private SharedPreferences prefs;
    //    String mURL = "http://192.168.168.141:8080/gridbot/driver/gkey/active/";
    public static final String ACC_USER_ID = "ACC_USER_ID";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USERNAME_ID = "USERNAME_ID_PREFS_String";
    public static final String ACC_BALENCE = "ACC_BALENCE";
//    ArrayList<GridKeyModel> gridKeyList = new ArrayList<GridKeyModel>();
    public GridKeyInfoFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.gridkey_view, container, false);

        tableLayout = (TableLayout) view.findViewById(R.id.TableGridkeyTable);
        tableLayout.setColumnStretchable(0, true);
        tableLayout.setColumnStretchable(1, true);
        tableLayout.setColumnStretchable(2, true);
        mConnectionDetector = new ConnectionDetector(getActivity());
        butGetGKCard = (Button) view.findViewById(R.id.buttonGetGKCard);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_id = prefs.getInt(ACC_USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");
//        SharedPreferences.Editor editor1 = prefs.edit();
        user_name = prefs.getString(USERNAME_ID, "NOT FOUND");


        if (mConnectionDetector.isOnline()) {
            new GridKeyInfo().execute();
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }


        butGetGKCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Toast.makeText(getContext(),"Get GridKey Card Clicked",Toast.LENGTH_SHORT).show();
                int[] location = new int[2];
                butGetGKCard.getLocationOnScreen(location);

                //Initialize the Point with x, and y positions
                p1 = new Point();
                p1.x = location[0];
                p1.y = location[1];
                //Open popup window
                if (p1 != null)
                    showPopup(getActivity(), p1);
            }
        });

        // butGK1 = (Button) view.findViewById(R.id.buttonGK1);
      /*  butGK1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                int[] location = new int[2];
                butGK1.getLocationOnScreen(location);

                //Initialize the Point with x, and y positions
                p2 = new Point();
                p2.x = location[0];
                p2.y = location[1];
                //Open popup window
                if (p2 != null)
                   // showPopup1(getActivity(), p2);
            }
        });*/

        return view;
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        gridKeyList.clear();
//        gridKeyList = null;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void showPopup(final Activity context, Point p1) {
        int popupWidth = 700;
        int popupHeight = 800;
        Button close, submit;
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.GetGridCardLayout);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.get_gridcard_view, viewGroup);

        submit = (Button) layout.findViewById(R.id.btnGetGkSubmit);
        close = (Button) layout.findViewById(R.id.btnGetGKCancel);

        // Creating the PopupWindow
      /*  final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);*/

        final PopupWindow popup = new PopupWindow(layout, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT, true);

        nopicker = (NumberPicker) layout.findViewById(R.id.GKnumberPicker);
        nopicker.setMaxValue(4);
        nopicker.setMinValue(1);
        nopicker.setWrapSelectorWheel(true);
        nopicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                pickerValue = nopicker.getValue();
            }
        });
        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "Submit Clicked", Toast.LENGTH_SHORT).show();
                pickerValue = nopicker.getValue();
                if(pickerValue == 0)
                {
                    pickerValue = 1;
                }
                gridkeyHeader = "{"+"\""+"noofcards"+"\""+":"+pickerValue+","+"\""+"username"+"\""+":"+"\""+user_name+"\"" +"}";

//                fixed the bug in sprint-7 Ticket_Id-114 stars
                if (mConnectionDetector.isOnline()) {
                    popup.dismiss();
                    accBalence = prefs.getInt(ACC_BALENCE, 0);
                    if(accBalence >= (pickerValue * 5)) {
                        new GridKeyRequest().execute();
                    }else{
                        showAlertBox(getResources().getString(R.string.requestFailed));
                    }
                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }

            }
        });
//        fixed the bug in sprint-7 Ticket_Id-114 end

        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    public class GridKeyRequest extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.ACCOUNT_GRIDKEY_NEW();
                response = mJsonUtil.postMethod(mUrl, null, null, null,null,null,null,null,gridkeyHeader,token,null,null,null);
//                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);


            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if(response.contains("Already Requested GridKey") || response.contains("not yet processed")){
                showAlertBox(getString(R.string.gridkeyalready));
            }else if(response.contains("have already requested") || response.contains("maximum gridkey")){
                showAlertBox(getString(R.string.gridkeymax));
            }else{
                showAlertBox(getString(R.string.gridkeysuccess));
//                new GridKeyInfo().execute();
            }
        }
    }

    public class GridKeyInfo extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.ACCOUNT_GRIDKEY() + user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);


            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            ArrayList<GridKeyModel> gridKeyList = new ArrayList<GridKeyModel>();
            try {
                JSONArray temp = new JSONArray(response);
                Log.d("MainActivity.java", "temp : " + temp);

                for (int i = 0; i < temp.length(); i++) {

                    GridKeyModel gridKeyModel = new GridKeyModel();

                    JSONObject jsonObject = temp.getJSONObject(i);
                    Log.d("GridKeyInfo", "Json Object Value " + jsonObject);

                    gridKeyModel.setRfId(jsonObject.getString("rfid"));
                    gridKeyModel.setPhone(jsonObject.getString("phone"));
                    gridKeyModel.setChargingObjectId(jsonObject.getInt("chargingObjectId"));

                    gridKeyList.add(gridKeyModel);
                    gridKeyListTemp.add(gridKeyModel);
                }

                for (int k = 0; k < gridKeyList.size(); k++) {
                    GridKeyModel gridKeyModel = new GridKeyModel();

                    gridKeyModel = gridKeyList.get(k);

                    final TableRow tableRow = new TableRow(getActivity());
                    horizontalbar = new View(getActivity());

                    cardNumber = new TextView(getActivity());
                    status = new TextView(getActivity());
                    requestBut = new Button(getActivity());

                    cardNumber.setText(gridKeyModel.getRfId());
                    status.setText("Active");
                    requestBut.setText("DEACTIVATE");

                    horizontalbar.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
                    horizontalbar.setBackgroundColor(Color.rgb(51, 51, 51));

                    tableRow.addView(cardNumber);
                    tableRow.addView(status);
                    tableRow.addView(requestBut);

                    tableLayout.addView(tableRow);
                    tableLayout.addView(horizontalbar);

                    requestBut.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int[] location = new int[2];
                            requestBut.getLocationOnScreen(location);
                            getCardNumber = (TextView)tableRow.getChildAt(0);
                            rfidText = getCardNumber.getText().toString();
                            for (int k = 0; k < gridKeyListTemp.size(); k++) {
                                if(gridKeyListTemp.get(k).getRfId().equals(rfidText)){
                                    charObjId = gridKeyListTemp.get(k).getChargingObjectId();
                                }
                            }
                            //Initialize the Point with x, and y positions
                            p2 = new Point();
                            p2.x = location[0];
                            p2.y = location[1];
                            //Open popup window
                            if (p2 != null)
                                showPopup1(getActivity(), p2);
                        }
                    });
                }

            }catch (Exception e){

            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void showPopup1(final Activity context, Point p2) {
        int popupWidth = 700;
        int popupHeight = 800;

        Button cancel,confirm;

        LinearLayout viewGroup1 = (LinearLayout) context.findViewById(R.id.GKDeactivateLayout);
        LayoutInflater layoutInflater1 = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout1 = layoutInflater1.inflate(R.layout.get_gk_deactivatebutton_view, viewGroup1);

        confirm = (Button) layout1.findViewById(R.id.btnGetGkSubmit);
        cancel = (Button) layout1.findViewById(R.id.btnGKDeactivateCancel);
        // Creating the PopupWindow
       /* final PopupWindow popup1 = new PopupWindow(context);
        popup1.setContentView(layout1);
        popup1.setWidth(popupWidth);
        popup1.setHeight(popupHeight);
        popup1.setFocusable(true);

        // Clear the default translucent background
        popup1.setBackgroundDrawable(new BitmapDrawable());
*/
        final PopupWindow popup1 = new PopupWindow(layout1, WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT, true);

        popup1.showAtLocation(layout1, Gravity.CENTER, 0, 0);


//        fixed the bug in sprint-7 Ticket_Id-114 starts
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(),"Confirm Clicked",Toast.LENGTH_SHORT).show();
                gKDeactiveHeader = "{"+"\""+"phone"+"\""+":"+"\""+"\""+","+"\""+"rfid"+"\""+":"+"\""+rfidText+"\"" +
                        ","+"\""+"chargingObjectId"+"\""+":"+charObjId+"}";
                if( mConnectionDetector.isOnline()) {
                    new gridKeyDeactive().execute();
                    popup1.dismiss();
                }else{
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }

            }
        });

//        fixed the bug in sprint-7 Ticket_Id-114 end


        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup1.dismiss();
            }
        });
    }


    /*private void getGridDeacItem(){
        String temp = getCardNumber.toString();
        for (int k = 0; k < gridKeyList.size(); k++) {
            if(temp.equals(gridKeyList[k].cardNumber))
        }
    }*/


    public class gridKeyDeactive extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.ACCOUNT_GRIDKEY_DEACTIVATE();
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.postMethod(mUrl, null, null,null, null,null,null, null,gKDeactiveHeader,token,null,null,null);
                OnePocketLog.d("card Payment resp----------", response);


            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            ArrayList<GridKeyModel> gridKeyList = new ArrayList<GridKeyModel>();
            if(response.contains("")){
                tableLayout.removeAllViews();
                new GridKeyInfo().execute();
            }
        }
    }

   /* class GridKeyInfo extends AsyncTask<String,String,List<GridKeyModel>> {

        GridKeyModel finalResult;

        @Override
        protected List<GridKeyModel> doInBackground(String... params) {
            String finalJson;
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                Log.d("Grid Key Information", "starts");
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                //  connection.setRequestProperty("X-Auth-Token", "ali:1458544315880:aa6b8139fd6fa8e9d4c6c0b961bb6572");
                Log.d("connection", "established" + connection);
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();

                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                finalJson = buffer.toString();
                Log.d("finalJson", ":" + finalJson);
                ArrayList<GridKeyModel> gridKeyList = new ArrayList<GridKeyModel>();

                JSONArray temp = new JSONArray(finalJson);
                Log.d("MainActivity.java", "temp : " + temp);

                for (int i = 0; i < temp.length(); i++) {

                    GridKeyModel gridKeyModel = new GridKeyModel();

                    JSONObject jsonObject = temp.getJSONObject(i);
                    Log.d("GridKeyInfo", "Json Object Value " + jsonObject);

                    gridKeyModel.setRfId(jsonObject.getString("rfid"));
                    gridKeyModel.setPhone(jsonObject.getString("phone"));
                    gridKeyModel.setChargingObjectId(jsonObject.getInt("chargingObjectId"));

                    gridKeyList.add(gridKeyModel);
                }
                return gridKeyList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(List<GridKeyModel> result) {
            super.onPostExecute(result);


            for (int k = 0; k < result.size(); k++) {
                GridKeyModel gridKeyModel = new GridKeyModel();

                gridKeyModel = result.get(k);

                tableRow = new TableRow(getActivity());
                horizontalbar = new View(getActivity());

                cardNumber = new TextView(getActivity());
                status = new TextView(getActivity());
                requestBut = new Button(getActivity());

                cardNumber.setText(gridKeyModel.getRfId());
                status.setText("Active");
                requestBut.setText("DeActive");

                horizontalbar.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
                horizontalbar.setBackgroundColor(Color.rgb(51, 51, 51));

                tableRow.addView(cardNumber);
                tableRow.addView(status);
                tableRow.addView(requestBut);

                tableLayout.addView(tableRow);
                tableLayout.addView(horizontalbar);

                requestBut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int[] location = new int[2];
                        requestBut.getLocationOnScreen(location);

                        //Initialize the Point with x, and y positions
                        p2 = new Point();
                        p2.x = location[0];
                        p2.y = location[1];
                        //Open popup window
                        if (p2 != null)
                            showPopup1(getActivity(), p2);
                    }
                });

            }

        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        private void showPopup1(final Activity context, Point p2) {
            int popupWidth = 700;
            int popupHeight = 800;

            Button cancel,confirm;

            LinearLayout viewGroup1 = (LinearLayout) context.findViewById(R.id.GKDeactivateLayout);
            LayoutInflater layoutInflater1 = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout1 = layoutInflater1.inflate(R.layout.get_gk_deactivatebutton_view, viewGroup1);

            confirm = (Button) layout1.findViewById(R.id.btnGetGkSubmit);
            cancel = (Button) layout1.findViewById(R.id.btnGKDeactivateCancel);
            // Creating the PopupWindow
            final PopupWindow popup1 = new PopupWindow(context);
            popup1.setContentView(layout1);
            popup1.setWidth(popupWidth);
            popup1.setHeight(popupHeight);
            popup1.setFocusable(true);

            // Clear the default translucent background
            popup1.setBackgroundDrawable(new BitmapDrawable());

            popup1.showAtLocation(layout1, Gravity.CENTER, 0, 0);


            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(),"Confirm Clicked",Toast.LENGTH_SHORT).show();
                    popup1.dismiss();
                }
            });


            cancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    popup1.dismiss();
                }
            });
        }

    }*/
}



