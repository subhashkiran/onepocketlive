package com.subhashe.onepocket.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.subhashe.onepocket.Core.PaymentForm;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Activities.CreditCardPayActivity;

import java.util.regex.Pattern;

public class PaymentFormFragment extends Fragment implements PaymentForm {

    Button saveButton;
    EditText cardNumber;
    EditText cvc;
    Spinner monthSpinner;
    Spinner yearSpinner;
    Spinner currencySpinner;
    EditText mCurrencyEdit;
    private static final String CURRENCY_UNSPECIFIED = "Unspecified";
    static final Pattern CODE_PATTERN = Pattern.compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");


    boolean mInside = false;
    boolean mDeleteHyphen = false;
    boolean mKeyListenerSet = false;
    final static String MARKER = "|";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_form_fragment, container, false);

        this.saveButton = (Button) view.findViewById(R.id.save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveForm(view);
            }
        });

        this.cardNumber = (EditText) view.findViewById(R.id.number);
//        this.cardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());

        cardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!mKeyListenerSet) {
                    cardNumber.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            try {
                                mDeleteHyphen = (keyCode == KeyEvent.KEYCODE_DEL
                                        && cardNumber.getSelectionEnd() - cardNumber.getSelectionStart() <= 1
                                        && cardNumber.getSelectionStart() > 0
                                        && cardNumber.getText().toString().charAt(cardNumber.getSelectionEnd() - 1) == '-');
                            } catch (IndexOutOfBoundsException e) {
                                // never to happen because of checks
                            }
                            return false;
                        }
                    });
                    mKeyListenerSet = true;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mInside) // to avoid recursive calls
                    return;
                mInside = true;

                int currentPos = cardNumber.getSelectionStart();
                String string = cardNumber.getText().toString().toUpperCase();
                String newString = makePrettyString(string);

                cardNumber.setText(newString);
                try {
                    cardNumber.setSelection(getCursorPos(string, newString, currentPos, mDeleteHyphen));
                } catch (IndexOutOfBoundsException e) {
                    cardNumber.setSelection(cardNumber.length()); // last resort never to happen
                }

                mDeleteHyphen = false;
                mInside = false;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

      /*  this.cardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && !CODE_PATTERN.matcher(s).matches()) {
                    String input = s.toString();
                    String numbersOnly = keepNumbersOnly(input);
                    String code = formatNumbersAsCode(numbersOnly);

                    Log.w("", "numbersOnly" + numbersOnly);
                    Log.w("", "code" + code);

                    cardNumber.removeTextChangedListener(this);
                    cardNumber.setText(code);
                    // You could also remember the previous position of the cursor
                    cardNumber.setSelection(code.length());
                    cardNumber.addTextChangedListener(this);
                }
            }
        });

        this.cvc = (EditText) view.findViewById(R.id.cvc);
        this.monthSpinner = (Spinner) view.findViewById(R.id.expMonth);
        this.yearSpinner = (Spinner) view.findViewById(R.id.expYear);
        this.mCurrencyEdit = (EditText) view.findViewById(R.id.currencyEdit);
//        this.currencySpinner = (Spinner) view.findViewById(R.id.currency);

        return view;
    }

    private String keepNumbersOnly(CharSequence s) {
        return s.toString().replaceAll("[^0-9]", ""); // Should of course be more robust
    }

    private String formatNumbersAsCode(CharSequence s) {
        int groupDigits = 0;
        String tmp = "";
        for (int i = 0; i < s.length(); ++i) {
            tmp += s.charAt(i);
            ++groupDigits;
            if (groupDigits == 4) {
                tmp += "-";
                groupDigits = 0;
            }
        }
        return tmp;
    }*/
        this.cvc = (EditText) view.findViewById(R.id.cvc);
        this.monthSpinner = (Spinner) view.findViewById(R.id.expMonth);
        this.yearSpinner = (Spinner) view.findViewById(R.id.expYear);
        this.mCurrencyEdit = (EditText) view.findViewById(R.id.currencyEdit);
//        this.currencySpinner = (Spinner) view.findViewById(R.id.currency);

        return view;
    }

    private String makePrettyString(String string) {
        String number = string.replaceAll("-", "");
        boolean isEndHyphen = string.endsWith("-") && (number.length()%4 == 0);
        return number.replaceAll("(.{4}(?!$))", "$1-") + (isEndHyphen ?"-":"");
    }

    private int getCursorPos(String oldString, String newString, int oldPos, boolean isDeleteHyphen) {
        int cursorPos = newString.length();
        if(oldPos != oldString.length()) {
            String stringWithMarker = oldString.substring(0, oldPos) + MARKER + oldString.substring(oldPos);

            cursorPos = (makePrettyString(stringWithMarker)).indexOf(MARKER);
            if(isDeleteHyphen)
                cursorPos -= 1;
        }
        return cursorPos;
    }
    @Override
    public String getCardNumber() {
        return this.cardNumber.getText().toString();
    }

    @Override
    public String getCvc() {
        return this.cvc.getText().toString();
    }

    @Override
    public Integer getExpMonth() {
        return getInteger(this.monthSpinner);
    }

    @Override
    public Integer getExpYear() {
        return getInteger(this.yearSpinner);
    }

    @Override
    public String getCurrency() {
       /* if (currencySpinner.getSelectedItemPosition() == 0) return null;
        String selected = (String) currencySpinner.getSelectedItem();
        if (selected.equals(CURRENCY_UNSPECIFIED))
            return null;
        else
            return selected.toLowerCase();*/
        return this.mCurrencyEdit.getText().toString();
    }

    public void saveForm(View button) {
//        if(!getCurrency().isEmpty()) {
            ((CreditCardPayActivity) getActivity()).saveCreditCard(this);
      /*  }else{
            Toast.makeText(getActivity(), "Enter Amount", Toast.LENGTH_SHORT).show();
        }*/
    }

    private Integer getInteger(Spinner spinner) {
    	try {
    		return Integer.parseInt(spinner.getSelectedItem().toString());
    	} catch (NumberFormatException e) {
    		return 0;
    	}
    }
}
