package com.subhashe.onepocket.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.subhashe.onepocket.Activities.LoginActivity;
import com.subhashe.onepocket.Activities.NavigationDrawerActivity;
import com.subhashe.onepocket.R;

/**
 * Created by SubhashE on 3/2/2016.
 */
public class LogoutFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Toast.makeText(getActivity(), "Logout Screen", Toast.LENGTH_SHORT).show();

//        showAlertBox(getResources().getString(R.string.logout_message));

       /* getActivity().finish();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);*/
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.Alert)
                .setMessage(R.string.exitWarning)
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
//                        NavigationDrawerActivity.super.onBackPressed();
                        if (getFragmentManager().getBackStackEntryCount() == 0) {

                        } else {
                            getFragmentManager().popBackStack();
                        }
                        getActivity().finish();
//                        Intent intent = new Intent(getActivity(), LoginActivity.class);
//                        startActivity(intent);
                    }
                }).create().show();
    }

    public void showAlertBox(final String msg) {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getContext());

            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);

        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();

                            getActivity().finish();
                            Intent intent1 = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent1);

                    }
                });

        alertDialogBuilder .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
                alertDialog.getButton(android.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

    }
}
