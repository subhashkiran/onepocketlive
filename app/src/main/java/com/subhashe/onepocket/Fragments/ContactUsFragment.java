package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.subhashe.onepocket.Activities.ForgotPassword;
import com.subhashe.onepocket.Activities.LoginActivity;
import com.subhashe.onepocket.Activities.NavigationDrawerActivity;
import com.subhashe.onepocket.Activities.login_payment_security;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.subhashe.onepocket.Utils.OnePocketUrls.CONTACTUS_FLAG;

/**
 * Created by SubhashE on 3/2/2016.
 */
public class ContactUsFragment extends Fragment {

    EditText contactSubject, contactMessage;
    CheckBox mEmailCopy;
    Button send;
    Intent emailIntent;
    private boolean emailFlag;
    private View rootView;
    private ProgressDialog progress;
    private ConnectionDetector mConnectionDetector;
    private String subject, message, response, contactUsHeader, checkState, userName, token;
    private JsonUtil mJsonUtil;
    public static final String USERNAME_ID = "USERNAME_ID_PREFS_String";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    InputMethodManager inputMethodManager;
    private Fragment fragment = null;
    private NavigationView navigationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Toast.makeText(getActivity(), "Contact Us Screen", Toast.LENGTH_SHORT).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_contact_us, container, false);

//        toolbar= (Toolbar)rootView. findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        final ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Contact Us");
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
        }

        new KeyBoardHide().hideKeyBoard(getActivity());

        mConnectionDetector = new ConnectionDetector(getActivity());
        checkState = "";
        mEmailCopy = (CheckBox) rootView.findViewById(R.id.emailCopy);
        contactSubject = (EditText) rootView.findViewById(R.id.contactSubject);
        contactMessage = (EditText) rootView.findViewById(R.id.contactMessage);
        send = (Button) rootView.findViewById(R.id.send);
        navigationView = (NavigationView) rootView.findViewById(R.id.navigation_view);

        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        userName = prefs.getString(USERNAME_ID, "NOT FOUND");

        mEmailCopy.setChecked(false);
        mEmailCopy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mEmailCopy.setChecked(true);
                    checkState = "";
                    checkState = "true";
                } else {
                    mEmailCopy.setChecked(false);
                    checkState = "";
                    checkState = "false";
                }
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactSubject.getText().toString().length() == 0) {
                    contactSubject.requestFocus();
                    showAlertBox(getString(R.string.emailSubject));
                } else if (contactMessage.getText().toString().length() == 0) {
                    contactMessage.requestFocus();
                    showAlertBox(getString(R.string.emailMessage));
                } else {
                    if (mConnectionDetector.isOnline()) {
                        inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                        sendEmail();
                    } else {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }
                }
            }
        });
        return rootView;
    }

    public void sendEmail() {
        String[] TO = {"esubhash@axxerainc.com"};
        emailIntent = new Intent(Intent.ACTION_SEND);
        subject = message = "";
        subject = contactSubject.getText().toString();
        message = contactMessage.getText().toString();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//        SharedPreferences.Editor editor1 = prefs.edit();
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

        contactUsHeader = "subject=" + subject + "&" + "message=" + message + "&" + "user=" + userName + "&" + "sendCopyToUser=" + checkState;
        if (mConnectionDetector.isOnline()) {
            if (validateEmail()) {
                new ContactAsync1().execute();
            }
        } else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }

       /* Log.d("subject", " : " + subject);
        Log.d("message", " : " + message);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);

        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        Log.i("sending email...", "");*/

    }

    private boolean validateEmail() {
        if ((subject.length() > 0) && (message.length() > 0)) {
            return true;
        } else {
            return false;
        }
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        if (!emailFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                                if (emailFlag) {
                                    contactSubject.setText("");
                                    contactMessage.setText("");
                                    if (mEmailCopy.isChecked()) {
                                        mEmailCopy.setChecked(false);
                                    }
                                    emailFlag = false;


                                    fragment = new MapsFragmentComplete();
                                    CONTACTUS_FLAG = true;
                                    new KeyBoardHide().hideKeyBoard(getActivity());
//                                    navigationView.setCheckedItem(R.id.drawer_home);
                                    if (mConnectionDetector.isOnline()) {
                                        FragmentManager fragmentManager;
                                        if (fragment != null) {
                                            fragmentManager = getActivity().getSupportFragmentManager();
                                            fragmentManager.beginTransaction()
                                                    .replace(R.id.frameLayout, fragment).addToBackStack(null).commit();
                                        } else {
                                            // error in creating fragment
                                            Log.d("NavigationDrActivity", "Error  creating fragment");
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                                                Toast.LENGTH_SHORT).show();

                                    }
                                   /* getActivity().finish();
                                    Intent intent1 = new Intent(getActivity(), MapsFragmentComplete.class);
                                    startActivity(intent1);*/

                                }
                            }
                        }

                );
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
              @Override
              public void onShow(DialogInterface dialog) {
                  alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
              }
          }

        );
        alertDialog.show();

      /*  AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        if(!emailFlag) {
            alertDialog.setTitle(R.string.Alert);
            alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }

        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                hideProgress();
                if(emailFlag){
                    contactSubject.setText("");
                    contactMessage.setText("");
                    if(mEmailCopy.isChecked()){
                        mEmailCopy.setChecked(false);
                    }
                    emailFlag = false;
                }
            }
        });
        alertDialog.show();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                new KeyBoardHide().hideKeyBoard(getActivity());
                getActivity().onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public class ContactAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            OnePocketLog.d("Contactus1----------", "one");
            showProgress(getString(R.string.pleaseWait));
            OnePocketLog.d("Contactus2----------", "two");
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                OnePocketLog.d("Contactus3----------", "three");
                response = mJsonUtil.postMethod(OnePocketUrls.CONTACTUS_URL(), null,null, null, null, null, null, null,
                        contactUsHeader, token,null,null,null);
                OnePocketLog.d("Contactus4----------", response);
//                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL());
            } catch (Exception e) {
                OnePocketLog.d("Contactus5----------", e.toString());
                return null;
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideProgress();
            if (result == null && (!response.contains("Your Email has been sent successfully"))) {
                showAlertBox(getString(R.string.netNotAvl));
            }

            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response.contains("User") || response.contains("not found")) {
                showAlertBox(getString(R.string.userNotFound));
            } else if (response.contains("Your Email has been sent successfully")) {
                emailFlag = true;
                showAlertBox(getString(R.string.emailSuccess));
            } else if (response.contains("java.net.ConnectException") || response.contains("Network is unreachable")) {
                showAlertBox(getString(R.string.netNotAvl));
            } else if (response.contains("Error Code-196")) {
                showAlertBox("Email cannot be sent. Please try again after sometime.");
            } else {
                try {
//
                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    public class ContactAsync1 extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                OnePocketLog.d("Contactus3----------", "three");
                response = mJsonUtil.postMethod(OnePocketUrls.CONTACTUS_URL(), null, null, null, null, null, null, null,
                        contactUsHeader, token,null,null,null);
                OnePocketLog.d("Contactus4----------", response);
                if (response.contains("java.net.SocketException") || response.contains("ETIMEDOUT")) {
                    showAlertBox(getString(R.string.netNotAvl));
                }
                return response;
//                response = mJsonUtil.getMethod(OnePocketUrls.USER_SECUTITY_URL());
            } catch (Exception e) {
                OnePocketLog.d("Contactus5----------", e.toString());
                return "error";
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
//            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgress();
            if (result != null && result.contains("error")) {
                showAlertBox(getString(R.string.netNotAvl));
            }

            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else if (response.contains("User") || response.contains("not found")) {
                showAlertBox(getString(R.string.userNotFound));
            } else if (response.contains("Your Email has been sent successfully")) {
                emailFlag = true;
                showAlertBox(getString(R.string.emailSuccess));
            } else if (response.contains("java.net.ConnectException") || response.contains("Network is unreachable")) {
                showAlertBox(getString(R.string.netNotAvl));
            }else if(response.contains("Authentication failed") || response.contains("Authentication unsuccessful")){
                showAlertBox(getString(R.string.emailFailure));
            }else if(response.contains("Error Code-203")){
                showAlertBox(getString(R.string.contactusMessageError));
            } else {
                try {
//
                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }
}
