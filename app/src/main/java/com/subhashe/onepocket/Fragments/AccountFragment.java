package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.subhashe.onepocket.Activities.CreditCardPayActivity;
import com.subhashe.onepocket.Activities.DRAlertsActivityScreen;
import com.subhashe.onepocket.Activities.EVListActivity;
import com.subhashe.onepocket.Activities.EditProfile;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.KeyBoardHide;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class AccountFragment extends Fragment {
    private static final String TAG = "ACCOUNT_FRAGMENT";
    private Toolbar toolbar;
    private View rootView;
    private JsonUtil mJsonUtil;
    private ConnectionDetector mConnectionDetector;
    private Fragment fragment;
    ListView listView;
    Intent intent = null;
    private LinearLayout my_profile, grid_cards, my_ev, acc_notifications;
    TextView tv_myprofile, tv_myev, tv_mymoney;
    private boolean successFlag;

    private TextView mAccBalance;
    private String response, token, getAccBal;
    private int user_id, accBalence;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor1;
    private ProgressDialog progress;
    LinearLayout ll_bal;
    ActionBar mActionBar;

    public static final String ACC_BALENCE = "ACC_BALENCE";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";
    private boolean paymentPopup = false;
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String USER_REG_ID = "USERID_REG_String";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        editor1 = prefs.edit();
        paymentPopup = prefs.getBoolean(PAYMENT_FLAG_ALERT, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_accounts, container, false);

        initToolbar();


        user_id = prefs.getInt(USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");

        mConnectionDetector = new ConnectionDetector(getActivity());

        ll_bal = (LinearLayout) rootView.findViewById(R.id.ll_activity_acc1_balance);
        //ll_bal.setVisibility(View.GONE);

        my_profile = (LinearLayout) rootView.findViewById(R.id.ll_accounts_myProfile);
        //grid_cards =(LinearLayout)rootView .findViewById(R.id.ll_accounts_gridCards);
        my_ev = (LinearLayout) rootView.findViewById(R.id.ll_accounts_myev);
        //my_fav =(LinearLayout) rootView.findViewById(R.id.ll_accounts_myfav);
        //notifications =(LinearLayout) rootView.findViewById(R.id.ll_accounts_notifications);

        tv_myprofile = (TextView) rootView.findViewById(R.id.accounts_myProfile);
        tv_myev = (TextView) rootView.findViewById(R.id.accounts_myev);
        tv_mymoney = (TextView) rootView.findViewById(R.id.accounts_mymoney);

        mAccBalance = (TextView) rootView.findViewById(R.id.accBalance);

        acc_notifications = (LinearLayout) rootView.findViewById(R.id.ll_accounts_mynotifications);

        my_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mConnectionDetector.isOnline()) {
                    Intent intent = new Intent(getActivity(), EditProfile.class);
                    getActivity().startActivity(intent);

                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });

        my_ev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                fragment = new ElectricVehicleFragment();

                if (mConnectionDetector.isOnline()) {
                    Intent intent = new Intent(getActivity() , EVListActivity.class);
                    startActivity(intent);

                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });
        tv_mymoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyBoardHide().hideKeyBoard(getActivity());

                if (mConnectionDetector.isOnline()) {
                    Intent intent = new Intent(getActivity(), CreditCardPayActivity.class);
                    getActivity().startActivity(intent);

                } else {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        new BalenceCheck().execute();
    }

    public class BalenceCheck extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.USER_BALENCE_CHECK() + user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    JSONObject jObject = new JSONObject(response);
                    JSONArray jObject1 = jObject.getJSONArray("accountses");
                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp = jObject1.getJSONObject(i);
                        getAccBal = mTemp.getString("accountBalance");
                        accBalence = mTemp.getInt("accountBalance");


                        NumberFormat num = NumberFormat.getCurrencyInstance(Locale.US);
//                        BigDecimal bd = new BigDecimal(getAccBal.toString());
//                        mAmount = num.format(getAccBal);
                        mAccBalance.setText("");
                        mAccBalance.setText(num.format(new BigDecimal(getAccBal)));

                        try {
//                            accBalence = Integer.parseInt(getAccBal);
                            editor1.putInt(ACC_BALENCE, accBalence);
                        } catch (NumberFormatException e) {
                            System.out.println("parse value is not valid : " + e);
                        }
                        /*Fragment currentFragment = new PaymentsFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.detach(currentFragment);
                        fragmentTransaction.attach(currentFragment);
                        fragmentTransaction.commit();*/
                    }

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }
    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        if (!successFlag) {
            alertDialogBuilder.setTitle(R.string.Alert);
            alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        }
        successFlag = false;
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                         hideProgress();
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

    }

    private void initToolbar() {

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

        AppCompatActivity activity = (AppCompatActivity) getActivity();

        mActionBar = activity.getSupportActionBar();

        mActionBar.setDisplayOptions( ActionBar.DISPLAY_SHOW_TITLE);

        final Drawable backArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        backArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        mActionBar.setHomeAsUpIndicator(backArrow);

        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#581b18")));

        String title = "Account";
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(spannableString);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //getActivity().finish();
                getFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
