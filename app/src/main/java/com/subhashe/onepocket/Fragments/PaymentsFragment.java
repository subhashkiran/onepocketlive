package com.subhashe.onepocket.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.subhashe.onepocket.Activities.CreditCardPayActivity;
import com.subhashe.onepocket.Activities.PayPalActivity;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;


/**
 * Created by SubhashE on 3/2/2016.
 */
public class PaymentsFragment extends Fragment {
    private View rootView;
    private LinearLayout mCreditCard,mPayPal;
    private TextView mAccBalance;
    private ProgressDialog progress;
    private String response,token,getAccBal,mAmount;
    private JsonUtil mJsonUtil;
    private ConnectionDetector mConnectionDetector;
    private int user_id,accBalence;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor1;
    public static final String USER_ID = "USERID_PREFS_String";
    public static final String TOKEN_ID = "TOKEN_ID_PREFS_String";
    public static final String ACC_BALENCE = "ACC_BALENCE";
    public static final String PAYMENT_FLAG_ALERT = "PAYMENT_FLAG_ALERT";
    public static final String USER_REG_ID = "USERID_REG_String";
    private FragmentManager fragmentManager;
    private boolean paymentPopup = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        editor1 = prefs.edit();
        paymentPopup = prefs.getBoolean(PAYMENT_FLAG_ALERT, false);
        Log.d("PAYMENT_FLAG"," value : "+PAYMENT_FLAG_ALERT);
       /* fragmentManager =  getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });*/
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_payments,container,false);

//        toolbar= (Toolbar)rootView. findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        final ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Payments");
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE );
        }
        mConnectionDetector = new ConnectionDetector(getActivity());
        mAccBalance = (TextView)rootView.findViewById(R.id.accBalance);
        mCreditCard = (LinearLayout)rootView.findViewById(R.id.creditCard);
        mPayPal = (LinearLayout)rootView.findViewById(R.id.payPal);

        if(paymentPopup){
            showAlertBox(getString(R.string.paymentPop1));
        }

        mCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),CreditCardPayActivity.class);
                getActivity().startActivity(intent);
//                getActivity(). finish();
            }
        });

        mPayPal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PayPalActivity.class);
                getActivity().startActivity(intent);
            }
        });


        user_id = prefs.getInt(USER_ID, 0);
        token = prefs.getString(TOKEN_ID, "NOT FOUND");
//        new BalenceCheck().execute();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        new BalenceCheck().execute();
    }

    public class BalenceCheck extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(getString(R.string.pleaseWait));
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String mUrl = OnePocketUrls.USER_BALENCE_CHECK()+user_id;
//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                response = mJsonUtil.getMethod(mUrl, null, token);
                OnePocketLog.d("card Payment resp----------", response);
            } catch (Exception e) {
                OnePocketLog.d("card Payment error----------", e.toString());
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (response == null || response == "error") {
                showAlertBox(getString(R.string.servernotresponce));
            } else {
                try {
                    JSONObject jObject = new JSONObject(response);
                    JSONArray jObject1 = jObject.getJSONArray("accountses");
                    for (int i = 0; i < jObject1.length(); i++) {
                        JSONObject mTemp = jObject1.getJSONObject(i);
                        getAccBal = mTemp.getString("accountBalance");
                        accBalence = mTemp.getInt("accountBalance");


                        NumberFormat num = NumberFormat.getCurrencyInstance(Locale.US);
//                        BigDecimal bd = new BigDecimal(getAccBal.toString());
//                        mAmount = num.format(getAccBal);
                        mAccBalance.setText("");
                        mAccBalance.setText(num.format(new BigDecimal(getAccBal)));

                        try {
//                            accBalence = Integer.parseInt(getAccBal);
                            editor1.putInt(ACC_BALENCE, accBalence);
                        } catch(NumberFormatException e) {
                            System.out.println("parse value is not valid : " + e);
                        }
                        /*Fragment currentFragment = new PaymentsFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.detach(currentFragment);
                        fragmentTransaction.attach(currentFragment);
                        fragmentTransaction.commit();*/
                    }

                } catch (Exception ep) {
//                        new ExceptionHandling("Signup - BackGroundTask1_post - "+ep.toString());
                }
            }
        }
    }

    public void showAlertBox(final String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.Alert);
        alertDialogBuilder.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
//                         hideProgress();
                        if(paymentPopup){
                            showAlertBox(getString(R.string.paymentPop2));
                            paymentPopup = false;
                            editor1.putBoolean(PAYMENT_FLAG_ALERT, false);

                            editor1.putInt(USER_REG_ID, user_id);

                            editor1.commit();
                        }
                    }
                });
               /* .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.primary));
            }
        });
        alertDialog.show();

       /* AlertDialog alertDialog = new AlertDialog.Builder(
                getActivity()).create();
        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
//        alertDialog.setCancelable(false);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                hideProgress();
                if(paymentPopup){
                    showAlertBox(getString(R.string.paymentPop2));
                    paymentPopup = false;
                    editor1.putBoolean(PAYMENT_FLAG, false);

                    editor1.putInt(USER_REG_ID, user_id);

                    editor1.commit();
                }
            }
        });
        alertDialog.show();*/
    }
    private void showProgress(String msg) {
        progress = ProgressDialog.show(getActivity(), "", msg);
        progress.setCancelable(false);
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }
}
