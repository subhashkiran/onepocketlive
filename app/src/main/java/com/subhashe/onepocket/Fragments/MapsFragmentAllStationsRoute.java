/*
package com.subhashe.onepocket.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.ClusterGroup;
import com.androidmapsextensions.Polyline;
import com.androidmapsextensions.PolylineOptions;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.MapsInitializer;
import com.subhashe.onepocket.Core.Constants;

import com.androidmapsextensions.Circle;
import com.androidmapsextensions.CircleOptions;
import com.androidmapsextensions.ClusteringSettings;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.SupportMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.ClusterManager;
import com.subhashe.onepocket.Activities.GPSTracker;
import com.subhashe.onepocket.Activities.MapMarkerInfoActivity;
import com.subhashe.onepocket.Adapters.ClearableAutoCompleteTextView;
import com.subhashe.onepocket.Adapters.GooglePlacesAutocompleteAdapter;
import com.subhashe.onepocket.Core.ImageClusterOptionsProvider;
import com.subhashe.onepocket.Core.LatLang;
import com.subhashe.onepocket.Core.LatLangAll;
import com.subhashe.onepocket.Core.MapListModel;
import com.subhashe.onepocket.Core.MapModel;
import com.subhashe.onepocket.Core.MapModelAll;
import com.subhashe.onepocket.Core.PlaceJSONParser;
import com.subhashe.onepocket.Core.RouteModel;
import com.subhashe.onepocket.DataBase.DbHelper;
import com.subhashe.onepocket.R;
import com.subhashe.onepocket.Utils.ConnectionDetector;
import com.subhashe.onepocket.Utils.JsonUtil;
import com.subhashe.onepocket.Utils.NetworkChecker;
import com.subhashe.onepocket.Utils.OnePocketLog;
import com.subhashe.onepocket.Utils.OnePocketUrls;
import com.subhashe.onepocket.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.Circle;
//import com.google.android.gms.maps.model.CircleOptions;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.subhashe.onepocket.Core.ClusteringSettings;

*/
/**
 * Created by SubhashE on 1/21/2016.
 *//*

public class MapsFragmentAllStationsRoute extends BaseFragment implements AdapterView.OnItemClickListener,
        RoutingListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    //    public static final String JSON_URL = "http://192.168.168.141:8080/gridbot/maps";
    private static final double[] CLUSTER_SIZES = new double[]{180, 160, 144, 120, 96};
    public static final String CONN2_PORT_TYPE = "conn2portType";
    public static final String STATION = "station";
    public static final String CONN1_PORT_TYPE = "conn1portType";
    public static final String STATION_STATUS = "stationStatus";
    public static final String CONN2_PORT_LEVEL = "conn2portLevel";
    public static final String CONN1_PORT_LEVEL = "conn1portLevel";
    public static final String PORT_QUANTITY = "portQuantity";

    */
/* route map*//*

    private Spinner spinner;
    ArrayAdapter<String> adapter;
    String[] mPlaceType = null;
    String[] mPlaceTypeName = null;
    String nearestUrl = null;

    ArrayList<Marker> markerArrayList = new ArrayList<Marker>();
    ArrayList<Marker> markerPointsList = new ArrayList<Marker>();
    Marker markerPointA, markerPointB;
    Polyline polyline;
    PolylineOptions polyOptions;
    //    EditText yourLocation;
    LatLng yourLocationLatLng;

    LinearLayout showStartTextView, showStartAutoCompleteView;
    TextView yourLocation;
    boolean currentLocationFlag = true;
    InputMethodManager inputMethodManager;


    //    protected com.google.android.gms.maps.GoogleMap map,mGoogleMap;
    protected LatLng start;
    protected LatLng end;
    //    @InjectView(R.id.start)
    AutoCompleteTextView starting;
    //    @InjectView(R.id.destination)
    AutoCompleteTextView destination;
    //    @InjectView(R.id.send)
//    ImageView send;
    protected GoogleApiClient mGoogleApiClient;
    private PlaceAutoCompleteAdapter mAdapter;
    private ProgressDialog progressDialog1;
    private ArrayList<Polyline> polylines = new ArrayList<>();

    private static final int[] COLORS = new int[]{R.color.primary_dark, R.color.account_number_blue,  R.color.blue,R.color.accent,R.color.primary_dark_material_light};


    Button send;
    double mLatitude = 0;
    double mLongitude = 0;
    GPSTracker gpsTracker1;

    double yourLocationLatitude = 0;
    double yourLocationLongitude = 0;

    //    Button checkRoute;
    TextView routeId1, routeDistance1, routeDuration1, routeColor1, routeId2, routeDistance2, routeDuration2, routeColor2, routeId3, routeDistance3, routeDuration3, routeColor3;
//    com.google.android.gms.maps.GoogleMap googleMapForPolylines;

//    private static final LatLngBounds BOUNDS_JAMAICA = new LatLngBounds(new LatLng(-57.965341647205726, 144.9987719580531),
//            new LatLng(72.77492067739843, -9.998857788741589));

    private static final LatLngBounds AXXERA_HYD = new LatLngBounds(new LatLng(17.2168886, 78.1599217),
            new LatLng(17.6078088, 78.6561694));
    */
/*route map end*//*

    private SupportMapFragment mapFragment;
    //    private GoogleMap mMap;
    Marker marker1, marker2;
    private View rootView, layout;
    FragmentManager myFragmentManager;
    SupportMapFragment mySupportMapFragment;
    private JsonUtil mJsonUtil;
    private ProgressDialog progDailog;
    private Button filterSubmit;
    private LinearLayout viewGroup;
    private Switch statusSwitch, priceSwitch, leve1Switch, leve2Switch, dcFastSwitch, dcFastComboSwitch, teslaSwitch,
            CPSwitch, BlkSwitch, SCSwitch, evgoSwitch, AVSwitch, EVCSwitch, EVSESwitch, GLSwitch, OPCSwitch, RASwitch, SPSwitch;
    private ConnectionDetector mConnectionDetector;
    JsonFactory factory;
    JsonToken token;
    JsonParser jsonParser;
    String fieldName;
    JSONArray jsonArray;
    int jsonArrayLength;
    int jsonLength;
    private URL url;
    private InputStream stream;
    StringBuffer buffer;
    String line = "";

    JSONObject jsonObject;
    String finalJson="";
    private ArrayList<MapModelAll> mapArrayList;
    ArrayList<MapModelAll> mapArrayListFilter = new ArrayList<MapModelAll>();
    Button mMapView, mSatelliteView;
    //    FrameLayout mBottomLayout;
    FrameLayout nearestPlacesFragment, routeFragment;
    private LinearLayout mViewA, mViewB;
    LinearLayout.LayoutParams mViewAget, mViewBget;
    FloatingActionButton mFab;
    TextView stationName, stationAddress, chargers, levelType1, levelType2, city;
    ImageButton imageButton, searchButton;
    GPSTracker gpsTracker;
    boolean singleMarker = false;
    boolean searchLocationMarker = false;
    Marker marker = null;
    Marker markerTell = null;
    List<Marker> markerList = new ArrayList<Marker>();
    List<Marker> markerListTell = new ArrayList<Marker>();
    List<Marker> markerListFilter = new ArrayList<Marker>();
    ArrayList<MapModelAll> dbList;
    ArrayList<MapModelAll> dbFilterList = new ArrayList<MapModelAll>();
    HashMap<String, MapModel> hashMapData;
    HashMap<String, MapModelAll> hashMapDataAll;
    HashMap<String, MapModelAll> hashMapFilter;
    String response, responseAll, markerCheck;
    String filterString, statusString, priceString, connectorTypeString, networkString;
    String chargerKey = null;
    private CameraPosition position;
    ProgressDialog progressDialog;
    private LatLng currentLoc, mLatLng;
    private GooglePlacesAutocompleteAdapter mGooglePlaceAdapter;
    LayoutInflater layoutInflater, layoutInflaterSwitch;
    List<MapListModel> mapListData;
    List<MapListModel> mapListDataFilter;
    private DbHelper dbHelper;
    View v;
    String searchString = null;
    AlertDialog alertDialog;
    boolean searchLocation, filterEnable;
    private ClusterManager<LatLang> mClusterManager;
    private LatLang clickedClusterItem;
    private Circle circle;
    private CircleOptions circleOptions;
    private List<Marker> declusterifiedMarkers;
    private static final String LOG_TAG = "MapSearch";
    private static final double LN2 = 0.6931471805599453;
    private static final int WORLD_PX_HEIGHT = 256;
    private static final int WORLD_PX_WIDTH = 256;
    private static final int ZOOM_MAX = 21;

    public static final String STATUSSWITCH = "STATUSSWITCH";
    public static final String PRICESWITCH = "PRICESWITCH";
    public static final String LEVE1SWITCH = "LEVE1SWITCH";
    public static final String LEVE2SWITCH = "LEVE2SWITCH";
    public static final String DCFASTSWITCH = "DCFASTSWITCH";
    public static final String DCFASTCOMBOSWITCH = "DCFASTCOMBOSWITCH";
    public static final String TESLASWITCH = "TESLASWITCH";
    public static final String CPSWITCH = "CPSWITCH";
    public static final String BLKSWITCH = "BLKSWITCH";
    public static final String SCSWITCH = "SCSWITCH";
    public static final String EVGOSWITCH = "EVGOSWITCH";
    public static final String AVSWITCH = "AVSWITCH";
    public static final String EVCSWITCH = "EVCSWITCH";
    public static final String EVSESWITCH = "EVSESWITCH";
    public static final String GLSWITCH = "GLSWITCH";
    public static final String OPCSWITCH = "OPCSWITCH";
    public static final String RASWITCH = "RASWITCH";
    public static final String SPSWITCH = "SPSWITCH

    private BroadcastReceiver ReceivefrmSERVICE;
    private boolean firstCHeck, networkCheckerFlag, reCheckFlag, alertFlag;
    //------------ make your specific key ------------
    private static final String API_KEY = "AIzaSyBE64USHAOPxQbSPl4jS78_zIDXRFgtDYY";
    //    private static final String BROWSER_API_KEY = "AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
    // private static final String API_KEY = "AIzaSyAU9ShujnIg3IDQxtPr7Q1qOvFVdwNmWc4";
    // private static final String API_KEY = "AIzaSyA5ngSBpnoj2JYvjTuMrCrgJpaQ3wXFBSc";
    // map search data ends
    public static final String JSON_ARRAY_NAME = "fuel_stations";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_maps, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        */
/*LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.search_bar, null);*//*

        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_SHOW_HOME);
        hashMapDataAll = new HashMap<String, MapModelAll>();
        hashMapFilter = new HashMap<String, MapModelAll>();
        filterString = statusString = priceString = connectorTypeString = networkString = "";
        connectorTypeString = "ev_connector_type=";
        networkString = "ev_network=";

        reCheckFlag = false;
        markerCheck = "";
        alertDialog = new AlertDialog.Builder(
                this.getActivity()).create();
        try {
            initilizeMap(rootView);
            initFab();
            intitilizeActionbar(v);
            actionBar.setCustomView(v);
            initilizeSwitch();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }

    public void currentLocationLatLng() {
        gpsTracker = new GPSTracker(getContext());

        if (gpsTracker.canGetLocation) {
            yourLocationLatitude = gpsTracker.getLatitude();
            yourLocationLongitude = gpsTracker.getLongitude();
            yourLocationLatLng = new LatLng(yourLocationLatitude, yourLocationLongitude);

            if (yourLocationLatLng != null) {
                start = yourLocationLatLng;
            }
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void initFab() {
        mFab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(rootView, "FAB Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void initilizeSwitch() {
        viewGroup = (LinearLayout) getActivity().findViewById(R.id.ProfileEditLayout);
        layoutInflaterSwitch = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = layoutInflater.inflate(R.layout.sortfilter, viewGroup);
        filterSubmit = (Button) layout.findViewById(R.id.filterSubmit);

        statusSwitch = (Switch) layout.findViewById(R.id.StatusSwitch);
        priceSwitch = (Switch) layout.findViewById(R.id.PriceSwitch);
        leve1Switch = (Switch) layout.findViewById(R.id.Level1Switch);
        leve2Switch = (Switch) layout.findViewById(R.id.Level2Switch);
        dcFastSwitch = (Switch) layout.findViewById(R.id.DcFastSwitch);
        dcFastComboSwitch = (Switch) layout.findViewById(R.id.DcFastComboSwitch);
        teslaSwitch = (Switch) layout.findViewById(R.id.TeslaSwitch);
        CPSwitch = (Switch) layout.findViewById(R.id.CharPointSwitch);
        BlkSwitch = (Switch) layout.findViewById(R.id.blinkSwitch);
        SCSwitch = (Switch) layout.findViewById(R.id.semchargeSwitch);
        evgoSwitch = (Switch) layout.findViewById(R.id.eVgoSwitch);
        AVSwitch = (Switch) layout.findViewById(R.id.AeroVironmentSwitch);
        EVCSwitch = (Switch) layout.findViewById(R.id.EVConnectSwitch);
        EVSESwitch = (Switch) layout.findViewById(R.id.EVSELLCWebNetSwitch);
        GLSwitch = (Switch) layout.findViewById(R.id.GreenlotsSwitch);
        OPCSwitch = (Switch) layout.findViewById(R.id.OpConnectSwitch);
        RASwitch = (Switch) layout.findViewById(R.id.RechargeAccessSwitch);
        SPSwitch = (Switch) layout.findViewById(R.id.ShorepowerSwitch);
    }

    private void initilizeMap(View view) {
        try {

            routeFragment = (FrameLayout) rootView.findViewById(R.id.routeFragment);

            routeId1 = (TextView) rootView.findViewById(R.id.routeId1);
            routeDistance1 = (TextView) rootView.findViewById(R.id.routeDistance1);
            routeDuration1 = (TextView) rootView.findViewById(R.id.routeDuration1);
            routeColor1 = (TextView) rootView.findViewById(R.id.routeColor1);

            routeId2 = (TextView) rootView.findViewById(R.id.routeId2);
            routeDistance2 = (TextView) rootView.findViewById(R.id.routeDistance2);
            routeDuration2 = (TextView) rootView.findViewById(R.id.routeDuration2);
            routeColor2 = (TextView) rootView.findViewById(R.id.routeColor2);

            routeId3 = (TextView) rootView.findViewById(R.id.routeId3);
            routeDistance3 = (TextView) rootView.findViewById(R.id.routeDistance3);
            routeDuration3 = (TextView) rootView.findViewById(R.id.routeDuration3);
            routeColor3 = (TextView) rootView.findViewById(R.id.routeColor3);

            routeFragment.setVisibility(View.INVISIBLE);

            // added for nearest places on 13Apr2016 starts

            currentLocationLatLng();

            inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            showStartAutoCompleteView = (LinearLayout) rootView.findViewById(R.id.showStartAutoCompleteView);
            showStartAutoCompleteView.setVisibility(View.GONE);
            showStartTextView = (LinearLayout) rootView.findViewById(R.id.showStartTextView);
            showStartTextView.setVisibility(View.VISIBLE);
            yourLocation = (TextView) rootView.findViewById(R.id.yourLocation);

            starting = (AutoCompleteTextView) rootView.findViewById(R.id.start);
//        yourLocation = (EditText) rootView.findViewById(R.id.yourLocation);
            destination = (AutoCompleteTextView) rootView.findViewById(R.id.destination);
//        send = (ImageView) rootView.findViewById(R.id.send);
            send = (Button) rootView.findViewById(R.id.send);

            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Util.Operations.isOnline(getContext())) {
                        nearestPlacesFragment.setVisibility(View.GONE);

//                    Toast.makeText(getContext(), "route clicked", Toast.LENGTH_SHORT).show();
                        route();
                    } else {
                        Toast.makeText(getContext(), "No internet connectivity", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            // call nearest palces fragment and spinner
            nearestPlacesFragment = (FrameLayout) rootView.findViewById(R.id.nearstLocationsFragment);
            nearestPlacesFragment.setVisibility(View.INVISIBLE);

            // Array of place types
            mPlaceType = getResources().getStringArray(R.array.place_type);
            // Array of place type names
            mPlaceTypeName = getResources().getStringArray(R.array.place_type_name);

            spinner = (Spinner) rootView.findViewById(R.id.placeTypeSpinner);
            adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, mPlaceTypeName);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);


            mMapView = (Button) rootView.findViewById(R.id.mapview);
           */
/* if (mMapView.getViewTreeObserver().isAlive()) {
                mMapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressLint("NewApi") // We check which build version we are using.
                    @Override
                    public void onGlobalLayout() {

                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            mMapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            mMapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
//                        myMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                    }});
            }*//*

//            mBottomLayout = (FrameLayout)rootView.findViewById(R.id.bottomFragment);

            mViewB = (LinearLayout) rootView.findViewById(R.id.viewB);
            mViewA = (LinearLayout) rootView.findViewById(R.id.viewA);
            stationName = (TextView) rootView.findViewById(R.id.stationName);
//            chargers = (TextView) rootView.findViewById(R.id.chargers);
            city = (TextView) rootView.findViewById(R.id.city);
            levelType1 = (TextView) rootView.findViewById(R.id.levelType1);
            levelType2 = (TextView) rootView.findViewById(R.id.levelType2);

         */
/*   mBottomLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getActivity(), "Bottom layout clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(),MapMarkerInfoActivity.class);
                    intent.putExtra("hashMap", hashMapData);
                    intent.putExtra("chargerMkey",chargerKey);
                    startActivity(intent);
                }
            }); *//*

            mViewB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getActivity(), "Bottom layout clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), MapMarkerInfoActivity.class);
                    if(hashMapData != null){
                        intent.putExtra("hashMap", hashMapData);
                        intent.putExtra("tellusmarker", true);
                    }
                    intent.putExtra("sendListData", (Serializable) mapListData);
                    intent.putExtra("sendListDataFilter", (Serializable) mapListDataFilter);
                    intent.putExtra("chargerMkey", chargerKey);
                    intent.putExtra("markerCheck", markerCheck);
                    startActivity(intent);
                }
            });
            mMapView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            });
            mSatelliteView = (Button) rootView.findViewById(R.id.satileteView);
            mSatelliteView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                }
            });
           */
/* if(mMap == null){
                FragmentManager fm = getChildFragmentManager();
                mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
              *//*
*/
/*  SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager()
                        .findFragmentById(R.id.map);*//*
*/
/*
//                mapFragment.getMapAsync(this);
               *//*
*/
/* if(mapFragment != null){
                    mapFragment = createMapFragment();
                }*//*
*/
/*

                mMap =mapFragment.getExtendedMap();
                if(mMap != null){
                    setUpMap();
                }

                mMapView = (Button)rootView.findViewById(R.id.mapview);
                mMapView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                });
                mSatelliteView = (Button)rootView.findViewById(R.id.satileteView);
                mSatelliteView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    }
                });
            }*//*

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void routeMapDisplay() {

//        polylines = new ArrayList<>();
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        MapsInitializer.initialize(getContext());
        mGoogleApiClient.connect();

        mAdapter = new PlaceAutoCompleteAdapter(getContext(), android.R.layout.simple_list_item_1,
                mGoogleApiClient, AXXERA_HYD, null);
        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        mAdapter.setBounds(bounds);


       */
/* if (currentLocationFlag) {

            if (Build.VERSION.SDK_INT >= 23 &&
                    ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                    CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(mLatitude, mLongitude));
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

                    mMap.moveCamera(center);
                    mMap.animateCamera(zoom);

                    int selectedPosition = spinner.getSelectedItemPosition();
                    String type = mPlaceType[selectedPosition];

                    nearestUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + mLatitude + "," + mLongitude + "&radius=1000&types=" + type + "&sensor=true&key=AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
                    new PlacesTask().execute(nearestUrl);

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });


            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                    CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(mLatitude, mLongitude));
//              CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude()));
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

                    mMap.moveCamera(center);
                    mMap.animateCamera(zoom);

                    int selectedPosition = spinner.getSelectedItemPosition();
                    String type = mPlaceType[selectedPosition];

                    nearestUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + mLatitude + "," + mLongitude + "&radius=1000&types=" + type + "&sensor=true&key=AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
                    new PlacesTask().execute(nearestUrl);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });

        }*//*

        startAndDestinationPoints();

    }

    public void startAndDestinationPoints() {


        destination.setAdapter(mAdapter);

        if (!currentLocationFlag) {

            starting.setAdapter(mAdapter);
            starting.requestFocus();

            starting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    //keyboard closing window code starts


                    inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                    //keyboard closing window code ends

                    final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                    final String placeId = String.valueOf(item.placeId);
                    Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);


                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                            .getPlaceById(mGoogleApiClient, placeId);
                    placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (!places.getStatus().isSuccess()) {
                                // Request did not complete successfully
                                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                                places.release();
                                return;
                            }
                            // Get the Place object from the buffer.
                            final Place place = places.get(0);

                            start = place.getLatLng();
                        }
                    });

                }
            });

            starting.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int startNum, int before, int count) {
                    if (start != null) {
                        start = null;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //keyboard closing window code starts
                inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                //keyboard closing window code ends

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);


                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (!places.getStatus().isSuccess()) {
                            // Request did not complete successfully
                            Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                            places.release();
                            return;
                        }
                        // Get the Place object from the buffer.
                        final Place place = places.get(0);

                        end = place.getLatLng();
                    }
                });

            }
        });


        destination.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (end != null) {
                    end = null;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void route() {
        if (start == null || end == null) {
            if (!currentLocationFlag) {
                if (start == null) {
                    if (starting.getText().length() > 0) {
                        starting.setError("Choose location from dropdown.");
                    } else {
                        Toast.makeText(getContext(), "Please choose a starting point.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            if (end == null) {
                if (destination.getText().length() > 0) {
                    destination.setError("Choose location from dropdown.");
                } else {
                    Toast.makeText(getContext(), "Please choose a destination.", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            progressDialog = ProgressDialog.show(getContext(), "Please wait.",
                    "Fetching route information.", true);


            int selectedPosition = spinner.getSelectedItemPosition();
            String type = mPlaceType[selectedPosition];

//            nearestUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + mLatitude + "," + mLongitude + "&radius=1000&types=" + type + "&sensor=true&key=AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
            nearestUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+start.latitude+","+start.longitude+"&radius=1000&types="+type+"&sensor=true&key=AIzaSyATQx0quIT732UvX6CjdxmgIdQQM5SKu7s";
            new PlacesTask().execute(nearestUrl);

            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(start, end)
                    .build();
            routing.execute();
        }


     */
/*   googleMapForPolylines.setOnPolygonClickListener(new com.google.android.gms.maps.GoogleMap.OnPolygonClickListener() {
            @Override
            public void onPolygonClick(Polygon polygon) {

            }
        });
*//*

    }

    @Override
    public void onRoutingFailure(RouteException e) {
        // The Routing request failed
        progressDialog.dismiss();
        if (e != null) {
            Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {
        // The Routing Request starts
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        //  progressDialog.dismiss();

        ArrayList<RouteModel> routeArrayList = new ArrayList<>();

        if (markerPointA != null && markerPointB != null && polyline != null) {
            markerPointA.remove();
            markerPointB.remove();

            for (int i = 0; i < polylines.size(); i++) {
                polyline = polylines.get(i);
                polyline.remove();
            }
            polylines.clear();
           */
/* for(Polyline line : polylines)
            {
                line.remove();
            }*//*

//            polyline.remove();
//            polylines.clear();
            //mMap.getPolylines().remove(polyOptions);
            //  polylines.remove(polyline);
        }
        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);

        mMap.moveCamera(center);


        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }


        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            RouteModel routeModel = new RouteModel();

            polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);

//            Log.d("", "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue());


            routeModel.setRouteId(i + 1);
            routeModel.setDistance((route.get(i).getDistanceValue()/1000));
            routeModel.setDuration((route.get(i).getDurationValue()/60));

            routeArrayList.add(routeModel);

//            route.get(i).getLatLgnBounds()
//            Toast.makeText(getContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
            Log.d("", "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue());
        }


        if (routeArrayList != null) {
            routeFragment.setVisibility(View.VISIBLE);

            routeColor1.setBackgroundColor(getResources().getColor(R.color.white));
            routeId1.setText("");
            routeDistance1.setText("");
            routeDuration1.setText("");

            routeColor2.setBackgroundColor(getResources().getColor(R.color.white));
            routeId2.setText("");
            routeDistance2.setText("");
            routeDuration3.setText("");

            routeColor3.setBackgroundColor(getResources().getColor(R.color.white));
            routeId3.setText("");
            routeDistance3.setText("");
            routeDuration3.setText("");

            for (int i = 0; i < routeArrayList.size(); i++) {

                int colorIndex = i % COLORS.length;

                RouteModel routeModelDisplay = new RouteModel();
                routeModelDisplay = routeArrayList.get(i);
//                polyOptions.color(getResources().getColor(COLORS[colorIndex]));
//                routeColor.setText(String.valueOf(COLORS[i]));
                if (i == 0) {
//                    routeColor1.setBackgroundColor(COLORS[colorIndex]);
                    routeColor1.setBackgroundColor(getResources().getColor(COLORS[colorIndex]));
                    routeId1.setText(String.valueOf(routeModelDisplay.getRouteId()));
                    routeDistance1.setText(String.valueOf(routeModelDisplay.getDistance()+" KM -"));
                    routeDuration1.setText(String.valueOf(routeModelDisplay.getDuration()+" Min"));
                }
                if (i == 1) {
//                    routeColor2.setBackgroundColor(COLORS[colorIndex]);
                    routeColor2.setBackgroundColor(getResources().getColor(COLORS[colorIndex]));
                    routeId2.setText(String.valueOf(routeModelDisplay.getRouteId()));
                    routeDistance2.setText(String.valueOf(routeModelDisplay.getDistance()+" KM -"));
                    routeDuration2.setText(String.valueOf(routeModelDisplay.getDuration()+" Min"));
                }
                if (i == 2) {
//                    routeColor3.setBackgroundColor(COLORS[colorIndex]);
                    routeColor3.setBackgroundColor(getResources().getColor(COLORS[colorIndex]));
                    routeId3.setText(String.valueOf(routeModelDisplay.getRouteId()));
                    routeDistance3.setText(String.valueOf(routeModelDisplay.getDistance()+" KM -"));
                    routeDuration3.setText(String.valueOf(routeModelDisplay.getDuration()+" Min"));
                }
            }
        }
        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue));
        markerPointA = mMap.addMarker(options);
        markerPointsList.add(markerPointA);


      */
/*  Uri gmmIntentUri = Uri.parse("geo:0,0?q=restaurants");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);*//*


        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green));
        markerPointB = mMap.addMarker(options);
        markerPointsList.add(markerPointB);

        for (Marker arrayListData : markerPointsList) {
            arrayListData.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
        }


    }

    @Override
    public void onRoutingCancelled() {
        Log.i(LOG_TAG, "Routing was cancelled.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.v(LOG_TAG, connectionResult.toString());
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }



    protected SupportMapFragment createMapFragment() {
        return SupportMapFragment.newInstance();
    }

    public void intitilizeActionbar(View v) {
        final ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        // the view that contains the new clearable autocomplete text view
        final ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);

        // start with the text view hidden in the action bar
//        mBottomLayout.setVisibility(View.INVISIBLE);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        mViewAget = (LinearLayout.LayoutParams) mViewA.getLayoutParams();
        mViewBget = (LinearLayout.LayoutParams) mViewB.getLayoutParams();

        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));

        mFab.setVisibility(View.INVISIBLE);
        mViewB.setVisibility(View.INVISIBLE);
        searchBox.setVisibility(View.INVISIBLE);
        searchIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleSearch(false);
            }
        });

        searchBox.setOnClearListener(new ClearableAutoCompleteTextView.OnClearListener() {

            @Override
            public void onClear() {
                toggleSearch(true);
            }
        });

        searchBox.setAdapter(mGooglePlaceAdapter);
        searchBox.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // handle clicks on search resaults here
                searchString = (String) adapterView.getItemAtPosition(position);
                searchLocation = true;
                //keyboard closing window code starts 19-Feb-2014

                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                //keyboard closing window code ends 19-Feb-2014

                //  Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

                if (searchLocationMarker) {
                    marker2.remove();
                }
                List<Address> addresList = null;
                Geocoder geocoder = new Geocoder(getActivity());
                try {
                    addresList = geocoder.getFromLocationName(searchString, 3);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Address address = addresList.get(0);

                LatLng locationInfo = new LatLng(address.getLatitude(), address.getLongitude());
                initCamera(locationInfo);
                moveToNewLocation(locationInfo);
                marker2 = mMap.addMarker(new MarkerOptions().position(locationInfo).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(searchString));
                // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationInfo, 1));
                searchLocationMarker = true;
            }

        });
    }

    protected void toggleSearch(boolean reset) {
      */
/*  LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.search_bar, null);*//*


        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);
        ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        if (mConnectionDetector.isOnline()) {
            if (reset) {
                // hide search box and show search icon
                searchBox.setText("");
                searchBox.setVisibility(View.GONE);
                searchIcon.setVisibility(View.VISIBLE);
                // hide the keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
            } else {
                // hide search icon and show search box
                searchIcon.setVisibility(View.GONE);
                searchBox.setVisibility(View.VISIBLE);
                searchBox.requestFocus();
                // show the keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchBox, InputMethodManager.SHOW_FORCED);
            }
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConnectionDetector = new ConnectionDetector(this.getActivity());
        mGooglePlaceAdapter = new GooglePlacesAutocompleteAdapter(this.getActivity(), R.layout.search_list);
        layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = layoutInflater.inflate(R.layout.search_bar, null);
        dbHelper = new DbHelper(getContext());

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.actionbaritems, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
           */
/* case R.id.action_search:
                // search action

                return true;*//*

            case R.id.action_gpslocater:
                if (mConnectionDetector.isOnline()) {
                    LocationFound();
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_sortFilter:
                if (mConnectionDetector.isOnline()) {
                    filterEnable = true;
                    SortFilterPopUp(getActivity());

                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_routeFilter:
                if (mConnectionDetector.isOnline()) {
                    if(nearestPlacesFragment.getVisibility() == View.GONE ||
                            nearestPlacesFragment.getVisibility() == View.INVISIBLE ) {
                        nearestPlacesFragment.setVisibility(View.VISIBLE);
                    }
                    RouteFound();
                } else {
//                    showAlertBox(getResources().getString(R.string.netNotAvl));
                    Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl),
                            Toast.LENGTH_SHORT).show();
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void RouteFound(){
        if(mViewB.getVisibility() == View.VISIBLE  ) {
            mViewB.setVisibility(View.GONE);
        }
        if(routeFragment.getVisibility() == View.VISIBLE  ) {
            routeFragment.setVisibility(View.GONE);
        }
        currentLocationFlag = true;
        destination.setText("");
        currentLocationLatLng();
//                    start = yourLocationLatLng;

        showStartTextView.setVisibility(View.VISIBLE);
        showStartAutoCompleteView.setVisibility(View.GONE);
        routeMapDisplay();


        yourLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLocationFlag = false;
                showStartTextView.setVisibility(View.GONE);
                starting.setText("");
                destination.setText("");
                showStartAutoCompleteView.setVisibility(View.VISIBLE);
                routeMapDisplay();
            }
        });

    }

    public void SortFilterPopUp(final Activity context) {

        final PopupWindow popup = new PopupWindow(layout, WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT, true);

        popup.showAtLocation(layout, Gravity.CENTER, 10, 10);

        filterSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                switchListeners();

                new GetJsonString().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL
                new GetJsonTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);//Constants.JSON_URL


            }
        });
    }

    */
/**
     * A class, to download Google Places
     *//*

    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try {
                Log.d("Background Task", "Places Task");
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result) {
            ParserTask parserTask = new ParserTask();
            Log.d("onPost", "Places Task");
            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }

    }

    */
/**
     * A method to download json data from url
     *//*

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            Log.d("Download url", "Places Task");
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
//            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }


    */
/**
     * A class to parse the Google Places in JSON format
     *//*

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                Log.d("Background Task", "parser Task");
                jObject = new JSONObject(jsonData[0]);

                */
/** Getting the parsed data as a List construct *//*

                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String, String>> list) {

            if (list == null) {
                Toast.makeText(getContext(), "No Data Found", Toast.LENGTH_SHORT).show();
            } else {

                Marker marker;

                // Clears all the existing markers

                if (markerArrayList != null) {
                    for (int i = 0; i < markerArrayList.size(); i++) {
                        Marker markerRemove = markerArrayList.get(i);
                        markerRemove.remove();
                    }
                    markerArrayList.clear();
                }

           */
/* Marker marker = null;
            marker.remove();*//*


                Log.d("onPostExecute", "parser Task");
                for (int i = 0; i < list.size(); i++) {

                    // Creating a marker
                    MarkerOptions markerOptions = new MarkerOptions();

                    // Getting a place from the places list
                    HashMap<String, String> hmPlace = list.get(i);

                    // Getting latitude of the place
                    double lat = Double.parseDouble(hmPlace.get("lat"));

                    // Getting longitude of the place
                    double lng = Double.parseDouble(hmPlace.get("lng"));

                    // Getting name
                    String name = hmPlace.get("place_name");

                    // Getting vicinity
                    String vicinity = hmPlace.get("vicinity");

                    LatLng latLng = new LatLng(lat, lng);

                    // Setting the position for the marker
                    markerOptions.position(latLng);

                    // Setting the title for the marker.
                    //This will be displayed on taping the marker
                    markerOptions.title(name + " : " + vicinity);

                    // Placing a marker on the touched position
//                    marker = googleMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.atmmarker)));
                    marker = googleMap.addMarker(markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);
                    googleMap.animateCamera(zoom);
                    markerArrayList.add(marker);

                 */
/*   ClusteringSettings clusteringSettings1 = new ClusteringSettings();
                    clusteringSettings1.enabled(false);
                    googleMap.setClustering(clusteringSettings1);*//*


//                    marker.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
                }

                for (Marker arrayListData : markerArrayList) {
                    arrayListData.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
                }
            }
            progressDialog.dismiss();
        }
    }


    private void switchListeners() {
        filterString = "";
        if(connectorTypeString.contains("")){
            connectorTypeString = "ev_connector_type=";
        }

        if(networkString.contains("")){
            networkString = "ev_network=";
        }
        if (mConnectionDetector.isOnline() && networkCheckerFlag) {

            if (statusSwitch.isChecked()) {
                statusSwitch.setChecked(true);
                statusString = "status=E";
            } else {
                statusSwitch.setChecked(false);
                statusString ="";
            }

            if (priceSwitch.isChecked()) {
                priceSwitch.setChecked(true);
                priceString = "cards_accepted=all";
            } else {
                priceSwitch.setChecked(false);
                priceString = "";
            }

            if (leve1Switch.isChecked()) {
                leve1Switch.setChecked(true);
                connectorTypeString += "NEMA515,NEMA520,NEMA1450,";
            } else {
                leve1Switch.setChecked(false);
                connectorTypeString +="";
            }

            if (leve2Switch.isChecked()) {
                leve2Switch.setChecked(true);
                connectorTypeString += "J1772,";
            } else {
                leve2Switch.setChecked(false);
                connectorTypeString += "";
            }

            if (dcFastSwitch.isChecked()) {
                dcFastSwitch.setChecked(true);
                connectorTypeString += "CHADEMO,";
            } else {
                dcFastSwitch.setChecked(false);
                connectorTypeString += "";
            }

            if (dcFastComboSwitch.isChecked()) {
                dcFastComboSwitch.setChecked(true);
                connectorTypeString += "J1772COMBO,";
            } else {
                dcFastComboSwitch.setChecked(false);
                connectorTypeString +="";
            }

            if (teslaSwitch.isChecked()) {
                teslaSwitch.setChecked(true);
                connectorTypeString += "TESLA,";
            } else {
                teslaSwitch.setChecked(false);
                connectorTypeString +="";
            }

            if (CPSwitch.isChecked()) {
                CPSwitch.setChecked(true);
                networkString += "ChargePoint Network,";
            } else {
                CPSwitch.setChecked(false);
                networkString += "";
            }

            if (BlkSwitch.isChecked()) {
                BlkSwitch.setChecked(true);
                networkString += "Blink Network,";
            } else {
                BlkSwitch.setChecked(false);
                networkString += "";
            }

            if (SCSwitch.isChecked()) {
                SCSwitch.setChecked(true);
                networkString += "SemaCharge Network,";
            } else {
                SCSwitch.setChecked(false);
                networkString += "";
            }

            if (evgoSwitch.isChecked()) {
                evgoSwitch.setChecked(true);
                networkString += "eVgo Network,";
            } else {
                evgoSwitch.setChecked(false);
                networkString += "";
            }

            if (AVSwitch.isChecked()) {
                AVSwitch.setChecked(true);
                networkString += "AeroVironment Network,";
            } else {
                AVSwitch.setChecked(false);
                networkString += "";
            }

            if (EVCSwitch.isChecked()) {
                EVCSwitch.setChecked(true);
                networkString += "EV Connect,";
            } else {
                EVCSwitch.setChecked(false);
                networkString += "";
            }

            if (EVSESwitch.isChecked()) {
                EVSESwitch.setChecked(true);
                networkString += "EVSE LLC WebNet,";
            } else {
                EVSESwitch.setChecked(false);
                networkString += "";
            }

            if (GLSwitch.isChecked()) {
                GLSwitch.setChecked(true);
                networkString += "Greenlots,";
            } else {
                GLSwitch.setChecked(false);
                networkString += "";
            }

            if (OPCSwitch.isChecked()) {
                OPCSwitch.setChecked(true);
                networkString += "OpConnect,";
            } else {
                OPCSwitch.setChecked(false);
                networkString += "";
            }

            if (RASwitch.isChecked()) {
                RASwitch.setChecked(true);
                networkString += "RechargeAccess,";
            } else {
                RASwitch.setChecked(false);
                networkString += "";
            }

            if (SPSwitch.isChecked()) {
                SPSwitch.setChecked(true);
                networkString += "Shorepower,";
            } else {
                SPSwitch.setChecked(false);
                networkString += "";
            }

            filterString = "&"+statusString+"&"+priceString+"&"+connectorTypeString+"&"+networkString;
            filterString.replaceAll(" ", "%20");
            if(filterString.contains("")){

            }
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }

    }


    public void LocationFound() {
        boolean locationUpdate = true;
        gpsTracker = new GPSTracker(getActivity().getApplicationContext());

        Log.d("marker", " : " + singleMarker);
        if (singleMarker) {
            if (marker1 != null) {
                marker1.remove();
//                marker1 = null;
            }
        }
        if (gpsTracker.canGetLocation) {
            Log.d("gps tracker", " : " + gpsTracker.canGetLocation);
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

            mViewB.setVisibility(View.INVISIBLE);
            mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));

            currentLoc = new LatLng(latitude, longitude);
//            marker1 = mMap.addMarker(new MarkerOptions().position(currentLoc));

            String locationString = getAddressFromLatLng(currentLoc);
            if (locationString.contains("Please try again")) {
                Toast.makeText(getActivity(), "Location service failed. Please try again", Toast.LENGTH_SHORT).show();
            } else {
                marker1 = mMap.addMarker(new MarkerOptions().position(currentLoc).
                        icon(*/
/*BitmapDescriptorFactory.fromResource(R.drawable.atm)*//*

                                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).
                        title(locationString));
                marker1.setClusterGroup(ClusterGroup.NOT_CLUSTERED);


//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 18));
                initCamera(currentLoc);
                moveToNewLocation(currentLoc);
                singleMarker = true;
            }
        } else {
            showSettingsAlert();
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getActivity().startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void initCamera(LatLng temp) {
//        if (searchLocation) {
        position = CameraPosition.builder()
                .target(temp)
                .zoom(12f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();
        searchLocation = false;
     */
/*   } else {
            position = CameraPosition.builder()
                    .target(temp)
                    .zoom(15f)
                    .bearing(0.0f)
                    .tilt(0.0f)
                    .build();
        }*//*

    }

    private void moveToNewLocation(LatLng temp) {
//        mMap.addMarker(new MarkerOptions().position(temp).title(getAddressFromLatLng(temp)));
//        float zoom = mMap.getMinZoomLevelNotClustered(marker1);
        mViewB.setVisibility(View.INVISIBLE);
        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);
        if (circle != null) {
            circle.remove();
        }
        circleOptions = new CircleOptions().center(temp)
                .radius(1000).strokeColor(Color.parseColor("#696464")).fillColor(Color.TRANSPARENT).strokeWidth(4);
        circle = mMap.addCircle(circleOptions);
//        mMap.addCircle(new CircleOptions().center(temp).radius(1000).strokeColor(Color.parseColor("#696464")).fillColor(Color.parseColor("lightgray")).strokeWidth(4));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLoc));
    }

    private String getAddressFromLatLng(LatLng latLng) {
        Geocoder geocoder = new Geocoder(this.getActivity());

//        Location address;
        String returnAdress = "Please try again";
        try {
            List<Address> address = geocoder
                    .getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (address != null && (address.size() != 0)) {
                Address fetchedAddress = address.get(0);
                if (fetchedAddress != null) {
                    returnAdress = fetchedAddress.getAddressLine(0);
                }
               */
/* for(int i=0; i<fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(.getAddressLine(i)).append("\n");
                }*//*


            }

        } catch (IOException e) {
        }

        return returnAdress;
    }

    protected void setUpBroadCast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");

        ReceivefrmSERVICE = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                networkCheckerFlag = NetworkChecker.getConnectivityStatusCheck(context);

                if (mConnectionDetector.isOnline() && networkCheckerFlag) {
//            if(hashMapData== null && hashMapDataAll == null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        if (!reCheckFlag) {
                            reCheckFlag = true;
                            new GoogleMapsTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            new GoogleMapsTaskAllStations().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                        }
                    } else {
                        new GoogleMapsTask().execute();
                    }
//            }
                } else {
                  */
/*  if(!alertDialog.isShowing()) {
                        showAlertBox(getResources().getString(R.string.netNotAvl));
                    }*//*

                    Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                }
            }
        };

        getActivity().registerReceiver(ReceivefrmSERVICE, filter);
    }

    protected void setUpMap() {
        if (!firstCHeck) {
            if (!networkCheckerFlag) {
                networkCheckerFlag = true;
            }*/
/*else{
                networkCheckerFlag = false;
            }*//*

            firstCHeck = true;
        }


    }
   */
/* @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        mClusterManager = new ClusterManager<>(this.getActivity(), mMap);
//        mClusterManager.setRenderer(new CustomRenderer<LatLang>(this.getActivity(), mMap, mClusterManager));
        if(mConnectionDetector.isOnline()) {
            new GoogleMapsTask().execute();
        }else {
            showAlertBox(getResources().getString(R.string.netNotAvl));
        }
    }*//*


    public class GoogleMapsTask extends AsyncTask<String, String, List<MapModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<MapModel> doInBackground(String... params) {

            try {
                if (mConnectionDetector.isOnline() && networkCheckerFlag) {

//                response = mJsonUtil.postMethod(OnePocketUrls.LOGIN_AUTHENTICATE_URL(),null,loginHeader);
                    response = mJsonUtil.getMethod(OnePocketUrls.GOOGLEMAPS_URL(), null, null);
                    OnePocketLog.d("singup resp----------", response);
                    int count = 0;
                    ArrayList<MapModel> mapArrayList = new ArrayList<MapModel>();

                    hashMapData = new HashMap<String, MapModel>();

                    JSONArray temp = new JSONArray(response);
                    Log.d("~~~~~~~~~~~~~", "m");
                    Log.d("MainActivity.java", "temp : " + temp);

                    for (int i = 0; i < temp.length(); i++) {
                        // for(int i=0;i<5;i++){
                        JSONObject e = temp.getJSONObject(i);
                        MapModel mapModel = new MapModel();
                        Log.d("Main Activity", "Json Object Value " + i + " : " + e);

                        JSONObject data = e.getJSONObject("data");
                        Log.d("Json Object ", "data " + data);

                        // mapModel.setId(i);
                        mapModel.setConn2PortType((String) data.getString(CONN2_PORT_TYPE));
                        mapModel.setStation((String) data.getString(STATION));
                        mapModel.setConn1portType((String) data.getString(CONN1_PORT_TYPE));
                        mapModel.setStationStatus((String) data.getString(STATION_STATUS));
                        mapModel.setConn2portLevel((String) data.getString(CONN2_PORT_LEVEL));
                        mapModel.setConn1portLevel((String) data.getString(CONN1_PORT_LEVEL));
                        mapModel.setPortQuantity((String) data.getString(PORT_QUANTITY));


                        LatLang latLang = new LatLang();

                        if ((!e.getJSONArray("latLng").get(0).equals(null)) && (!e.getJSONArray("latLng").get(1).equals(null))) {
                            latLang.setLattitude((Double) e.getJSONArray("latLng").get(0));
                            latLang.setLongitude((Double) e.getJSONArray("latLng").get(1));

                        } else {
                            latLang.setLattitude(1.1);
                            latLang.setLongitude(1.1);

                        }
                        mapModel.setLatLang(latLang);

                        mapArrayList.add(mapModel);
                        Log.d("map arraylist " + count, " : " + mapArrayList);
                        count++;
                    }
                    Log.d("map arraylist", " : " + mapArrayList);
                    Log.d("map ArrayList size :", " " + mapArrayList.size());

                    return mapArrayList;

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                OnePocketLog.d("singup error----------", e.toString());
//                showAlertBox(getResources().getString(R.string.netNotAvl));
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
//                new ExceptionHandling("Signup - BackGroundTask1 - "+e.toString());
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<MapModel> result) {
            super.onPostExecute(result);

            //To do need the set the data to list
            //   LatLng mapDisplay;
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                for (int i = 0; i < result.size(); i++) {
                    MapModel mMapModel = new MapModel();
                    mMapModel = result.get(i);

                    LatLng latlng = new LatLng(mMapModel.getLatLang().getLattitude(), mMapModel.getLatLang().getLongitude());
                    LatLang latLang = new LatLang(latlng.latitude, latlng.longitude);

                    MarkerOptions markerOptions = new MarkerOptions().position(latlng);

                    Marker marker = mMap.addMarker(markerOptions);
                    markerListTell.add(marker);
                    hashMapData.put(marker.getId(), mMapModel);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
                    mMap.getUiSettings().setMapToolbarEnabled(true);
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                }
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    routeFragment.setVisibility((View.GONE));
                    nearestPlacesFragment.setVisibility(View.GONE);
                    if (marker.isCluster()) {
                        if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                            declusterify(marker);
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                    return false;
                }
            });
            //information window code starts
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    Log.d("clicked", "view");
                    view = null;
                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                        for (Map.Entry m : hashMapData.entrySet()) {
                            MapModel mm = new MapModel();
                            mm = (MapModel) m.getValue();


                          */
/*  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.clear();
                            editor1.putString(CHARGER_ID, m.getKey().toString());
                            editor1.commit();*//*

                        */
/*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*//*

                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
                                chargerKey = m.getKey().toString();
//                                mBottomLayout.setVisibility(View.VISIBLE);

                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                nearestPlacesFragment.setVisibility(View.GONE);
                                routeFragment.setVisibility(View.GONE);

                                stationName.setText(mm.getStation());
                                markerCheck = "";
                                markerCheck = "Type1";
                                // stationAddress.setText(infoWindowModel.getStationAddress());
//                                chargers.setText(mm.getPortQuantity());
                                city.setText("");
                                levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                                levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    Log.d("data", " view " + view);
                    return view;
                }
            });

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng position) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    } else {
                        mViewB.setVisibility(View.INVISIBLE);
                        routeFragment.setVisibility(View.INVISIBLE);
                        nearestPlacesFragment.setVisibility(View.INVISIBLE);
                        mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));
                    }
                }
            });

            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if (!mConnectionDetector.isOnline()) {
//                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
//                        mMap.animateCamera(CameraUpdateFactory.zoomTo(0.0f));
//                        mBottomLayout.setVisibility(View.INVISIBLE);
                    }
                }
            });
            //information window code ends
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }

                    if (!mConnectionDetector.isOnline()) {
                        Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    public class GoogleMapsTaskAllStations extends AsyncTask<String, String, List<MapModelAll>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setCancelable(false);
//            progressDialog.setTitle("Connecting to Server");
//            progressDialog.setMessage("This process can take a few seconds. Please wait....");
                progressDialog.setMessage("Please wait....");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.show();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected List<MapModelAll> doInBackground(String... params) {

          */
/*  try {
                responseAll = mJsonUtil.getMethod(OnePocketUrls.GOOGLEMAPS_ALL_STATIONS_URL(),null,null);
                OnePocketLog.d("GOOGLEMAPS_ALL_STATIONS_URL----------", responseAll);

                ArrayList<MapModelAll> mapArrayList = new ArrayList<MapModelAll>();
                hashMapDataAll = new HashMap<String, MapModelAll>();


                JsonFactory factory = new MappingJsonFactory();

                Log.d("Json factory: ", "" + factory);

                JsonParser jsonParser = factory.createParser(responseAll);

                Log.d("Json Parser: ", "" + jsonParser);

                JsonToken token;
                token = jsonParser.nextToken();
                Log.d("Json token: ", "" + token);
               *//*
*/
/* ArrayList objectArray = new ArrayList();*//*
*/
/*

                if (token != JsonToken.START_OBJECT) {
                    System.out.println("Error: root should be object: quiting.");
                }

                while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
                    String fieldName = jsonParser.getCurrentName();
                    Log.d("fieldName: ", "" + fieldName);
                    // move from field name to field value
                    token = jsonParser.nextToken();
                    Log.d("token: ", "" + token);
                    if(fieldName == null){
                        return null;
                    }
                    if (fieldName.equals(Constants.JSON_ARRAY_NAME)) {
                        if (token == JsonToken.START_ARRAY) {
                            // For each of the records in the array
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                // read the record into a tree model,
                                // this moves the parsing position to the end of it
                                JsonNode node = jsonParser.readValueAsTree();
                                Log.d("node: ", "" + node);

                                MapModelAll mapModel = new MapModelAll();

                                mapModel.setAccess_days_time(node.get(Constants.ACCESS_DAYS_TIME).textValue());
                                mapModel.setCards_accepted(node.get(Constants.CARDS_ACCEPTED).textValue());
                                mapModel.setDate_last_confirmed(node.get(Constants.DATE_LAST_CONFIRMED).textValue());
                                mapModel.setExpected_date(node.get(Constants.EXPECTED_DATE).textValue());
                                mapModel.setFuel_type_code(node.get(Constants.FUEL_TYPE_CODE).textValue());
                                mapModel.setId(node.get(Constants.ID).intValue());
                                mapModel.setGroups_with_access_code(node.get(Constants.GROUPS_WITH_ACCESS_CODE).textValue());

                                mapModel.setOpen_date(node.get(Constants.OPEN_DATE).textValue());
                                mapModel.setOwner_type_code(node.get(Constants.OWNER_TYPE_CODE).textValue());
                                mapModel.setStatus_code(node.get(Constants.STATUS_CODE).textValue());
                                mapModel.setStation_name(node.get(Constants.STATION_NAME).textValue());
                                mapModel.setStation_phone(node.get(Constants.STATION_PHONE).textValue());
                                mapModel.setUpdated_at(node.get(Constants.UPDATED_AT).textValue());
                                mapModel.setGeocode_status(node.get(Constants.GEOCODE_STATUS).textValue());

                                LatLangAll latLang = new LatLangAll();
                                latLang.setLatitude(node.get(Constants.LATITUDE).doubleValue());
                                latLang.setLongitude(node.get(Constants.LONGITUDE).doubleValue());
                                mapModel.setLatLang(latLang);

                                mapModel.setCity(node.get(Constants.CITY).textValue());
                                mapModel.setIntersection_directions(node.get(Constants.INTERSECTION_DIRECTION).textValue());
                                mapModel.setPlus4(node.get(Constants.PLUS4).textValue());
                                mapModel.setState(node.get(Constants.STATE).textValue());
                                mapModel.setStreet_address(node.get(Constants.STREET_ADDRESS).textValue());
                                mapModel.setZip(node.get(Constants.ZIP).textValue());
                                mapModel.setBd_blends(node.get(Constants.BD_BLENDS).textValue());
                                mapModel.setE85_blender_pump(node.get(Constants.E85_BLENDER_PUMP).textValue());


                                //added code starts on 9-3-16
                                List connectorList = new ArrayList();

                                Log.d("Ev Size", " : " + node.get(Constants.EV_CONNECTOR_TYPES).size());
                                for(int count=0;count<node.get(Constants.EV_CONNECTOR_TYPES).size();count++) {
                                    connectorList.add( node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                    Log.d("Ev node type", " : " + node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                }

                                mapModel.setConnectorTypesList(connectorList);
                                //added code ends on 9-3-16

                                mapModel.setEv_connector_types(node.get(Constants.EV_CONNECTOR_TYPES).textValue());
                                mapModel.setEv_dc_fast_num(node.get(Constants.EV_DC_FAST_NUM).intValue());
                                mapModel.setEv_level1_evse_num(node.get(Constants.EV_LEVEL1_EVSE_NUM).intValue());
                                mapModel.setEv_level2_evse_num(node.get(Constants.EV_LEVEL2_EVSE_NUM).intValue());
                                mapModel.setEv_network(node.get(Constants.EV_NETWORK).textValue());
                                mapModel.setEv_network_web(node.get(Constants.EV_NETWORK_WEB).textValue());
                                mapModel.setEv_other_evse(node.get(Constants.EV_OTHER_EVSE).textValue());
                                mapModel.setHy_status_link(node.get(Constants.HY_STATUS_LINK).textValue());
                                mapModel.setLpg_primary(node.get(Constants.LPG_PRIMARY).textValue());
                                mapModel.setNg_fill_type_code(node.get(Constants.NG_FILL_TYPE_CODE).textValue());
                                mapModel.setNg_psi(node.get(Constants.NG_PSI).textValue());
                                mapModel.setNg_vehicle_class(node.get(Constants.NG_VEHICLE_CLASS).textValue());


                                mapArrayList.add(mapModel);
                                //  dbHelper.addStationDetails(mapModel);


                            }
                        } else {
                            System.out.println("Error: records should be an array: skipping.");
                            jsonParser.skipChildren();
                        }
                    } else {
                        System.out.println("Unprocessed property: " + fieldName);
                        jsonParser.skipChildren();
                    }
                }

                return mapArrayList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;*//*

            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
//                ArrayList<MapModelAll> dbList = dbHelper.getAllStationRecords();
                dbList = dbHelper.getAllStationRecords();
                Log.d("mapModel list size", ":" + dbList.size());
               */
/* if (filterEnable) {
                    filterEnable = false;
                    if (markerList.size() > 0) {
                        markerList.clear();
                    }
                    return dbFilterList;
                } else {
                    return dbList;
                }*//*

                return dbList;

            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
               */
/* if(!alertDialog.isShowing()) {
                    showAlertBox(getResources().getString(R.string.netNotAvl));
                }*//*

            }
            return null;
        }


        @Override
        protected void onPostExecute(List<MapModelAll> result) {
            super.onPostExecute(result);

            if (result == null) {
                return;
            }
            progressDialog.dismiss();
            Log.d("on PostExecute", " : " + result);
            Log.d("result list size", ":" + result.size());

            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                // int i;
                for (int i = 0; i < result.size(); i++) {
                    MapModelAll mMapModel = new MapModelAll();
                    mMapModel = result.get(i);
                    // MyMarker myMarker = new MyMarker();
                    LatLng latlng = new LatLng(mMapModel.getLatLang().getLatitude(), mMapModel.getLatLang().getLongitude());

                    MarkerOptions markerOptions = new MarkerOptions().position(latlng);

                    marker = mMap.addMarker(markerOptions);
                    markerList.add(marker);

                    hashMapDataAll.put(marker.getId(), mMapModel);
                }
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();

            mViewB.setVisibility(View.GONE);
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    routeFragment.setVisibility((View.GONE));
                    nearestPlacesFragment.setVisibility(View.GONE);

                    if (marker.isCluster()) {
                        declusterify(marker);
                        return true;
                    }
                    return false;
                }
            });

//            progressDialog.dismiss();
            // Log.d("Total Markers ", ":" + i);

            // information window code starts

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    view = null;
                    Log.d("declusterifiedMarkers", "" + declusterifiedMarkers);

                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                        for (Map.Entry m : hashMapDataAll.entrySet()) {
                            MapModelAll mm = new MapModelAll();
                            mm = (MapModelAll) m.getValue();

                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
//                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());

                                chargerKey = m.getKey().toString();
                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                nearestPlacesFragment.setVisibility(View.GONE);
                                routeFragment.setVisibility(View.GONE);

                                mapListData = new ArrayList<MapListModel>();

                                MapListModel mapListModel = new MapListModel();

                                mapListModel.setAccess_days_time(mm.getAccess_days_time());
                                mapListModel.setFuel_type_code(mm.getFuel_type_code());
                                mapListModel.setGroups_with_access_code(mm.getGroups_with_access_code());
                                mapListModel.setStation_name(mm.getStation_name());
                                mapListModel.setStation_phone(mm.getStation_phone());
                                mapListModel.setStatus_code(mm.getStatus_code());
                                mapListModel.setCity(mm.getCity());
                                mapListModel.setState(mm.getState());
                                mapListModel.setStreet_address(mm.getStreet_address());
                                mapListModel.setZip(mm.getZip());
                                mapListModel.setBd_blends(mm.getBd_blends());
                                mapListModel.setE85_blender_pump(mm.getE85_blender_pump());
                                markerCheck = "";
                                markerCheck = "Type2";

                               */
/* List connectorList = null;
                                StringBuffer sb = new StringBuffer();
                                // Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());
                                for (int count = 0; count < mm.getConnectorTypesList().size() ; count++) {
                                    Log.d("get data", ": " + mm.getConnectorTypesList().get(count));
                                    sb.append(mm.getConnectorTypesList().get(count) + "  ");
                                    // connectorList = new ArrayList();
                                    //   connectorList.add(mm.getConnectorTypesList().get(count));
                                }
                                sb.toString();
                                Log.d("sb value", " : " + sb.toString());

                                Log.d("cL at fragment", ": " + connectorList);*//*

                                //   mapListModel.setConnectorTypesList(connectorList);
                                Log.d("cL at after", ": " + mapListModel.getConnectorTypesList());
                                mapListModel.setEv_connector_types(mm.getEv_connector_types());
                                // mapListModel.setEv_connector_types(mm.getEv_connector_types());
                                mapListModel.setEv_dc_fast_num(mm.getEv_dc_fast_num());
                                mapListModel.setEv_level1_evse_num(mm.getEv_level1_evse_num());
                                mapListModel.setEv_level2_evse_num(mm.getEv_level2_evse_num());
                                mapListModel.setEv_network(mm.getEv_network());
                                mapListModel.setEv_network_web(mm.getEv_network_web());
                                mapListModel.setEv_other_evse(mm.getEv_other_evse());
                                mapListModel.setHy_status_link(mm.getHy_status_link());
                                mapListModel.setLpg_primary(mm.getLpg_primary());
                                mapListModel.setNg_fill_type_code(mm.getNg_fill_type_code());
                                mapListModel.setNg_psi(mm.getNg_psi());
                                mapListModel.setNg_vehicle_class(mm.getNg_vehicle_class());

                                mapListData.add(mapListModel);

                                Log.d("MapModelList", ": " + mapListData.get(0));

                            */
/*for(int i= 0;i<mapListData.size();i++) {
                                MapListModel mapListModel1 = new MapListModel();
                                mapListModel1 = mapListData.get(i);
                                Log.d("MLD getStatus_code", "" + mapListModel1.getStatus_code());
                                Log.d("MLD getStation_name", "" + mapListModel1.getStation_name());
                                Log.d("MLD getCity", "" + mapListModel1.getCity());
                                Log.d("MLD connector_types", "" + mapListModel1.getEv_connector_types());
                                Log.d("MLD level1_evse_num", "" + mapListModel1.getEv_level1_evse_num());
                                Log.d("MLD level2_evse_num", "" + mapListModel1.getEv_level2_evse_num());
                            }*//*


                                stationName.setText(mm.getStation_name());
                                //    statusCode.setText(mm.getStatus_code());
                                city.setText(mm.getCity());
                                //   evconnectortypes.setText(mm.getEv_connector_types());
                                //   evlevel1evsenum.setText(mm.getEv_level1_evse_num());
                                // evlevel2evsenum.setText(mm.getEv_level1_evse_num()+"/"+mm.getEv_level2_evse_num());
                                //  evconnectortypes.setText((String) mm.getConnectorTypesList().get(0));

                               */
/* //add code starts 9-3-16
                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());
                                for (int count = 0; count < mm.getConnectorTypesList().size(); count++) {
                                    //  String temp = (String) mm.getConnectorTypesList().get(count);
                                    Log.d("get data", ": " + mm.getConnectorTypesList().get(count));
                                }
                                //add code starts 9-3-16*//*


                           */
/* Log.d("getStation_name", ":" + mm.getStation_name());
                            Log.d("getCity", ":" + mm.getCity());
                            Log.d("getEv_connector_types",":"+ mm.getEv_connector_types());
                            Log.d("getEv_level1_evse_num", ":" + mm.getEv_level1_evse_num());
                            Log.d("getEv_level2_evse_num",":"+ mm.getEv_level2_evse_num());*//*

                            }
                        }
                        //
                        for (Map.Entry m : hashMapData.entrySet()) {
                            MapModel mm = new MapModel();
                            mm = (MapModel) m.getValue();


                          */
/*  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.clear();
                            editor1.putString(CHARGER_ID, m.getKey().toString());
                            editor1.commit();*//*

                        */
/*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*//*

                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
                                chargerKey = m.getKey().toString();
//                                mBottomLayout.setVisibility(View.VISIBLE);

                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                nearestPlacesFragment.setVisibility(View.GONE);
//                                String temp = initStationName(mm.getLatLang());

                                stationName.setText(mm.getStation());
                                // stationAddress.setText(infoWindowModel.getStationAddress());
//                                chargers.setText(mm.getPortQuantity());
                                city.setText("null");
                                levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                                levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    return view;
                }
            });
            // information window code end

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }
                }
            });
            Log.d("Info window success ", "");


            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {


                @Override
                public void onCameraChange(CameraPosition cameraPosition) {

                    //  Toast.makeText(getContext(), "Please Wait...", Toast.LENGTH_SHORT).show();

                    LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

                   */
/* Log.d("LatlngbOunds",":"+bounds);
                    Log.d("markerList",":"+markerList);
                    Log.d("markerList size",":"+markerList.size());*//*


                    for (Marker marker1 : markerList) {
                        if (bounds.contains(marker1.getPosition())) {
                            marker1.setVisible(true);
                        } else {
                            marker1.setVisible(false);
                        }
                    }
                    for (Marker markerT : markerListTell) {
                        if (bounds.contains(markerT.getPosition())) {
                            markerT.setVisible(true);
                        } else {
                            markerT.setVisible(false);
                        }
                    }
                }
            });
        }
    }

    */
/* filter async task*//*


    public class GetJsonString extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... url1) {

            //public String jsonString(String url1){

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                String jsonurl = Constants.JSON_URL + filterString;
                if (mConnectionDetector.isOnline()) {
                    url = new URL(jsonurl.replaceAll(" ","%20"));
//                    url = new URL(Constants.DUP_JSON_URL);
                } else {
                    return "network";
                }
                if (mConnectionDetector.isOnline()) {
                    connection = (HttpURLConnection) url.openConnection();
                    //connection.connect();
                } else {
                    return "network";
                }

                if (mConnectionDetector.isOnline()) {
                    connection.connect();
                } else {
                    return "network";
                }

                if (mConnectionDetector.isOnline()) {
                    stream = connection.getInputStream();
                } else {
                    return "network";
                }

                if (mConnectionDetector.isOnline()) {
                    reader = new BufferedReader(new InputStreamReader(stream));
                } else {
                    return "network";
                }

                if (mConnectionDetector.isOnline()) {
                    buffer = new StringBuffer();
                } else {
                    return "network";
                }

                while ((line = reader.readLine()) != null) {
                    if (mConnectionDetector.isOnline()) {
                        buffer.append(line);
                        finalJson = buffer.toString();
                    } else {
                        return "network";
                    }
                }

                try {
                    if (mConnectionDetector.isOnline()) {
                        jsonObject = new JSONObject(finalJson);
                        jsonArray = jsonObject.getJSONArray(Constants.JSON_ARRAY_NAME);
                        jsonArrayLength = jsonArray.length();

                        Log.d("Json Array Length", " : " + jsonArrayLength);
                    } else {
                        return "network";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return finalJson;

        }
    }

    public class GetJsonTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(getActivity());
            progDailog.setMessage("Loading... Please Wait");
//            progDailog.getWindow().setGravity(Gravity.BOTTOM);
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected String doInBackground(String... params) {


            try {

                if(mConnectionDetector.isOnline()) {
                    factory = new MappingJsonFactory();
                }else{
                    return "network";
                }

                Log.d("Json factory: ", "" + factory);
                if(mConnectionDetector.isOnline()) {
                    jsonParser = factory.createParser(finalJson);
                    Log.d("Json Parser: ", "" + jsonParser);
                }else{
                    return "network";
                }


                if(mConnectionDetector.isOnline()) {
                    token = jsonParser.nextToken();
                }else{
                    return "network";
                }

                Log.d("Json token: ", "" + token);
                /*/
/* ArrayList objectArray = new ArrayList();*//*
*/
/*

                if (token != JsonToken.START_OBJECT) {
                    if(mConnectionDetector.isOnline()) {
                        System.out.println("Error: root should be object: quiting.");
                    }else{
                        return "network";
                    }
                }

                while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
                    if(mConnectionDetector.isOnline()) {
                        fieldName = jsonParser.getCurrentName();
                        Log.d("fieldName: ", "" + fieldName);
                        // move from field name to field value
                        token = jsonParser.nextToken();
                        Log.d("token: ", "" + token);

                        if (fieldName.equals(Constants.JSON_ARRAY_NAME)) {
                            if (token == JsonToken.START_ARRAY) {
                                if (mConnectionDetector.isOnline()) {

                                    // For each of the records in the array
                                    Long startTime = System.currentTimeMillis();
                                    while (jsonParser.nextToken() != JsonToken.END_ARRAY) {

                                        if (mConnectionDetector.isOnline()) {
                                            // read the record into a tree model,
                                            // this moves the parsing position to the end of it
                                            JsonNode node = jsonParser.readValueAsTree();
                                            Log.d("node: ", "" + node);

                                            MapModelAll mapModel = new MapModelAll();

                                            mapModel.setAccess_days_time(node.get(Constants.ACCESS_DAYS_TIME).textValue());
                                            mapModel.setCards_accepted(node.get(Constants.CARDS_ACCEPTED).textValue());
                                            mapModel.setDate_last_confirmed(node.get(Constants.DATE_LAST_CONFIRMED).textValue());
                                            mapModel.setExpected_date(node.get(Constants.EXPECTED_DATE).textValue());
                                            mapModel.setFuel_type_code(node.get(Constants.FUEL_TYPE_CODE).textValue());
                                            mapModel.setId(node.get(Constants.ID).intValue());
                                            mapModel.setGroups_with_access_code(node.get(Constants.GROUPS_WITH_ACCESS_CODE).textValue());

                                            mapModel.setOpen_date(node.get(Constants.OPEN_DATE).textValue());
                                            mapModel.setOwner_type_code(node.get(Constants.OWNER_TYPE_CODE).textValue());
                                            mapModel.setStatus_code(node.get(Constants.STATUS_CODE).textValue());
                                            mapModel.setStation_name(node.get(Constants.STATION_NAME).textValue());
                                            mapModel.setStation_phone(node.get(Constants.STATION_PHONE).textValue());
                                            mapModel.setUpdated_at(node.get(Constants.UPDATED_AT).textValue());
                                            mapModel.setGeocode_status(node.get(Constants.GEOCODE_STATUS).textValue());

                                            LatLangAll latLang = new LatLangAll();
                                            latLang.setLatitude(node.get(Constants.LATITUDE).doubleValue());
                                            latLang.setLongitude(node.get(Constants.LONGITUDE).doubleValue());
                                            mapModel.setLatLang(latLang);

                                            mapModel.setCity(node.get(Constants.CITY).textValue());
                                            mapModel.setIntersection_directions(node.get(Constants.INTERSECTION_DIRECTION).textValue());
                                            mapModel.setPlus4(node.get(Constants.PLUS4).textValue());
                                            mapModel.setState(node.get(Constants.STATE).textValue());
                                            mapModel.setStreet_address(node.get(Constants.STREET_ADDRESS).textValue());
                                            mapModel.setZip(node.get(Constants.ZIP).textValue());
                                            mapModel.setBd_blends(node.get(Constants.BD_BLENDS).textValue());
                                            mapModel.setE85_blender_pump(node.get(Constants.E85_BLENDER_PUMP).textValue());



                                            List connectorList = new ArrayList();

                                            for (int count = 0; count < node.get(Constants.EV_CONNECTOR_TYPES).size(); count++) {
                                                connectorList.add(node.get(Constants.EV_CONNECTOR_TYPES).get(count));
                                            }

                                            mapModel.setConnectorTypesList(connectorList);

                                            StringBuffer sb = new StringBuffer();
                                            for (int count = 0; count < mapModel.getConnectorTypesList().size(); count++) {
                                                sb.append(mapModel.getConnectorTypesList().get(count) + " \t");
                                            }


                                            mapModel.setEv_connector_types(sb.toString());
                                            mapModel.setEv_dc_fast_num(node.get(Constants.EV_DC_FAST_NUM).intValue());
                                            mapModel.setEv_level1_evse_num(node.get(Constants.EV_LEVEL1_EVSE_NUM).intValue());
                                            mapModel.setEv_level2_evse_num(node.get(Constants.EV_LEVEL2_EVSE_NUM).intValue());
                                            mapModel.setEv_network(node.get(Constants.EV_NETWORK).textValue());
                                            mapModel.setEv_network_web(node.get(Constants.EV_NETWORK_WEB).textValue());
                                            mapModel.setEv_other_evse(node.get(Constants.EV_OTHER_EVSE).textValue());
                                            mapModel.setHy_status_link(node.get(Constants.HY_STATUS_LINK).textValue());
                                            mapModel.setLpg_primary(node.get(Constants.LPG_PRIMARY).textValue());
                                            mapModel.setNg_fill_type_code(node.get(Constants.NG_FILL_TYPE_CODE).textValue());
                                            mapModel.setNg_psi(node.get(Constants.NG_PSI).textValue());
                                            mapModel.setNg_vehicle_class(node.get(Constants.NG_VEHICLE_CLASS).textValue());


                                            mapArrayListFilter.add(mapModel);

                                        } else {
                                            return "network";
                                        }
                                    }

//                                    dbHelper.bulkInsert(mapArrayList);

                                    Long endTime = System.currentTimeMillis();
                                    Log.d("Total Time", ":" + (endTime - startTime));
                                }else {
                                    return "network";
                                }
                            } else {
                                System.out.println("Error: records should be an array: skipping.");
                                jsonParser.skipChildren();
                            }
                        } else {
                            System.out.println("Unprocessed property: " + fieldName);
                            jsonParser.skipChildren();
                        }
                    }else{
                        return "network";
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return finalJson;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progDailog.dismiss();

            if(result.equals("network")||result.equalsIgnoreCase("null")||result.equals("")){
                Toast.makeText(getActivity(), R.string.netNotAvl, Toast.LENGTH_SHORT).show();
//                showAlertBox(getResources().getString(R.string.netNotAvl));
            }

//            mapArrayListFilter
            if((markerList.size() >0 ) && (markerListTell.size() > 0)){
                mMap.clear();
            }
            if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                // int i;
                for (int i = 0; i < mapArrayListFilter.size(); i++) {
                    MapModelAll mMapModel = new MapModelAll();
                    mMapModel = mapArrayListFilter.get(i);
                    // MyMarker myMarker = new MyMarker();
                    LatLng latlng = new LatLng(mMapModel.getLatLang().getLatitude(), mMapModel.getLatLang().getLongitude());

                    MarkerOptions markerOptions = new MarkerOptions().position(latlng);

                    marker = mMap.addMarker(markerOptions);
                    markerListFilter.add(marker);

                    hashMapFilter.put(marker.getId(), mMapModel);
                }
            }else{
                Toast.makeText(getContext(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
            }
            updateClustering();

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    routeFragment.setVisibility((View.GONE));
                    nearestPlacesFragment.setVisibility(View.GONE);
                    if (marker.isCluster()) {
                        declusterify(marker);
                        return true;
                    }
                    return false;
                }
            });

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                View view;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    view = null;
                    Log.d("declusterifiedMarkers", "" + declusterifiedMarkers);

                    if (mConnectionDetector.isOnline() && networkCheckerFlag) {
                        for (Map.Entry m : hashMapFilter.entrySet()) {
                            MapModelAll mm = new MapModelAll();
                            mm = (MapModelAll) m.getValue();

                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
//                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());

                                chargerKey = m.getKey().toString();
                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
                                nearestPlacesFragment.setVisibility(View.GONE);
                                routeFragment.setVisibility(View.GONE);

                                mapListDataFilter = new ArrayList<MapListModel>();

                                MapListModel mapListModel = new MapListModel();

                                mapListModel.setAccess_days_time(mm.getAccess_days_time());
                                mapListModel.setFuel_type_code(mm.getFuel_type_code());
                                mapListModel.setGroups_with_access_code(mm.getGroups_with_access_code());
                                mapListModel.setStation_name(mm.getStation_name());
                                mapListModel.setStation_phone(mm.getStation_phone());
                                mapListModel.setStatus_code(mm.getStatus_code());
                                mapListModel.setCity(mm.getCity());
                                mapListModel.setState(mm.getState());
                                mapListModel.setStreet_address(mm.getStreet_address());
                                mapListModel.setZip(mm.getZip());
                                mapListModel.setBd_blends(mm.getBd_blends());
                                mapListModel.setE85_blender_pump(mm.getE85_blender_pump());
                                markerCheck="";
                                markerCheck = "Type2";

                               */
/* List connectorList = null;
                                StringBuffer sb = new StringBuffer();
                                // Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());
                                for (int count = 0; count < mm.getConnectorTypesList().size() ; count++) {
                                    Log.d("get data", ": " + mm.getConnectorTypesList().get(count));
                                    sb.append(mm.getConnectorTypesList().get(count) + "  ");
                                    // connectorList = new ArrayList();
                                    //   connectorList.add(mm.getConnectorTypesList().get(count));
                                }
                                sb.toString();
                                Log.d("sb value", " : " + sb.toString());

                                Log.d("cL at fragment", ": " + connectorList);*//*

                                //   mapListModel.setConnectorTypesList(connectorList);
                                Log.d("cL at after", ": " + mapListModel.getConnectorTypesList());
                                mapListModel.setEv_connector_types(mm.getEv_connector_types());
                                // mapListModel.setEv_connector_types(mm.getEv_connector_types());
                                mapListModel.setEv_dc_fast_num(mm.getEv_dc_fast_num());
                                mapListModel.setEv_level1_evse_num(mm.getEv_level1_evse_num());
                                mapListModel.setEv_level2_evse_num(mm.getEv_level2_evse_num());
                                mapListModel.setEv_network(mm.getEv_network());
                                mapListModel.setEv_network_web(mm.getEv_network_web());
                                mapListModel.setEv_other_evse(mm.getEv_other_evse());
                                mapListModel.setHy_status_link(mm.getHy_status_link());
                                mapListModel.setLpg_primary(mm.getLpg_primary());
                                mapListModel.setNg_fill_type_code(mm.getNg_fill_type_code());
                                mapListModel.setNg_psi(mm.getNg_psi());
                                mapListModel.setNg_vehicle_class(mm.getNg_vehicle_class());

                                mapListDataFilter.add(mapListModel);

                                Log.d("MapModelList", ": " + mapListDataFilter.get(0));

                            */
/*for(int i= 0;i<mapListData.size();i++) {
                                MapListModel mapListModel1 = new MapListModel();
                                mapListModel1 = mapListData.get(i);
                                Log.d("MLD getStatus_code", "" + mapListModel1.getStatus_code());
                                Log.d("MLD getStation_name", "" + mapListModel1.getStation_name());
                                Log.d("MLD getCity", "" + mapListModel1.getCity());
                                Log.d("MLD connector_types", "" + mapListModel1.getEv_connector_types());
                                Log.d("MLD level1_evse_num", "" + mapListModel1.getEv_level1_evse_num());
                                Log.d("MLD level2_evse_num", "" + mapListModel1.getEv_level2_evse_num());
                            }*//*


                                stationName.setText(mm.getStation_name());
                                //    statusCode.setText(mm.getStatus_code());
                                city.setText(mm.getCity());
                                //   evconnectortypes.setText(mm.getEv_connector_types());
                                //   evlevel1evsenum.setText(mm.getEv_level1_evse_num());
                                // evlevel2evsenum.setText(mm.getEv_level1_evse_num()+"/"+mm.getEv_level2_evse_num());
                                //  evconnectortypes.setText((String) mm.getConnectorTypesList().get(0));

                               */
/* //add code starts 9-3-16
                                Log.d("Connector type list", " : " + mm.getConnectorTypesList().size());
                                for (int count = 0; count < mm.getConnectorTypesList().size(); count++) {
                                    //  String temp = (String) mm.getConnectorTypesList().get(count);
                                    Log.d("get data", ": " + mm.getConnectorTypesList().get(count));
                                }
                                //add code starts 9-3-16*//*


                           */
/* Log.d("getStation_name", ":" + mm.getStation_name());
                            Log.d("getCity", ":" + mm.getCity());
                            Log.d("getEv_connector_types",":"+ mm.getEv_connector_types());
                            Log.d("getEv_level1_evse_num", ":" + mm.getEv_level1_evse_num());
                            Log.d("getEv_level2_evse_num",":"+ mm.getEv_level2_evse_num());*//*

                            }
                        }
                        // for tellus charging info
                        for (Map.Entry m : hashMapData.entrySet()) {
                            MapModel mm = new MapModel();
                            mm = (MapModel) m.getValue();


                          */
/*  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.clear();
                            editor1.putString(CHARGER_ID, m.getKey().toString());
                            editor1.commit();*//*

                        */
/*Log.d("markerId", " " + marker.getId());
                        Log.d("data", " " + m.getValue());*//*

                            if ((marker.getId()).equals(m.getKey())) {
                                Log.d("markerId", " " + marker.getId());
                                Log.d("data", " " + m.getKey());
                                chargerKey = m.getKey().toString();
//                                mBottomLayout.setVisibility(View.VISIBLE);

                                mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width, mViewAget.height, mViewAget.weight));
                                mViewB.setLayoutParams(new LinearLayout.LayoutParams(mViewBget.width, mViewBget.height, mViewBget.weight));
                                mViewB.setVisibility(View.VISIBLE);
//                                String temp = initStationName(mm.getLatLang());

                                stationName.setText(mm.getStation());
                                // stationAddress.setText(infoWindowModel.getStationAddress());
//                                chargers.setText(mm.getPortQuantity());
                                city.setText("null");
                                levelType1.setText(mm.getConn1portLevel() + " / " + mm.getConn1portType());
                                levelType2.setText(mm.getConn1portLevel() + " / " + mm.getConn2PortType());

//                            Log.d("closeButton", "clicked " + closewindow);
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
                    }

                    return view;
                }
            });
            // information window code end

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    if (marker.isInfoWindowShown()) {
                        marker.hideInfoWindow();
                    }
                }
            });
            Log.d("Info window success ", "");


            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {


                @Override
                public void onCameraChange(CameraPosition cameraPosition) {

                    //  Toast.makeText(getContext(), "Please Wait...", Toast.LENGTH_SHORT).show();

                    LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

                  */
/*  for (Marker marker1 : markerList) {
                        if(!filterEnable) {
                            if (bounds.contains(marker1.getPosition())) {
                                marker1.setVisible(true);
                            } else {
                                marker1.setVisible(false);
                            }
                        }
                    }
                    for (Marker markerT : markerListTell) {
                        if (bounds.contains(markerT.getPosition())) {
                            markerT.setVisible(true);
                        } else {
                            markerT.setVisible(false);
                        }
                    }*//*

                    for (Marker markerT : markerListFilter) {
                        if (bounds.contains(markerT.getPosition())) {
                            markerT.setVisible(true);
                        } else {
                            markerT.setVisible(false);
                        }
                    }
                }
            });

        }
    }

    */
/*filter async task end*//*


    private String initStationName(LatLang latLang) {

        double lat = latLang.getLattitude();
        double lon = latLang.getLongitude();
        mLatLng = new LatLng(lat, lon);

        String locationString = getAddressFromLatLng(mLatLng);
        return locationString;
    }

    private void declusterify(Marker cluster) {
//        clusterifyMarkers();
        if (mConnectionDetector.isOnline() && networkCheckerFlag) {
            Toast.makeText(getContext(), "Please Wait....", Toast.LENGTH_SHORT).show();
            declusterifiedMarkers = cluster.getMarkers();
//        LatLng clusterPosition = cluster.getPosition();
//        double distance = calculateDistanceBetweenMarkers();
//        double currentDistance = -declusterifiedMarkers.size() / 2 * distance;
    */
/*    for (Marker marker : declusterifiedMarkers) {
            marker.setData(marker.getPosition());
            marker.setClusterGroup(ClusterGroup.NOT_CLUSTERED);
            LatLng newPosition = new LatLng(marker.getPosition().latitude,marker.getPosition().longitude*//*
*/
/*clusterPosition.latitude, clusterPosition.longitude + currentDistance*//*
*/
/*);
            marker.animatePosition(newPosition);
//            currentDistance += distance;
        }*//*

//        mBottomLayout.setVisibility(View.INVISIBLE);
            mViewB.setVisibility(View.INVISIBLE);
            mViewA.setLayoutParams(new LinearLayout.LayoutParams(mViewAget.width + mViewBget.width, mViewAget.height + mViewBget.height));
            LatLngBounds.Builder builder = LatLngBounds.builder();
//        List<Marker> val= checkDupMarkers(declusterifiedMarkers);
            Set set = new HashSet();
            List newList = new ArrayList<Marker>();
            for (Iterator iter = declusterifiedMarkers.iterator(); iter.hasNext(); ) {
                Marker element = (Marker) iter.next();
                if (set.add(element.getPosition()))
                    newList.add(element);
            }
            declusterifiedMarkers.clear();
            declusterifiedMarkers.addAll(newList);

     */
/*   HashSet<Marker> listToSet = new HashSet<Marker>(declusterifiedMarkers);
        List<Marker> listWithoutDuplicates = new ArrayList<Marker>(listToSet);*//*


            for (Marker marker : declusterifiedMarkers) {
//            if(!marker.equals(marker1)) {
                builder.include(marker.getPosition());
//            }
            }
            final LatLngBounds bounds = builder.build();
        */
/*int zoomLevel;
        zoomLevel = getBoundsZoomLevel(bounds,);*//*

            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(adjustBoundsForMaxZoomLevel(bounds),
                    getResources().getDimensionPixelSize(R.dimen.padding)));
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.netNotAvl), Toast.LENGTH_SHORT).show();
        }
//        mMap.animateCamera(CameraUpdateFactory.zoomBy(zoomLevel));

        */
/*if(mMap.getCameraPosition().zoom > 4.0f){
           mMap.animateCamera(CameraUpdateFactory.zoom);
        }*//*

    }

  */
/*  private List<Marker> checkDupMarkers( List<Marker> decluster){
        Set<Marker> s = new TreeSet<Marker>(new Comparator<Marker>() {
            @Override
            public int compare(Marker lhs, Marker rhs) {
                if(lhs.getId().equalsIgnoreCase(rhs.getId())){
                    return 0;
                }
                return 1;
            }
        });
        s.addAll(decluster);
        return decluster;
    }*//*


    private LatLngBounds adjustBoundsForMaxZoomLevel(LatLngBounds bounds) {
        LatLng sw = bounds.southwest;
        LatLng ne = bounds.northeast;
        double deltaLat = Math.abs(sw.latitude - ne.latitude);
        double deltaLon = Math.abs(sw.longitude - ne.longitude);

        final double zoomN = 0.005; // minimum zoom coefficient
        if (deltaLat < zoomN) {
            sw = new LatLng(sw.latitude - (zoomN - deltaLat / 2), sw.longitude);
            ne = new LatLng(ne.latitude + (zoomN - deltaLat / 2), ne.longitude);
            bounds = new LatLngBounds(sw, ne);
        } else if (deltaLon < zoomN) {
            sw = new LatLng(sw.latitude, sw.longitude - (zoomN - deltaLon / 2));
            ne = new LatLng(ne.latitude, ne.longitude + (zoomN - deltaLon / 2));
            bounds = new LatLngBounds(sw, ne);
        }

        return bounds;
    }

    public int getBoundsZoomLevel(LatLngBounds bounds, int mapWidthPx, int mapHeightPx) {

        LatLng ne = bounds.northeast;
        LatLng sw = bounds.southwest;

        double latFraction = (latRad(ne.latitude) - latRad(sw.latitude)) / Math.PI;

        double lngDiff = ne.longitude - sw.longitude;
        double lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        double latZoom = zoom(mapHeightPx, WORLD_PX_HEIGHT, latFraction);
        double lngZoom = zoom(mapWidthPx, WORLD_PX_WIDTH, lngFraction);

        int result = Math.min((int) latZoom, (int) lngZoom);
        return Math.min(result, ZOOM_MAX);
    }

    private double latRad(double lat) {
        double sin = Math.sin(lat * Math.PI / 180);
        double radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }

    private double zoom(int mapPx, int worldPx, double fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / LN2);
    }


    */
/*private void clusterifyMarkers() {
        if (declusterifiedMarkers != null) {
            for (Marker marker : declusterifiedMarkers) {
                LatLng position = marker.getPosition();
                marker.setPosition(position);
                marker.setClusterGroup(ClusterGroup.DEFAULT);
            }
            declusterifiedMarkers = null;
        }
    }*//*


    void updateClustering() {
        if (mMap == null) {
            return;
        }
        ClusteringSettings clusteringSettings = new ClusteringSettings();
        clusteringSettings.clusterOptionsProvider(new ImageClusterOptionsProvider(getResources()));

        double clusterSize = CLUSTER_SIZES[2];
        clusteringSettings.clusterSize(clusterSize);
        mMap.setClustering(clusteringSettings);
    }
    //map search code starts

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        searchString = (String) adapterView.getItemAtPosition(position);

        //keyboard closing window code starts 19-Feb-2014

        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

        //keyboard closing window code ends 19-Feb-2014

        //  Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

        if (searchLocationMarker) {
            if (marker2 != null) {
                marker2.remove();
            }
        }
        List<Address> addresList = null;
        Geocoder geocoder = new Geocoder(this.getActivity());
        try {
            addresList = geocoder.getFromLocationName(searchString, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Address address = addresList.get(0);

        LatLng locationInfo = new LatLng(address.getLatitude(), address.getLongitude());
        initCamera(locationInfo);
        moveToNewLocation(locationInfo);
        marker2 = mMap.addMarker(new MarkerOptions().position(locationInfo).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title(searchString));
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationInfo, 1));
        searchLocationMarker = true;

    }


    public void showAlertBox(final String msg) {

        alertDialog.setTitle(R.string.Alert);
        alertDialog.setIcon(R.drawable.ic_dialog_alert_holo_light);
        alertDialog.setMessage(msg);
        alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ImageView searchIcon = (ImageView) v.findViewById(R.id.search_icon);
        getActivity().unregisterReceiver(ReceivefrmSERVICE);
        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) v.findViewById(R.id.search_box);
        if (searchIcon != null) {
            searchIcon.setVisibility(View.GONE);
        }

        if (searchBox != null) {
            searchBox.setVisibility(View.GONE);
        }
    }
// map search code ends
}
*/
