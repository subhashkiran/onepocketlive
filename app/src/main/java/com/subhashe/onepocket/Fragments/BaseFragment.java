package com.subhashe.onepocket.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.SupportMapFragment;
import com.subhashe.onepocket.R;

/**
 * Created by SubhashE on 2/25/2016.
 */
public abstract class BaseFragment extends Fragment {
    private SupportMapFragment mapFragment;
    protected GoogleMap mMap;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createMapFragmentIfNeeded();
        setUpBroadNeeded();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void createMapFragmentIfNeeded() {
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (com.androidmapsextensions.SupportMapFragment) fm.findFragmentById(R.id.map);
        if (mapFragment == null) {
            mapFragment = createMapFragment();
            FragmentTransaction tx = fm.beginTransaction();
            tx.add(R.id.map, mapFragment);
            tx.commit();
        }
    }

    protected SupportMapFragment createMapFragment() {
        return com.androidmapsextensions.SupportMapFragment.newInstance();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null ) {
            mMap = mapFragment.getExtendedMap();
//            googleMap = mapFragment.getExtendedMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }
    private void setUpBroadNeeded() {
                setUpBroadCast();
    }

    protected abstract void setUpMap();
    protected abstract void setUpBroadCast();
}
