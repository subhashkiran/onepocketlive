package com.subhashe.onepocket.DataBase;



import com.subhashe.onepocket.Core.MapModelAll;

import java.util.ArrayList;

/**
 * Created by Subhash on 2/25/2016.
 */
public interface StationListener {
    public void addStationDetails(MapModelAll mapModel);

    public ArrayList<MapModelAll> getAllStationRecords();
}
