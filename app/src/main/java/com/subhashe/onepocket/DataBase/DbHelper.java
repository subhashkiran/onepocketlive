package com.subhashe.onepocket.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;


import com.subhashe.onepocket.Core.Constants;
import com.subhashe.onepocket.Core.LatLangAll;
import com.subhashe.onepocket.Core.MapModelAll;
import com.subhashe.onepocket.Core.MapModelFilters;

import java.util.ArrayList;


/**
 * Created by praveen on 1/21/2016.
 */
public class DbHelper implements StationListener {

    private static final String DATABASE_NAME = "FuelStationsDb.db";
    private static final String TABLE_NAME = "FuelStationsTable";
    private static final int DATABASE_VERSION =1;
    int i= 0;
    public boolean flag = true;

    public static final String CREATE_QUERY = "create table "+TABLE_NAME+
            " ("+ Constants.ACCESS_DAYS_TIME+" varchar(255), "+Constants.CARDS_ACCEPTED+" varchar(255), "
            +Constants.DATE_LAST_CONFIRMED+" varchar(255), "+Constants.EXPECTED_DATE+" varchar(255),"+
            Constants.FUEL_TYPE_CODE+" varchar(255), "+Constants.ID+" integer primary key,"+
            Constants.GROUPS_WITH_ACCESS_CODE+" varchar(255), "+Constants.OPEN_DATE+" varchar(255), "+
            Constants.OWNER_TYPE_CODE+" varchar(255), "+Constants.STATUS_CODE+" varchar(255), "+
            Constants.STATION_NAME+" varchar(255), "+Constants.STATION_PHONE+" varchar(255), "+
            Constants.UPDATED_AT+" varchar(255),"+Constants.GEOCODE_STATUS+" varchar(255),"+
            Constants.LATITUDE+" varchar(255),"+Constants.LONGITUDE+" varchar(255), "+
            Constants.CITY+" varchar(255), "+Constants.INTERSECTION_DIRECTION+" varchar(255), "+
            Constants.PLUS4+" varchar(255), "+Constants.STATE+" varchar(255), "+
            Constants.STREET_ADDRESS+" varchar(255), "+Constants.ZIP+" varchar(255),"+
            Constants.BD_BLENDS+" varchar(255), "+Constants.E85_BLENDER_PUMP+" varchar(255), "+
            Constants.EV_CONNECTOR_TYPES+" varchar(255), "+Constants.EV_DC_FAST_NUM+" varchar(255), "+
            Constants.EV_LEVEL1_EVSE_NUM+" varchar(255), "+Constants.EV_LEVEL2_EVSE_NUM+" varchar(255), "+
            Constants.EV_NETWORK+" varchar(255), "+Constants.EV_NETWORK_WEB+" varchar(255),"+
            Constants.EV_OTHER_EVSE+" varchar(255),"+Constants.HY_STATUS_LINK+" varchar(255), "+
            Constants.LPG_PRIMARY+" varchar(255), "+Constants.NG_FILL_TYPE_CODE+" varchar(255), "+
            Constants.NG_PSI+" varchar(255),"+Constants.NG_VEHICLE_CLASS+" varchar(255))";


    private static final String DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE_NAME;
    //   private static final String INSERT_TABLE = "INSERT INTO "+TABLE_NAME+" VALUES (1,'PRAVEEN',25)";

    private final Context context;

    private DatabaseHelper DbHelper;
    private SQLiteDatabase myDataBase;


    public DbHelper(Context context) {
        //super(context, DATABASE_NAME, null , DATABASE_VERSION);
        this.context = context;
        DbHelper = new DatabaseHelper(context);
    }


    private static class DatabaseHelper extends SQLiteOpenHelper {
        Context context;

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("on create starts", "");
           // db.execSQL(DROP_TABLE);
            db.execSQL(CREATE_QUERY);
            Log.d("Query", " : " + CREATE_QUERY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(DROP_TABLE);
                Log.d("db droped success", "");
                onCreate(db);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                Log.d("db droped error","");
            }
        }
    }

    // ---opens the database---
    public DbHelper open() throws SQLException {
        myDataBase = DbHelper.getWritableDatabase();
        return this;
    }

    // ---closes the database---
    public void close() {
        DbHelper.close();
    }

    /*
     * To Begin Transaction
     */
    public void beginTransaction() {
        myDataBase.beginTransaction();
       // addStationDetails(mapModel);

    }

    /*
     * To Rollback Transaction
     */
    public void rollbackTransaction() {
        myDataBase.endTransaction();
    }

    /*
     * To Commit Transaction
     */
    public void commitTransaction() {
        myDataBase.setTransactionSuccessful();
      //  myDataBase.endTransaction();

    }


/*    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("on create starts","");
        db.execSQL(CREATE_QUERY);
        Log.d("on create ends", "");
        Log.d("Query"," : "+CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TABLE);
            Log.d("db droped success", "");
            onCreate(db);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            Log.d("db droped error","");
        }
    }*/


    public void dropTable(){
        myDataBase.execSQL(DROP_TABLE);
    }

    @Override
    public void addStationDetails(MapModelAll mapModel) {

        open();
        // SQLiteDatabase db = this.getWritableDatabase();
      //  myDataBase = DbHelper.getWritableDatabase();

        try{
            ContentValues values = new ContentValues();
            //Log.e("problem", "" + values);
            // add insertion fields
            values.put(Constants.ACCESS_DAYS_TIME,mapModel.getAccess_days_time());
            values.put(Constants.CARDS_ACCEPTED,mapModel.getCards_accepted());
            values.put(Constants.DATE_LAST_CONFIRMED,mapModel.getDate_last_confirmed());
            values.put(Constants.EXPECTED_DATE,mapModel.getExpected_date());
            values.put(Constants.FUEL_TYPE_CODE,mapModel.getFuel_type_code());
            values.put(Constants.ID,mapModel.getId());
            values.put(Constants.GROUPS_WITH_ACCESS_CODE,mapModel.getGroups_with_access_code());
            values.put(Constants.OPEN_DATE,mapModel.getOpen_date());
            values.put(Constants.OWNER_TYPE_CODE,mapModel.getOwner_type_code());
            values.put(Constants.STATUS_CODE,mapModel.getStatus_code());
            values.put(Constants.STATION_NAME,mapModel.getStation_name());
            values.put(Constants.STATION_PHONE,mapModel.getStation_phone());
            values.put(Constants.UPDATED_AT,mapModel.getUpdated_at());
            values.put(Constants.GEOCODE_STATUS,mapModel.getGeocode_status());
            values.put(Constants.LATITUDE,mapModel.getLattitude());
            values.put(Constants.LONGITUDE,mapModel.getLongitude());
            values.put(Constants.CITY,mapModel.getCity());
            values.put(Constants.INTERSECTION_DIRECTION ,mapModel.getIntersection_directions());
            values.put(Constants.PLUS4,mapModel.getPlus4());
            values.put(Constants.STATE,mapModel.getState());
            values.put(Constants.STREET_ADDRESS,mapModel.getStreet_address());
            values.put(Constants.ZIP,mapModel.getZip());
            values.put(Constants.BD_BLENDS, mapModel.getBd_blends());
            values.put(Constants.E85_BLENDER_PUMP,mapModel.getE85_blender_pump());
            values.put(Constants.EV_CONNECTOR_TYPES,mapModel.getEv_connector_types());
            values.put(Constants.EV_DC_FAST_NUM,mapModel.getEv_dc_fast_num());
            values.put(Constants.EV_LEVEL1_EVSE_NUM,mapModel.getEv_level1_evse_num());
            values.put(Constants.EV_LEVEL2_EVSE_NUM,mapModel.getEv_level2_evse_num());
            values.put(Constants.EV_NETWORK,mapModel.getEv_network());
            values.put(Constants.EV_NETWORK_WEB,mapModel.getEv_network_web());
            values.put(Constants.EV_OTHER_EVSE ,mapModel.getEv_other_evse());
            values.put(Constants.HY_STATUS_LINK,mapModel.getHy_status_link());
            values.put(Constants.LPG_PRIMARY ,mapModel.getLpg_primary());
            values.put(Constants.NG_FILL_TYPE_CODE,mapModel.getNg_fill_type_code());
            values.put(Constants.NG_PSI,mapModel.getNg_psi());
            values.put(Constants.NG_VEHICLE_CLASS, mapModel.getNg_vehicle_class());

            beginTransaction();
            myDataBase.insert(TABLE_NAME, null, values);
            // db.insert(TABLE_NAME, null, values);
            Log.d("inserted successfully" + i, ""+values);
            i++;
            flag=false;
            commitTransaction();
            //  db.close();
        }catch (Exception e){
            Log.e("problem",e+"");
            rollbackTransaction();
        }
        finally{
            myDataBase.endTransaction();
        }
    }

    public void deleteTableData(){
       // String DELETE_TABLE = "delete table"+TABLE_NAME;

        myDataBase.delete(TABLE_NAME,null,null);
        Log.d("table data deleted","");
    }

    public void bulkInsert(ArrayList<MapModelAll> list){

        String INSERT_QUERY = "INSERT INTO "+TABLE_NAME+" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        open();

        SQLiteStatement statement = myDataBase.compileStatement(INSERT_QUERY);

        myDataBase.beginTransaction();

        try {
            for (MapModelAll mapModelList : list) {

                if(mapModelList.getAccess_days_time()==null){
                    statement.bindString(1, "NA");
                }else {
                    statement.bindString(1, mapModelList.getAccess_days_time());
                }

                if(mapModelList.getCards_accepted()==null){
                    statement.bindString(2, "NA");
                }else {
                    statement.bindString(2, mapModelList.getCards_accepted());
                }

                if(mapModelList.getDate_last_confirmed()==null){
                    statement.bindString(3, "NA");
                }else {
                    statement.bindString(3, mapModelList.getDate_last_confirmed());
                }

                if(mapModelList.getExpected_date()==null){
                    statement.bindString(4, "NA");
                }else {
                    statement.bindString(4, mapModelList.getExpected_date());
                }

                /*statement.bindString(1, mapModelList.getAccess_days_time());
                statement.bindString(2, mapModelList.getCards_accepted());
                statement.bindString(3, mapModelList.getDate_last_confirmed());
                statement.bindString(4, mapModelList.getExpected_date());*/

                if(mapModelList.getFuel_type_code()==null){
                    statement.bindString(5, "NA");
                }else {
                    statement.bindString(5, mapModelList.getFuel_type_code());
                }
               // statement.bindString(5, mapModelList.getFuel_type_code());

                if(String.valueOf(mapModelList.getId())==null){
                    statement.bindString(6, "NA");
                }else {
                    statement.bindLong(6, mapModelList.getId());
                }
                //statement.bindLong(6, mapModelList.getId());

                if(mapModelList.getGroups_with_access_code()==null){
                    statement.bindString(7, "NA");
                }else {
                    statement.bindString(7, mapModelList.getGroups_with_access_code());
                }
               //statement.bindString(7, mapModelList.getGroups_with_access_code());

                if(mapModelList.getOpen_date()==null){
                    statement.bindString(8, "NA");
                }else {
                    statement.bindString(8, mapModelList.getOpen_date());
                }
                //statement.bindString(8, mapModelList.getOpen_date());

                if(mapModelList.getOwner_type_code()==null){
                    statement.bindString(9, "NA");
                }else {
                    statement.bindString(9, mapModelList.getOwner_type_code());
                }
                //statement.bindString(9, mapModelList.getOwner_type_code());

                if(mapModelList.getStatus_code()==null){
                    statement.bindString(10, "NA");
                }else {
                    statement.bindString(10, mapModelList.getStatus_code());
                }
                //statement.bindString(10, mapModelList.getStatus_code());

                if(mapModelList.getStation_name()==null){
                    statement.bindString(11, "NA");
                }else {
                    statement.bindString(11, mapModelList.getStation_name());
                }
                //statement.bindString(11, mapModelList.getStation_name());

                if(mapModelList.getStation_phone()==null){
                    statement.bindString(12, "NA");
                }else {
                    statement.bindString(12, mapModelList.getStation_phone());
                }
                //statement.bindString(12, mapModelList.getStation_phone());

                if(mapModelList.getUpdated_at()==null){
                    statement.bindString(13, "NA");
                }else {
                    statement.bindString(13, mapModelList.getUpdated_at());
                }
                //statement.bindString(13, mapModelList.getUpdated_at());

                if(mapModelList.getGeocode_status()==null){
                    statement.bindString(14, "NA");
                }else {
                    statement.bindString(14, mapModelList.getGeocode_status());
                }
                //statement.bindString(14, mapModelList.getGeocode_status());

                if(mapModelList.getLattitude()==null){
                    statement.bindString(15, "NA");
                }else {
                    statement.bindDouble(15, mapModelList.getLattitude());
                }
                //statement.bindDouble(15, mapModelList.getLatLang().getLatitude());

                if(mapModelList.getLongitude()==null){
                    statement.bindString(16, "NA");
                }else {
                    statement.bindDouble(16, mapModelList.getLongitude());
                }
               // statement.bindDouble(16, mapModelList.getLatLang().getLongitude());

                if(mapModelList.getCity()==null){
                    statement.bindString(17, "NA");
                }else {
                    statement.bindString(17, mapModelList.getCity());
                }
                //statement.bindString(17, mapModelList.getCity());


                if(mapModelList.getIntersection_directions()==null){
                    statement.bindString(18, "NA");
                }else {
                    statement.bindString(18, mapModelList.getIntersection_directions());
                }
                //statement.bindString(18, mapModelList.getIntersection_directions());

                if(mapModelList.getPlus4()==null){
                    statement.bindString(19, "NA");
                }else {
                    statement.bindString(19, mapModelList.getPlus4());
                }
                // statement.bindString(19, mapModelList.getPlus4());

                if(mapModelList.getState()==null){
                    statement.bindString(20, "NA");
                }else {
                    statement.bindString(20, mapModelList.getState());
                }
                //statement.bindString(20, mapModelList.getState());

                if(mapModelList.getStreet_address()==null){
                    statement.bindString(21, "NA");
                }else {
                    statement.bindString(21, mapModelList.getStreet_address());
                }
                //statement.bindString(21, mapModelList.getStreet_address());

                if(mapModelList.getZip()==null){
                    statement.bindString(22, "NA");
                }else {
                    statement.bindString(22, mapModelList.getZip());
                }
                //statement.bindString(22, mapModelList.getZip());

                if(mapModelList.getBd_blends()==null){
                    statement.bindString(23, "NA");
                }else {
                    statement.bindString(23, mapModelList.getBd_blends());
                }

               //  statement.bindString(23, mapModelList.getBd_blends());

                if(mapModelList.getE85_blender_pump()==null){
                    statement.bindString(24, "NA");
                }else {
                    statement.bindString(24, mapModelList.getE85_blender_pump());
                }
                //statement.bindString(24, mapModelList.getE85_blender_pump());
                statement.bindString(25, mapModelList.getEv_connector_types());

                if(String.valueOf(mapModelList.getEv_dc_fast_num())==null){
                    statement.bindString(26, "NA");
                }else {
                    statement.bindLong(26, mapModelList.getEv_dc_fast_num());
                }
                //statement.bindLong(26, mapModelList.getEv_dc_fast_num());

                if(String.valueOf(mapModelList.getEv_level1_evse_num())==null){
                    statement.bindString(27, "NA");
                }else {
                    statement.bindLong(27, mapModelList.getEv_level1_evse_num());
                }
                //statement.bindLong(27, mapModelList.getEv_level1_evse_num());

                if(String.valueOf(mapModelList.getEv_level2_evse_num())==null){
                    statement.bindString(28, "NA");
                }else {
                    statement.bindLong(28, mapModelList.getEv_level2_evse_num());
                }
                //statement.bindLong(28, mapModelList.getEv_level2_evse_num());

                if(mapModelList.getEv_network()==null){
                    statement.bindString(29, "NA");
                }else {
                    statement.bindString(29, mapModelList.getEv_network());
                }
               // statement.bindString(29, mapModelList.getEv_network());

                if(mapModelList.getEv_network_web()==null){
                    statement.bindString(30, "NA");
                }else {
                    statement.bindString(30, mapModelList.getEv_network_web());
                }
                //statement.bindString(30, mapModelList.getEv_network_web());

                if(mapModelList.getEv_other_evse()==null){
                    statement.bindString(31, "NA");
                }else {
                    statement.bindString(31, mapModelList.getEv_other_evse());
                }
                //statement.bindString(31, mapModelList.getEv_other_evse());

                if(mapModelList.getHy_status_link()==null){
                    statement.bindString(32, "NA");
                }else {
                    statement.bindString(32, mapModelList.getHy_status_link());
                }
                //statement.bindString(32, mapModelList.getHy_status_link());

                if(mapModelList.getLpg_primary()==null){
                    statement.bindString(33, "NA");
                }else {
                    statement.bindString(33, mapModelList.getLpg_primary());
                }
               //statement.bindString(33, mapModelList.getLpg_primary());


                if(mapModelList.getNg_fill_type_code()==null){
                    statement.bindString(34, "NA");
                }else {
                    statement.bindString(34, mapModelList.getNg_fill_type_code());
                }
                //statement.bindString(34, mapModelList.getNg_fill_type_code());

                if(mapModelList.getNg_psi()==null){
                    statement.bindString(35, "NA");
                }else {
                    statement.bindString(35, mapModelList.getNg_psi());
                }
                //statement.bindString(35, mapModelList.getNg_psi());

                if(mapModelList.getNg_vehicle_class()==null){
                    statement.bindString(36, "NA");
                }else {
                    statement.bindString(36, mapModelList.getNg_vehicle_class());
                }
                //statement.bindString(36, mapModelList.getNg_vehicle_class());

                statement.execute();
                Log.d("inserted successfully" + i, "" + statement);
                i++;

            }
        }catch (Exception e){
            Log.e("problem",e+"");
            rollbackTransaction();
        }
        finally{
            flag=false;
            commitTransaction();
            myDataBase.endTransaction();
        }

    }


    @Override
    public ArrayList<MapModelAll> getAllStationRecords() {
        Log.d("all stations called","");
        // SQLiteDatabase db = this.getReadableDatabase();
        myDataBase = DbHelper.getReadableDatabase();

        ArrayList<MapModelAll> mapModelList = null;
        try{
            mapModelList = new ArrayList<MapModelAll>();
            String GETALLRECORDSQUERY = "SELECT * FROM "+TABLE_NAME;
            Cursor cursor = myDataBase.rawQuery(GETALLRECORDSQUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    MapModelAll mapModel = new MapModelAll();

                    // add retriving code
                    mapModel.setAccess_days_time(cursor.getString(0));
                    mapModel.setCards_accepted(cursor.getString(1));
                    mapModel.setDate_last_confirmed(cursor.getString(2));
                    mapModel.setExpected_date(cursor.getString(3));
                    mapModel.setFuel_type_code(cursor.getString(4));
                    mapModel.setId(cursor.getInt(5));
                    mapModel.setGroups_with_access_code(cursor.getString(6));

                    mapModel.setOpen_date(cursor.getString(7));
                    mapModel.setOwner_type_code(cursor.getString(8));
                    mapModel.setStatus_code(cursor.getString(9));
                    mapModel.setStation_name(cursor.getString(10));
                    mapModel.setStation_phone(cursor.getString(11));
                    mapModel.setUpdated_at(cursor.getString(12));
                    mapModel.setGeocode_status(cursor.getString(13));

                    mapModel.setLattitude(cursor.getDouble(14));
                    mapModel.setLongitude(cursor.getDouble(15));

                    /*LatLangAll latLang = new LatLangAll();
                    latLang.setLatitude(cursor.getDouble(14));
                    latLang.setLongitude(cursor.getDouble(15));
                    mapModel.setLatLang(latLang);*/

                    mapModel.setCity(cursor.getString(16));
                    mapModel.setIntersection_directions(cursor.getString(17));
                    mapModel.setPlus4(cursor.getString(18));
                    mapModel.setState(cursor.getString(19));
                    mapModel.setStreet_address(cursor.getString(20));
                    mapModel.setZip(cursor.getString(21));
                    mapModel.setBd_blends(cursor.getString(22));
                    mapModel.setE85_blender_pump(cursor.getString(23));
                    mapModel.setEv_connector_types(cursor.getString(24));
                    mapModel.setEv_dc_fast_num(cursor.getInt(25));
                    mapModel.setEv_level1_evse_num(cursor.getInt(26));
                    mapModel.setEv_level2_evse_num(cursor.getInt(27));
                    mapModel.setEv_network(cursor.getString(28));
                    mapModel.setEv_network_web(cursor.getString(29));
                    mapModel.setEv_other_evse(cursor.getString(30));
                    mapModel.setHy_status_link(cursor.getString(31));
                    mapModel.setLpg_primary(cursor.getString(32));
                    mapModel.setNg_fill_type_code(cursor.getString(33));
                    mapModel.setNg_psi(cursor.getString(34));
                    mapModel.setNg_vehicle_class(cursor.getString(35));

                    mapModelList.add(mapModel);
                }
            }
            // db.close();
        }catch (Exception e) {
            Log.e("error", e + "");
        }
        return mapModelList;
    }




    public ArrayList<MapModelFilters> getFreeStationRecords() {
        Log.d("free stations called","");
        // SQLiteDatabase db = this.getReadableDatabase();
        myDataBase = DbHelper.getReadableDatabase();

        ArrayList<MapModelFilters> freeStationsMapModelList = null;
        try{
            freeStationsMapModelList = new ArrayList<MapModelFilters>();
            String FREESTATIONSQUERY = "SELECT * FROM "+TABLE_NAME+" WHERE "+ Constants.CARDS_ACCEPTED+" = 'NA'";
            Cursor cursor = myDataBase.rawQuery(FREESTATIONSQUERY, null);
            if(!cursor.isLast())
            {
                i=0;
                while (cursor.moveToNext())
                {
                    MapModelFilters mapModel = new MapModelFilters();

                    // add retriving code
                    mapModel.setAccess_days_time(cursor.getString(0));
                    mapModel.setCards_accepted(cursor.getString(1));
                    mapModel.setDate_last_confirmed(cursor.getString(2));
                    mapModel.setExpected_date(cursor.getString(3));
                    mapModel.setFuel_type_code(cursor.getString(4));
                    mapModel.setId(cursor.getInt(5));
                    mapModel.setGroups_with_access_code(cursor.getString(6));

                    mapModel.setOpen_date(cursor.getString(7));
                    mapModel.setOwner_type_code(cursor.getString(8));
                    mapModel.setStatus_code(cursor.getString(9));
                    mapModel.setStation_name(cursor.getString(10));
                    mapModel.setStation_phone(cursor.getString(11));
                    mapModel.setUpdated_at(cursor.getString(12));
                    mapModel.setGeocode_status(cursor.getString(13));

                    /*LatLangAll latLang = new LatLangAll();
                    latLang.setLatitude(cursor.getDouble(14));
                    latLang.setLongitude(cursor.getDouble(15));
                    mapModel.setLatLang(latLang);*/

                    mapModel.setLattitude(cursor.getDouble(14));
                    mapModel.setLongitude(cursor.getDouble(15));

                    mapModel.setCity(cursor.getString(16));
                    mapModel.setIntersection_directions(cursor.getString(17));
                    mapModel.setPlus4(cursor.getString(18));
                    mapModel.setState(cursor.getString(19));
                    mapModel.setStreet_address(cursor.getString(20));
                    mapModel.setZip(cursor.getString(21));
                    mapModel.setBd_blends(cursor.getString(22));
                    mapModel.setE85_blender_pump(cursor.getString(23));
                    mapModel.setEv_connector_types(cursor.getString(24));
                    mapModel.setEv_dc_fast_num(cursor.getInt(25));
                    mapModel.setEv_level1_evse_num(cursor.getInt(26));
                    mapModel.setEv_level2_evse_num(cursor.getInt(27));
                    mapModel.setEv_network(cursor.getString(28));
                    mapModel.setEv_network_web(cursor.getString(29));
                    mapModel.setEv_other_evse(cursor.getString(30));
                    mapModel.setHy_status_link(cursor.getString(31));
                    mapModel.setLpg_primary(cursor.getString(32));
                    mapModel.setNg_fill_type_code(cursor.getString(33));
                    mapModel.setNg_psi(cursor.getString(34));
                    mapModel.setNg_vehicle_class(cursor.getString(35));

                    freeStationsMapModelList.add(mapModel);

                    Log.d("retrived successfully", " : " + freeStationsMapModelList.get(i));
                    i++;
                }
            }
            // db.close();
        }catch (Exception e) {
            Log.e("error", e + "");
        }
        return freeStationsMapModelList;
    }
}

