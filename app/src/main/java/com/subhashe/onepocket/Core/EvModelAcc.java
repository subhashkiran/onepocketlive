package com.subhashe.onepocket.Core;

public class EvModelAcc {
    private String year;
    private String description;
    private String model;
    private int accountId;
    private String make;
    private String vin;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getVehicleId() {
        return accountId;
    }

    public void setVehicleId(int vehicleId) {
        this.accountId = vehicleId;
    }
}
