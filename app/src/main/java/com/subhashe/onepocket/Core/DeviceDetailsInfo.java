package com.subhashe.onepocket.Core;

/**
 * Created by axxera on 17-04-2017.
 */

public class DeviceDetailsInfo {

    private int userId;
    private String appInfo;
    private String deviceName;
    private String deviceType;
    private String deviceVersion;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAppVersion() {
        return appInfo;
    }

    public void setAppVersion(String appInfo) {
        this.appInfo = appInfo;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }
}
