package com.subhashe.onepocket.Core;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by praveen on 2/2/2016.
 */
public class MapModel implements Parcelable {

    private String markerId;
    private String LiveStatus;
    private String conn2PortType;
    private String stationName;
    private String stationID;
    private String conn1portType;
    private String StationStatus;
    private String stationAddress;
    private String conn2portLevel;
    private String conn1portLevel;
    private String portQuantity;

    private Double lattitude;
    private Double longitude;
    private LatLang latLang;
    private String vendingPrice1;
    private String vendingPrice2;
    private String parkingPricePH;
    private String stationADAStatus;
    private String vendingPriceUnit1;
    private String notes;
    private String OpenTime;
    private String CloseTime;

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getStationAddress() {
        return stationAddress;
    }

    public void setStationAddress(String stationAddress) {
        this.stationAddress = stationAddress;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getLiveStatus() {
        return LiveStatus;
    }

    public void setLiveStatus(String liveStatus) {
        LiveStatus = liveStatus;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getOpenTime() {
        return OpenTime;
    }

    public void setOpenTime(String openTime) {
        OpenTime = openTime;
    }

    public String getCloseTime() {
        return CloseTime;
    }

    public void setCloseTime(String closeTime) {
        CloseTime = closeTime;
    }

    /* @Override
    public LatLng getPosition() {
        return null;
    }



    public MapModel(LatLang latLang) {
        this.latLang = latLang;
    }*/

    public MapModel() {
    }

   public MapModel(String markerId, String liveStatus, String conn2PortType,String stationName,String stationID, String conn1portType,
                   String StationStatus, String stationAddress, String conn2portLevel,String conn1portLevel, String portQuantity,
                   Double lattitude,Double longitude,String vendingPrice1,String vendingPrice2, String parkingPricePH, String stationADAStatus , String vendingPriceUnit1 ,
                   String notes , String OpenTime , String CloseTime) {
       this.markerId = markerId;
       this.LiveStatus = liveStatus;
       this.conn2PortType = conn2PortType;
       this.stationName = stationName;
       this.stationID = stationID;
       this.conn1portType = conn1portType;
       this.StationStatus = StationStatus;
       this.stationAddress = stationAddress;
       this.conn2portLevel = conn2portLevel;
       this.conn1portLevel = conn1portLevel;
       this.portQuantity = portQuantity;
       this.lattitude= lattitude;
       this.longitude = longitude;
       this.vendingPrice1 = vendingPrice1;
       this.vendingPrice2 = vendingPrice2;
       this.parkingPricePH = parkingPricePH;
       this.stationADAStatus = stationADAStatus;
       this.vendingPriceUnit1 = vendingPriceUnit1;
       this.notes = notes;
       this.OpenTime = OpenTime;
       this.CloseTime = CloseTime;
   }
    public MapModel(Parcel in) {
        markerId = in.readString();
        LiveStatus = in.readString();
        conn2PortType = in.readString();
        stationName = in.readString();
        stationID = in.readString();
        conn1portType = in.readString();
        StationStatus = in.readString();
        stationAddress = in.readString();
        conn2portLevel = in.readString();
        conn1portLevel = in.readString();
        portQuantity = in.readString();
        lattitude = in.readDouble();
        longitude = in.readDouble();
        vendingPrice1 = in.readString();
        vendingPrice2 = in.readString();
        parkingPricePH = in.readString();
        stationADAStatus = in.readString();
        vendingPriceUnit1 = in.readString();
        notes = in.readString();
        OpenTime = in.readString();
        CloseTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(markerId);
        dest.writeString(LiveStatus);
        dest.writeString(conn2PortType);
        dest.writeString(stationName);
        dest.writeString(stationID);
        dest.writeString(conn1portType);
        dest.writeString(StationStatus);
        dest.writeString(stationAddress);
        dest.writeString(conn2portLevel);
        dest.writeString(conn1portLevel);
        dest.writeString(portQuantity);
        dest.writeDouble(lattitude);
        dest.writeDouble(longitude);
        dest.writeString(vendingPrice1);
        dest.writeString(vendingPrice2);
        dest.writeString(parkingPricePH);
        dest.writeString(stationADAStatus);
        dest.writeString(vendingPriceUnit1);
        dest.writeString(notes);
        dest.writeString(OpenTime);
        dest.writeString(CloseTime);
    }
    public String getMarkerId() {
        return markerId;
    }

    public void setMarkerId(String markerId) {
        this.markerId = markerId;
    }

    public String getConn2PortType() {
        return conn2PortType;
    }

    public void setConn2PortType(String conn2PortType) {
        this.conn2PortType = conn2PortType;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getConn1portType() {
        return conn1portType;
    }

    public void setConn1portType(String conn1portType) {
        this.conn1portType = conn1portType;
    }

    public String getStationStatus() {
        return StationStatus;
    }

    public void setStationStatus(String stationStatus) {
        StationStatus = stationStatus;
    }

    public String getConn2portLevel() {
        return conn2portLevel;
    }

    public void setConn2portLevel(String conn2portLevel) {
        this.conn2portLevel = conn2portLevel;
    }

    public String getConn1portLevel() {
        return conn1portLevel;
    }

    public void setConn1portLevel(String conn1portLevel) {
        this.conn1portLevel = conn1portLevel;
    }

    public String getPortQuantity() {
        return portQuantity;
    }

    public void setPortQuantity(String portQuantity) {
        this.portQuantity = portQuantity;
    }

    public LatLang getLatLang() {
        return latLang;
    }

    public void setLatLang(LatLang latLang) {
        this.latLang = latLang;
    }

    public String getVendingPrice1() {
        return vendingPrice1;
    }

    public void setVendingPrice1(String vendingPrice1) {
        this.vendingPrice1 = vendingPrice1;
    }

    public String getVendingPrice2() {
        return vendingPrice2;
    }

    public void setVendingPrice2(String vendingPrice2) {
        this.vendingPrice2 = vendingPrice2;
    }

    public String getParkingPricePH() {
        return parkingPricePH;
    }

    public void setParkingPricePH(String parkingPricePH) {
        this.parkingPricePH = parkingPricePH;
    }

    public String getStationADAStatus() {
        return stationADAStatus;
    }

    public void setStationADAStatus(String stationADAStatus) {
        this.stationADAStatus = stationADAStatus;
    }

    public String getVendingPriceUnit1() {
        return vendingPriceUnit1;
    }

    public void setVendingPriceUnit1(String vendingPriceUnit1) {
        this.vendingPriceUnit1 = vendingPriceUnit1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<MapModel> CREATOR = new Parcelable.Creator<MapModel>() {
        @Override
        public MapModel createFromParcel(Parcel source) {
            return new MapModel(source);
        }

        @Override
        public MapModel[] newArray(int size) {
            return new MapModel[0];
        }
    };
}
