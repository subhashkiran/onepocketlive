package com.subhashe.onepocket.Core;

/**
 * Created by admin on 12/15/2016.
 */

public class FavModel {

    private int idFav;
    private String stationNameModel;
    private String stationAddModel;
    private String stationAddressModel;
    private String stationId;
    private int userId;

    private String LiveStatus;
    private String conn1portType;
    private String conn2PortType;
    private String vendingPrice1;
    private String vendingPrice2;
    private String parkingPricePH;
    private String adaStatus;


    public String getStationid() {
        return stationId;
    }

    public void setStationid(String stationid) {
        this.stationId = stationid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getStationAddressModel() {
        return stationAddressModel;
    }

    public void setStationAddressModel(String stationAddressModel) {
        this.stationAddressModel = stationAddressModel;
    }

    public String getStationNameModel() {
        return stationNameModel;
    }

    public void setStationNameModel(String stationNameModel) {
        this.stationNameModel = stationNameModel;
    }

    public String getStationAddModel() {
        return stationAddModel;
    }

    public void setStationAddModel(String stationAddModel) {
        this.stationAddModel = stationAddModel;
    }

    public int getIdFav() {
        return idFav;
    }

    public void setIdFav(int idFav) {
        this.idFav = idFav;
    }

    public String getConn1portType() {
        return conn1portType;
    }

    public void setConn1portType(String conn1portType) {
        this.conn1portType = conn1portType;
    }

    public String getConn2PortType() {
        return conn2PortType;
    }

    public void setConn2PortType(String conn2PortType) {
        this.conn2PortType = conn2PortType;
    }

    public String getVendingPrice1() {
        return vendingPrice1;
    }

    public void setVendingPrice1(String vendingPrice1) {
        this.vendingPrice1 = vendingPrice1;
    }

    public String getVendingPrice2() {
        return vendingPrice2;
    }

    public void setVendingPrice2(String vendingPrice2) {
        this.vendingPrice2 = vendingPrice2;
    }

    public String getParkingPricePH() {
        return parkingPricePH;
    }

    public void setParkingPricePH(String parkingPricePH) {
        this.parkingPricePH = parkingPricePH;
    }

    public String getAdaStatus() {
        return adaStatus;
    }

    public void setAdaStatus(String adaStatus) {
        this.adaStatus = adaStatus;
    }

    public String getLiveStatus() {
        return LiveStatus;
    }

    public void setLiveStatus(String liveStatus) {
        LiveStatus = liveStatus;
    }
}
