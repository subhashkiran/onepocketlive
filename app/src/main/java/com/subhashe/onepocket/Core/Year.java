package com.subhashe.onepocket.Core;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by axxera on 30-01-2017.
 */

public class Year implements Parcelable {

    private String year;
    private int id;

    public Year(String year, int id) {
        this.year = year;
        this.id = id;
    }
    public Year(Parcel in) {
        year = in.readString();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(year);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Year> CREATOR = new Parcelable.Creator<Year>() {
        @Override
        public Year createFromParcel(Parcel source) {
            return new Year(source);
        }

        @Override
        public Year[] newArray(int size) {
            return new Year[0];
        }
    };

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString()
    {
        return year;
    }
}
