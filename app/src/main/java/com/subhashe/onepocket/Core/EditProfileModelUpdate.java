package com.subhashe.onepocket.Core;

/**
 * Created by praveen on 3/23/2016.
 */
public class EditProfileModelUpdate {
    private int Id;
    ProfileInfoModel address;
    private String answer1;
    private String answer2;
    private String confirmPassword;
    private String firstName;
    private int id;
    private String lastName;
    private String username;
    private String newPassword;
    private String oldPassword;
    private String securityQuesId1;
    private String securityQuesId2;
    private boolean showPassword;
    private boolean showSecQuestions;


    public boolean isShowSecQuestions() {
        return showSecQuestions;
    }

    public void setShowSecQuestions(boolean showSecQuestions) {
        this.showSecQuestions = showSecQuestions;
    }

    public boolean isShowPassword() {
        return showPassword;
    }

    public void setShowPassword(boolean showPassword) {
        this.showPassword = showPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }



    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getid() {
        return id;
    }

    public void setid(int idt) {
        id = idt;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getSecurityQuesId1() {
        return securityQuesId1;
    }

    public void setSecurityQuesId1(String securityQuesId1) {
        this.securityQuesId1 = securityQuesId1;
    }

    public String getSecurityQuesId2() {
        return securityQuesId2;
    }

    public void setSecurityQuesId2(String securityQuesId2) {
        this.securityQuesId2 = securityQuesId2;
    }

    public ProfileInfoModel getAddress() {
        return address;
    }

    public void setAddress(ProfileInfoModel address) {
        this.address = address;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
