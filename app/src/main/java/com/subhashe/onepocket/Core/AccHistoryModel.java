package com.subhashe.onepocket.Core;

import java.math.BigDecimal;

/**
 * Created by praveen on 3/22/2016.
 */
public class AccHistoryModel {
   // private Long createTimestamp;
   private BigDecimal amtCredit;
    private BigDecimal amtDebit;
    private BigDecimal currentBalance;
    private String date;
    private String stationId;
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getAmtCredit() {
        return amtCredit;
    }

    public void setAmtCredit(BigDecimal amtCredit) {
        this.amtCredit = amtCredit;
    }

    public BigDecimal getAmtDebit() {
        return amtDebit;
    }

    public void setAmtDebit(BigDecimal amtDebit) {
        this.amtDebit = amtDebit;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }
}
