package com.subhashe.onepocket.Core;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by praveen on 2/2/2016.
 */
public class LatLang implements ClusterItem {
    private Double lattitude;
    private Double longitude;
    private LatLng mPosition;
    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public LatLang(Double lat, Double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public LatLang() {
    }
/*  public double toADouble(){
        return lattitude;
    }*/
}
