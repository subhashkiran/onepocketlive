package com.subhashe.onepocket.Core;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by praveen on 2/2/2016.
 */
public class MapModelAll implements Parcelable {

    private String markerId;
    private String access_days_time;
    private String cards_accepted;
    private String date_last_confirmed;
    private String expected_date;
    private String fuel_type_code;
    private int id;
    private String groups_with_access_code;
    private String open_date;
    private String owner_type_code;
    private String status_code;
    private String station_name;
    private String station_phone;
    private String updated_at;
    private String geocode_status;
//    private LatLangAll latLang;
    private Double lattitude;
    private Double longitude;
    private String city;
    private String intersection_directions;
    private String plus4;
    private String state;
    private String street_address;
    private String zip;
    private String bd_blends;
    private String e85_blender_pump;
    private String ev_connector_types;
    private int ev_dc_fast_num;
    private int ev_level1_evse_num;
    private int ev_level2_evse_num;
    private String ev_network;
    private String ev_network_web;
    private String ev_other_evse;
    private String hy_status_link;
    private String lpg_primary;
    private String ng_fill_type_code;
    private String ng_psi;
    private String ng_vehicle_class;
    private String live_status;
    private String station_address;

    private List connectorTypesList;

    public List getConnectorTypesList() {
        return connectorTypesList;
    }

    public void setConnectorTypesList(List connectorTypesList) {
        this.connectorTypesList = connectorTypesList;
    }

    public MapModelAll() {
    }

    public MapModelAll(String markerId, String ng_vehicle_class, String access_days_time,
                    String cards_accepted, String date_last_confirmed, String expected_date,
                    String fuel_type_code, int id, String groups_with_access_code, String open_date,
                    String owner_type_code, String status_code, String station_name, String station_phone,
                    String updated_at, String geocode_status, Double lattitude, Double longitude, String city, String intersection_directions,
                    String plus4, String state, String street_address, String zip, String bd_blends, String e85_blender_pump,
                    String ev_connector_types, int ev_dc_fast_num, int ev_level1_evse_num, int ev_level2_evse_num,
                    String ev_network, String ev_network_web, String ev_other_evse, String hy_status_link, String lpg_primary,
                    String ng_fill_type_code, String ng_psi , String live_status , String station_address) {
        this.markerId = markerId;
        this.live_status = live_status;
        this.station_address = station_address;
        this.ng_vehicle_class = ng_vehicle_class;
        this.access_days_time = access_days_time;
        this.cards_accepted = cards_accepted;
        this.date_last_confirmed = date_last_confirmed;
        this.expected_date = expected_date;
        this.fuel_type_code = fuel_type_code;
        this.id = id;
        this.groups_with_access_code = groups_with_access_code;
        this.open_date = open_date;
        this.owner_type_code = owner_type_code;
        this.status_code = status_code;
        this.station_name = station_name;
        this.station_phone = station_phone;
        this.updated_at = updated_at;
        this.geocode_status = geocode_status;
        // this.latLang = latLang;
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.city = city;
        this.intersection_directions = intersection_directions;
        this.plus4 = plus4;
        this.state = state;
        this.street_address = street_address;
        this.zip = zip;
        this.bd_blends = bd_blends;
        this.e85_blender_pump = e85_blender_pump;
        this.ev_connector_types = ev_connector_types;
        this.ev_dc_fast_num = ev_dc_fast_num;
        this.ev_level1_evse_num = ev_level1_evse_num;
        this.ev_level2_evse_num = ev_level2_evse_num;
        this.ev_network = ev_network;
        this.ev_network_web = ev_network_web;
        this.ev_other_evse = ev_other_evse;
        this.hy_status_link = hy_status_link;
        this.lpg_primary = lpg_primary;
        this.ng_fill_type_code = ng_fill_type_code;
        this.ng_psi = ng_psi;
    }


    protected MapModelAll(Parcel in) {
        markerId = in.readString();
        live_status = in.readString();
        station_address = in.readString();
        access_days_time = in.readString();
        cards_accepted = in.readString();
        date_last_confirmed = in.readString();
        expected_date = in.readString();
        fuel_type_code = in.readString();
        id = in.readInt();
        groups_with_access_code = in.readString();
        open_date = in.readString();
        owner_type_code = in.readString();
        status_code = in.readString();
        station_name = in.readString();
        station_phone = in.readString();
        updated_at = in.readString();
        geocode_status = in.readString();
        lattitude = in.readDouble();
        longitude = in.readDouble();
        city = in.readString();
        intersection_directions = in.readString();
        plus4 = in.readString();
        state = in.readString();
        street_address = in.readString();
        zip = in.readString();
        bd_blends = in.readString();
        e85_blender_pump = in.readString();
        ev_connector_types = in.readString();
        ev_dc_fast_num = in.readInt();
        ev_level1_evse_num = in.readInt();
        ev_level2_evse_num = in.readInt();
        ev_network = in.readString();
        ev_network_web = in.readString();
        ev_other_evse = in.readString();
        hy_status_link = in.readString();
        lpg_primary = in.readString();
        ng_fill_type_code = in.readString();
        ng_psi = in.readString();
        ng_vehicle_class = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(markerId);
        dest.writeString(live_status);
        dest.writeString(station_address);
        dest.writeString(access_days_time);
        dest.writeString(cards_accepted);
        dest.writeString(date_last_confirmed);
        dest.writeString(expected_date);
        dest.writeString(fuel_type_code);
        dest.writeInt(id);
        dest.writeString(groups_with_access_code);
        dest.writeString(open_date);
        dest.writeString(owner_type_code);
        dest.writeString(status_code);
        dest.writeString(station_name);
        dest.writeString(station_phone);
        dest.writeString(updated_at);
        dest.writeString(geocode_status);
        dest.writeDouble(lattitude);
        dest.writeDouble(longitude);
        dest.writeString(city);
        dest.writeString(intersection_directions);
        dest.writeString(plus4);
        dest.writeString(state);
        dest.writeString(street_address);
        dest.writeString(zip);
        dest.writeString(bd_blends);
        dest.writeString(e85_blender_pump);
        dest.writeString(ev_connector_types);
        dest.writeInt(ev_dc_fast_num);
        dest.writeInt(ev_level1_evse_num);
        dest.writeInt(ev_level2_evse_num);
        dest.writeString(ev_network);
        dest.writeString(ev_network_web);
        dest.writeString(ev_other_evse);
        dest.writeString(hy_status_link);
        dest.writeString(lpg_primary);
        dest.writeString(ng_fill_type_code);
        dest.writeString(ng_psi);
        dest.writeString(ng_vehicle_class);
    }

    public static final Creator<MapModel> CREATOR = new Creator<MapModel>() {
        @Override
        public MapModel createFromParcel(Parcel in) {
            return new MapModel(in);
        }

        @Override
        public MapModel[] newArray(int size) {
            return new MapModel[0];
        }
    };

    public String getMarkerId() {
        return markerId;
    }

    public void setMarkerId(String markerId) {
        this.markerId = markerId;
    }



    public String getAccess_days_time() {
        return access_days_time;
    }

    public void setAccess_days_time(String access_days_time) {
        this.access_days_time = access_days_time;
    }

    public String getCards_accepted() {
        return cards_accepted;
    }

    public void setCards_accepted(String cards_accepted) {
        this.cards_accepted = cards_accepted;
    }

    public String getDate_last_confirmed() {
        return date_last_confirmed;
    }

    public void setDate_last_confirmed(String date_last_confirmed) {
        this.date_last_confirmed = date_last_confirmed;
    }

    public String getExpected_date() {
        return expected_date;
    }

    public void setExpected_date(String expected_date) {
        this.expected_date = expected_date;
    }

    public String getFuel_type_code() {
        return fuel_type_code;
    }

    public void setFuel_type_code(String fuel_type_code) {
        this.fuel_type_code = fuel_type_code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroups_with_access_code() {
        return groups_with_access_code;
    }

    public void setGroups_with_access_code(String groups_with_access_code) {
        this.groups_with_access_code = groups_with_access_code;
    }

    public String getOpen_date() {
        return open_date;
    }

    public void setOpen_date(String open_date) {
        this.open_date = open_date;
    }

    public String getOwner_type_code() {
        return owner_type_code;
    }

    public void setOwner_type_code(String owner_type_code) {
        this.owner_type_code = owner_type_code;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }

    public String getStation_address() {
        return station_address;
    }

    public void setStation_address(String station_address) {
        this.station_address = station_address;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }

    public String getStation_phone() {
        return station_phone;
    }

    public void setStation_phone(String station_phone) {
        this.station_phone = station_phone;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getGeocode_status() {
        return geocode_status;
    }

    public void setGeocode_status(String geocode_status) {
        this.geocode_status = geocode_status;
    }

   /* public LatLangAll getLatLang() {
        return latLang;
    }

    public void setLatLang(LatLangAll latLang) {
        this.latLang = latLang;
    }*/
    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getIntersection_directions() {
        return intersection_directions;
    }

    public void setIntersection_directions(String intersection_directions) {
        this.intersection_directions = intersection_directions;
    }

    public String getPlus4() {
        return plus4;
    }

    public void setPlus4(String plus4) {
        this.plus4 = plus4;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet_address() {
        return street_address;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getBd_blends() {
        return bd_blends;
    }

    public void setBd_blends(String bd_blends) {
        this.bd_blends = bd_blends;
    }

    public String getE85_blender_pump() {
        return e85_blender_pump;
    }

    public void setE85_blender_pump(String e85_blender_pump) {
        this.e85_blender_pump = e85_blender_pump;
    }

    public String getEv_connector_types() {
        return ev_connector_types;
    }

    public void setEv_connector_types(String ev_connector_types) {
        this.ev_connector_types = ev_connector_types;
    }

    public int getEv_dc_fast_num() {
        return ev_dc_fast_num;
    }

    public void setEv_dc_fast_num(int ev_dc_fast_num) {
        this.ev_dc_fast_num = ev_dc_fast_num;
    }

    public int getEv_level1_evse_num() {
        return ev_level1_evse_num;
    }

    public void setEv_level1_evse_num(int ev_level1_evse_num) {
        this.ev_level1_evse_num = ev_level1_evse_num;
    }

    public int getEv_level2_evse_num() {
        return ev_level2_evse_num;
    }

    public void setEv_level2_evse_num(int ev_level2_evse_num) {
        this.ev_level2_evse_num = ev_level2_evse_num;
    }

    public String getEv_network() {
        return ev_network;
    }

    public void setEv_network(String ev_network) {
        this.ev_network = ev_network;
    }

    public String getEv_network_web() {
        return ev_network_web;
    }

    public void setEv_network_web(String ev_network_web) {
        this.ev_network_web = ev_network_web;
    }

    public String getEv_other_evse() {
        return ev_other_evse;
    }

    public void setEv_other_evse(String ev_other_evse) {
        this.ev_other_evse = ev_other_evse;
    }

    public String getHy_status_link() {
        return hy_status_link;
    }

    public void setHy_status_link(String hy_status_link) {
        this.hy_status_link = hy_status_link;
    }

    public String getLpg_primary() {
        return lpg_primary;
    }

    public void setLpg_primary(String lpg_primary) {
        this.lpg_primary = lpg_primary;
    }

    public String getNg_fill_type_code() {
        return ng_fill_type_code;
    }

    public void setNg_fill_type_code(String ng_fill_type_code) {
        this.ng_fill_type_code = ng_fill_type_code;
    }

    public String getNg_psi() {
        return ng_psi;
    }

    public void setNg_psi(String ng_psi) {
        this.ng_psi = ng_psi;
    }

    public String getNg_vehicle_class() {
        return ng_vehicle_class;
    }

    public void setNg_vehicle_class(String ng_vehicle_class) {
        this.ng_vehicle_class = ng_vehicle_class;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}