package com.subhashe.onepocket.Core;

/**
 * Created by praveen on 4/19/2016.
 */
public class RouteLatLngModel {

    double latitude;
    double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
