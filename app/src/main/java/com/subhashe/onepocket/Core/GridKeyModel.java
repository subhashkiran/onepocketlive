package com.subhashe.onepocket.Core;

/**
 * Created by praveen on 3/21/2016.
 */
public class GridKeyModel {
    private String rfId;
    private int chargingObjectId;
    private String phone;

    public String getRfId() {
        return rfId;
    }

    public void setRfId(String rfId) {
        this.rfId = rfId;
    }

    public int getChargingObjectId() {
        return chargingObjectId;
    }

    public void setChargingObjectId(int chargingObjectId) {
        this.chargingObjectId = chargingObjectId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
