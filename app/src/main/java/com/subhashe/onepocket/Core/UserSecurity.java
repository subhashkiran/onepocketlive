package com.subhashe.onepocket.Core;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by SubhashE on 1/21/2016.
 */
public class UserSecurity implements Parcelable{
    private String question;
    private int id;

    public UserSecurity(String question, int id) {
        this.question = question;
        this.id = id;
    }
    public UserSecurity(Parcel in) {
        question = in.readString();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<UserSecurity> CREATOR = new Parcelable.Creator<UserSecurity>() {
        @Override
        public UserSecurity createFromParcel(Parcel source) {
            return new UserSecurity(source);
        }

        @Override
        public UserSecurity[] newArray(int size) {
            return new UserSecurity[0];
        }
    };

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString()
    {
        return question;
    }
}
