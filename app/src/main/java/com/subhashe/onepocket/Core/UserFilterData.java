package com.subhashe.onepocket.Core;

/**
 * Created by SubhashE on 1/21/2016.
 */
public class UserFilterData {
//    @SerializedName("address")
   private boolean availability;
    private boolean price;
    private boolean level1;
    private boolean level2;

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public boolean isPrice() {
        return price;
    }

    public void setPrice(boolean price) {
        this.price = price;
    }

    public boolean isLevel1() {
        return level1;
    }

    public void setLevel1(boolean level1) {
        this.level1 = level1;
    }

    public boolean isLevel2() {
        return level2;
    }

    public void setLevel2(boolean level2) {
        this.level2 = level2;
    }
}
