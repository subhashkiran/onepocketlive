package com.subhashe.onepocket.Core;

/**
 * Created by praveen on 4/15/2016.
 */
public class RouteModel {
    private int routeId;
    private double distance;
    private double duration;

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}
