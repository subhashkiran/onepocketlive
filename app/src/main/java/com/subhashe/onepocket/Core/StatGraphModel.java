package com.subhashe.onepocket.Core;



/**
 * Created by subhash on 3/11/2016.
 */
public class StatGraphModel {

    private Double kwUsed;
    private Double spent;
    private String date;



    public Double getSpent() {
        return spent;
    }

    public void setSpent(Double spent) {
        this.spent = spent;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getKwUsed() {
        return kwUsed;
    }

    public void setKwUsed(Double kwUsed) {
        this.kwUsed = kwUsed;
    }
}
