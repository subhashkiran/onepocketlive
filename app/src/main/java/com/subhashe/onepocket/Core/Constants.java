package com.subhashe.onepocket.Core;

/**
 * Created by SubhashE on 3/14/2016.
 */
public class Constants {

    public static final String JSON_URL = "https://api.data.gov/nrel/alt-fuel-stations/v1.json?fuel_type=ELEC&api_key=1vWB8MccC1alVSuFO29CxE4nq5Y3cmqgLLhy1BGL";
    public static final String DUP_JSON_URL = "https://api.data.gov/nrel/alt-fuel-stations/v1.json?fuel_type=ELEC&api_key=1vWB8MccC1alVSuFO29CxE4nq5Y3cmqgLLhy1BGL&status=E&ev_connector_type=NEMA520";
    public static final String JSON_ARRAY_NAME = "fuel_stations";

    public static final String ACCESS_DAYS_TIME = "access_days_time";
    public static final String CARDS_ACCEPTED = "cards_accepted";
    public static final String DATE_LAST_CONFIRMED = "date_last_confirmed";
    public static final String EXPECTED_DATE = "expected_date";
    public static final String FUEL_TYPE_CODE = "fuel_type_code";
    public static final String ID = "id";
    public static final String GROUPS_WITH_ACCESS_CODE = "groups_with_access_code";
    public static final String OPEN_DATE = "open_date";
    public static final String OWNER_TYPE_CODE = "owner_type_code";
    public static final String STATUS_CODE = "status_code";
    public static final String STATION_NAME = "station_name";
    public static final String STATION_ADDRESS = "station_address";
    public static final String STATION_LIVE_STATUS = "station_live_status";
    public static final String STATION_PHONE = "station_phone";
    public static final String UPDATED_AT = "updated_at";
    public static final String GEOCODE_STATUS = "geocode_status";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String CITY = "city";
    public static final String INTERSECTION_DIRECTION = "intersection_directions";
    public static final  String PLUS4 = "plus4";
    public static final  String STATE = "state";
    public static final String STREET_ADDRESS = "street_address";
    public static final String ZIP = "zip";
    public static final String BD_BLENDS = "bd_blends";
    public static final String E85_BLENDER_PUMP = "e85_blender_pump";
    public static final String EV_CONNECTOR_TYPES = "ev_connector_types";
    public static final String EV_DC_FAST_NUM = "ev_dc_fast_num";
    public static final String EV_LEVEL1_EVSE_NUM = "ev_level1_evse_num";
    public static final String EV_LEVEL2_EVSE_NUM = "ev_level2_evse_num";
    public static final String EV_NETWORK = "ev_network";
    public static final String EV_NETWORK_WEB = "ev_network_web";
    public static final String EV_OTHER_EVSE = "ev_other_evse";
    public static final String HY_STATUS_LINK = "hy_status_link";
    public static final String LPG_PRIMARY = "lpg_primary";
    public static final String NG_FILL_TYPE_CODE = "ng_fill_type_code";
    public static final String NG_PSI = "ng_psi";
    public static final String NG_VEHICLE_CLASS = "ng_vehicle_class";
}

