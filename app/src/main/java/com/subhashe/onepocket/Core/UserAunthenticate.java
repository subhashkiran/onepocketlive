package com.subhashe.onepocket.Core;

/**
 * Created by SubhashE on 1/28/2016.
 */
public class UserAunthenticate {
    private UserAddres address;
    private int id;
    private boolean enabled;
    private UserRoles roles;
    private String groups;
    private String username;
    private UserRoles authorities;
    private String token;
    private UserAccounts accountses;
    private String profiles;
    private String firstName;
    private String lastName;
    private String gender;
    private boolean accountNotExpired;
    private String email;
    private UserSecurity securityAnswerses;

    public UserAddres getAddress() {
        return address;
    }

    public void setAddress(UserAddres address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public UserRoles getRoles() {
        return roles;
    }

    public void setRoles(UserRoles roles) {
        this.roles = roles;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserRoles getAuthorities() {
        return authorities;
    }

    public void setAuthorities(UserRoles authorities) {
        this.authorities = authorities;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserAccounts getAccountses() {
        return accountses;
    }

    public void setAccountses(UserAccounts accountses) {
        this.accountses = accountses;
    }

    public String getProfiles() {
        return profiles;
    }

    public void setProfiles(String profiles) {
        this.profiles = profiles;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isAccountNotExpired() {
        return accountNotExpired;
    }

    public void setAccountNotExpired(boolean accountNotExpired) {
        this.accountNotExpired = accountNotExpired;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserSecurity getSecurityAnswerses() {
        return securityAnswerses;
    }

    public void setSecurityAnswerses(UserSecurity securityAnswerses) {
        this.securityAnswerses = securityAnswerses;
    }
}
