package com.subhashe.onepocket.Core;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyAxisValueFormatterDay implements IAxisValueFormatter
{

    private DecimalFormat mFormat;

    public MyAxisValueFormatterDay() {
        mFormat = new DecimalFormat("###,###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return "Day " + mFormat.format(Math.round(value)) /*+ " $"*/;
    }
}
