package com.subhashe.onepocket.Core;

/**
 * Created by SubhashE on 2/2/2016.
 */
public class UserForgotPassword {
    private int securityQuesId1;
    private String answer1;
    private int securityQuesId2;
    private String answer2;
    private String username;
    private String options;

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public int getSecurityQuesId1() {
        return securityQuesId1;
    }

    public void setSecurityQuesId1(int securityQuesId1) {
        this.securityQuesId1 = securityQuesId1;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public int getSecurityQuesId2() {
        return securityQuesId2;
    }

    public void setSecurityQuesId2(int securityQuesId2) {
        this.securityQuesId2 = securityQuesId2;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
