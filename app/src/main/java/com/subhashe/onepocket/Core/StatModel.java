package com.subhashe.onepocket.Core;



/**
 * Created by praveen on 3/11/2016.
 */
public class StatModel {

    private int statLocalId;
    private String sessionId;
    private Double kwUsed;
    private Double timeUsedPerHr;
    private String chargeEnd;
    private String rateKWPerTime;
    private Double spent;
    private String date;
    private String portId;
    private String chargeStart;
    private String stationName;

    public int getStatLocalId() {
        return statLocalId;
    }

    public void setStatLocalId(int statLocalId) {
        this.statLocalId = statLocalId;
    }

    public String getStationAddress() {
        return stationAddress;
    }

    public void setStationAddress(String stationAddress) {
        this.stationAddress = stationAddress;
    }

    private String stationAddress;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Double getKwUsed() {
        return kwUsed;
    }

    public void setKwUsed(Double kwUsed) {
        this.kwUsed = kwUsed;
    }

    public Double getTimeUsedPerHr() {
        return timeUsedPerHr;
    }

    public void setTimeUsedPerHr(Double timeUsedPerHr) {
        this.timeUsedPerHr = timeUsedPerHr;
    }

    public String getChargeEnd() {
        return chargeEnd;
    }

    public void setChargeEnd(String chargeEnd) {
        this.chargeEnd = chargeEnd;
    }

    public String getRateKWPerTime() {
        return rateKWPerTime;
    }

    public void setRateKWPerTime(String rateKWPerTime) {
        this.rateKWPerTime = rateKWPerTime;
    }

    public Double getSpent() {
        return spent;
    }

    public void setSpent(Double spent) {
        this.spent = spent;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getChargeStart() {
        return chargeStart;
    }

    public void setChargeStart(String chargeStart) {
        this.chargeStart = chargeStart;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }
}
