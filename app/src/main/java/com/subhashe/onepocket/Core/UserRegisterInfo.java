package com.subhashe.onepocket.Core;

/**
 * Created by SubhashE on 1/21/2016.
 */
public class UserRegisterInfo {
//    @SerializedName("address")
    private UserAddres address;

    private String id;
    private int userId;
    private String rolename;
    private String firstName;
    private String lastName;
    private String username;
    private String deviceToken;
    private String appInfo;
    private String deviceName;
    private String deviceType;
    private String deviceVersion;
    private String gender;
    private String email;
    private String password;
    private String confirmPassword;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getAppVersion() {
        return appInfo;
    }

    public void setAppVersion(String appVersion) {
        this.appInfo = appVersion;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(String appInfo) {
        this.appInfo = appInfo;
    }

    public UserAddres getAddress() {
        return address;
    }

    public void setAddress(UserAddres address) {
        this.address = address;
    }

    //    private EvModel vehicle;

   /* public EvModel getVehicle() {
        return vehicle;
    }

    public void setVehicle(EvModel vehicle) {
        this.vehicle = vehicle;
    }*/

    public UserAddres getMuserAddres() {
        return address;
    }

    public void setMuserAddres(UserAddres muserAddres) {
        this.address = muserAddres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}
