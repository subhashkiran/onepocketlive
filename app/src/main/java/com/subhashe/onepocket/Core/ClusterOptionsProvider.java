package com.subhashe.onepocket.Core;

import com.google.android.gms.maps.model.Marker;

import java.util.List;

/**
 * Created by SubhashE on 2/25/2016.
 */
public interface ClusterOptionsProvider {
    ClusterOptions getClusterOptions(List<Marker> markers);
}
