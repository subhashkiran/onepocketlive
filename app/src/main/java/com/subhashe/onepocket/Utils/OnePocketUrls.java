package com.subhashe.onepocket.Utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;


public class OnePocketUrls {

    public static boolean CONTACTUS_FLAG;
    public static ServerConfig CURRENT_BUILD_CONFIG;

    /*Cloud IP*/
     //private static String BASE_URL_INT = "http://198.90.20.106/gridbot";

     //private static String BASE_URL_PROD = "http://198.90.20.106/gridbot";

     private static String BASE_URL_QA = "http://198.90.20.106/gridbot";

    /*Harin Ip*/
    private static String BASE_URL_INT = "http://192.168.168.99:8080/gridbot";//Harin IP

    private static String BASE_URL_PROD = "http://192.168.168.99:8080/gridbot";//Harin IP

//    private static String BASE_URL_QA = "http://192.168.168.99:8080/gridbot";//Harin IP

     /*Production IP*/
    //private static String BASE_URL_INT = "https://gridkey.telluspowertech.com/gridbot";

    //private static String BASE_URL_PROD = "https://gridkey.telluspowertech.com/gridbot";

    //private static String BASE_URL_QA = "https://gridkey.telluspowertech.com/gridbot";


    /*  private static String BASE_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "";
            case ServerConfigQA:
                return BASE_URL_QA + "";
            default:
                return BASE_URL_PROD + ";
        }
    }*/


    /*Registration*/
    public static String REGISTER_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/register";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/register";
            default:
                return BASE_URL_PROD + "/driver/register";
        }
    }

    /*Login*/
    public static String LOGIN_AUTHENTICATE_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/login/authenticate";
            case ServerConfigQA:
                return BASE_URL_QA + "/login/authenticate";
            default:
                return BASE_URL_PROD + "/login/authenticate";
        }
    }

    /*Email Verification*/
    public static String EMAIL_VERIFICATION_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/getUserFlagValue";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/getUserFlagValue";
            default:
                return BASE_URL_PROD + "/driver/getUserFlagValue";
        }
    }

    /*Getting Filters List*/
    public static String GETFILTERLIST() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/getfilterlist";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/getfilterlist";
            default:
                return BASE_URL_PROD + "/driver/getfilterlist";
        }
    }

    /*For Setting Favorites List*/
    public static String SETFAVORITELIST() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/setFavoritelist";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/setFavoritelist";
            default:
                return BASE_URL_PROD + "/driver/setFavoritelist";
        }
    }

    /*For Getting Favorites List*/
    public static String GETALLFAVORITELIST() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/getFavoritelist/";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/getFavoritelist/";
            default:
                return BASE_URL_PROD + "/driver/getFavoritelist/";
        }
    }

    /*For Deleting Favorites List*/
    public static String DELETEFAVORITELIST() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/deleteFavoritelist";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/deleteFavoritelist";
            default:
                return BASE_URL_PROD + "/driver/deleteFavoritelist";
        }
    }

    /*FOR GETTING LIST EV*/
    public static String EV_LIST() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/ev/";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/ev/";
            default:
                return BASE_URL_PROD + "/driver/ev/";
        }
    }

    /*FOR ADDING EV*/
    public static String EV_ADD() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/ev";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/ev";
            default:
                return BASE_URL_PROD + "/driver/ev";
        }
    }

    /*For Updating EV*/
    public static String EV_UPDATE() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/updateEV";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/updateEV";
            default:
                return BASE_URL_PROD + "/driver/updateEV";
        }
    }

    /*Deleting EV List*/
    public static String DELETEEVLIST() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/deleteEV";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/deleteEV";
            default:
                return BASE_URL_PROD + "/driver/deleteEV";
        }
    }

    /*Getting Total DR Events*/
    public static String GETALLDRALERTTOTALEVENTS() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/totalEvents";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/totalEvents";
            default:
                return BASE_URL_PROD + "/driver/totalEvents";
        }
    }

    /*Getting DR Alerts Schedule Events*/
    public static String GETALLDRALERTSCHEDULE() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/events";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/events";
            default:
                return BASE_URL_PROD + "/driver/events";
        }
    }


    /*Getting DR Alerts Active Events*/
    public static String GETALLDRALERTACTIVE() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/strtdEvents";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/strtdEvents";
            default:
                return BASE_URL_PROD + "/driver/strtdEvents";
        }
    }

    /*Getting DR Alerts Completed Events*/
    public static String GETALLDRALERTCOMPLETED() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/CmpltdEvents";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/CmpltdEvents";
            default:
                return BASE_URL_PROD + "/driver/CmpltdEvents";
        }
    }

    /*For Device Details*/
    public static String DEVICE_DETAILS_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/device";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/device";
            default:
                return BASE_URL_PROD + "/driver/device";
        }
    }

    /*For Security Questions*/
    public static String USER_SECUTITY_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/services/users/quest";
            case ServerConfigQA:
                return BASE_URL_QA + "/services/users/quest";
            default:
                return BASE_URL_PROD + "/services/users/quest";
        }
    }

    /*For Balance Checking*/
    public static String USER_BALENCE_CHECK() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/services/users/";
            case ServerConfigQA:
                return BASE_URL_QA + "/services/users/";
            default:
                return BASE_URL_PROD + "/services/users/";
        }
    }

    /*For Edit Profile*/
    public static String SETTINGS_EDITPROFILE() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/services/users/";
            case ServerConfigQA:
                return BASE_URL_QA + "/services/users/";
            default:
                return BASE_URL_PROD + "/services/users/";
        }
    }

    /*For Profile Update*/
    public static String PROFILE_UPDATE() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/services/address";
            case ServerConfigQA:
                return BASE_URL_QA + "/services/address";
            default:
                return BASE_URL_PROD + "/services/address";
        }
    }

    /*Driver Info*/
    public static String DRIVERINFO_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/addinfo";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/addinfo";
            default:
                return BASE_URL_PROD + "/driver/addinfo";
        }
    }

    /*For Maps*/
    public static String GOOGLEMAPS_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/maps";
            case ServerConfigQA:
                return BASE_URL_QA + "/maps";
            default:
                return BASE_URL_PROD + "/maps";
        }
    }

    /*For Forget Password*/
    public static String FORGET_PASSWORD_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/password/resetPassword?";
            case ServerConfigQA:
                return BASE_URL_QA + "/password/resetPassword?";
            default:
                return BASE_URL_PROD + "/password/resetPassword?";
        }
    }

    /*For getting stats*/
    public static String STATS_DRIVER() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/stat/";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/stat/";
            default:
                return BASE_URL_PROD + "/driver/stat/";
        }
    }

    /*For getting Weekly stats*/
    public static String STATS_WEEKLY_DRIVER() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/weekGraph/";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/weekGraph/";
            default:
                return BASE_URL_PROD + "/driver/weekGraph/";
        }
    }

    /*For getting Monthly stats*/
    public static String STATS_MONTHLY_DRIVER() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/monthGraph/";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/monthGraph/";
            default:
                return BASE_URL_PROD + "/driver/monthGraph/";
        }
    }

    /*For ContactUs*/
    public static String CONTACTUS_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/sendemail";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/sendemail";
            default:
                return BASE_URL_PROD + "/driver/sendemail";
        }
    }

    public static String SERVICES_USER_ID_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/services/profile/user/";
            case ServerConfigQA:
                return BASE_URL_QA + "/services/profile/user/";
            default:
                return BASE_URL_PROD + "/services/profile/user/";
        }
    }

    public static String FORGET_LOGIN_VALIDATE_URL() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/login/validateQuestions";
            case ServerConfigQA:
                return BASE_URL_QA + "/login/validateQuestions";
            default:
                return BASE_URL_PROD + "/login/validateQuestions";
        }
    }

    //    http://192.168.168.111:8080/gridbot/login/resetPassword
    public static String RESET_PASSWORD_WITH_SEC_QUESTIONS() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/login/resetPassword";
            case ServerConfigQA:
                return BASE_URL_QA + "/login/resetPassword";
            default:
                return BASE_URL_PROD + "/login/resetPassword";
        }
    }

    public static String CARD_PAYPAL_PAYMENT() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/services/payment";
            case ServerConfigQA:
                return BASE_URL_QA + "/services/payment";
            default:
                return BASE_URL_PROD + "/services/payment";
        }
    }

    public static String GET_SECURITY_QUE_IDS() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/login/getqueid?";
            case ServerConfigQA:
                return BASE_URL_QA + "/login/getqueid?";
            default:
                return BASE_URL_PROD + "/login/getqueid?";
        }
    }

    public static String CARD_PAYPAL_GETTOKEN() {
        return "https://api.sandbox.paypal.com/v1/oauth2/token";
    }

    public static String CARD_PAYPAL_SENDTOKEN() {
        return "https://api.sandbox.paypal.com/v1/payments/payment/";
    }

    public static String CARD_PAYPAL_SERVER() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/services/payment/paypalOnePocket";
            case ServerConfigQA:
                return BASE_URL_QA + "/services/payment/paypalOnePocket";
            default:
                return BASE_URL_PROD + "/services/payment/paypalOnePocket";
        }
    }

    public static String ACCOUNT_GRIDKEY() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/gkey/active/";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/gkey/active/";
            default:
                return BASE_URL_PROD + "/driver/gkey/active/";
        }
    }

    public static String ACCOUNT_GRIDKEY_DEACTIVATE() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/gkDeactivate";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/gkDeactivate";
            default:
                return BASE_URL_PROD + "/driver/gkDeactivate";
        }
    }

    public static String ACCOUNT_ACCOUNT_HISTORY() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/accHistory/";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/accHistory/";
            default:
                return BASE_URL_PROD + "/driver/accHistory/";
        }
    }

    public static String ACCOUNT_GRIDKEY_NEW() {
        switch (CURRENT_BUILD_CONFIG) {
            case ServerConfigInt:
                return BASE_URL_INT + "/driver/gkeyRequest";
            case ServerConfigQA:
                return BASE_URL_QA + "/driver/gkeyRequest";
            default:
                return BASE_URL_PROD + "/driver/gkeyRequest";
        }
    }

    public static String GOOGLEMAPS_ALL_STATIONS_URL() {
//        switch (CURRENT_BUILD_CONFIG) {
        return "https://api.data.gov/nrel/alt-fuel-stations/v1.json?fuel_type=ELEC&api_key=1vWB8MccC1alVSuFO29CxE4nq5Y3cmqgLLhy1BGL";
//        }
    }

    public static final String getVersionType(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        if (version.contains("PROD")) {
            return "PROD";
        } else if (version.contains("QA")) {
            return "QA";
        } else {
            return "INT";
        }
    }


    public enum ServerConfig {
        ServerConfigInt, ServerConfigProd, ServerConfigQA
    }
}
