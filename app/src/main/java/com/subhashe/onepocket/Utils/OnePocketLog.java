package com.subhashe.onepocket.Utils;

import android.util.Log;

/**
 * Created by SubhashE on 1/21/2016.
 */
public class OnePocketLog {
    /*
	 * Add Log to Logcat
	 */
    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }
}
