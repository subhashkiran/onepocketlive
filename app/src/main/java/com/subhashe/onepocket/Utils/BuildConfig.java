package com.subhashe.onepocket.Utils;

/**
 * Created by SubhashE on 1/21/2016.
 */
public final class BuildConfig {
    public static boolean DEBUG = true;

    // Static Block to Enable or Disable Logs.
    static {
        if (OnePocketUrls.CURRENT_BUILD_CONFIG
                .equals(OnePocketUrls.ServerConfig.ServerConfigQA)
                || OnePocketUrls.CURRENT_BUILD_CONFIG
                .equals(OnePocketUrls.ServerConfig.ServerConfigInt)) {
            DEBUG = true;
        } else {
            DEBUG = false;
        }
    }
}