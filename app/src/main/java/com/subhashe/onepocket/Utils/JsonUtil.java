package com.subhashe.onepocket.Utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.subhashe.onepocket.Core.DeviceDetailsInfo;
import com.subhashe.onepocket.Core.EditProfileModelUpdate;
import com.subhashe.onepocket.Core.EvModel;
import com.subhashe.onepocket.Core.EvModelAcc;
import com.subhashe.onepocket.Core.FavModel;
import com.subhashe.onepocket.Core.PayPalHeader;
import com.subhashe.onepocket.Core.ProfileInfoModel;
import com.subhashe.onepocket.Core.UserFilterData;
import com.subhashe.onepocket.Core.UserForgotPassword;
import com.subhashe.onepocket.Core.UserRegisterInfo;
import com.subhashe.onepocket.Core.UserSecurityPayment;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by SubhashE on 1/21/2016.
 */
public class JsonUtil {
    private static String token;
    public static String getMethod(String mURL,String header,String token) {
        HttpURLConnection conn = null;
        BufferedReader input = null;
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(mURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            if(!mURL.contains(OnePocketUrls.EV_LIST())) {
                if(mURL.contains(OnePocketUrls.CARD_PAYPAL_SENDTOKEN())){
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    conn.setRequestProperty("authorization", token);
                }else{
                    conn.setRequestProperty("X-Auth-Token", token);
                }
//                conn.setRequestProperty("X-Auth-Token", token);
            }
            conn.setConnectTimeout(10000); // set timeout to 10 seconds
            conn.setReadTimeout(10000);
            conn.connect();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

                input = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()), 8192);
                String strLine = null;
                while ((strLine = input.readLine()) != null) {
                    response.append(strLine);
                }

            }
        } catch (Exception e) {
            OnePocketLog.d("Error at ", e + "");
        } finally {
            // close the reader; this can throw an exception too, so
            // wrap it in another try/catch block.
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        return response.toString();

    }

    /*public static String getMethod(String mURL,String header,String token) {
        HttpURLConnection urlConnection = null;
        BufferedReader input = null;
//        StringBuilder response = new StringBuilder();
        String response = "";
        try {
            urlConnection = (HttpURLConnection) makeRequestGet("GET", mURL, null, "application/json", null);
            InputStream inputStream;
            String temp = "";
            // get stream
            if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
            }
            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            return e.toString();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

    }*/

    public static String postMethod(String mURL, UserRegisterInfo registerInfo, UserSecurityPayment securityInfo,
                                    UserForgotPassword passwordInfo, ProfileInfoModel profileInfoModel, EvModelAcc evListEdit,
                                    EditProfileModelUpdate profileEditModel, PayPalHeader payPalHeader, String header,
                                    String authToken, UserFilterData mUserFilterData, FavModel mFavList , DeviceDetailsInfo mdeviceDetailsInfo) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String postJson ;
//        String postJson1 = "{\"address\":{\"addressLine1\":\"test\",\"addressLine2\":\"pocket\",\"city\":\"Hyderabad\",\"country\":\"india\",\"id\":null,\"phone\":\"546-487-76\",\"state\":\"telangana\",\"zipCode\":\"546544\"},\"confirmPassword\":\"1234\",\"email\":\"testpocket@tet.com\",\"firstName\":\"test\",\"gender\":\"male\",\"id\":null,\"lastName\":\"pocket\",\"password\":\"1234\",\"rolename\":\"Driver\",\"username\":\"testpocket\"}";
//        OnePocketLog.d("JsonUtil", "Posting for register: " + postJson);
        token = authToken;
        if (mURL.contains(OnePocketUrls.LOGIN_AUTHENTICATE_URL())||
                mURL.contains(OnePocketUrls.EMAIL_VERIFICATION_URL())||
                mURL.contains(OnePocketUrls.RESET_PASSWORD_WITH_SEC_QUESTIONS())||
                mURL.contains(OnePocketUrls.CONTACTUS_URL()) ||
                mURL.contains(OnePocketUrls.ACCOUNT_GRIDKEY_NEW())||
                mURL.contains(OnePocketUrls.ACCOUNT_GRIDKEY_DEACTIVATE()) ||
                mURL.contains(OnePocketUrls.DELETEFAVORITELIST())  ||
                mURL.contains(OnePocketUrls.EV_UPDATE())  ||
                mURL.contains(OnePocketUrls.DELETEEVLIST())){
            postJson = header;
        }else if(mURL.contains(OnePocketUrls.DRIVERINFO_URL())){
            postJson = gson.toJson(securityInfo);
        }else if(mURL.contains(OnePocketUrls.FORGET_LOGIN_VALIDATE_URL())){
            postJson = gson.toJson(passwordInfo);
        }else if(mURL.contains(OnePocketUrls.PROFILE_UPDATE())){
            postJson = gson.toJson(profileInfoModel);
        }else if(mURL.contains(OnePocketUrls.EV_ADD())){
            postJson = gson.toJson(evListEdit);
        }else if(mURL.contains(OnePocketUrls.USER_BALENCE_CHECK())){
            postJson = gson.toJson(profileEditModel);
        }else if(mURL.contains(OnePocketUrls.CARD_PAYPAL_SERVER())){
            postJson = gson.toJson(payPalHeader);
        }else if(mURL.contains(OnePocketUrls.GETFILTERLIST())){
            postJson = gson.toJson(mUserFilterData);
        }else if(mURL.contains(OnePocketUrls.SETFAVORITELIST())){
            postJson = gson.toJson(mFavList);
        } else if(mURL.contains(OnePocketUrls.DEVICE_DETAILS_URL())){
            postJson = gson.toJson(mdeviceDetailsInfo);
        }
        else{
            postJson = gson.toJson(registerInfo);
        }



        if(mURL.contains(OnePocketUrls.CARD_PAYPAL_GETTOKEN())){
          /*  MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials");
            postJson = body.toString();*/
            postJson ="grant_type=client_credentials";
        }

        HttpURLConnection urlConnection = null;
        BufferedReader input = null;
//        StringBuilder response = new StringBuilder();
        String response = "";
        try {
            urlConnection = (HttpURLConnection) makeRequestPost("POST", mURL, null, "application/json", postJson);
            InputStream inputStream;
            String temp = "";
            String temp1= "Success";
            // get stream
            if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
            }
            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            if (mURL.contains("services") &&mURL.contains("payment") && response == "") {
                response += temp1;
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            return e.toString();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    public static HttpURLConnection makeRequestPost(String method, String apiAddress,
                                                String accessToken, String mimeType, String requestBody) throws IOException {
        URL url = new URL(apiAddress);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(!method.equals("GET"));
        urlConnection.setRequestMethod(method);

        if(apiAddress.contains(OnePocketUrls.REGISTER_URL()) || apiAddress.contains(OnePocketUrls.ACCOUNT_GRIDKEY_NEW())
                || apiAddress.contains(OnePocketUrls.PROFILE_UPDATE())
                || apiAddress.contains(OnePocketUrls.DRIVERINFO_URL())
                || apiAddress.contains(OnePocketUrls.EV_ADD())
                || apiAddress.contains(OnePocketUrls.USER_BALENCE_CHECK())
                || apiAddress.contains(OnePocketUrls.FORGET_LOGIN_VALIDATE_URL()) ||
                apiAddress.contains(OnePocketUrls.RESET_PASSWORD_WITH_SEC_QUESTIONS())||
                apiAddress.contains(OnePocketUrls.CARD_PAYPAL_PAYMENT())||
                apiAddress.contains(OnePocketUrls.CARD_PAYPAL_SERVER())||
                apiAddress.contains(OnePocketUrls.ACCOUNT_GRIDKEY_DEACTIVATE()) ||
                apiAddress.contains(OnePocketUrls.GETFILTERLIST()) ||
                apiAddress.contains(OnePocketUrls.SETFAVORITELIST()) ||
                apiAddress.contains(OnePocketUrls.DELETEEVLIST())||
                apiAddress.contains(OnePocketUrls.EV_UPDATE()) ||
                apiAddress.contains(OnePocketUrls.DEVICE_DETAILS_URL())){
            urlConnection.setRequestProperty("Accept", "application/json,text/plain, */*");
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            urlConnection.setRequestProperty("X-Auth-Token", token);
        }else if(apiAddress.contains(OnePocketUrls.CARD_PAYPAL_GETTOKEN())){
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            urlConnection.setRequestProperty("accept-language", "en_US");
            urlConnection.setRequestProperty("accept", "application/json");
            urlConnection.setRequestProperty("cache-control", "no-cache");
            urlConnection.setRequestProperty("authorization", "Basic QVlMOWhGYTFuMWtITk9Wd2xtZ3RHSGJ2NWtpSzhfSHQ0LThSa3NiVzFKQmdMeFZ5S1lBLXUtLTd2OTFoS216MTZBeFVOSDlVdzNETzg2dkw6RUx3dS1ob3htZWdSeldBUFc3RTgxd0FIT3VZZGQ2Z2x6SzVfbDhpNEFLdDlLdE5vSDVITnc4Q19ZOWJjT3p0dGtDXzdpUUNSNGM5VTZHUnc=");
        }/*else if(apiAddress.contains(OnePocketUrls.CARD_PAYPAL_SENDTOKEN())){
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            urlConnection.setRequestProperty("authorization", token);
        }*/
        else{
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        }


        OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
        writer.write(requestBody);
        writer.flush();
        writer.close();
        outputStream.close();

        urlConnection.connect();

        return urlConnection;
    }

//    public static HttpURLConnection makeRequestGet(String method, String apiAddress,
//                                                    String accessToken, String mimeType, String requestBody) throws IOException {
//        URL url = new URL(apiAddress);
//        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//
//        urlConnection.setDoInput(true);
//        urlConnection.setDoOutput(!method.equals("POST"));
//        urlConnection.setRequestMethod(method);
//
//        if(apiAddress.contains(OnePocketUrls.SERVICES_USER_ID_URL())){
//            urlConnection.setRequestProperty("Accept", "application/json,text/plain, **/*//*");
//            urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
//            urlConnection.setRequestProperty("X-Auth-Token", token);
//        }else{
//            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//        }
//        OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
//        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
//        writer.write(requestBody);
//        writer.flush();
//        writer.close();
//        outputStream.close();
//
//        urlConnection.connect();
//
//        return urlConnection;
//    }
}
