package com.subhashe.onepocket.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.subhashe.onepocket.Activities.LoginActivity;
import com.subhashe.onepocket.Activities.OnePocketApp;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by SubhashE on 2/4/2016.
 */
public class SharedPreferencesStore {

    private static final String LOG_TAG = "SharedPreferencesStore";
    private static final String SHARED_PREFERENCE_NAME = LoginActivity.class
            .getSimpleName();

    public static final String KEY_UNASSIGNED = "PrefsDefaultValue";
    public static final String ONEPOCKET_USER_NAME = "ONEPOCKET_USER_NAME";
    public static final String ONEPOCKET_PASSWORD = "ONEPOCKET_PASSWORD";
    public static final String ONEPOCKET_REMEMBERME = "ONEPOCKET_REMEMBERME";
//    public static final String NOTIFICATIONS_ENABLED = "NOTIFICATIONS_FLAG";
//    public static final String PUSH_NOTIFICATIONS_ENABLED = "PUSH_NOTIFICATIONS_FLAG";
//    public static final String COOKIE = "COOKIE";

    // Since the credentials are already secured through shared prefs, we're
    // using this as a lightweight solution for obfuscation. The seed should be
    // generated securely, but since we would need to store it somewhere on the
    // client, this defeats the purpose
    private static String seed = "95DA0F5669DB628DA492CFF6C26B2E0F";

    public static void setRememberMe(boolean value) {
        SharedPreferences prefs = OnePocketApp.getContext()
                .getSharedPreferences(SHARED_PREFERENCE_NAME,
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ONEPOCKET_REMEMBERME, value);
        editor.apply();
    }

    public static boolean getRememberMe() {
        SharedPreferences prefs = OnePocketApp.getContext()
                .getSharedPreferences(SHARED_PREFERENCE_NAME,
                        Context.MODE_PRIVATE);
        return prefs.getBoolean(ONEPOCKET_REMEMBERME,false);
    }


    public static void setEncryptedSharedPref(String key, String data) {
        SharedPreferences prefs = OnePocketApp.getContext()
                .getSharedPreferences(SHARED_PREFERENCE_NAME,
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        if (data == null) {
            OnePocketLog.d(LOG_TAG, "Data is null, cannot save");
            return;
        }

        try {
            editor.putString(key, SimpleCrypto.encrypt(seed, data));
            editor.apply();
        } catch (InvalidKeyException e) {
            OnePocketLog.d(LOG_TAG,
                    "InvalidKeyException, data will not be stored for key: "
                            + key);
        } catch (NoSuchAlgorithmException e) {
            OnePocketLog.d(LOG_TAG,
                    "NoSuchAlgorithmException, data will not be stored for key: "
                            + key);
        } catch (NoSuchPaddingException e) {
            OnePocketLog.d(LOG_TAG,
                    "NoSuchPaddingException, data will not be stored for key: "
                            + key);
        } catch (IllegalBlockSizeException e) {
            OnePocketLog.d(LOG_TAG,
                    "IllegalBlockSizeException, data will not be stored for key: "
                            + key);
        } catch (BadPaddingException e) {
            OnePocketLog.d(LOG_TAG,
                    "BadPaddingException, data will not be stored for key: "
                            + key);
        }
    }


    public static String getEncryptedSharedPref(String key) {
        SharedPreferences prefs = OnePocketApp.getContext()
                .getSharedPreferences(SHARED_PREFERENCE_NAME,
                        Context.MODE_PRIVATE);
        String encryptedData = prefs.getString(key, null);

        try {
            if (encryptedData != null) {
                return SimpleCrypto.decrypt(seed, encryptedData);
            }
        } catch (InvalidKeyException e) {
            OnePocketLog.d(LOG_TAG,
                    "InvalidKeyException, returning default value for key: "
                            + key);
        } catch (NoSuchAlgorithmException e) {
            OnePocketLog.d(LOG_TAG,
                    "NoSuchAlgorithmException, returning default value for key: "
                            + key);
        } catch (NoSuchPaddingException e) {
            OnePocketLog.d(LOG_TAG,
                    "NoSuchPaddingException, returning default value for key: "
                            + key);
        } catch (IllegalBlockSizeException e) {
            OnePocketLog.d(LOG_TAG,
                    "IllegalBlockSizeException, returning default value for key: "
                            + key);
        } catch (BadPaddingException e) {
            OnePocketLog.d(LOG_TAG,
                    "BadPaddingException, returning default value for key: "
                            + key);
        }

        return KEY_UNASSIGNED;
    }

}
