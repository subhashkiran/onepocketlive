/*
package com.subhashe.onepocket.Utils;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;

import java.util.regex.Pattern;

*/
/**
 * Created by SubhashE on 3/10/2016.
 * Formats the watched EditText to a credit card number
 *//*

public class FourDigitCardFormatWatcher implements TextWatcher {

    static final Pattern CODE_PATTERN = Pattern.compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");
    // Change this to what you want... ' ', '-' etc..
    private static final char space = ' ';

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
       */
/* // Remove spacing char
        if (s.length() > 0 && (s.length() % 5) == 0) {
            final char c = s.charAt(s.length() - 1);
            if (space == c) {
                s.delete(s.length() - 1, s.length());
            }
        }
        // Insert char where needed.
        if (s.length() > 0 && (s.length() % 5) == 0) {
            char c = s.charAt(s.length() - 1);
            // Only if its a digit where there should be a space we insert a space
            if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                s.insert(s.length() - 1, String.valueOf(space));
            }
        }*//*


        if (s.length() > 0 && !CODE_PATTERN.matcher(s).matches()) {
            String input = s.toString();
            String numbersOnly = keepNumbersOnly(input);
            String code = formatNumbersAsCode(numbersOnly);

            Log.w("", "numbersOnly" + numbersOnly);
            Log.w("", "code" + code);

            editText.removeTextChangedListener(this);
            editText.setText(code);
            // You could also remember the previous position of the cursor
            editText.setSelection(code.length());
            editText.addTextChangedListener(this);
        }
    }

    private String keepNumbersOnly(CharSequence s) {
        return s.toString().replaceAll("[^0-9]", ""); // Should of course be more robust
    }
    private String formatNumbersAsCode(CharSequence s) {
        int groupDigits = 0;
        String tmp = "";
        for (int i = 0; i < s.length(); ++i) {
            tmp += s.charAt(i);
            ++groupDigits;
            if (groupDigits == 4) {
                tmp += "-";
                groupDigits = 0;
            }
        }
        return tmp;
    }
}
*/
