package com.subhashe.onepocket.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by SubhashE on 1/21/2016.
 */
public class ConnectionDetector {
    private Context _context;

    public ConnectionDetector(Context context) {
        this._context = context;
    }

    /*
     * It will check whether the device is in online mode or not [ Check Both Mobile Data or WIFI ].
     */
    public boolean isOnline() {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    /*
     * Alert Dialog to Display message.
     */
    public void alertDialog(String title,String str) {

        AlertDialog.Builder alertbox = new AlertDialog.Builder(_context);
        alertbox.setMessage(str);
        alertbox.setTitle(title);
        alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                arg0.dismiss();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertbox.create();
        alertDialog.setCanceledOnTouchOutside(false);
        // show it
        alertDialog.show();
    }
}
